#!/bin/bash

#
# generate shlibs.override.pg with new libs updates
# Version: 1.0
#

MACH=`uname -p`
ROOTDIR=$(git rev-parse --show-toplevel)

DGDIR="$ROOTDIR/packages/${MACH}/debs"
PGSHLIBS="shlibs.override.pg.${MACH}"
#TEMPFILE="$PGSHLIBS"
TEMPFILE="shlibs.temp"

function get_pkg_name()
{
    local PKGPATH="$1"
    local PKG=""
    PKG=`echo $PKGPATH | sed -e 's/.*debs\///' -e 's/\/.*$//'`
    echo $PKG
}

rm -f $TEMPFILE $PGSHLIBS

for shlibs in `sudo find $DGDIR -name "shlibs" | sort `
do
    DEBPKG=`get_pkg_name $shlibs`
    if [ "$DEBPKG" = "system-flash-fwflash" -o "$DEBPKG" = "developer-build-onbld" -o "$DEBPKG" = "libcurses" ]; then
	continue
    fi
#    echo $shlibs >> temp1.txt
#    cat $shlibs | sed -e 's/.*\/lib\///' -e 's/.*amd64\///' | sort -u >> $TEMPFILE
    cat $shlibs | sort -u | sed -e '/^fstyp /d' >> $TEMPFILE
#    echo "" >> $TEMPFILE
done

{
    while read LINE; do
	`echo $LINE | grep -q "^lib"`
	if (( $? == 0 )); then
	    echo $LINE >> $PGSHLIBS
	fi
    done
} < $TEMPFILE
