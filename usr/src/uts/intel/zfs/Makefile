#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the "License").
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/OPENSOLARIS.LICENSE.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets "[]" replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#
#
# Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#	This makefile drives the production of the zfs file system
#	kernel module.
#
# Copyright 2013 Saso Kiselkov. All rights reserved.
#
# Copyright (c) 2016 by Delphix. All rights reserved.
#
# Copyright 2019 Joyent, Inc.

#
#	Path to the base of the uts directory tree (usually /usr/src/uts).
#
UTSBASE	= ../..

ARCHDIR:sh = cd ..; basename `pwd`

#
#	Define the module and object file sets.
#
MODULE		= zfs
OBJECTS		= $(ZFS_OBJS:%=$(OBJS_DIR)/%) $(LUA_OBJS:%=$(OBJS_DIR)/%)
ROOTMODULE	= $(ROOT_DRV_DIR)/$(MODULE)
ROOTLINK	= $(ROOT_FS_DIR)/$(MODULE)
CONF_SRCDIR	= $(UTSBASE)/common/fs/zfs

#
#	Include common rules.
#
include ../Makefile.$(ARCHDIR)

#
#	Define targets
#
ALL_TARGET	= $(BINARY) $(SRC_CONFILE)
INSTALL_TARGET	= $(BINARY) $(ROOTMODULE) $(ROOTLINK) $(ROOT_CONFFILE)

#
#	Overrides and depends_on
#
LDFLAGS		+= -Nfs/specfs -Ncrypto/swrand -Nmisc/idmap -Nmisc/sha2 \
	-Nmisc/skein -Nmisc/edonr

INC_PATH	+= -I$(UTSBASE)/common/fs/zfs
INC_PATH	+= -I$(UTSBASE)/common/fs/zfs/lua
INC_PATH	+= -I$(UTSBASE)/common/zstd/include
INC_PATH	+= -I$(SRC)/common
INC_PATH	+= -I$(COMMONBASE)/zfs
INC_PATH	+= -I$(UTSBASE)/i86pc

amd64_CPPFLAGS_OPTS= -DHAVE_SSE2 -DHAVE_SSSE3 \
	-DHAVE_AVX -DHAVE_AVX2 \
	-DHAVE_AVX512F -DHAVE_AVX512BW

CPPFLAGS += $($(MACH64)_CPPFLAGS_OPTS)
AS_CPPFLAGS += $($(MACH64)_CPPFLAGS_OPTS)

# FIXME: temp solution because optimization issue
#COPTFLAG64=-xO0

CSTD=		$(CSTD_GNU99)

#
# For now, disable these warnings; maintainers should endeavor
# to investigate and remove these for maximum coverage.
# Please do not carry these forward to new Makefiles.
#

# needs work
SMOFF += all_func_returns,indenting
$(OBJS_DIR)/llex.o := SMOFF += index_overflow
$(OBJS_DIR)/metaslab.o := SMOFF += no_if_block
$(OBJS_DIR)/zfs_vnops.o := SMOFF += signed
$(OBJS_DIR)/zfs_vnops_os.o := SMOFF += signed
# needs work
$(OBJS_DIR)/zvol.o := SMOFF += deref_check,signed

# false positive
$(OBJS_DIR)/zfs_ctldir.o := SMOFF += strcpy_overflow

# FIXME: temporary disable
CCWARNINLINE=

#CERRWARN	+= -_gcc10=-Wno-empty-body
$(OBJS_DIR)/zstd_common.o := CERRWARN += -_gcc10=-Wno-empty-body
$(OBJS_DIR)/zstd_common.o := CERRWARN += -_gcc6=-Wno-empty-body

ZFS_ZSTD_FLAGS = -_gcc=-include $(SRC)/uts/common/zstd/include/zstd_compat_wrapper.h
ZFS_ZSTD_FLAGS += -_gcc=-U__BMI__ -_gcc=-fno-tree-vectorize
# ZSTD CFLAGS += $(ZFS_ZSTD_FLAGS)
$(OBJS_DIR)/zfs_zstd.o := CFLAGS += $(ZFS_ZSTD_FLAGS)
$(OBJS_DIR)/zstd_sparc.o := CFLAGS += $(ZFS_ZSTD_FLAGS)
$(OBJS_DIR)/entropy_common.o := CFLAGS += $(ZFS_ZSTD_FLAGS)
$(OBJS_DIR)/error_private.o := CFLAGS += $(ZFS_ZSTD_FLAGS)
$(OBJS_DIR)/fse_decompress.o := CFLAGS += $(ZFS_ZSTD_FLAGS)
$(OBJS_DIR)/zstd_pool.o := CFLAGS += $(ZFS_ZSTD_FLAGS)
$(OBJS_DIR)/zstd_common.o := CFLAGS += $(ZFS_ZSTD_FLAGS)
$(OBJS_DIR)/fse_compress.o := CFLAGS += $(ZFS_ZSTD_FLAGS)
$(OBJS_DIR)/hist.o := CFLAGS += $(ZFS_ZSTD_FLAGS)
$(OBJS_DIR)/huf_compress.o := CFLAGS += $(ZFS_ZSTD_FLAGS)
$(OBJS_DIR)/zstd_compress.o := CFLAGS += $(ZFS_ZSTD_FLAGS)
$(OBJS_DIR)/zstd_compress_literals.o := CFLAGS += $(ZFS_ZSTD_FLAGS)
$(OBJS_DIR)/zstd_compress_sequences.o := CFLAGS += $(ZFS_ZSTD_FLAGS)
$(OBJS_DIR)/zstd_compress_superblock.o := CFLAGS += $(ZFS_ZSTD_FLAGS)
$(OBJS_DIR)/zstd_double_fast.o := CFLAGS += $(ZFS_ZSTD_FLAGS)
$(OBJS_DIR)/zstd_fast.o := CFLAGS += $(ZFS_ZSTD_FLAGS)
$(OBJS_DIR)/zstd_lazy.o := CFLAGS += $(ZFS_ZSTD_FLAGS)
$(OBJS_DIR)/zstd_ldm.o := CFLAGS += $(ZFS_ZSTD_FLAGS)
$(OBJS_DIR)/zstd_opt.o := CFLAGS += $(ZFS_ZSTD_FLAGS)
$(OBJS_DIR)/huf_decompress.o := CFLAGS += $(ZFS_ZSTD_FLAGS)
$(OBJS_DIR)/zstd_ddict.o := CFLAGS += $(ZFS_ZSTD_FLAGS)
$(OBJS_DIR)/zstd_decompress.o := CFLAGS += $(ZFS_ZSTD_FLAGS)
$(OBJS_DIR)/zstd_decompress_block.o := CFLAGS += $(ZFS_ZSTD_FLAGS)


#
#	Default build targets.
#
.KEEP_STATE:

def:		$(DEF_DEPS)

all:		$(ALL_DEPS)

clean:		$(CLEAN_DEPS)

clobber:	$(CLOBBER_DEPS)

install:	$(INSTALL_DEPS)

$(ROOTLINK):	$(ROOT_FS_DIR) $(ROOTMODULE)
	-$(RM) $@; $(LN) $(ROOTMODULE) $@

#
#	Include common targets.
#
include ../Makefile.targ
