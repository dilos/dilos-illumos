/*
 * This file and its contents are supplied under the terms of the
 * Common Development and Distribution License ("CDDL"), version 1.0.
 * You may only use this file in accordance with the terms of version
 * 1.0 of the CDDL.
 *
 * A full copy of the text of the CDDL should have accompanied this
 * source.  A copy of the CDDL is also available via the Internet at
 * http://www.illumos.org/license/CDDL.
 */

/*
 * Copyright 2024 Oxide Computer Company
 */

/*
 * SMN registers that are CCD-specific (core complex die) but are spread across
 * multiple functional units. This could be combined with <sys/amdzen/ccx.h>
 * once the duplication between that and <sys/controlregs.h> is dealt with.
 *
 * Currently this covers two different groups:
 *
 * SMU::PWR	This group describes information about the CCD and, unlike the
 *		DF CCM entries, this is only present if an actual die is
 *		present in the package. These registers are always present
 *		starting in Zen 2.
 *
 * L3::SCFCTP	The Scalable Control Fabric, Clocks, Test, and Power Gating
 *		registers exist on a per-core basis within each CCD. The first
 *		point that we can find that this exists started in Zen 3.
 *
 * L3::SOC	This was added starting in Zen 5 and contains several of the
 *		registers that used to exist in SMU::PWR.
 *
 * The register naming and fields generally follows the conventions that the DF
 * and UMC have laid out. The one divergence right now is that the functional
 * blocks only exist starting in a given Zen uarch (e.g. Zen 2). Once we have
 * divergences from that introduction point then like the MSRs and others we
 * will introduce the generation-specific part of the name.
 */

#include <sys/bitext.h>
#include <sys/debug.h>
#include <sys/types.h>
#include <sys/amdzen/smn.h>
#include <sys/amdzen/ccd.h>

/*
 * SMU::PWR registers, per-CCD.  This functional unit is present starting in Zen
 * based platforms.  Note that there is another aperture at 0x4008_1000 that is
 * documented to alias CCD 0.  It's not really clear what if any utility that's
 * supposed to have, except that the name given to these aliases contains
 * "LOCAL" which implies that perhaps rather than aliasing CCD 0 it instead is
 * decoded by the unit on the originating CCD.  We don't use that in any case.
 *
 * Once SoCs started supporting more than 8 CCDs with Zen 4, they added a second
 * aperture that starts at 4a08_1000h and uses the same shifts. This leads to
 * some awkwardness below. This does make it harder to get at this. We should
 * investigate to include the uarch to determine limits at some point in the
 * future like we have done with some of our DF registers.
 *
 * Starting in Zen 5, a chunk of the registers described here are all now in
 * the L3::SOC block.
 */
smn_reg_t
amdzen_smupwr_smn_reg(const uint8_t ccdno, const smn_reg_def_t def,
    const uint16_t reginst)
{
	const uint32_t APERTURE_BASE = 0x30081000;
	const uint32_t APERTURE_HI_BASE = 0x4a081000;
	const uint32_t APERTURE_MASK = 0xfffff000;
	CTASSERT((APERTURE_BASE & ~APERTURE_MASK) == 0);
	CTASSERT((APERTURE_HI_BASE & ~APERTURE_MASK) == 0);

	const uint32_t ccdno32 = (const uint32_t)ccdno;
	const uint32_t reginst32 = (const uint32_t)reginst;
	const uint32_t size32 = (def.srd_size == 0) ? 4 :
	    (const uint32_t)def.srd_size;

	const uint32_t stride = (def.srd_stride == 0) ? size32 : def.srd_stride;
	const uint32_t nents = (def.srd_nents == 0) ? 1 :
	    (const uint32_t)def.srd_nents;

	ASSERT(size32 == 1 || size32 == 2 || size32 == 4);
	ASSERT3S(def.srd_unit, ==, SMN_UNIT_SMUPWR);
	ASSERT3U(ccdno32, <, 16);
	ASSERT3U(nents, >, reginst32);

	uint32_t aperture_base, aperture_off;
	if (ccdno >= 8) {
		aperture_base = APERTURE_HI_BASE;
		aperture_off = (ccdno32 - 8) << 25;
	} else {
		aperture_base = APERTURE_BASE;
		aperture_off = ccdno32 << 25;
	}
	ASSERT3U(aperture_off, <=, UINT32_MAX - aperture_base);

	const uint32_t aperture = aperture_base + aperture_off;
	ASSERT0(aperture & ~APERTURE_MASK);

	const uint32_t reg = def.srd_reg + reginst32 * stride;
	ASSERT0(reg & APERTURE_MASK);

	return (SMN_MAKE_REG_SIZED(aperture + reg, size32));
}

/*
 * L3::SOC registers, per-CCD.  This functional unit is present starting in Zen
 * 5 based platforms.  This covers a majority of the things that are described
 * above in the SMU::PWR section, except for SMU::PWR::CCD_DIE_ID. CCDs are at a
 * 23-bit stride.
 */
smn_reg_t
amdzen_l3soc_smn_reg(const uint8_t ccdno, const smn_reg_def_t def,
    const uint16_t reginst)
{
	const uint32_t APERTURE_BASE = 0x203c0000;
	const uint32_t APERTURE_MASK = 0xfffc0000;
	CTASSERT((APERTURE_BASE & ~APERTURE_MASK) == 0);

	const uint32_t ccdno32 = (const uint32_t)ccdno;
	const uint32_t reginst32 = (const uint32_t)reginst;
	const uint32_t size32 = (def.srd_size == 0) ? 4 :
	    (const uint32_t)def.srd_size;

	const uint32_t stride = (def.srd_stride == 0) ? size32 : def.srd_stride;
	const uint32_t nents = (def.srd_nents == 0) ? 1 :
	    (const uint32_t)def.srd_nents;

	ASSERT(size32 == 1 || size32 == 2 || size32 == 4);
	ASSERT3S(def.srd_unit, ==, SMN_UNIT_L3SOC);
	ASSERT3U(ccdno32, <, 16);
	ASSERT3U(nents, >, reginst32);

	uint32_t aperture_base, aperture_off;
	aperture_base = APERTURE_BASE;
	aperture_off = ccdno32 << 23;
	ASSERT3U(aperture_off, <=, UINT32_MAX - aperture_base);

	const uint32_t aperture = aperture_base + aperture_off;
	ASSERT0(aperture & ~APERTURE_MASK);

	const uint32_t reg = def.srd_reg + reginst32 * stride;
	ASSERT0(reg & APERTURE_MASK);

	return (SMN_MAKE_REG_SIZED(aperture + reg, size32));

}

/*
 * SCFCTP registers. A copy of these exists for each core. One thing to be aware
 * of is that not all cores are enabled and this requires looking at the
 * SMU::PWR/L3::SOC registers above or the DF::CoreEnable. The aperture for
 * these starts at 2000_0000h. Each core is then spaced 2_0000h apart while each
 * CCD has a 23-bit stride and each CCX has a 22 bit stride. The number of cores
 * and CCXes varies based upon the generation. We size this based on what we
 * anticipate the maximums to be.
 *
 * In the future, it'd be good to have a way to constrain the values we accept
 * to something less than the maximum across all products, but this is often
 * used before we have fully flushed out the uarchrev part of CPUID making it
 * challenging at the moment.
 */
smn_reg_t
amdzen_scfctp_smn_reg(const uint8_t ccdno, const uint8_t ccxno,
    const smn_reg_def_t def, const uint16_t reginst)
{
	const uint32_t APERTURE_BASE = 0x20000000;
	const uint32_t APERTURE_MASK = 0xffc00000;
	CTASSERT((APERTURE_BASE & ~APERTURE_MASK) == 0);

	const uint32_t ccdno32 = (const uint32_t)ccdno;
	const uint32_t ccxno32 = (const uint32_t)ccxno;
	const uint32_t reginst32 = (const uint32_t)reginst;
	const uint32_t size32 = (def.srd_size == 0) ? 4 :
	    (const uint32_t)def.srd_size;

	const uint32_t stride = (def.srd_stride == 0) ? 4 : def.srd_stride;
	const uint32_t nents = (def.srd_nents == 0) ? 1 :
	    (const uint32_t)def.srd_nents;

	ASSERT(size32 == 1 || size32 == 2 || size32 == 4);
	ASSERT3S(def.srd_unit, ==, SMN_UNIT_SCFCTP);
	ASSERT3U(stride, ==, SCFCTP_CORE_STRIDE);
	ASSERT3U(nents, ==, SCFCTP_MAX_ENTS);
	ASSERT3U(ccdno32, <, 16);
	ASSERT3U(ccxno32, <, 2);
	ASSERT3U(nents, >, reginst32);

	const uint32_t aperture_off = (ccdno32 << 23) + (ccxno << 22);
	ASSERT3U(aperture_off, <=, UINT32_MAX - APERTURE_BASE);

	const uint32_t aperture = APERTURE_BASE + aperture_off;
	ASSERT0(aperture & ~APERTURE_MASK);

	const uint32_t reg = def.srd_reg + reginst32 * stride;
	ASSERT0(reg & APERTURE_MASK);

	return (SMN_MAKE_REG_SIZED(aperture + reg, size32));
}
