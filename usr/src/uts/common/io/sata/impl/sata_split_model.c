/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the "License").
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/OPENSOLARIS.LICENSE.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets "[]" replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright (c) 2006, 2010, Oracle and/or its affiliates. All rights reserved.
 */
/*
 * Copyright 2015 Nexenta Systems, Inc.  All rights reserved.
 */

#include <sys/sata/impl/sata.h>
#include <sys/sata/sata_defs.h>

/*
 * sata_split_model splits the model ID into vendor and product IDs.
 * It assumes that a vendor ID cannot be longer than 8 characters, and
 * that vendor and product ID are separated by a whitespace.
 */
void
sata_split_model(char *model, char **vendor, char **product)
{
	int i, modlen;
	char *vid, *pid;

	/*
	 * remove whitespace at the end of model
	 */
	for (i = SATA_ID_MODEL_LEN; i > 0; i--)
		if (model[i] == ' ' || model[i] == '\t' || model[i] == '\0')
			model[i] = '\0';
		else
			break;

	/*
	 * try to split model into into vid/pid
	 */
	modlen = strlen(model);
	for (i = 0, pid = model; i < modlen; i++, pid++)
		if ((*pid == ' ') || (*pid == '\t'))
			break;

	/*
	 * only use vid if it is less than 8 chars (as in SCSI)
	 */
	if (i < modlen && i <= 8) {
		vid = model;
		/*
		 * terminate vid, establish pid
		 */
		*pid++ = '\0';
	} else {
		/*
		 * vid will stay "ATA     "
		 */
		vid = NULL;
		/*
		 * model is all pid
		 */
		pid = model;
	}

	*vendor = vid;
	*product = pid;
}
