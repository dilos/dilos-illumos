/*
 * This file and its contents are supplied under the terms of the
 * Common Development and Distribution License ("CDDL"), version 1.0.
 * You may only use this file in accordance with the terms of version
 * 1.0 of the CDDL.
 *
 * A full copy of the text of the CDDL should have accompanied this
 * source.  A copy of the CDDL is also available via the Internet at
 * http://www.illumos.org/license/CDDL.
 */

/*
 * Copyright (c) 2021, DilOS
 */

/*
 * Platform specific wrappers.
 */

#include <sys/zfs_context.h>

void *
zfs_kmem_alloc(size_t size, int flag)
{
	void *res = kmem_alloc(size, flag);
	return (res);
}

void *
zfs_kmem_zalloc(size_t size, int flag)
{
	void *res = kmem_zalloc(size, flag);
	return (res);
}

void
zfs_kmem_free(void *buf, size_t size)
{
	kmem_free(buf, size);
}
