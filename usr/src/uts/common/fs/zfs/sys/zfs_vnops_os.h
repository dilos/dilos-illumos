/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the "License").
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
 * or https://opensource.org/licenses/CDDL-1.0.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/OPENSOLARIS.LICENSE.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets "[]" replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright (c) 2022 Argo Technologie East
 */

#ifndef	_SYS_FS_ZFS_VNOPS_OS_H
#define	_SYS_FS_ZFS_VNOPS_OS_H

#include <sys/debug.h>	/* ASSERT */

extern int dmu_write_pages(objset_t *os, uint64_t object, uint64_t offset,
    uint64_t size, struct page *pp, dmu_tx_t *tx);

/* common implementation */
extern int zfs_setsecattr(znode_t *zp, vsecattr_t *vsecp, int flag,
    cred_t *cr);

/* for call in routines */
static inline int
zfs_setsecattr_call(znode_t *zp, vsecattr_t *vsecp, int flag, cred_t *cr)
{
	ASSERT(zp != NULL);
	return (VOP_SETSECATTR(ZTOV(zp), vsecp, flag, cr, NULL));
}

static inline int
zfs_remove(znode_t *dzp, const char *name, cred_t *cr, int flags)
{
	ASSERT(dzp != NULL);
	return (VOP_REMOVE(ZTOV(dzp), (char *)name, cr, NULL, flags));
}

static inline int
zfs_mkdir(znode_t *dzp, const char *dirname, vattr_t *vap, znode_t **zpp,
    cred_t *cr, int flags, vsecattr_t *vsecp, zidmap_t *mnt_ns)
{
	(void) mnt_ns;
	vnode_t *vp = NULL;

	ASSERT(dzp != NULL);
	int error = VOP_MKDIR(ZTOV(dzp), (char *)dirname, vap,
	    &vp, cr, NULL, flags, vsecp);
	*zpp = vp ? VTOZ(vp) : NULL;

	return (error);
}

static inline int
zfs_rmdir(znode_t *dzp, const char *name, znode_t *cwd, cred_t *cr, int flags)
{
	ASSERT(dzp != NULL);
	vnode_t *vcwd = cwd ? ZTOV(cwd) : NULL;
	return (VOP_RMDIR(ZTOV(dzp), (char *)name, vcwd, cr, NULL, flags));
}

static inline int
zfs_setattr(znode_t *zp, vattr_t *vap, int flag, cred_t *cr,
    zidmap_t *mnt_ns)
{
	(void) mnt_ns;

	ASSERT(zp != NULL);
	return (VOP_SETATTR(ZTOV(zp), vap, flag, cr, NULL));
}

static inline int
zfs_rename(znode_t *sdzp, const char *snm, znode_t *tdzp, const char *tnm,
    cred_t *cr, int flags, uint64_t rflags, vattr_t *wo_vap, zidmap_t *mnt_ns)
{
	(void) rflags, (void) wo_vap, (void) mnt_ns;

	ASSERT(sdzp != NULL);
	ASSERT(tdzp != NULL);
	return (VOP_RENAME(ZTOV(sdzp), (char *)snm, ZTOV(tdzp), (char *)tnm,
	    cr, NULL, flags));
}

static inline int
zfs_symlink(znode_t *dzp, const char *name, vattr_t *vap, const char *link,
    znode_t **zpp, cred_t *cr, int flags, zidmap_t *mnt_ns)
{
	(void) mnt_ns;

	ASSERT(dzp != NULL);
	*zpp = NULL;
	return (VOP_SYMLINK(ZTOV(dzp), (char *)name, vap, (char *)link, cr,
	    NULL, flags));
}

static inline int
zfs_link(znode_t *dzp, znode_t *zp, const char *name, cred_t *cr, int flags)
{
	ASSERT(dzp != NULL);
	return (VOP_LINK(ZTOV(dzp), ZTOV(zp), (char *)name, cr, NULL, flags));
}

static inline int
zfs_space(znode_t *zp, int cmd, struct flock64 *bfp, int flag, offset_t offset,
    cred_t *cr)
{
	ASSERT(zp != NULL);
	return (VOP_SPACE(ZTOV(zp), cmd, bfp, flag, offset, cr, NULL));
}

/* FIXME: move to normal function and not inline */
/*
static inline int
zfs_create(znode_t *dzp, const char *name, vattr_t *vap, int excl, int mode,
    znode_t **zpp, cred_t *cr, int flags, vsecattr_t *vsecp, zidmap_t *mnt_ns)
{
	(void) mnt_ns;

	ASSERT(dzp != NULL);
	vnode_t *vp = NULL;

	int error = VOP_CREATE(ZTOV(dzp), (char *)name, vap, excl, mode, &vp,
	    cr, flags, NULL, vsecp);
	*zpp = vp ? VTOZ(vp) : NULL;

	return (error);
}
*/

static inline int
zfs_write_simple(znode_t *zp, void *data, size_t len, loff_t pos,
    size_t *residp)
{
	ASSERT(zp != NULL);
	size_t resid;

	if (residp == NULL)
		residp = &resid;

	return (vn_rdwr(UIO_WRITE, ZTOV(zp), data, len, pos,
	    UIO_SYSSPACE, 0, RLIM64_INFINITY, kcred, (ssize_t *)residp));
}

#endif /* _SYS_FS_ZFS_VNOPS_OS_H */
