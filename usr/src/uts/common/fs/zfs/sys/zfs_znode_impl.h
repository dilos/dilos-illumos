/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the "License").
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
 * or https://opensource.org/licenses/CDDL-1.0.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/OPENSOLARIS.LICENSE.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets "[]" replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/*
 * Copyright (c) 2005, 2010, Oracle and/or its affiliates. All rights reserved.
 * Copyright (c) 2012, 2015 by Delphix. All rights reserved.
 * Copyright (c) 2014 Integros [integros.com]
 * Copyright 2016 Nexenta Systems, Inc. All rights reserved.
 */

#ifndef	_SYS_ZFS_ZNODE_IMPL_H
#define	_SYS_ZFS_ZNODE_IMPL_H

#include <sys/list.h>
#include <sys/dmu.h>
#include <sys/sa.h>
#include <sys/zfs_vfsops.h>
#include <sys/rrwlock.h>
#include <sys/zfs_sa.h>
#include <sys/zfs_stat.h>
#include <sys/zfs_rlock.h>
#include <sys/zfs_acl.h>
#include <sys/zil.h>
#include <sys/zfs_project.h>
#include <sys/uio.h>

#ifdef	__cplusplus
extern "C" {
#endif

/*
 * Directory entry locks control access to directory entries.
 * They are used to protect creates, deletes, and renames.
 * Each directory znode has a mutex and a list of locked names.
 */
#define	ZNODE_OS_FIELDS	\
	struct zfsvfs		*z_zfsvfs;  \
	vnode_t			*z_vnode;   \
	caller_context_t	*z_context; \
	boolean_t	z_moved;	/* Has this znode been moved? */ \
	uint64_t	z_uid;		/* uid fuid (cached) */          \
	uint64_t	z_gid;		/* gid fuid (cached) */          \
	uint64_t	z_gen;		/* generation (cached) */        \
	uint64_t	z_atime[2];	/* atime (cached) */             \
	uint64_t	z_links;	/* file links (cached) */

#define	ZFS_LINK_MAX	UINT32_MAX

/*
 * Range locking rules
 * --------------------
 * 1. When truncating a file (zfs_create, zfs_setattr, zfs_space) the whole
 *    file range needs to be locked as RL_WRITER. Only then can the pages be
 *    freed etc and zp_size reset. zp_size must be set within range lock.
 * 2. For writes and punching holes (zfs_write & zfs_space) just the range
 *    being written or freed needs to be locked as RL_WRITER.
 *    Multiple writes at the end of the file must coordinate zp_size updates
 *    to ensure data isn't lost. A compare and swap loop is currently used
 *    to ensure the file size is at least the offset last written.
 * 3. For reads (zfs_read, zfs_get_data & zfs_putapage) just the range being
 *    read needs to be locked as RL_READER. A check against zp_size can then
 *    be made for reading beyond end of file.
 */

/*
 * Convert between znode pointers and vnode pointers
 */
#define	ZTOV(zp)	((zp)->z_vnode)
#define	ZTOCT(zp)	((zp)->z_context)
#define	VTOZ(vp)	((struct znode *)(vp)->v_data)
#define	zhold(zp)	VN_HOLD(ZTOV((zp)))
#define	zrele(zp)	VN_RELE(ZTOV((zp)))

#define	ZTOZSB(zp)	((zp)->z_zfsvfs)
#define	ZTOTYPE(zp)	(ZTOV(zp)->v_type)
#define	ZTOGID(zp)	((zp)->z_gid)
#define	ZTOUID(zp)	((zp)->z_uid)
#define	ZTONLNK(zp)	((zp)->z_links)
#define	Z_ISBLK(type)	((type) == VBLK)
#define	Z_ISCHR(type)	((type) == VCHR)
#define	Z_ISLNK(type)	((type) == VLNK)

#define	KUID_TO_SUID(x)	(x)
#define	KGID_TO_SGID(x)	(x)

#define	zn_has_cached_data(zp, start, end) \
	vn_has_cached_data(ZTOV(zp))
#define	zn_flush_cached_data(zp, sync)	\
	VOP_FSYNC(ZTOV(zp), (sync) ? FSYNC|FDSYNC : 0, kcred, NULL)
#define	zn_rlimit_fsize(size)		(0)
#define	zn_rlimit_fsize_uio(zp, uio)	(0)

/* Called on entry to each ZFS inode and vfs operation. */
static inline int
zfs_enter(zfsvfs_t *zfsvfs, const char *tag)
{
	ZFS_TEARDOWN_ENTER_READ(zfsvfs, tag);
	if (zfsvfs->z_unmounted) {
		ZFS_TEARDOWN_EXIT_READ(zfsvfs, tag);
		return (SET_ERROR(EIO));
	}
	return (0);
}

/* Must be called before exiting the operation. */
static inline void
zfs_exit(zfsvfs_t *zfsvfs, const char *tag)
{
	ZFS_TEARDOWN_EXIT_READ(zfsvfs, tag);
}

/*
 * Macros for dealing with dmu_buf_hold
 */
#define	ZFS_OBJ_MTX_SZ		64
#define	ZFS_OBJ_MTX_MAX		(1024 * 1024)
#define	ZFS_OBJ_HASH(zfsvfs, obj)	((obj) & ((zfsvfs->z_hold_size) - 1))

extern unsigned int zfs_object_mutex_size;

/*
 * Encode ZFS stored time values from a struct timespec / struct timespec64.
 */
#define	ZFS_TIME_ENCODE(tp, stmp)		\
do {						\
	(stmp)[0] = (uint64_t)(tp)->tv_sec;	\
	(stmp)[1] = (uint64_t)(tp)->tv_nsec;	\
} while (0)

/*
 * Decode ZFS stored time values to a struct timespec
 */
#define	ZFS_TIME_DECODE(tp, stmp)		\
do {						\
	(tp)->tv_sec = (time_t)(stmp)[0];	\
	(tp)->tv_nsec = (long)(stmp)[1];	\
} while (0)

#define	ZFS_ACCESSTIME_STAMP(zfsvfs, zp) \
	if ((zfsvfs)->z_atime && !((zfsvfs)->z_vfs->vfs_flag & VFS_RDONLY)) \
		zfs_tstamp_update_setup_ext(zp, ACCESSED, NULL, NULL, B_FALSE);

extern void zfs_tstamp_update_setup_ext(struct znode *,
    uint_t, uint64_t [2], uint64_t [2], boolean_t have_tx);
extern void zfs_znode_free(struct znode *);
extern int zfs_sync(vfs_t *vfsp, short flag, cred_t *cr);
extern int zfs_create_share_dir(zfsvfs_t *zfsvfs, dmu_tx_t *tx);

extern caddr_t zfs_map_page(page_t *, enum seg_rw);
extern void zfs_unmap_page(page_t *, caddr_t);

extern zil_get_data_t zfs_get_data;
extern zil_replay_func_t *const zfs_replay_vector[TX_MAX_TYPE];
extern int zfsfstype;

#ifdef	__cplusplus
}
#endif

#endif	/* _SYS_ZFS_ZNODE_IMPL_H */
