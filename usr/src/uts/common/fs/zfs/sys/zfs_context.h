/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the "License").
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
 * or https://opensource.org/licenses/CDDL-1.0.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/OPENSOLARIS.LICENSE.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets "[]" replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

/*
 * Copyright 2011 Nexenta Systems, Inc.  All rights reserved.
 * Copyright (c) 2013 by Delphix. All rights reserved.
 */

#ifndef _SYS_ZFS_CONTEXT_H
#define	_SYS_ZFS_CONTEXT_H

#ifdef	__cplusplus
extern "C" {
#endif

#if defined(__KERNEL__) || defined(_STANDALONE)
#incldue <sys/condvar.h>
#endif

#include <sys/types.h>
#include <sys/t_lock.h>
#include <sys/atomic.h>
#include <sys/sysmacros.h>
#include <sys/bitmap.h>
#include <sys/cmn_err.h>
#include <sys/kmem.h>
#include <sys/kmem_cache.h>
#include <sys/taskq.h>
#include <sys/taskq_impl.h>
#include <sys/buf.h>
#include <sys/param.h>
#include <sys/systm.h>
#include <sys/cpuvar.h>
#include <sys/kobj.h>
#include <sys/conf.h>
#include <sys/disp.h>
#include <sys/debug.h>
#include <sys/random.h>
#include <sys/byteorder.h>
#include <sys/systm.h>
#include <sys/list.h>
#include <sys/uio.h>
#include <sys/dirent.h>
#include <sys/time.h>
#include <vm/seg_kmem.h>
#include <sys/zone.h>
#include <sys/uio.h>
#include <sys/zfs_debug.h>
#include <sys/sysevent.h>
#include <sys/sysevent/eventdefs.h>
#include <sys/sysevent/dev.h>
#include <sys/fm/util.h>
#include <sys/sunddi.h>
#include <sys/cyclic.h>
#include <sys/disp.h>
#include <sys/callo.h>
#include <sys/zfs_util.h>
#include <sys/utsname.h>
#include <sys/thread.h>

#define	HAVE_LARGE_STACKS	1

#define	ZFS_MODULE_PARAM(scope_prefix, name_prefix, name, type, perm, desc)
#define	ZFS_MODULE_PARAM_CALL(scope_prefix, name_prefix, name, setfunc, \
    getfunc, perm, desc)
#define	ZFS_MODULE_VIRTUAL_PARAM_CALL( \
    scope_prefix, name_prefix, name, setfunc, getfunc, perm, desc)

/*
 * Exported symbols
 */
#define	EXPORT_SYMBOL(x)

#define	ZFS_MODULE_DESCRIPTION(s)
#define	ZFS_MODULE_AUTHOR(s)
#define	ZFS_MODULE_LICENSE(s)
#define	ZFS_MODULE_VERSION(s)

#ifndef zfs_fallthrough
#ifdef __GNUC__
#if __GNUC__ > 6
#define	zfs_fallthrough	__attribute__((__fallthrough__))
#else
#define	zfs_fallthrough	((void)0)
#endif	/* __GNUC__ > 6 */
#endif	/* __GNUC__ */
#endif	/* zfs_fallthrough */

/*
 * Stack
 */
#define	noinline	__attribute__((noinline))
#define	likely(x)	__builtin_expect((x), 1)
#define	unlikely(x)	__builtin_expect((x), 0)

#define	CPU_SEQID	(CPU->cpu_seqid)
#define	CPU_SEQID_UNSTABLE	(CPU->cpu_seqid)

#ifndef	cv_timedwait_sig_hires
#define	cv_timedwait_sig_hires(cv, mp, t, r, f) \
	cv_timedwait_hires(cv, mp, t, r, f)
#endif	/* cv_timedwait_sig_hires */

#define	F_FREESP	11

/*
#if defined(HAVE_FILE_FALLOCATE) && \
	defined(FALLOC_FL_PUNCH_HOLE) && \
	defined(FALLOC_FL_KEEP_SIZE)
#define	VOP_SPACE(vp, cmd, flck, fl, off, cr, ct) \
	fallocate((vp)->v_fd, FALLOC_FL_PUNCH_HOLE | FALLOC_FL_KEEP_SIZE, \
	    (flck)->l_start, (flck)->l_len)
#else
#define	VOP_SPACE(vp, cmd, flck, fl, off, cr, ct) (0)
#endif
*/

#define	ddi_time_before(a, b)	(a < b)
#define	ddi_time_after(a, b)	ddi_time_before(b, a)

#ifndef	PROC_T
typedef struct proc proc_t;
#endif	/* PROC_T */

#if !defined(__KERNEL__) && !defined(_STANDALONE)
#define	taskq_init_ent(tent)
#endif	/* !__KERNEL__ && !_STANDALONE */

#define	____cacheline_aligned

static inline struct utsname *
zfs_utsname(void)
{
	return (&utsname);
}
#define utsname()		zfs_utsname()

typedef int fstrans_cookie_t;

static inline fstrans_cookie_t
spl_fstrans_mark(void)
{
	return (0);
}

static inline void
spl_fstrans_unmark(fstrans_cookie_t x)
{
	(void) x;
}

#ifdef	_KERNEL
static inline kthread_t *
thread_create_named(const char *name, caddr_t stk, size_t stksize,
    void (*proc)(), void *arg, size_t len, proc_t *pp, int state, pri_t pri)
{
	kthread_t *td =
	    thread_create(stk, stksize, proc, arg, len, pp, state, pri);
	(void) thread_setname(td, name);

	return (td);
}
#endif	/* _KERNEL */

#ifdef	__cplusplus
}
#endif

#endif	/* _SYS_ZFS_CONTEXT_H */
