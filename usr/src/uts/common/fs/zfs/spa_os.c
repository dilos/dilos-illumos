/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the "License").
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
 * or https://opensource.org/licenses/CDDL-1.0.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/OPENSOLARIS.LICENSE.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets "[]" replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

#include <sys/zfs_context.h>
#include <sys/spa_impl.h>
#include <sys/vdev_impl.h>

#ifdef	_KERNEL
#include <sys/bootprops.h>
#endif	/* _KERNEL */

#ifdef _KERNEL
/*
 * Get the root pool information from the root disk, then import the root pool
 * during the system boot up time.
 */
static nvlist_t *
spa_generate_rootconf(const char *devpath, const char *devid, uint64_t *guid,
    uint64_t pool_guid)
{
	nvlist_t *config;
	nvlist_t *nvtop, *nvroot;
	uint64_t pgid;

	if (vdev_disk_read_rootlabel(devpath, devid, &config) != 0)
		return (NULL);

	/*
	 * Add this top-level vdev to the child array.
	 */
	VERIFY(nvlist_lookup_nvlist(config, ZPOOL_CONFIG_VDEV_TREE,
	    &nvtop) == 0);
	VERIFY(nvlist_lookup_uint64(config, ZPOOL_CONFIG_POOL_GUID,
	    &pgid) == 0);
	VERIFY(nvlist_lookup_uint64(config, ZPOOL_CONFIG_GUID, guid) == 0);

	if (pool_guid != 0 && pool_guid != pgid) {
		/*
		 * The boot loader provided a pool GUID, but it does not match
		 * the one we found in the label.  Return failure so that we
		 * can fall back to the full device scan.
		 */
		zfs_dbgmsg("spa_generate_rootconf: loader pool guid %llu != "
		    "label pool guid %llu", (u_longlong_t)pool_guid,
		    (u_longlong_t)pgid);
		nvlist_free(config);
		return (NULL);
	}

	/*
	 * Put this pool's top-level vdevs into a root vdev.
	 */
	VERIFY(nvlist_alloc(&nvroot, NV_UNIQUE_NAME, KM_SLEEP) == 0);
	VERIFY(nvlist_add_string(nvroot, ZPOOL_CONFIG_TYPE,
	    VDEV_TYPE_ROOT) == 0);
	VERIFY(nvlist_add_uint64(nvroot, ZPOOL_CONFIG_ID, 0ULL) == 0);
	VERIFY(nvlist_add_uint64(nvroot, ZPOOL_CONFIG_GUID, pgid) == 0);
	VERIFY(nvlist_add_nvlist_array(nvroot, ZPOOL_CONFIG_CHILDREN,
	    (const nvlist_t **)&nvtop, 1) == 0);

	/*
	 * Replace the existing vdev_tree with the new root vdev in
	 * this pool's configuration (remove the old, add the new).
	 */
	VERIFY(nvlist_add_nvlist(config, ZPOOL_CONFIG_VDEV_TREE, nvroot) == 0);
	nvlist_free(nvroot);
	return (config);
}

/*
 * Walk the vdev tree and see if we can find a device with "better"
 * configuration. A configuration is "better" if the label on that
 * device has a more recent txg.
 */
static void
spa_alt_rootvdev(vdev_t *vd, vdev_t **avd, uint64_t *txg)
{
	for (int c = 0; c < vd->vdev_children; c++)
		spa_alt_rootvdev(vd->vdev_child[c], avd, txg);

	if (vd->vdev_ops->vdev_op_leaf) {
		nvlist_t *label;
		uint64_t label_txg;

		if (vdev_disk_read_rootlabel(vd->vdev_physpath, vd->vdev_devid,
		    &label) != 0)
			return;

		VERIFY(nvlist_lookup_uint64(label, ZPOOL_CONFIG_POOL_TXG,
		    &label_txg) == 0);

		/*
		 * Do we have a better boot device?
		 */
		if (label_txg > *txg) {
			*txg = label_txg;
			*avd = vd;
		}
		nvlist_free(label);
	}
}

/*
 * Import a root pool.
 *
 * For x86. devpath_list will consist of devid and/or physpath name of
 * the vdev (e.g. "id1,sd@SSEAGATE..." or "/pci@1f,0/ide@d/disk@0,0:a").
 * The GRUB "findroot" command will return the vdev we should boot.
 *
 * For Sparc, devpath_list consists the physpath name of the booting device
 * no matter the rootpool is a single device pool or a mirrored pool.
 * e.g.
 *	"/pci@1f,0/ide@d/disk@0,0:a"
 */
int
spa_import_rootpool(char *devpath, char *devid, uint64_t pool_guid,
    uint64_t vdev_guid)
{
	spa_t *spa;
	vdev_t *rvd, *bvd, *avd = NULL;
	nvlist_t *config, *nvtop;
	uint64_t guid, txg;
	char *pname;
	int error;
	const char *altdevpath = NULL;
	const char *rdpath = NULL;

	if ((rdpath = vdev_disk_preroot_force_path()) != NULL) {
		/*
		 * We expect to import a single-vdev pool from a specific
		 * device such as a ramdisk device.  We don't care what the
		 * pool label says.
		 */
		config = spa_generate_rootconf(rdpath, NULL, &guid, 0);
		if (config != NULL) {
			goto configok;
		}
		cmn_err(CE_NOTE, "Cannot use root disk device '%s'", rdpath);
		return (SET_ERROR(EIO));
	}

	/*
	 * Read the label from the boot device and generate a configuration.
	 */
	config = spa_generate_rootconf(devpath, devid, &guid, pool_guid);
#if defined(_OBP) && defined(_KERNEL)
	if (config == NULL) {
		if (strstr(devpath, "/iscsi/ssd") != NULL) {
			/* iscsi boot */
			get_iscsi_bootpath_phy(devpath);
			config = spa_generate_rootconf(devpath, devid, &guid,
			    pool_guid);
		}
	}
#endif

	/*
	 * We were unable to import the pool using the /devices path or devid
	 * provided by the boot loader.  This may be the case if the boot
	 * device has been connected to a different location in the system, or
	 * if a new boot environment has changed the driver used to access the
	 * boot device.
	 *
	 * Attempt an exhaustive scan of all visible block devices to see if we
	 * can locate an alternative /devices path with a label that matches
	 * the expected pool and vdev GUID.
	 */
	if (config == NULL && (altdevpath =
	    vdev_disk_preroot_lookup(pool_guid, vdev_guid)) != NULL) {
		cmn_err(CE_NOTE, "Original /devices path (%s) not available; "
		    "ZFS is trying an alternate path (%s)", devpath,
		    altdevpath);
		config = spa_generate_rootconf(altdevpath, NULL, &guid,
		    pool_guid);
	}

	if (config == NULL) {
		cmn_err(CE_NOTE, "Cannot read the pool label from '%s'",
		    devpath);
		return (SET_ERROR(EIO));
	}

configok:
	VERIFY(nvlist_lookup_string(config, ZPOOL_CONFIG_POOL_NAME,
	    &pname) == 0);
	VERIFY(nvlist_lookup_uint64(config, ZPOOL_CONFIG_POOL_TXG, &txg) == 0);

	mutex_enter(&spa_namespace_lock);
	if ((spa = spa_lookup(pname)) != NULL) {
		/*
		 * Remove the existing root pool from the namespace so that we
		 * can replace it with the correct config we just read in.
		 */
		spa_remove(spa);
	}

	spa = spa_add(pname, config, NULL);
	spa->spa_is_root = B_TRUE;
	spa->spa_import_flags = ZFS_IMPORT_VERBATIM;
	if (nvlist_lookup_uint64(config, ZPOOL_CONFIG_VERSION,
	    &spa->spa_ubsync.ub_version) != 0)
		spa->spa_ubsync.ub_version = SPA_VERSION_INITIAL;

	/*
	 * Build up a vdev tree based on the boot device's label config.
	 */
	VERIFY(nvlist_lookup_nvlist(config, ZPOOL_CONFIG_VDEV_TREE,
	    &nvtop) == 0);
	spa_config_enter(spa, SCL_ALL, FTAG, RW_WRITER);
	error = spa_config_parse(spa, &rvd, nvtop, NULL, 0,
	    VDEV_ALLOC_ROOTPOOL);
	spa_config_exit(spa, SCL_ALL, FTAG);
	if (error) {
		mutex_exit(&spa_namespace_lock);
		nvlist_free(config);
		cmn_err(CE_NOTE, "Can not parse the config for pool '%s'",
		    pname);
		return (error);
	}

	/*
	 * Get the boot vdev.
	 */
	if ((bvd = vdev_lookup_by_guid(rvd, guid)) == NULL) {
		cmn_err(CE_NOTE, "Can not find the boot vdev for guid %llu",
		    (u_longlong_t)guid);
		error = SET_ERROR(ENOENT);
		goto out;
	}

	/*
	 * Determine if there is a better boot device.
	 */
	avd = bvd;
	spa_alt_rootvdev(rvd, &avd, &txg);
	if (avd != bvd) {
		cmn_err(CE_NOTE, "The boot device is 'degraded'. Please "
		    "try booting from '%s'", avd->vdev_path);
		error = SET_ERROR(EINVAL);
		goto out;
	}

	/*
	 * If the boot device is part of a spare vdev then ensure that
	 * we're booting off the active spare.
	 */
	if (bvd->vdev_parent->vdev_ops == &vdev_spare_ops &&
	    !bvd->vdev_isspare) {
		cmn_err(CE_NOTE, "The boot device is currently spared. Please "
		    "try booting from '%s'",
		    bvd->vdev_parent->
		    vdev_child[bvd->vdev_parent->vdev_children - 1]->vdev_path);
		error = SET_ERROR(EINVAL);
		goto out;
	}

	/*
	 * The root disk may have been expanded while the system was offline.
	 * Kick off an async task to check for and handle expansion.
	 */
	spa_async_request(spa, SPA_ASYNC_AUTOEXPAND);

	error = 0;
out:
	spa_config_enter(spa, SCL_ALL, FTAG, RW_WRITER);
	vdev_free(rvd);
	spa_config_exit(spa, SCL_ALL, FTAG);
	mutex_exit(&spa_namespace_lock);

	nvlist_free(config);
	return (error);
}

const char *
spa_history_zone(void)
{
        if (INGLOBALZONE(curproc))
                return (NULL);
        return (curproc->p_zone->zone_name);
}
#endif /* _KERNEL */
