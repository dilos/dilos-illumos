/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the "License").
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
 * or https://opensource.org/licenses/CDDL-1.0.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/OPENSOLARIS.LICENSE.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets "[]" replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/*
 * Copyright (c) 2010, Oracle and/or its affiliates. All rights reserved.
 * Copyright (c) 2012, 2014 by Delphix. All rights reserved.
 */

#include <sys/zfs_context.h>
#include <sys/kstat.h>

typedef struct zfs_dbgmsg {
	list_node_t zdm_node;
	uint64_t zdm_timestamp;
	hrtime_t zdm_hrtime;
	char zdm_msg[1]; /* variable length allocation */
} zfs_dbgmsg_t;

/*
 * Print debug messages:
 *
 * # echo '::zfs_dbgmsg' | mdb -k
 */
list_t zfs_dbgmsgs;

/*
 * Clear debug messages, will be realy cleared on next insert
 * Use:
 *
 * # echo '::zfs_dbgmsg -Z' | mdb -kw
 */
int zfs_dbgmsgs_zero = 0;
static uint_t zfs_dbgmsg_size = 0;
static kmutex_t zfs_dbgmsgs_lock;
uint_t zfs_dbgmsg_maxsize = 4<<20; /* 4MB */
static boolean_t zfs_dbgmsg_initialized = B_FALSE;

/*
 * Disable the kernel debug message log:
 *
 * # echo 'zfs_dbgmsg_enable/W 0' | mdb -kw
 */
int zfs_dbgmsg_enable = B_TRUE;

static void
zfs_dbgmsg_purge(uint_t max_size)
{
	ASSERT(MUTEX_HELD(&zfs_dbgmsgs_lock));

	while (zfs_dbgmsg_size > max_size) {
		zfs_dbgmsg_t *zdm = list_remove_head(&zfs_dbgmsgs);
		if (zdm == NULL)
			return;

		uint_t size = sizeof (zfs_dbgmsg_t) + strlen(zdm->zdm_msg);
		kmem_free(zdm, size);
		zfs_dbgmsg_size -= size;
	}
}

void
zfs_dbgmsg_init(void)
{
	ASSERT(zfs_dbgmsg_initialized == B_FALSE);

	list_create(&zfs_dbgmsgs, sizeof (zfs_dbgmsg_t),
	    offsetof(zfs_dbgmsg_t, zdm_node));
	mutex_init(&zfs_dbgmsgs_lock, NULL, MUTEX_DEFAULT, NULL);
	zfs_dbgmsg_initialized = B_TRUE;
}

void
zfs_dbgmsg_fini(void)
{
	ASSERT(zfs_dbgmsg_initialized);
	zfs_dbgmsg_initialized = B_FALSE;

	mutex_enter(&zfs_dbgmsgs_lock);
	zfs_dbgmsg_purge(0);
	mutex_exit(&zfs_dbgmsgs_lock);
	mutex_destroy(&zfs_dbgmsgs_lock);
	ASSERT0(zfs_dbgmsg_size);
}

/*
 * Print these messages by running:
 * echo ::zfs_dbgmsg | mdb -k
 *
 * Monitor these messages by running:
 * dtrace -qn 'zfs-dbgmsg{printf("%s\n", stringof(arg0))}'
 *
 * When used with libzpool, monitor with:
 * dtrace -qn 'zfs$pid::zfs_dbgmsg:probe1{printf("%s\n", copyinstr(arg1))}'
 */
void
__zfs_dbgmsg(char *buf)
{
	DTRACE_PROBE1(zfs__dbgmsg, char *, buf);

	if (zfs_dbgmsg_initialized == B_FALSE) {
		printf("%s\n", buf);
		return;
	}

	uint_t size = sizeof (zfs_dbgmsg_t) + strlen(buf);
	zfs_dbgmsg_t *zdm = kmem_alloc(size, KM_SLEEP);
	zdm->zdm_timestamp = gethrestime_sec();
	zdm->zdm_hrtime = gethrtime();
	strcpy(zdm->zdm_msg, buf);

	mutex_enter(&zfs_dbgmsgs_lock);
	if (zfs_dbgmsgs_zero) {
		zfs_dbgmsg_purge(0);
		zfs_dbgmsgs_zero = 0;
	}
	list_insert_tail(&zfs_dbgmsgs, zdm);
	zfs_dbgmsg_size += size;
	zfs_dbgmsg_purge(MAX(zfs_dbgmsg_maxsize, 0));
	mutex_exit(&zfs_dbgmsgs_lock);
}

#ifdef _KERNEL
void
__dprintf(boolean_t dprint, const char *file, const char *func,
    int line, const char *fmt, ...)
{
	const char *newfile;
	va_list adx;

	/*
	 * Get rid of annoying prefix to filename.
	 */
	newfile = strrchr(file, '/');
	if (newfile != NULL) {
		newfile = newfile + 1; /* Get rid of leading / */
	} else {
		newfile = file;
	}

	va_start(adx, fmt);
	int size = vsnprintf(NULL, 0, fmt, adx) + 1;
	va_end(adx);

	char *buf = kmem_alloc(size, KM_SLEEP);

	va_start(adx, fmt);
	(void) vsnprintf(buf, size, fmt, adx);
	va_end(adx);
	/*
	 * Get rid of trailing newline.
	 */
	char *nl = strrchr(buf, '\n');
	if (nl != NULL)
		*nl = '\0';

	if (dprint) {
		/*
		 * To get this data, use the zfs-dprintf probe as so:
		 * dtrace -q -n 'zfs-dprintf \
		 *      /stringof(arg0) == "dbuf.c"/ \
		 *      {printf("%s: %s", stringof(arg1), stringof(arg3))}'
		 * arg0 = file name
		 * arg1 = function name
		 * arg2 = line number
		 * arg3 = message
		 */
		DTRACE_PROBE4(zfs__dprintf,
		    char *, newfile, char *, func, int, line, char *, buf);
	} else {
		__zfs_dbgmsg(buf);
	}
	kmem_free(buf, size);
}

#else

void
zfs_dbgmsg_print(const char *tag)
{
	if (zfs_dbgmsg_initialized == B_FALSE)
		return;

	(void) printf("ZFS_DBGMSG(%s):\n", tag);
	mutex_enter(&zfs_dbgmsgs_lock);
	if (!zfs_dbgmsgs_zero)
		for (zfs_dbgmsg_t *zdm = list_head(&zfs_dbgmsgs); zdm;
		    zdm = list_next(&zfs_dbgmsgs, zdm))
			(void) printf("%s\n", zdm->zdm_msg);
	mutex_exit(&zfs_dbgmsgs_lock);
}
#endif /* _KERNEL */

