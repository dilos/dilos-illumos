/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the "License").
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/OPENSOLARIS.LICENSE.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets "[]" replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */
/*
 * Copyright (c) 2018 Igor Kozhukhov <igor@dilos.org>
 */

/*
 * gnttab.c
 *
 * Granting foreign access to our memory reservation.
 *
 * Copyright (c) 2005-2006, Christopher Clark
 * Copyright (c) 2004-2005, K A Fraser
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation; or, when distributed
 * separately from the Linux kernel or incorporated into other
 * software packages, subject to the following license:
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this source file (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy, modify,
 * merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include <sys/types.h>
#include <sys/archsystm.h>
#ifdef XPV_HVM_DRIVER
#include <sys/xpv_support.h>
#include <sys/mman.h>
#include <vm/hat.h>
#endif
#include <sys/hypervisor.h>
#include <sys/gnttab.h>
#include <sys/sysmacros.h>
#include <sys/machsystm.h>
#include <sys/systm.h>
#include <sys/mutex.h>
#include <sys/atomic.h>
#include <sys/spl.h>
#include <sys/condvar.h>
#include <sys/cpuvar.h>
#include <sys/taskq.h>
#include <sys/panic.h>
#include <sys/cmn_err.h>
#include <sys/promif.h>
#include <sys/cpu.h>
#include <sys/vmem.h>
#include <vm/hat_i86.h>
#include <sys/bootconf.h>
#include <sys/bootsvcs.h>
#ifndef XPV_HVM_DRIVER
#include <sys/bootinfo.h>
#include <sys/multiboot.h>
#include <vm/kboot_mmu.h>
#endif
#include <sys/bootvfs.h>
#include <sys/bootprops.h>
#include <vm/seg_kmem.h>
#include <sys/mman.h>

/* Globals */

static grant_ref_t **gnttab_list;
static uint_t nr_grant_frames;
static int gnttab_free_count;
static grant_ref_t gnttab_free_head;
static kmutex_t gnttab_list_lock;
static struct gnttab_free_callback *gnttab_free_callback_list;

static union {
	struct grant_entry_v1 *v1;
	union grant_entry_v2 *v2;
	void *addr;
} gnttab_shared;

/*This is a structure of function pointers for grant table*/
struct gnttab_ops {
	/*
	 * Mapping a list of frames for storing grant entries. Frames parameter
	 * is used to store grant table address when grant table being setup,
	 * nr_gframes is the number of frames to map grant table. Returning
	 * GNTST_okay means success and negative value means failure.
	 */
	int (*map_frames)(xen_pfn_t *frames, uint_t nr_gframes);
	/*
	 * Release a list of frames which are mapped in map_frames for grant
	 * entry status.
	 */
	void (*unmap_frames)(void);
	/*
	 * Introducing a valid entry into the grant table, granting the frame of
	 * this grant entry to domain for accessing or transfering. Ref
	 * parameter is reference of this introduced grant entry, domid is id of
	 * granted domain, frame is the page frame to be granted, and flags is
	 * status of the grant entry to be updated.
	 */
	void (*update_entry)(grant_ref_t ref, domid_t domid,
			     unsigned long frame, unsigned flags);
	/*
	 * Stop granting a grant entry to domain for accessing. Ref parameter is
	 * reference of a grant entry whose grant access will be stopped,
	 * readonly is not in use in this function. If the grant entry is
	 * currently mapped for reading or writing, just return failure(==0)
	 * directly and don't tear down the grant access. Otherwise, stop grant
	 * access for this entry and return success(==1).
	 */
	int (*end_foreign_access_ref)(grant_ref_t ref, int readonly);
	/*
	 * Stop granting a grant entry to domain for transfer. Ref parameter is
	 * reference of a grant entry whose grant transfer will be stopped. If
	 * tranfer has not started, just reclaim the grant entry and return
	 * failure(==0). Otherwise, wait for the transfer to complete and then
	 * return the frame.
	 */
	unsigned long (*end_foreign_transfer_ref)(grant_ref_t ref);
	/*
	 * Query the status of a grant entry. Ref parameter is reference of
	 * queried grant entry, return value is the status of queried entry.
	 * Detailed status(writing/reading) can be gotten from the return value
	 * by bit operations.
	 */
	int (*query_foreign_access)(grant_ref_t ref);
	/*
	 * Grant a domain to access a range of bytes within the page referred by
	 * an available grant entry. Ref parameter is reference of a grant entry
	 * which will be sub-page accessed, domid is id of grantee domain, frame
	 * is frame address of subpage grant, flags is grant type and flag
	 * information, page_off is offset of the range of bytes, and length is
	 * length of bytes to be accessed.
	 */
	void (*update_subpage_entry)(grant_ref_t ref, domid_t domid,
				     unsigned long frame, int flags,
				     unsigned page_off, unsigned length);
	/*
	 * Redirect an available grant entry on domain A to another grant
	 * reference of domain B, then allow domain C to use grant reference
	 * of domain B transitively. Ref parameter is an available grant entry
	 * reference on domain A, domid is id of domain C which accesses grant
	 * entry transitively, flags is grant type and flag information,
	 * trans_domid is id of domain B whose grant entry is finally accessed
	 * transitively, trans_gref is grant entry transitive reference of
	 * domain B.
	 */
	void (*update_trans_entry)(grant_ref_t ref, domid_t domid, int flags,
				   domid_t trans_domid, grant_ref_t trans_gref);
};

static struct gnttab_ops *gnttab_interface;

/*This reflects status of grant entries, so act as a global value*/
static grant_status_t *grstatus;

static int grant_table_version;
static int grefs_per_grant_frame;

/* Macros */

#define	GT_PGADDR1(i) ((uintptr_t)gnttab_shared.v1 + ((i) << MMU_PAGESHIFT))
#define	GT_PGADDR2(i) ((uintptr_t)gnttab_shared.v2 + ((i) << MMU_PAGESHIFT))
#define	VALID_GRANT_REF(r) ((r) < (nr_grant_frames * grefs_per_grant_frame))
#define	RPP (PAGESIZE / sizeof (grant_ref_t))
#define	GNTTAB_ENTRY(entry) (gnttab_list[(entry) / RPP][(entry) % RPP])
#define	CMPXCHG(t, c, n) atomic_cas_16((t), (c), (n))
/* External tools reserve first few grant table entries. */
#define	NR_RESERVED_ENTRIES 8
#define	GNTTAB_LIST_END 0xffffffff

/* Implementation */

static uint_t
max_nr_grant_frames(void)
{
	struct gnttab_query_size query;
	int rc;

	query.dom = DOMID_SELF;

	rc = HYPERVISOR_grant_table_op(GNTTABOP_query_size, &query, 1);
	if ((rc < 0) || (query.status != GNTST_okay))
		return (4); /* Legacy max supported number of frames */

	ASSERT(query.max_nr_frames);
	return (query.max_nr_frames);
}

static void
do_free_callbacks(void)
{
	struct gnttab_free_callback *callback, *next;

	callback = gnttab_free_callback_list;
	gnttab_free_callback_list = NULL;

	while (callback != NULL) {
		next = callback->next;
		if (gnttab_free_count >= callback->count) {
			callback->next = NULL;
			callback->fn(callback->arg);
		} else {
			callback->next = gnttab_free_callback_list;
			gnttab_free_callback_list = callback;
		}
		callback = next;
	}
}

static void
check_free_callbacks(void)
{
	if (gnttab_free_callback_list)
		do_free_callbacks();
}

static int
grow_gnttab_list(uint_t more_frames)
{
	uint_t new_nr_grant_frames, extra_entries, i;
	uint_t nr_glist_frames, new_nr_glist_frames;

	ASSERT(MUTEX_HELD(&gnttab_list_lock));

	new_nr_grant_frames = nr_grant_frames + more_frames;
	extra_entries = more_frames * grefs_per_grant_frame;

	nr_glist_frames = (nr_grant_frames * grefs_per_grant_frame + RPP - 1)
	    / RPP;
	new_nr_glist_frames = (new_nr_grant_frames * grefs_per_grant_frame
	    + RPP - 1) / RPP;
	for (i = nr_glist_frames; i < new_nr_glist_frames; i++)
		gnttab_list[i] = kmem_alloc(PAGESIZE, KM_SLEEP);

	for (i = grefs_per_grant_frame * nr_grant_frames;
	    i < grefs_per_grant_frame * new_nr_grant_frames - 1; i++)
		GNTTAB_ENTRY(i) = i + 1;

	GNTTAB_ENTRY(i) = gnttab_free_head;
	gnttab_free_head = grefs_per_grant_frame * nr_grant_frames;
	gnttab_free_count += extra_entries;

	nr_grant_frames = new_nr_grant_frames;

	check_free_callbacks();

	return (0);
}

static int
gnttab_expand(uint_t req_entries)
{
	uint_t cur, extra;

	ASSERT(MUTEX_HELD(&gnttab_list_lock));

	cur = nr_grant_frames;
	extra = ((req_entries + (grefs_per_grant_frame - 1)) /
	    grefs_per_grant_frame);
	if (cur + extra > max_nr_grant_frames())
		return (-1);

	return (grow_gnttab_list(extra));
}

static int
get_free_entries(int count)
{
	int ref, rc;
	grant_ref_t head;

	mutex_enter(&gnttab_list_lock);
	if (gnttab_free_count < count &&
	    ((rc = gnttab_expand(count - gnttab_free_count)) < 0)) {
		mutex_exit(&gnttab_list_lock);
		return (rc);
	}
	ref = head = gnttab_free_head;
	gnttab_free_count -= count;
	while (count-- > 1)
		head = GNTTAB_ENTRY(head);
	gnttab_free_head = GNTTAB_ENTRY(head);
	GNTTAB_ENTRY(head) = GNTTAB_LIST_END;
	mutex_exit(&gnttab_list_lock);
	return (ref);
}

static void
put_free_entry(grant_ref_t ref)
{
	ASSERT(VALID_GRANT_REF(ref));

	mutex_enter(&gnttab_list_lock);
	GNTTAB_ENTRY(ref) = gnttab_free_head;
	gnttab_free_head = ref;
	gnttab_free_count++;
	check_free_callbacks();
	mutex_exit(&gnttab_list_lock);
}

/*
 * Following applies to gnttab_update_entry_v1 and gnttab_update_entry_v2.
 * Introducing a valid entry into the grant table:
 *  1. Write ent->domid.
 *  2. Write ent->frame:
 *      GTF_permit_access:   Frame to which access is permitted.
 *      GTF_accept_transfer: Pseudo-phys frame slot being filled by new
 *                           frame, or zero if none.
 *  3. Write memory barrier (WMB).
 *  4. Write ent->flags, inc. valid type.
 */
static void gnttab_update_entry_v1(grant_ref_t ref, domid_t domid,
				   unsigned long frame, unsigned flags)
{
	gnttab_shared.v1[ref].domid = domid;
	gnttab_shared.v1[ref].frame = frame;
/*	wmb();*/
	membar_producer();
	gnttab_shared.v1[ref].flags = flags;
}

static void gnttab_update_entry_v2(grant_ref_t ref, domid_t domid,
				   unsigned long frame, unsigned flags)
{
	gnttab_shared.v2[ref].hdr.domid = domid;
	gnttab_shared.v2[ref].full_page.frame = frame;
/*	wmb();*/
	membar_producer();
	gnttab_shared.v2[ref].hdr.flags = GTF_permit_access | flags;
}

/*
 * Public grant-issuing interface functions
 */
void
gnttab_grant_foreign_access_ref(grant_ref_t ref, domid_t domid,
				     gnttab_frame_t frame, int readonly)
{
	gnttab_interface->update_entry(ref, domid, frame,
			   GTF_permit_access | (readonly ? GTF_readonly : 0));
}

int
gnttab_grant_foreign_access(domid_t domid, gnttab_frame_t frame, int readonly)
{
	int ref;

	ref = get_free_entries(1);
	if (ref < 0)
		return -ENOSPC;

/*	ASSERT(VALID_GRANT_REF(ref));*/

	gnttab_grant_foreign_access_ref(ref, domid, frame, readonly);

	return (ref);
}

static void
gnttab_update_subpage_entry_v2(grant_ref_t ref, domid_t domid,
					   unsigned long frame, int flags,
					   unsigned page_off, unsigned length)
{
	gnttab_shared.v2[ref].sub_page.frame = frame;
	gnttab_shared.v2[ref].sub_page.page_off = page_off;
	gnttab_shared.v2[ref].sub_page.length = length;
	gnttab_shared.v2[ref].hdr.domid = domid;
/*	wmb();*/
	membar_producer();
	gnttab_shared.v2[ref].hdr.flags =
				GTF_permit_access | GTF_sub_page | flags;
}

int
gnttab_grant_foreign_access_subpage_ref(grant_ref_t ref, domid_t domid,
					    unsigned long frame, int flags,
					    unsigned page_off,
					    unsigned length)
{
	if (flags & (GTF_accept_transfer | GTF_reading |
		     GTF_writing | GTF_transitive))
		return -EPERM;

	if (gnttab_interface->update_subpage_entry == NULL)
		return -ENOSYS;

	gnttab_interface->update_subpage_entry(ref, domid, frame, flags,
					       page_off, length);

	return 0;
}

int
gnttab_grant_foreign_access_subpage(domid_t domid, unsigned long frame,
					int flags, unsigned page_off,
					unsigned length)
{
	int ref, rc;

	ref = get_free_entries(1);
	if (ref < 0)
		return -ENOSPC;

	rc = gnttab_grant_foreign_access_subpage_ref(ref, domid, frame, flags,
						     page_off, length);
	if (rc < 0) {
		put_free_entry(ref);
		return rc;
	}

	return ref;
}

boolean_t
gnttab_subpage_grants_available(void)
{
	return (gnttab_interface->update_subpage_entry != NULL);
}

static void
gnttab_update_trans_entry_v2(grant_ref_t ref, domid_t domid,
					 int flags, domid_t trans_domid,
					 grant_ref_t trans_gref)
{
	gnttab_shared.v2[ref].transitive.trans_domid = trans_domid;
	gnttab_shared.v2[ref].transitive.gref = trans_gref;
	gnttab_shared.v2[ref].hdr.domid = domid;
/*	wmb();*/
	membar_producer();
	gnttab_shared.v2[ref].hdr.flags =
				GTF_permit_access | GTF_transitive | flags;
}

int
gnttab_grant_foreign_access_trans_ref(grant_ref_t ref, domid_t domid,
					  int flags, domid_t trans_domid,
					  grant_ref_t trans_gref)
{
	if (flags & (GTF_accept_transfer | GTF_reading |
		     GTF_writing | GTF_sub_page))
		return -EPERM;

	if (gnttab_interface->update_trans_entry == NULL)
		return -ENOSYS;

	gnttab_interface->update_trans_entry(ref, domid, flags, trans_domid,
					     trans_gref);

	return 0;
}

int
gnttab_grant_foreign_access_trans(domid_t domid, int flags,
				      domid_t trans_domid,
				      grant_ref_t trans_gref)
{
	int ref, rc;

	ref = get_free_entries(1);
	if (ref < 0)
		return -ENOSPC;

	rc = gnttab_grant_foreign_access_trans_ref(ref, domid, flags,
						   trans_domid, trans_gref);
	if (rc < 0) {
		put_free_entry(ref);
		return rc;
	}

	return ref;
}

boolean_t
gnttab_trans_grants_available(void)
{
	return (gnttab_interface->update_trans_entry != NULL);
}

static int
gnttab_query_foreign_access_v1(grant_ref_t ref)
{
	return gnttab_shared.v1[ref].flags & (GTF_reading|GTF_writing);
}

static int
gnttab_query_foreign_access_v2(grant_ref_t ref)
{
	return grstatus[ref] & (GTF_reading|GTF_writing);
}

int gnttab_query_foreign_access(grant_ref_t ref)
{
/*	ASSERT(VALID_GRANT_REF(ref));*/
	return gnttab_interface->query_foreign_access(ref);
}

static int
gnttab_end_foreign_access_ref_v1(grant_ref_t ref, int readonly)
{
	uint16_t flags, nflags;
	uint16_t *pflags;

	pflags = &gnttab_shared.v1[ref].flags;
	nflags = *pflags;
	do {
		flags = nflags;
		if (flags & (GTF_reading|GTF_writing))
			return 0;
	} while ((nflags = CMPXCHG(pflags, flags, 0)) != flags);

	return 1;
}

static int
gnttab_end_foreign_access_ref_v2(grant_ref_t ref, int readonly)
{
	gnttab_shared.v2[ref].hdr.flags = 0;
/*	mb();*/
	membar_exit();
	if (grstatus[ref] & (GTF_reading|GTF_writing)) {
		return 0;
	} else {
		/* The read of grstatus needs to have acquire
		semantics.  On x86, reads already have
		that, and we just need to protect against
		compiler reorderings.  On other
		architectures we may need a full
		barrier. */
/*		barrier();*/
		membar_enter();
	}

	return 1;
}

static inline int
_gnttab_end_foreign_access_ref(grant_ref_t ref, int readonly)
{
	return gnttab_interface->end_foreign_access_ref(ref, readonly);
}

int
gnttab_end_foreign_access_ref(grant_ref_t ref, int readonly)
{
	if (_gnttab_end_foreign_access_ref(ref, readonly))
		return 1;
/*	pr_warn("WARNING: g.e. %#x still in use!\n", ref);*/
	cmn_err(CE_WARN, "g.e. still in use!");
	return 0;
}

void
gnttab_end_foreign_access(grant_ref_t ref, int readonly, gnttab_frame_t page)
{
	ASSERT(VALID_GRANT_REF(ref));

	if (gnttab_end_foreign_access_ref(ref, readonly)) {
		put_free_entry(ref);
		/*
		 * XXPV - we don't support freeing a page here
		 */
		if (page != 0) {
			cmn_err(CE_WARN,
	"gnttab_end_foreign_access_ref: using unsupported free_page interface");
			/* free_page(page); */
		}
	} else {
		/*
		 * XXX This needs to be fixed so that the ref and page are
		 * placed on a list to be freed up later.
		 */
		cmn_err(CE_WARN, "leaking g.e. and page still in use!");
	}
}



void
gnttab_grant_foreign_transfer_ref(grant_ref_t ref, domid_t domid, pfn_t pfn)
{
/*	ASSERT(VALID_GRANT_REF(ref));*/
	gnttab_interface->update_entry(ref, domid, pfn, GTF_accept_transfer);
}

int
gnttab_grant_foreign_transfer(domid_t domid, pfn_t pfn)
{
	int ref;

	ref = get_free_entries(1);
	if (ref < 0)
		return -ENOSPC;

	ASSERT(VALID_GRANT_REF(ref));

	gnttab_grant_foreign_transfer_ref(ref, domid, pfn);

	return (ref);
}


static gnttab_frame_t
gnttab_end_foreign_transfer_ref_v1(grant_ref_t ref)
{
	gnttab_frame_t frame;
	uint16_t       flags;
	uint16_t       *pflags;

	pflags = &gnttab_shared.v1[ref].flags;

	/*
	 * If a transfer is not even yet started, try to reclaim the grant
	 * reference and return failure (== 0).
	 */
	while (!((flags = *pflags) & GTF_transfer_committed)) {
		if (CMPXCHG(pflags, flags, 0) == flags)
			return 0;
/*		cpu_relax();*/
		(void) HYPERVISOR_yield();
	}

	/* If a transfer is in progress then wait until it is completed. */
	while (!(flags & GTF_transfer_completed)) {
		flags = *pflags;
/*		cpu_relax();*/
		(void) HYPERVISOR_yield();
	}

	/*rmb();*/	/* Read the frame number /after/ reading completion status. */
	membar_consumer();
	frame = gnttab_shared.v1[ref].frame;
	ASSERT(frame == 0);

	return frame;
}

static gnttab_frame_t
gnttab_end_foreign_transfer_ref_v2(grant_ref_t ref)
{
	gnttab_frame_t frame;
	uint16_t       flags;
	uint16_t       *pflags;

	pflags = &gnttab_shared.v2[ref].hdr.flags;

	/*
	 * If a transfer is not even yet started, try to reclaim the grant
	 * reference and return failure (== 0).
	 */
	while (!((flags = *pflags) & GTF_transfer_committed)) {
		if (CMPXCHG(pflags, flags, 0) == flags)
			return 0;
/*		cpu_relax();*/
		(void) HYPERVISOR_yield();
	}

	/* If a transfer is in progress then wait until it is completed. */
	while (!(flags & GTF_transfer_completed)) {
		flags = *pflags;
/*		cpu_relax();*/
		(void) HYPERVISOR_yield();
	}

	/*rmb();*/  /* Read the frame number /after/ reading completion status. */
	membar_consumer();
	frame = gnttab_shared.v2[ref].full_page.frame;
	ASSERT(frame == 0);

	return frame;
}


gnttab_frame_t
gnttab_end_foreign_transfer_ref(grant_ref_t ref)
{
/*	ASSERT(VALID_GRANT_REF(ref));*/
	return gnttab_interface->end_foreign_transfer_ref(ref);
}

gnttab_frame_t
gnttab_end_foreign_transfer(grant_ref_t ref)
{
	gnttab_frame_t frame = gnttab_end_foreign_transfer_ref(ref);
/*	ASSERT(VALID_GRANT_REF(ref));*/
	put_free_entry(ref);
	return frame;
}

void
gnttab_free_grant_reference(grant_ref_t ref)
{
	ASSERT(VALID_GRANT_REF(ref));

	put_free_entry(ref);
}

void
gnttab_free_grant_references(grant_ref_t head)
{
	grant_ref_t ref;
	int count = 1;

	if (head == GNTTAB_LIST_END)
		return;
	mutex_enter(&gnttab_list_lock);
	ref = head;
	while (GNTTAB_ENTRY(ref) != GNTTAB_LIST_END) {
		ref = GNTTAB_ENTRY(ref);
		count++;
	}
	GNTTAB_ENTRY(ref) = gnttab_free_head;
	gnttab_free_head = head;
	gnttab_free_count += count;
	check_free_callbacks();
	mutex_exit(&gnttab_list_lock);
}

int
gnttab_alloc_grant_references(uint16_t count, grant_ref_t *head)
{
	int h = get_free_entries(count);

	if (h == -1)
		return (-1);

	*head = h;

	return (0);
}

int
gnttab_empty_grant_references(const grant_ref_t *private_head)
{
	return (*private_head == GNTTAB_LIST_END);
}

int
gnttab_claim_grant_reference(grant_ref_t *private_head)
{
	grant_ref_t g = *private_head;

	if (g == GNTTAB_LIST_END)
		return (-1);
	*private_head = GNTTAB_ENTRY(g);
	return (g);
}

void
gnttab_release_grant_reference(grant_ref_t *private_head, grant_ref_t release)
{
	ASSERT(VALID_GRANT_REF(release));

	GNTTAB_ENTRY(release) = *private_head;
	*private_head = release;
}

void
gnttab_request_free_callback(struct gnttab_free_callback *callback,
	void (*fn)(void *), void *arg, uint16_t count)
{
	mutex_enter(&gnttab_list_lock);
	if (callback->next)
		goto out;
	callback->fn = fn;
	callback->arg = arg;
	callback->count = count;
	callback->next = gnttab_free_callback_list;
	gnttab_free_callback_list = callback;
	check_free_callbacks();
out:
	mutex_exit(&gnttab_list_lock);
}

void
gnttab_cancel_free_callback(struct gnttab_free_callback *callback)
{
	struct gnttab_free_callback **pcb;

	mutex_enter(&gnttab_list_lock);
	for (pcb = &gnttab_free_callback_list; *pcb; pcb = &(*pcb)->next) {
		if (*pcb == callback) {
			*pcb = callback->next;
			break;
		}
	}
	mutex_exit(&gnttab_list_lock);
}

static struct gnttab_ops gnttab_v1_ops = {
/*	.map_frames			= gnttab_map_frames_v1,
	.unmap_frames			= gnttab_unmap_frames_v1,*/
	.update_entry			= gnttab_update_entry_v1,
	.end_foreign_access_ref		= gnttab_end_foreign_access_ref_v1,
	.end_foreign_transfer_ref	= gnttab_end_foreign_transfer_ref_v1,
	.query_foreign_access		= gnttab_query_foreign_access_v1,
};

static struct gnttab_ops gnttab_v2_ops = {
/*	.map_frames			= gnttab_map_frames_v2,
	.unmap_frames			= gnttab_unmap_frames_v2,*/
	.update_entry			= gnttab_update_entry_v2,
	.end_foreign_access_ref		= gnttab_end_foreign_access_ref_v2,
	.end_foreign_transfer_ref	= gnttab_end_foreign_transfer_ref_v2,
	.query_foreign_access		= gnttab_query_foreign_access_v2,
	.update_subpage_entry		= gnttab_update_subpage_entry_v2,
	.update_trans_entry		= gnttab_update_trans_entry_v2,
};

static
void gnttab_request_version(void)
{
	int rc;
	struct gnttab_set_version gsv;

	gsv.version = 1;

	rc = HYPERVISOR_grant_table_op(GNTTABOP_set_version, &gsv, 1);
	if (rc == 0 && gsv.version == 2) {
		grant_table_version = 2;
		grefs_per_grant_frame = PAGESIZE / sizeof(union grant_entry_v2);
		gnttab_interface = &gnttab_v2_ops;
	} else if (grant_table_version == 2) {
		/*
		 * If we've already used version 2 features,
		 * but then suddenly discover that they're not
		 * available (e.g. migrating to an older
		 * version of Xen), almost unbounded badness
		 * can happen.
		 */
		panic("we need grant tables version 2, but only version 1 is available");
	} else {
		grant_table_version = 1;
		grefs_per_grant_frame = PAGESIZE / sizeof(struct grant_entry_v1);
		gnttab_interface = &gnttab_v1_ops;
	}
	cmn_err(CE_WARN, "Grant tables using version %d layout\n", grant_table_version);
}

static gnttab_frame_t *
gnttab_setup(gnttab_setup_table_t *pset)
{
	gnttab_frame_t *frames;

	frames = kmem_alloc(pset->nr_frames * sizeof (gnttab_frame_t),
	    KM_SLEEP);

	/*LINTED: constant in conditional context*/
	set_xen_guest_handle(pset->frame_list, frames);

#ifndef XPV_HVM_DRIVER
	/*
	 * Take pset->nr_frames pages of grant table space from
	 * the hypervisor and map it
	 */
	if ((HYPERVISOR_grant_table_op(GNTTABOP_setup_table, pset, 1) != 0) ||
	    (pset->status != 0)) {
		cmn_err(CE_PANIC, "Grant Table setup failed");
	}
#endif

	return (frames);
}

#ifdef XPV_HVM_DRIVER
static void
gnttab_map(void)
{
	struct xen_add_to_physmap xatp;
	caddr_t va = NULL;
	pfn_t pfn;
	int i;

	if(grant_table_version == 1)
		va = (caddr_t)gnttab_shared.v1;
	if(grant_table_version == 2)
		va = (caddr_t)gnttab_shared.v2;
	for (i = 0; i < max_nr_grant_frames(); i++) {
		if ((pfn = hat_getpfnum(kas.a_hat, va)) == PFN_INVALID)
			cmn_err(CE_PANIC, "gnttab_map: Invalid pfn");

		xatp.domid = DOMID_SELF;
		xatp.idx = i;
		xatp.space = XENMAPSPACE_grant_table;
		xatp.gpfn = pfn;
		hat_unload(kas.a_hat, va, MMU_PAGESIZE, HAT_UNLOAD);
		/*
		 * This call replaces the existing machine page backing
		 * the given gpfn with the page from the allocated grant
		 * table at index idx. The existing machine page is
		 * returned to the free list.
		 */
		if (HYPERVISOR_memory_op(XENMEM_add_to_physmap, &xatp) != 0)
			panic("Couldn't map grant table");
		hat_devload(kas.a_hat, va, MMU_PAGESIZE, pfn,
		    PROT_READ | PROT_WRITE | HAT_STORECACHING_OK,
		    HAT_LOAD | HAT_LOAD_LOCK | HAT_LOAD_NOCONSIST);
		va += MMU_PAGESIZE;
	}
}
#endif /* XPV_HVM_DRIVER */

void
gnttab_init(void)
{
	gnttab_setup_table_t set;
	int i;
	uint_t nr_init_grefs, max_nr_glist_frames, nr_glist_frames;
	gnttab_frame_t *frames;

	/*
	 * gnttab_init() should only be invoked once.
	 */
	gnttab_request_version();
	mutex_enter(&gnttab_list_lock);
	ASSERT(nr_grant_frames == 0);
	nr_grant_frames = 1;
	mutex_exit(&gnttab_list_lock);

	max_nr_glist_frames = (max_nr_grant_frames() *
	    grefs_per_grant_frame / RPP);

	set.dom = DOMID_SELF;
	set.nr_frames = max_nr_grant_frames();
	frames = gnttab_setup(&set);

#ifdef XPV_HVM_DRIVER
	if(grant_table_version == 1)
		gnttab_shared.v1 = xen_alloc_pages(set.nr_frames);
	if(grant_table_version == 2)
		gnttab_shared.v2 = xen_alloc_pages(set.nr_frames);

	gnttab_map();
#else /* XPV_HVM_DRIVER */
	if(grant_table_version == 1) {
	    gnttab_shared.v1 = vmem_xalloc(heap_arena, set.nr_frames * MMU_PAGESIZE,
		MMU_PAGESIZE, 0, 0, 0, 0, VM_SLEEP);
	    for (i = 0; i < set.nr_frames; i++) {
		hat_devload(kas.a_hat, (caddr_t)GT_PGADDR1(i), PAGESIZE,
		    xen_assign_pfn(frames[i]),
		    PROT_READ | PROT_WRITE | HAT_STORECACHING_OK,
		    HAT_LOAD_LOCK);
	    }
	}
	if(grant_table_version == 2) {
	    gnttab_shared.v2 = vmem_xalloc(heap_arena, set.nr_frames * MMU_PAGESIZE,
		MMU_PAGESIZE, 0, 0, 0, 0, VM_SLEEP);
	    for (i = 0; i < set.nr_frames; i++) {
		hat_devload(kas.a_hat, (caddr_t)GT_PGADDR2(i), PAGESIZE,
		    xen_assign_pfn(frames[i]),
		    PROT_READ | PROT_WRITE | HAT_STORECACHING_OK,
		    HAT_LOAD_LOCK);
	    }
	}
#endif

	gnttab_list = kmem_alloc(max_nr_glist_frames * sizeof (grant_ref_t *),
	    KM_SLEEP);

	nr_glist_frames = (nr_grant_frames * grefs_per_grant_frame + RPP - 1)
	    / RPP;
	for (i = 0; i < nr_glist_frames; i++) {
		gnttab_list[i] = kmem_alloc(PAGESIZE, KM_SLEEP);
	}

	kmem_free(frames, set.nr_frames * sizeof (gnttab_frame_t));

	nr_init_grefs = nr_grant_frames * grefs_per_grant_frame;

	for (i = NR_RESERVED_ENTRIES; i < nr_init_grefs - 1; i++)
		GNTTAB_ENTRY(i) = i + 1;

	GNTTAB_ENTRY(nr_init_grefs - 1) = GNTTAB_LIST_END;
	gnttab_free_count = nr_init_grefs - NR_RESERVED_ENTRIES;
	gnttab_free_head  = NR_RESERVED_ENTRIES;
}

void
gnttab_resume(void)
{
	gnttab_setup_table_t set;
	int i;
	gnttab_frame_t *frames;
	uint_t available_frames = max_nr_grant_frames();

	if (available_frames < nr_grant_frames) {
		cmn_err(CE_PANIC, "Hypervisor does not have enough grant "
		    "frames: required(%u), available(%u)", nr_grant_frames,
		    available_frames);
	}

#ifdef XPV_HVM_DRIVER
	gnttab_map();
#endif /* XPV_HVM_DRIVER */

	set.dom = DOMID_SELF;
	set.nr_frames = available_frames;
	frames = gnttab_setup(&set);

	for (i = 0; i < available_frames; i++) {
	    if(grant_table_version == 1) {
		(void) HYPERVISOR_update_va_mapping(GT_PGADDR1(i),
		    FRAME_TO_MA(frames[i]) | PT_VALID | PT_WRITABLE,
		    UVMF_INVLPG | UVMF_ALL);
	    }
	    if(grant_table_version == 2) {
		(void) HYPERVISOR_update_va_mapping(GT_PGADDR2(i),
		    FRAME_TO_MA(frames[i]) | PT_VALID | PT_WRITABLE,
		    UVMF_INVLPG | UVMF_ALL);
	    }
	}
	kmem_free(frames, set.nr_frames * sizeof (gnttab_frame_t));
}

void
gnttab_suspend(void)
{
	int i;

	/*
	 * clear grant table mappings before suspending
	 */
	for (i = 0; i < max_nr_grant_frames(); i++) {
	    if(grant_table_version == 1) {
		(void) HYPERVISOR_update_va_mapping(GT_PGADDR1(i),
		    0, UVMF_INVLPG);
	    }
	    if(grant_table_version == 2) {
		(void) HYPERVISOR_update_va_mapping(GT_PGADDR2(i),
		    0, UVMF_INVLPG);
	    }
	}
}

/*
 * Local variables:
 *  c-file-style: "solaris"
 *  indent-tabs-mode: t
 *  c-indent-level: 8
 *  c-basic-offset: 8
 *  tab-width: 8
 * End:
 */
