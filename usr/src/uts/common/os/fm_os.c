/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the "License").
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/OPENSOLARIS.LICENSE.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets "[]" replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

#include <sys/types.h>
#include <sys/time.h>
#include <sys/sysevent.h>
#include <sys/sysevent_impl.h>
#include <sys/nvpair.h>
#include <sys/cmn_err.h>
#include <sys/cpuvar.h>
#include <sys/sysmacros.h>
#include <sys/systm.h>
#include <sys/ddifm.h>
#include <sys/ddifm_impl.h>
#include <sys/spl.h>
#include <sys/dumphdr.h>
#include <sys/compress.h>
#include <sys/cpuvar.h>
#include <sys/console.h>
#include <sys/panic.h>
#include <sys/kobj.h>
#include <sys/sunddi.h>
#include <sys/systeminfo.h>
#include <sys/sysevent/eventdefs.h>
#include <sys/fm/util.h>
#include <sys/fm/protocol.h>

errorq_t *ereport_errorq;
void *ereport_dumpbuf;
size_t ereport_dumplen;

static uint_t ereport_chanlen = ERPT_EVCH_MAX;
static evchan_t *ereport_chan = NULL;
static ulong_t ereport_qlen = 0;
static size_t ereport_size = 0;
static int ereport_cols = 80;
static evchan_t *zpool_events_chan = NULL;

/*
 * URL and SUNW-MSG-ID value to display for fm_panic(), defined below.  These
 * values must be kept in sync with the FMA source code in usr/src/cmd/fm.
 */
static const char *fm_url = "http://illumos.org/msg";
static const char *fm_msgid = "SUNOS-8000-0G";
static char *volatile fm_panicstr = NULL;

extern void fastreboot_disable_highpil(void);

/*
 * Formatting utility function for fm_nvprintr.  We attempt to wrap chunks of
 * output so they aren't split across console lines, and return the end column.
 */
/*PRINTFLIKE4*/
static int
fm_printf(int depth, int c, int cols, const char *format, ...)
{
	va_list ap;
	int width;
	char c1;

	va_start(ap, format);
	width = vsnprintf(&c1, sizeof (c1), format, ap);
	va_end(ap);

	if (c + width >= cols) {
		console_printf("\n\r");
		c = 0;
		if (format[0] != ' ' && depth > 0) {
			console_printf(" ");
			c++;
		}
	}

	va_start(ap, format);
	console_vprintf(format, ap);
	va_end(ap);

	return ((c + width) % cols);
}

/*
 * Recursively print a nvlist in the specified column width and return the
 * column we end up in.  This function is called recursively by fm_nvprint(),
 * below.  We generically format the entire nvpair using hexadecimal
 * integers and strings, and elide any integer arrays.  Arrays are basically
 * used for cache dumps right now, so we suppress them so as not to overwhelm
 * the amount of console output we produce at panic time.  This can be further
 * enhanced as FMA technology grows based upon the needs of consumers.  All
 * FMA telemetry is logged using the dump device transport, so the console
 * output serves only as a fallback in case this procedure is unsuccessful.
 */
static int
fm_nvprintr(nvlist_t *nvl, int d, int c, int cols)
{
	nvpair_t *nvp;

	for (nvp = nvlist_next_nvpair(nvl, NULL);
	    nvp != NULL; nvp = nvlist_next_nvpair(nvl, nvp)) {

		data_type_t type = nvpair_type(nvp);
		const char *name = nvpair_name(nvp);

		boolean_t b;
		uint8_t i8;
		uint16_t i16;
		uint32_t i32;
		uint64_t i64;
		char *str;
		nvlist_t *cnv;

		if (strcmp(name, FM_CLASS) == 0)
			continue; /* already printed by caller */

		c = fm_printf(d, c, cols, " %s=", name);

		switch (type) {
		case DATA_TYPE_BOOLEAN:
			c = fm_printf(d + 1, c, cols, " 1");
			break;

		case DATA_TYPE_BOOLEAN_VALUE:
			(void) nvpair_value_boolean_value(nvp, &b);
			c = fm_printf(d + 1, c, cols, b ? "1" : "0");
			break;

		case DATA_TYPE_BYTE:
			(void) nvpair_value_byte(nvp, &i8);
			c = fm_printf(d + 1, c, cols, "%x", i8);
			break;

		case DATA_TYPE_INT8:
			(void) nvpair_value_int8(nvp, (void *)&i8);
			c = fm_printf(d + 1, c, cols, "%x", i8);
			break;

		case DATA_TYPE_UINT8:
			(void) nvpair_value_uint8(nvp, &i8);
			c = fm_printf(d + 1, c, cols, "%x", i8);
			break;

		case DATA_TYPE_INT16:
			(void) nvpair_value_int16(nvp, (void *)&i16);
			c = fm_printf(d + 1, c, cols, "%x", i16);
			break;

		case DATA_TYPE_UINT16:
			(void) nvpair_value_uint16(nvp, &i16);
			c = fm_printf(d + 1, c, cols, "%x", i16);
			break;

		case DATA_TYPE_INT32:
			(void) nvpair_value_int32(nvp, (void *)&i32);
			c = fm_printf(d + 1, c, cols, "%x", i32);
			break;

		case DATA_TYPE_UINT32:
			(void) nvpair_value_uint32(nvp, &i32);
			c = fm_printf(d + 1, c, cols, "%x", i32);
			break;

		case DATA_TYPE_INT64:
			(void) nvpair_value_int64(nvp, (void *)&i64);
			c = fm_printf(d + 1, c, cols, "%llx",
			    (u_longlong_t)i64);
			break;

		case DATA_TYPE_UINT64:
			(void) nvpair_value_uint64(nvp, &i64);
			c = fm_printf(d + 1, c, cols, "%llx",
			    (u_longlong_t)i64);
			break;

		case DATA_TYPE_HRTIME:
			(void) nvpair_value_hrtime(nvp, (void *)&i64);
			c = fm_printf(d + 1, c, cols, "%llx",
			    (u_longlong_t)i64);
			break;

		case DATA_TYPE_STRING:
			(void) nvpair_value_string(nvp, &str);
			c = fm_printf(d + 1, c, cols, "\"%s\"",
			    str ? str : "<NULL>");
			break;

		case DATA_TYPE_NVLIST:
			c = fm_printf(d + 1, c, cols, "[");
			(void) nvpair_value_nvlist(nvp, &cnv);
			c = fm_nvprintr(cnv, d + 1, c, cols);
			c = fm_printf(d + 1, c, cols, " ]");
			break;

		case DATA_TYPE_NVLIST_ARRAY: {
			nvlist_t **val;
			uint_t i, nelem;

			c = fm_printf(d + 1, c, cols, "[");
			(void) nvpair_value_nvlist_array(nvp, &val, &nelem);
			for (i = 0; i < nelem; i++) {
				c = fm_nvprintr(val[i], d + 1, c, cols);
			}
			c = fm_printf(d + 1, c, cols, " ]");
			}
			break;

		case DATA_TYPE_BOOLEAN_ARRAY:
		case DATA_TYPE_BYTE_ARRAY:
		case DATA_TYPE_INT8_ARRAY:
		case DATA_TYPE_UINT8_ARRAY:
		case DATA_TYPE_INT16_ARRAY:
		case DATA_TYPE_UINT16_ARRAY:
		case DATA_TYPE_INT32_ARRAY:
		case DATA_TYPE_UINT32_ARRAY:
		case DATA_TYPE_INT64_ARRAY:
		case DATA_TYPE_UINT64_ARRAY:
		case DATA_TYPE_STRING_ARRAY:
			c = fm_printf(d + 1, c, cols, "[...]");
			break;
		case DATA_TYPE_UNKNOWN:
			c = fm_printf(d + 1, c, cols, "<unknown>");
			break;

		default:
			break;
		}
	}

	return (c);
}

void
fm_nvprint(nvlist_t *nvl)
{
	char *class;
	int c = 0;

	console_printf("\r");

	if (nvlist_lookup_string(nvl, FM_CLASS, &class) == 0)
		c = fm_printf(0, c, ereport_cols, "%s", class);

	if (fm_nvprintr(nvl, 0, c, ereport_cols) != 0)
		console_printf("\n");

	console_printf("\n");
}

/*
 * Wrapper for panic() that first produces an FMA-style message for admins.
 * Normally such messages are generated by fmd(1M)'s syslog-msgs agent: this
 * is the one exception to that rule and the only error that gets messaged.
 * This function is intended for use by subsystems that have detected a fatal
 * error and enqueued appropriate ereports and wish to then force a panic.
 */
/*PRINTFLIKE1*/
void
fm_panic(const char *format, ...)
{
	va_list ap;

	(void) atomic_cas_ptr((void *)&fm_panicstr, NULL, (void *)format);
#if defined(__x86)
	fastreboot_disable_highpil();
#endif /* __x86 */
	va_start(ap, format);
	vpanic(format, ap);
	va_end(ap);
}

/*
 * Simply tell the caller if fm_panicstr is set, ie. an fma event has
 * caused the panic. If so, something other than the default panic
 * diagnosis method will diagnose the cause of the panic.
 */
int
is_fm_panic()
{
	if (fm_panicstr)
		return (1);
	else
		return (0);
}

/*
 * Print any appropriate FMA banner message before the panic message.  This
 * function is called by panicsys() and prints the message for fm_panic().
 * We print the message here so that it comes after the system is quiesced.
 * A one-line summary is recorded in the log only (cmn_err(9F) with "!" prefix).
 * The rest of the message is for the console only and not needed in the log,
 * so it is printed using console_printf().  We break it up into multiple
 * chunks so as to avoid overflowing any small legacy prom_printf() buffers.
 */
void
fm_banner(void)
{
	timespec_t tod;
	hrtime_t now;

	if (!fm_panicstr)
		return; /* panic was not initiated by fm_panic(); do nothing */

	if (panicstr) {
		tod = panic_hrestime;
		now = panic_hrtime;
	} else {
		gethrestime(&tod);
		now = gethrtime_waitfree();
	}

	cmn_err(CE_NOTE, "!SUNW-MSG-ID: %s, "
	    "TYPE: Error, VER: 1, SEVERITY: Major\n", fm_msgid);

	console_printf(
"\n\rSUNW-MSG-ID: %s, TYPE: Error, VER: 1, SEVERITY: Major\n"
"EVENT-TIME: 0x%lx.0x%lx (0x%llx)\n",
	    fm_msgid, tod.tv_sec, tod.tv_nsec, (u_longlong_t)now);

	console_printf(
"PLATFORM: %s, CSN: -, HOSTNAME: %s\n"
"SOURCE: %s, REV: %s %s\n",
	    platform, utsname.nodename, utsname.sysname,
	    utsname.release, utsname.version);

	console_printf(
"DESC: Errors have been detected that require a reboot to ensure system\n"
"integrity.  See %s/%s for more information.\n",
	    fm_url, fm_msgid);

	console_printf(
"AUTO-RESPONSE: Solaris will attempt to save and diagnose the error telemetry\n"
"IMPACT: The system will sync files, save a crash dump if needed, and reboot\n"
"REC-ACTION: Save the error summary below in case telemetry cannot be saved\n");

	console_printf("\n");
}

void
print_msg_hwerr(ctid_t ct_id, proc_t *p)
{
	uprintf("Killed process %d (%s) in contract id %d "
	    "due to hardware error\n", p->p_pid, p->p_user.u_comm, ct_id);
}

/*ARGSUSED*/
static void
fm_drain(void *private, void *data, errorq_elem_t *eep)
{
	(void) private, (void) data;

	nvlist_t *nvl = errorq_elem_nvl(ereport_errorq, eep);

	if (!panicstr)
		(void) fm_ereport_post(nvl, EVCH_TRYHARD);
	else
		fm_nvprint(nvl);
}

/*
 * Utility function to write all of the pending ereports to the dump device.
 * This function is called at either normal reboot or panic time, and simply
 * iterates over the in-transit messages in the ereport sysevent channel.
 */
void
fm_ereport_dump(void)
{
	evchanq_t *chq;
	sysevent_t *sep;
	erpt_dump_t ed;

	timespec_t tod;
	hrtime_t now;
	char *buf;
	size_t len;

	if (panicstr) {
		tod = panic_hrestime;
		now = panic_hrtime;
	} else {
		if (ereport_errorq != NULL)
			errorq_drain(ereport_errorq);
		gethrestime(&tod);
		now = gethrtime_waitfree();
	}

	/*
	 * In the panic case, sysevent_evc_walk_init() will return NULL.
	 */
	if ((chq = sysevent_evc_walk_init(ereport_chan, NULL)) == NULL &&
	    !panicstr)
		return; /* event channel isn't initialized yet */

	while ((sep = sysevent_evc_walk_step(chq)) != NULL) {
		if ((buf = sysevent_evc_event_attr(sep, &len)) == NULL)
			break;

		ed.ed_magic = ERPT_MAGIC;
		ed.ed_chksum = checksum32(buf, len);
		ed.ed_size = (uint32_t)len;
		ed.ed_pad = 0;
		ed.ed_hrt_nsec = SE_TIME(sep);
		ed.ed_hrt_base = now;
		ed.ed_tod_base.sec = tod.tv_sec;
		ed.ed_tod_base.nsec = tod.tv_nsec;

		dumpvp_write(&ed, sizeof (ed));
		dumpvp_write(buf, len);
	}

	sysevent_evc_walk_fini(chq);
}

/*
 * Post an error report (ereport) to the sysevent error channel.  The error
 * channel must be established with a prior call to sysevent_evc_create()
 * before publication may occur.
 */
void
fm_ereport_post(nvlist_t *ereport, int evc_flag)
{
	size_t nvl_size = 0;
	evchan_t *error_chan;
	int error;

	void zfs_zevent_post_dropped(void);

	error = nvlist_size(ereport, &nvl_size, NV_ENCODE_NATIVE);
	if (error) {
		zfs_zevent_post_dropped();
		return;
	}

	if (nvl_size > ERPT_DATA_SZ || nvl_size == 0) {
		zfs_zevent_post_dropped();
		return;
	}

	if (sysevent_evc_bind(FM_ERROR_CHAN, &error_chan,
	    EVCH_CREAT|EVCH_HOLD_PEND) != 0) {
		zfs_zevent_post_dropped();
		return;
	}

	if (sysevent_evc_publish(error_chan, EC_FM, ESC_FM_ERROR,
	    SUNW_VENDOR, FM_PUB, ereport, evc_flag) != 0) {
		zfs_zevent_post_dropped();
		(void) sysevent_evc_unbind(error_chan);
		return;
	}
	(void) sysevent_evc_unbind(error_chan);
}

void
fm_init_os(void)
{
	(void) sysevent_evc_bind(FM_ERROR_CHAN,
	    &ereport_chan, EVCH_CREAT | EVCH_HOLD_PEND);

	(void) sysevent_evc_control(ereport_chan,
	    EVCH_SET_CHAN_LEN, &ereport_chanlen);

	if (ereport_qlen == 0)
		ereport_qlen = ERPT_MAX_ERRS * MAX(max_ncpus, 4);

	if (ereport_size == 0)
		ereport_size = ERPT_DATA_SZ;

	ereport_errorq = errorq_nvcreate("fm_ereport_queue",
	    (errorq_func_t)fm_drain, NULL, ereport_qlen, ereport_size,
	    FM_ERR_PIL, ERRORQ_VITAL);
	if (ereport_errorq == NULL)
		panic("failed to create required ereport error queue");

	ereport_dumpbuf = kmem_alloc(ereport_size, KM_SLEEP);
	ereport_dumplen = ereport_size;
}
