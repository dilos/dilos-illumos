PROG= bootadm

SRCTREE=../../..
PROTO=$(SRCTREE)/../../proto/root_i386

SRCS= bootadm.c bootadm_upgrade.c bootadm_hyper.c bootadm_digest.c \
bootadm_loader.c

CC=gcc
OBJS=$(SRCS:%.c=%.o)

CFLAGS+= -ggdb3 -O0 -D__sun -D__dilos__ -D__dilos -m64 -Ui386 -U__i386 \
-W -Wall -Wextra -Werror -Wno-unknown-pragmas -std=gnu99 \
-DTEXT_DOMAIN=\"SUNW_OST_OSCMD\" -D_TS_ERRNO -D_FILE_OFFSET_BITS=64 \
-I$(PROTO)/usr/include -I$(SRCTREE)/uts/common -I$(SRCTREE)/common \
-I$(SRCTREE)/common/ficl -I$(SRCTREE)/uts/common/fs/zfs

LDFLAGS+= -nodefaultlibs -m64 -L$(PROTO)/lib/amd64 -L$(PROTO)/usr/lib/amd64 \
-lficl-sys -lcryptoutil -lnvpair -lgen -ladm -lefi -lscf -lz -lbe -lzfs -lofmt -lc

all: $(PROG)

$(PROG): $(OBJS)
	$(CC) -o $@ $^ $(LDFLAGS)

clean:
	rm -f $(PROG) $(OBJS)
