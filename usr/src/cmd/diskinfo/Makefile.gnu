PROG= diskinfo

SRCS= diskinfo.c
OBJS= $(SRCS:%.c=%.o)

SRCTREE=../../../..
PROTO= $(SRCTREE)/proto/root_i386
CC= gcc-6
CFLAGS+= -O0 -ggdb3 -nodefaultlibs -D__sun -D__dilos__ -D__dilos \
-Ui386 -U__i386 -std=gnu99 -DTEXT_DOMAIN=\"SUNW_OST_OSCMD\" -D_TS_ERRNO \
-I$(PROTO)/usr/include -I../../lib/fm/topo

LDFLAGS+= -nodefaultlibs \
-L$(PROTO)/lib/amd64 -L$(PROTO)/usr/lib/amd64 \
-L$(PROTO)/usr/lib/fm/amd64 -R/usr/lib/fm/amd64 \
-ldiskmgt -lnvpair -ltopo -lcmdutils -lc 

all: $(PROG)

$(PROG): $(OBJS)
	$(CC) -o $@ $^ $(LDFLAGS)

clean:
	rm -f $(PROG) $(OBJS)
