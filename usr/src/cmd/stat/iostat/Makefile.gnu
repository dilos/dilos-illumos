PROG= iostat

VPATH=../common
SRCS= iostat.c acquire.c walkers.c acquire_iodevs.c dsr.c mnt.c common.c \
timestamp.c

CC=gcc
SRCTREE=../../../..
PROTO=$(SRCTREE)/../proto/root_i386
OBJS= $(SRCS:%.c=%.o)

CFLAGS=-ggdb3 -O0 -nodefaultlibs -D__sun -D__dilos__ -D__dilos \
-Ui386 -U__i386 -std=gnu89 \
-I../common -DTEXT_DOMAIN=\"SUNW_OST_OSCMD\" -D_TS_ERRNO \
-I$(PROTO)/usr/include

LDFLAGS=-nodefaultlibs -L$(PROTO)/lib/amd64 \
-L$(PROTO)/usr/lib/amd64 -lkstat -ldevinfo -lavl -lc

all: $(PROG)

$(PROG): $(OBJS)
	$(CC) -o $@ $^ $(LDFLAGS)

clean:
	rm -f $(PROG) $(OBJS)
