PROG=zfs
SRCS=zfs_iter.c zfs_main.c zfs_project.c
OBJS=$(SRCS:%.c=%.o)
SRCTREE=../../../..
CC=gcc
CFLAGS=-nodefaultlibs -D__sun -D__dilos__ -D__dilos -O0 -ggdb3 -m64 -Ui386 -U__i386 \
-std=gnu99 -msave-args -DTEXT_DOMAIN=\"SUNW_OST_OSCMD\" -D_TS_ERRNO \
-I${SRCTREE}/proto/root_i386/usr/include \
-D_LARGEFILE64_SOURCE=1 -D_REENTRANT \
-I../../common/zfs \
-I${SRCTREE}/usr/src/uts/common/fs/zfs \
-I${SRCTREE}/usr/src/lib/libzfs/common \
-DDEBUG

LDFLAGS=-m64 \
-L${SRCTREE}/proto/root_i386/lib/amd64 \
-L${SRCTREE}/proto/root_i386/usr/lib/amd64 \
-lzutil -lzfs_core -lzfs -luutil -lumem -lnvpair -lsec -lidmap -lc -lcmdutils

all: $(PROG)

$(PROG): $(OBJS)
	$(CC) -o $@ $^ $(LDFLAGS)

clean:
	$(RM) $(OBJS) $(PROG)
