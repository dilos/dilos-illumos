#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the "License").
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/OPENSOLARIS.LICENSE.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets "[]" replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#
#
# Copyright (c) 1994, 2010, Oracle and/or its affiliates. All rights reserved.
# Copyright 2016 Igor Kozhukhov <ikozhukhov@gmail.com>
#
# Makefile for name service cache daemon
#

PROG=		nscd
MANIFEST=	name-service-cache.xml
SVCMETHOD=	svc-nscd

#include ../Makefile.cmd
#include ../Makefile.cmd.64
#include ../Makefile.ctf

CC= gcc
CFLAGS+= -m32
CFLAGS+= -ggdb3 -O0 -D__EXTENSIONS__ -DTEXT_DOMAIN="\"\""
CFLAGS+= -W -Wall -Wextra -Werror -Wno-unknown-pragmas -std=c99

ROOTMANIFESTDIR=	$(ROOTSVCSYSTEM)

SRCS=	server.c getpw.c getgr.c gethost.c getnode.c \
	getether.c getrpc.c getproto.c getnet.c \
	getbootp.c getauth.c getserv.c \
	getnetmasks.c getprinter.c getproject.c \
	getexec.c getprof.c getuser.c cache.c \
	nscd_biggest.c nscd_wait.c \
	nscd_init.c nscd_access.c nscd_cfgfile.c nscd_config.c \
	nscd_dbimpl.c nscd_getentctx.c nscd_intaddr.c \
	nscd_log.c nscd_nswconfig.c nscd_nswstate.c nscd_nswcfgst.c \
	nscd_seqnum.c nscd_smfmonitor.c \
	nscd_switch.c nscd_nswparse.c nscd_initf.c nscd_selfcred.c \
	nscd_frontend.c nscd_admin.c nscd_door.c \
	gettnrhtp.c gettnrhdb.c

CLOBBERFILES=	nscd

OBJS=	$(SRCS:%.c=%.o)

CFLAGS +=	$(CCVERBOSE)
CPPFLAGS +=	-D_REENTRANT -DSUN_THREADS \
		-I../../lib/libc/port/gen -I../../lib/libc/inc \
		-I../../lib/libsldap/common
LINTFLAGS +=	-erroff=E_GLOBAL_COULD_BE_STATIC2
LINTFLAGS +=	-erroff=E_NAME_USED_NOT_DEF2
LINTFLAGS +=	-erroff=E_NAME_DEF_NOT_USED2

CERRWARN +=	-_gcc=-Wno-uninitialized

# nscd interposes on many symbols, and must export others for its own dlsym()
# use, and dlsym() calls from libc.  Itemizing the interfaces within a mapfile
# is error-prone, so establish the whole object as an interposer.
LDFLAGS +=	$(ZINTERPOSE)

# TCOV_FLAG=	-ql
# GPROF_FLAG=	-xpg
# DEBUG_FLAG=	-g

PROGLIBS=	$(LDLIBS) -lresolv -lumem -lscf -lavl

# install macros and rule
#
ROOTPROG=	$(ROOTUSRSBIN)/nscd

.KEEP_STATE:

all: $(PROG) $(NISPROG)

$(PROG): $(OBJS)
	$(LINK.c) $(OPT) -o $@ $(OBJS) $(PROGLIBS)
	$(POST_PROCESS)

lint:
	$(LINT.c) $(SRCS) $(PROGLIBS)

cstyle:
	$(CSTYLE) $(SRCS)

install: all $(ROOTPROG) $(ROOTMANIFEST) $(ROOTSVCMETHOD)

check:	$(CHKMANIFEST)

clean:
	$(RM) $(OBJS)

$(ROOTUSRSBIN)/%: %
	$(INS.file)

$(ROOTUSRLIB)/%: %
	$(INS.file)

#include ../Makefile.targ
