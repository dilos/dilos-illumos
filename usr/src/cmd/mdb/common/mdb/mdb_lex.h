/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License, Version 1.0 only
 * (the "License").  You may not use this file except in compliance
 * with the License.
 *
 * You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/OPENSOLARIS.LICENSE.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets "[]" replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/*
 * Copyright 2003 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_MDB_LEX_H
#define	_MDB_LEX_H

#include <mdb/mdb_argvec.h>
#include <mdb/mdb_nv.h>
#include <mdb/mdb_types.h>
#include <mdb/mdb_module.h>
#include <mdb/mdb_umem.h>
#include <stdio.h>

#include "mdb_grammar.h"

#ifdef	__cplusplus
extern "C" {
#endif

#ifdef _MDB

#ifndef YY_TYPEDEF_YY_SCANNER_T
#define YY_TYPEDEF_YY_SCANNER_T
typedef void* yyscan_t;
#endif

#ifndef YY_TYPEDEF_YY_SIZE_T
#define YY_TYPEDEF_YY_SIZE_T
typedef size_t yy_size_t;
#endif

struct mdb_frame;

typedef struct mdb_lex_state {
	yyscan_t scanner;
	yypstate *parser;
	YYSTYPE pushed_value;
	YYLTYPE pushed_loc;
	mdb_mblk_t *allocs;
	int eof_seen;
} mdb_lex_state_t;

extern void mdb_lex_debug(int);

void mdb_lex_state_create(struct mdb_frame *);
void mdb_lex_state_destroy(struct mdb_frame *);
void mdb_lex_state_set_yylineno(struct mdb_frame *, int);
int mdb_lex_state_get_yylineno(struct mdb_frame *);
void mdb_lex_state_yydiscard(struct mdb_frame *);
void mdb_lex_undo(struct mdb_frame *);
void mdb_lex_state_yyerror(struct mdb_frame *, const char *format, ...);
void mdb_lex_state_yyperror(struct mdb_frame *, const char *format, ...);
void mdb_lex_state_yyparse(struct mdb_frame *);
void mdb_yyerror(const char *format, ...);

void yyerror(YYLTYPE* locp, yyscan_t scanner, const char *format, ...);
void yyperror(YYLTYPE* locp, yyscan_t scanner, const char *format, ...);
int yyfprintf(FILE *stream, const char *format, ...);
YYLTYPE *yyget_lloc(yyscan_t scanner);

void *yyalloc(yy_size_t size, yyscan_t yyscanner);
void *yyrealloc(void *ptr, yy_size_t size, yyscan_t yyscanner);
void yyfree(void *ptr, yyscan_t yyscanner);

#define YYFPRINTF   yyfprintf

#endif /* _MDB */

#ifdef	__cplusplus
}
#endif

#endif	/* _MDB_LEX_H */
