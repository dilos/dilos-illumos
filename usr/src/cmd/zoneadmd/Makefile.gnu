PROG= zoneadmd

SRCTREE=../../../..
PROTO=$(SRCTREE)/proto/root_i386

SRCS= zoneadmd.c zcons.c zfd.c vplat.c log.c
OBJS= $(SRCS:%.c=%.o)

CC= gcc
CFLAGS+= -O0 -ggdb3 -nodefaultlibs -D__sun -D__dilos__ -D__dilos \
-Ui386 -U__i386 -std=gnu99 \
-DTEXT_DOMAIN=\"SUNW_OST_OSCMD\" -D_TS_ERRNO \
-I$(PROTO)/usr/include -I../../uts/common/fs/zfs

LDFLAGS+= -nodefaultlibs \
-L$(PROTO)/lib/amd64 -L$(PROTO)/usr/lib/amd64 \
-lzonecfg -ldevinfo -ldevice -lnvpair \
-lgen -lbsm -lcontract -lzfs -luuid -lbrand -ldladm \
-ltsnet -ltsol -linetutil -lscf -lppt -lc -lcustr

all: $(PROG)

$(PROG): $(OBJS)
	$(CC) -o $@ $^ $(LDFLAGS)

clean:
	rm -f $(PROG) $(OBJS)

install:
	pfexec install $(PROG) /usr/lib/zones