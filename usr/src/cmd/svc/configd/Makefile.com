#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the "License").
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/OPENSOLARIS.LICENSE.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets "[]" replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#
#
# Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#

MYPROG = svc.configd
MYOBJS = \
	backend.o \
	configd.o \
	client.o \
	file_object.o \
	maindoor.o \
	object.o \
	rc_node.o \
	snapshot.o

PROG = $(MYPROG)
OBJS = $(MYOBJS)

SRCS = $(MYOBJS:%.o=../%.c)

include ../../../Makefile.cmd
include ../../../Makefile.ctf

NATIVE_BUILD=$(POUND_SIGN)
$(NATIVE_BUILD)PROG = $(MYPROG:%=%-native)
$(NATIVE_BUILD)OBJS = $(MYOBJS:%.o=%-native.o)

#ROOTCMDDIR=	$(ROOT)/lib/svc/bin
#ROOTLIBSVCBIN32=	$(ROOT)/lib/svc/bin/$(MACH32)
#ROOTLIBSVCBIN64=	$(ROOT)/lib/svc/bin/$(MACH64)
ROOTLIBSVCBIN=		$(ROOT)/lib/svc/bin
SQLITE_ADAP =		../../../../lib/sqlite_adap

MYCPPFLAGS = -I.. -I../../common -I../../../../common/svc -D_REENTRANT
# for sys/ccompile.h
MYCPPFLAGS += -I$(ADJUNCT_PROTO)/usr/include
# sqlite adapter
MYCPPFLAGS += -I$(SQLITE_ADAP)
CPPFLAGS += $(MYCPPFLAGS)
$(NOT_RELEASE_BUILD)CPPFLAGS += -DDEBUG
$(RELEASE_BUILD)CPPFLAGS += -DNDEBUG
CFLAGS	+= $(CCVERBOSE)

CERRWARN += -_gcc=-Wno-parentheses
CERRWARN += -_gcc=-Wno-unused-label
CERRWARN += -_gcc=-Wno-unused-variable
CERRWARN += -_gcc=-Wno-unused-function
CERRWARN += $(CNOWARN_UNINIT)

# strange false positive
SMOFF += free

MYLDLIBS = -lumem -luutil
LDLIBS	+= -lsecdb -lbsm $(MYLDLIBS)
LINTFLAGS += -errtags -erroff=E_BAD_FORMAT_ARG_TYPE2 -erroff=E_NAME_DEF_NOT_USED2

CLOBBERFILES +=	$(MYPROG:%=%-native)

LIBUUTIL	= $(SRC)/lib/libuutil
LIBSCF		= $(SRC)/lib/libscf

SCRIPTFILE	= restore_repository
ROOTSCRIPTFILE	= $(ROOTCMDDIR)/$(SCRIPTFILE)

OBJSQLITE = sqlite_adap.o
LIBSQLITE = -lsqlite3
$(NATIVE_BUILD)CC =	$(NATIVECC)
$(NATIVE_BUILD)LD =	$(NATIVELD)
$(NATIVE_BUILD)CPPFLAGS = $(MYCPPFLAGS) -I$(LIBUUTIL)/common -I$(LIBSCF)/inc
$(NATIVE_BUILD)CPPFLAGS += -DNATIVE_BUILD
$(NATIVE_BUILD)LDFLAGS =
$(NATIVE_BUILD)LIBSQLITE =

DIRMODE = 0755
FILEMODE = 0555

OBJS += $(OBJSQLITE)
LDLIBS += $(LIBSQLITE)

CSTD = $(CSTD_GNU99)

.KEEP_STATE:
.PARALLEL: $(MYOBJS) $(MYOBJS:%.o=%-native.o)

all: $(PROG)

native32: FRC
	@cd $(LIBUUTIL)/native32; pwd; $(MAKE) $(MFLAGS) install
	@NATIVE_BUILD= $(MAKE) $(MFLAGS) all

native64: FRC
	@cd $(LIBUUTIL)/native64; pwd; $(MAKE) $(MFLAGS) install
	@NATIVE_BUILD= $(MAKE) $(MFLAGS) all

$(PROG): $(OBJS)
	$(LINK.c) -o $@ $(OBJS) $(LDLIBS)
	$(POST_PROCESS)

sqlite_adap.o: $(SQLITE_ADAP)/sqlite_adap.c
	$(COMPILE.c) -o $@ $(SQLITE_ADAP)/sqlite_adap.c
	$(POST_PROCESS_O)

%-native.o: ../%.c
	$(COMPILE.c) -o $@ $<
	$(POST_PROCESS_O)

%.o: ../%.c
	$(COMPILE.c) $<
	$(POST_PROCESS_O)

#$(ROOTCMDDIR)/%: %.sh
#	$(INS.rename)

#$(ROOTLIBSVCBIN32) $(ROOTLIBSVCBIN64):
#	$(INS.dir)

#$(ROOTLIBSVCBIN32)/%: $(ROOTLIBSVCBIN32) %
#	$(INS.file)

#$(ROOTLIBSVCBIN64)/%: $(ROOTLIBSVCBIN64) %
#	$(INS.file)

$(ROOTLIBSVCBIN)/%: $(ROOTLIBSVCBIN) %
	$(INS.file)

#install: all $(ROOTCMD) $(ROOTVARSADMFILE) $(ROOTSCRIPTFILE)

clean: FRC
	$(RM) $(MYOBJS) $(MYOBJS:%.o=%-native.o)

#clobber:

#lint:   lint_SRCS

#lint_SRCS:

include ../../../Makefile.targ

FRC:
