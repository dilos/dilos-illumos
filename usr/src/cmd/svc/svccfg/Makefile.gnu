PROG=svccfg

SRCTREE= ../../..
PROTO=$(SRCTREE)/../../proto/root_i386
VPATH+= ../common

SRCS= svccfg_main.c svccfg_engine.c svccfg_internal.c svccfg_libscf.c \
svccfg_tmpl.c svccfg_xml.c svccfg_help.c svccfg_grammar.c svccfg_lex.c \
manifest_find.c manifest_hash.c notify_params.c

OBJS= $(SRCS:.c=.o)

CC= gcc

CFLAGS+= -O0 -ggdb3 \
-D__sun -D__dilos__ -D__dilos -Ui386 -U__i386 \
-Wall -Wextra -std=gnu89 \
-DTEXT_DOMAIN=\"SUNW_OST_OSCMD\" -D_TS_ERRNO \
-I$(PROTO)/usr/include -I../common \
-I/usr/include/libxml2

LDFLAGS+= -L$(PROTO)/lib/amd64 -L$(PROTO)/usr/lib/amd64 \
-lxml2 -lscf -luutil -lumem -lmd5 -lnvpair -ltecla -lc

all: $(PROG)

$(PROG): $(OBJS)
	$(CC) -o $@ $^ $(LDFLAGS)

clean:
	$(RM) -f $(PROG) $(OBJS)
