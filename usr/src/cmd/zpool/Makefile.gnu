PROG= zpool

CC=gcc
VPATH=../stat/common
SRCTREE=../../../..
SRCS= zpool_main.c zpool_vdev.c zpool_iter.c zpool_util.c timestamp.c
OBJS= $(SRCS:%.c=%.o)

CFLAGS=-O0 -ggdb3 -nodefaultlibs -D__sun -D__dilos__ -D__dilos \
-Ui386 -U__i386 -std=gnu99 -DTEXT_DOMAIN=\"SUNW_OST_OSCMD\" -D_TS_ERRNO \
-I$(SRCTREE)/proto/root_i386/usr/include \
-D_LARGEFILE64_SOURCE=1 -D_REENTRANT \
-I$(SRCTREE)/usr/src/common/zfs \
-I$(SRCTREE)/usr/src/cmd/stat/common \
-I$(SRCTREE)/usr/src/lib/libc/inc \
-I$(SRCTREE)/usr/src/uts/common/fs/zfs \
-DDEBUG

LDFLAGS=-m64 -nodefaultlibs \
-L$(SRCTREE)/proto/root_i386/lib/amd64 \
-L$(SRCTREE)/proto/root_i386/usr/lib/amd64 \
-lzfs -lzutil -lnvpair -ldevid -lefi -ldiskmgt -luutil -lumem -lc


all: $(PROG)

$(PROG): $(OBJS)
	$(CC) -o $@ $^ $(LDFLAGS)


clean:
	rm -f $(PROG) $(OBJS)
