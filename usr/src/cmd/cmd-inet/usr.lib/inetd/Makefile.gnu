PROG= inetd

CC= gcc
CFLAGS+= -ggdb3 -O0
CFLAGS+= -I../../../../lib/librestart/common
CFLAGS+= -I../../../../lib/libinetutil/common
CFLAGS+= -I../../../../lib/libinetsvc/common
CFLAGS+= -I../../../../lib/libcontract/common
CFLAGS+= -I../../../../uts/common
CPPFLAGS+= -D_FILE_OFFSET_BITS=64 -I../../common -D_REENTRANT
LDFLAGS+= -L.
LDADD+= -lrestart -lscf -lcontract -linetutil -lwrap -linetsvc\
	-luutil -lumem -lbsm

SRCS= inetd.c tlx.c config.c util.c contracts.c repval.c wait.c env.c
OBJS= $(SRCS:.c=.o)

all: $(PROG)

$(PROG): $(OBJS)
	ln -sf /lib/64/libinetutil.so.1 libinetutil.so
	ln -sf /usr/lib/64/libinetsvc.so.1 libinetsvc.so
	$(CC) -o $@ $^ $(LDFLAGS) $(LDADD)
	rm -f libinetutil.so libinetsvc.so

clean:
	rm -f $(OBJS) $(PROG)
