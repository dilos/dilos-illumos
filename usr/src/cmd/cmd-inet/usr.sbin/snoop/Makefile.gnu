PROG=	snoop
SRCS=	nfs4_xdr.c snoop.c snoop_aarp.c snoop_adsp.c snoop_aecho.c \
	snoop_apple.c snoop_arp.c snoop_atp.c snoop_bparam.c \
	snoop_bpdu.c \
	snoop_capture.c snoop_dhcp.c snoop_dhcpv6.c snoop_display.c \
	snoop_dns.c snoop_ether.c \
	snoop_filter.c snoop_http.c snoop_icmp.c snoop_igmp.c snoop_ip.c \
	snoop_ipaddr.c snoop_ipsec.c snoop_isis.c \
	snoop_ldap.c snoop_mip.c snoop_mount.c \
	snoop_nbp.c snoop_netbios.c snoop_nfs.c snoop_nfs3.c snoop_nfs4.c \
	snoop_nfs_acl.c snoop_nis.c snoop_nlm.c snoop_ntp.c \
	snoop_pf.c snoop_ospf.c snoop_ospf6.c snoop_pmap.c snoop_ppp.c \
	snoop_pppoe.c snoop_rip.c snoop_rip6.c snoop_rpc.c snoop_rpcprint.c \
	snoop_rpcsec.c snoop_rport.c snoop_rquota.c snoop_rstat.c snoop_rtmp.c \
	snoop_sctp.c snoop_slp.c snoop_smb.c snoop_socks.c snoop_solarnet.c \
	snoop_tcp.c snoop_tftp.c snoop_trill.c snoop_udp.c snoop_vxlan.c \
	snoop_zip.c

CC= gcc
SRC= ../../../..
UTS= $(SRC)/uts
OBJS=	$(SRCS:.c=.o)
HDRS=	snoop.h snoop_mip.h at.h snoop_ospf.h snoop_ospf6.h

#BITS= -m64
CFLAGS+= $(BITS) -ggdb3 -O0 -W -Wall -Wextra
CPPFLAGS += -I. -I$(SRC)/common/net/dhcp -I$(UTS)/common\
	-I$(SRC)/lib/libdhcputil/common -I$(SRC)/head\
	-D_LARGEFILE_SOURCE -D_FILE_OFFSET_BITS=64
LDLIBS += -ldhcputil -ldlpi -ltsol
LDFLAGS += $(BITS) $(MAPFILE.NGB:%=-M%)
#CERRWARN += -_gcc=-Wno-switch
#CERRWARN += -_gcc=-Wno-implicit-function-declaration
#CERRWARN += -_gcc=-Wno-clobbered
#CERRWARN += -_gcc=-Wno-unused-value

.KEEP_STATE:

.PARALLEL: $(OBJS)

all:	$(PROG)

$(PROG): $(OBJS) $(MAPFILE.NGB)
	ln -sf /lib/64/libdhcputil.so.1 libdhcputil.so
	$(LINK.c) -o $@ $(OBJS) $(LDLIBS) -L.
	rm -f libdhcputil.so
	$(POST_PROCESS)

#install:	all $(ROOTUSRSBINPROG)

clean:
	$(RM) $(OBJS)

#lint:	lint_SRCS

#include	../../../Makefile.targ
