PROG= ifconfig

TOPSRC=../../../..
PROTODIR=$(TOPSRC)/../../proto/root_i386
COMMONDIR= $(TOPSRC)/common
VPATH= ../../common
SRCS= ifconfig.c revarp.c compat.c

CC= gcc
OBJS= $(SRCS:%.c=%.o)

CFLAGS+= -D__sun -D__dilos__ -D__dilos -O0 -ggdb3 -Ui386 -U__i386 -std=gnu89 \
-DTEXT_DOMAIN=\"SUNW_OST_OSCMD\" -D_TS_ERRNO \
-I$(PROTODIR)/usr/include -I../../common -I$(COMMONDIR)/net/dhcp \
-D_XOPEN_SOURCE=500 -D__EXTENSIONS__

LDFLAGS+= -nodefaultlibs -Wl,-I/lib/amd64/ld.so.1 \
-L$(PROTODIR)/lib/amd64 -L$(PROTODIR)/usr/lib/amd64 \
-ldhcpagent -ldlpi -linetutil -lipmp -ldladm -lipadm -lxnet -lc

all: $(PROG)

$(PROG): $(OBJS)
	$(CC) -o $@ $^ $(LDFLAGS)

clean:
	rm -f $(PROG) $(OBJS)

# -c 
# -c  
#-c /export/home/denis/projects/dilos/illumos/usr/src/cmd/cmd-inet/common/ 
#-fident -finline -fno-inline-functions -fno-builtin -fno-asm -fdiagnostics-show-option -nodefaultlibs -D__sun -D__dilos__ -D__dilos -fno-strict-aliasing -fno-unit-at-a-time -fno-optimize-sibling-calls -O2 -m64 -mtune=opteron -Ui386 -U__i386 -Wall -Wextra -Werror -Wno-missing-braces -Wno-sign-compare -Wno-unknown-pragmas -Wno-unused-parameter -Wno-missing-field-initializers -Wno-array-bounds -Wno-maybe-uninitialized -Wno-parentheses -std=gnu89 -fno-inline-small-functions -fno-inline-functions-called-once -fno-ipa-cp -fno-ipa-icf -fno-ipa-sra -fno-clone-functions -fno-reorder-functions -fno-aggressive-loop-optimizations -g -gdwarf-2 -std=gnu89 -msave-args -DTEXT_DOMAIN="SUNW_OST_OSCMD" -D_TS_ERRNO -I/export/home/denis/projects/dilos/illumos/proto/root_i386/usr/include -I/export/home/denis/projects/dilos/illumos/usr/src/cmd/cmd-inet/common -I/export/home/denis/projects/dilos/illumos/usr/src/common/net/dhcp -m64 -mtune=opteron -Wl,-Bdirect -Wl,-M/export/home/denis/projects/dilos/illumos/usr/src/common/mapfiles/common/map.noexstk -Wl,-M/export/home/denis/projects/dilos/illumos/usr/src/common/mapfiles/common/map.pagealign -Wl,-M/export/home/denis/projects/dilos/illumos/usr/src/common/mapfiles/common/map.noexdata -Wl,-I/lib/amd64/ld.so.1 ifconfig.o revarp.o compat.o -o ifconfig -L/export/home/denis/projects/dilos/illumos/proto/root_i386/lib/amd64 -L/export/home/denis/projects/dilos/illumos/proto/root_i386/usr/lib/amd64 -ldhcpagent -ldlpi -linetutil -lipmp -ldladm -lipadm -lxnet -lsocket -lnsl -lc 
