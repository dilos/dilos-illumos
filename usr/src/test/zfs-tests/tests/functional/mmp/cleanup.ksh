#!/bin/ksh -p
#
# CDDL HEADER START
#
# This file and its contents are supplied under the terms of the
# Common Development and Distribution License ("CDDL"), version 1.0.
# You may only use this file in accordance with the terms of version
# 1.0 of the CDDL.
#
# A full copy of the text of the CDDL should have accompanied this
# source.  A copy of the CDDL is also available via the Internet at
# http://www.illumos.org/license/CDDL.
#
# CDDL HEADER END
#

#
# Copyright (c) 2017 by Lawrence Livermore National Security, LLC.
# Copyright 2019 Joyent, Inc.
#

. $STF_SUITE/include/libtest.shlib
. $STF_SUITE/tests/functional/mmp/mmp.cfg
. $STF_SUITE/tests/functional/mmp/mmp.kshlib

verify_runnable "global"

if is_dilos; then
	if [[ -f /var/tmp/zfs_test_hostid.txt ]]; then
		h=$(cat /var/tmp/zfs_test_hostid.txt)
		log_must mmp_set_hostid $h
		log_must rm -f /var/tmp/zfs_test_hostid.txt
	fi
	if [[ ! -f $HOSTID_FILE ]]; then
		log_must /usr/sbin/svcadm restart system/hostid
	fi
fi
if is_linux; then
log_must set_tunable64 MULTIHOST_HISTORY $MMP_HISTORY_OFF
fi

log_pass "mmp cleanup passed"
