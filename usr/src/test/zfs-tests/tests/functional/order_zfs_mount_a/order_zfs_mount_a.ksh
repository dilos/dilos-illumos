#!/bin/ksh -p
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the "License").
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
# or https://opensource.org/licenses/CDDL-1.0.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/OPENSOLARIS.LICENSE.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets "[]" replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright (c) 2022 Argo Technologie East
#

. $STF_SUITE/include/libtest.shlib

# DESCRIPTION:
#       Verify that 'zfs mount -a' has correct mount point order.
#
# STRATEGY:
#       1. Create a group of pools with specified vdev.
#       2. Create zfs filesystems within the given pools.
#       4. Verify that 'test_mount_a' command succeed.
#

verify_runnable "both"

# pools
TPOOLS=(TESTPOOL1 TESTPOOL2 TESTPOOL3)
DISK_ARR=($DISKS)

# vols and mount pointers
# the create order must be correct
VOLS=(
"TESTPOOL1/vol11		/TESTPOOL1/vol11"
"TESTPOOL1/vol12		/TESTPOOL1/vol12"
"TESTPOOL1/vol11/vol111		/TESTPOOL1/vol11/vol111"
"TESTPOOL1/vol11/vol112		/TESTPOOL1/vol12/vol112"
"TESTPOOL2/vol21		/TESTPOOL2/vol21"
"TESTPOOL2/vol22		/TESTPOOL2/vol22"
"TESTPOOL2/vol123		/TESTPOOL1/vol123"
"TESTPOOL2/vol124		/TESTPOOL1/vol124"
"TESTPOOL3/vol1			/TESTPOOL3/vol1"
"TESTPOOL3/vol2			/TESTPOOL3/vol2"
"TESTPOOL3/vol3			/TESTPOOL3/vol3"
"TESTPOOL3/vol1/v11		/TESTPOOL3/vol1/v11"
"TESTPOOL3/vol1/v11/va		/TESTPOOL3/vol1/v11/va"
"TESTPOOL3/vol1/v11/vb		/TESTPOOL3/vol1/v11/vb"
"TESTPOOL3/vol1/v11/vc		/TESTPOOL3/vol1/v11/vc"
"TESTPOOL3/vol1/v11/vd		/TESTPOOL3/vol1/v11/vd"
"TESTPOOL3/vol1/v11/vd/vd1	/TESTPOOL3/vol1/v11/vd/vd1"
"TESTPOOL3/vol1/v11/vd/vd1/vd2	/TESTPOOL3/vol1/v11/va/vd1/vd2"
"TESTPOOL3/vol1/v11/vd/vd1/vd2/vd3	/TESTPOOL3/vol1/v11/va/vd1/vd2/vd3"
"TESTPOOL3/vol1/v11/vd/vd1/vd2/vd3/vd4	/TESTPOOL3/vol1/v11/va/vd1/vd2/vd3/vd4"
"TESTPOOL3/vol2/v21		/TESTPOOL3/vol2/v21"
"TESTPOOL3/vol3/v31		/TESTPOOL3/vol3/v31"
"TESTPOOL3/vol3/v32		/TESTPOOL3/vol3/v32"
"TESTPOOL3/vol3/v33		/TESTPOOL3/vol3/v33"
"TESTPOOL1/volA1		/TESTPOOL1/volA1"
"TESTPOOL1/volA2		/TESTPOOL1/volA2"
"TESTPOOL1/volA3		/TESTPOOL1/volA3"
"TESTPOOL1/volA4		/TESTPOOL1/volA4"
"TESTPOOL1/volA5		/TESTPOOL1/volA5"
"TESTPOOL1/volA6		/TESTPOOL1/volA6"
"TESTPOOL1/volA7		/TESTPOOL1/volA7"
"TESTPOOL1/volA8		/TESTPOOL1/volA8"
"TESTPOOL1/volA9		/TESTPOOL1/volA9"
"TESTPOOL1/volA10		/TESTPOOL1/volA10"
"TESTPOOL1/volA11		/TESTPOOL1/volA11"
"TESTPOOL1/volA12		/TESTPOOL1/volA12"
"TESTPOOL1/volA13		/TESTPOOL1/volA13"
"TESTPOOL1/volA14		/TESTPOOL1/volA14"
"TESTPOOL1/volA15		/TESTPOOL1/volA15"
"TESTPOOL1/volA16		/TESTPOOL1/volA16"
"TESTPOOL1/volA17		/TESTPOOL1/volA17"
"TESTPOOL1/volA18		/TESTPOOL1/volA18"
"TESTPOOL1/volA19		/TESTPOOL1/volA19"
"TESTPOOL1/volA20		/TESTPOOL1/volA20")

function create_vol
{
	typeset vol=$1
	typeset mp=$2

	log_must zfs create -u -o mountpoint=$mp $vol
}

function setup_all
{
	typeset i=0

	for i in "${!TPOOLS[@]}"; do
		typeset tpool=${TPOOLS[i]}
		typeset disk=${DISK_ARR[i]}

		log_must zpool create -f $tpool $disk
	done


	for i in "${!VOLS[@]}"; do
		create_vol ${VOLS[i]}
	done
}

function cleanup_all
{
	for i in "${!TPOOLS[@]}"; do
		typeset tpool=${TPOOLS[i]}
		log_must zpool destroy -f $tpool
	done
}

log_assert "Verify that oreder of mount points of 'zfs mount -a' is correct"

log_onexit cleanup_all

setup_all

log_must test_mount_a -vt "/TESTPOOL"

log_pass "Verify that oreder of mount points of 'zfs mount -a' is correct"
