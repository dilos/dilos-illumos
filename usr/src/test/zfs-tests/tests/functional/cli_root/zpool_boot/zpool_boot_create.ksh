#!/bin/ksh -p
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the "License").
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
# or https://opensource.org/licenses/CDDL-1.0.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/OPENSOLARIS.LICENSE.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets "[]" replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2023 DilOS Team
#

. $STF_SUITE/include/libtest.shlib
. $STF_SUITE/tests/functional/cli_root/zpool_create/zpool_create.shlib
. $STF_SUITE/tests/functional/cli_root/zpool_boot/zpool_boot.cfg

#
# STRATEGY:
# 1. Create an zpool with different reservers for boot.
# 2. Vedify slice.
# 3. Try to criate zpool with incorrect reserver sizes.

verify_runnable "global"

function cleanup
{
	# Clean up the pool created if we failed to abort.
	poolexists $TESTPOOL && destroy_pool $TESTPOOL
}

set -A good_sizes -- "" "-o bootsize=1Mb" "-o bootsize=2Mb"

# bad checks:
# bootsize == 100 - must multiply 256
# bootsize == 1K  - for UEFI the minimum is 1Mb
# double bootsize
set -A bad_sizes -- "-o bootsize=100" "-o bootsize=1K" \
	"-o bootsize=1Mb -o bootsize=2Mb"

log_assert "zpool create With -B and different boot_size"
log_onexit cleanup

poolexists $TESTPOOL && destroy_pool $TESTPOOL

typeset -i i=0
while (( i < ${#good_sizes[*]} )); do
	typeset prop="${good_sizes[i]}"

	log_must zpool create -f -B $prop $TESTPOOL $DISK0
	checkslice $TESTPOOL $DISK0 "s1"
	log_must destroy_pool $TESTPOOL
	i=$((i + 1))
done

i=0
while (( i < ${#bad_sizes[*]} )); do
	typeset prop="${bad_sizes[i]}"

	log_mustnot zpool create -f -B $prop $TESTPOOL $DISK0
	i=$((i + 1))
done

log_pass "zpool create With -B and different boot_size"
