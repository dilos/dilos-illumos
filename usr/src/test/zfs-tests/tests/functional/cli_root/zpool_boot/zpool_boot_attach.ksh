#!/bin/ksh -p
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the "License").
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
# or https://opensource.org/licenses/CDDL-1.0.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/OPENSOLARIS.LICENSE.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets "[]" replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2023 DilOS Team
#

. $STF_SUITE/include/libtest.shlib
. $STF_SUITE/tests/functional/cli_root/zpool_create/zpool_create.shlib
. $STF_SUITE/tests/functional/cli_root/zpool_boot/zpool_boot.cfg

#
# STRATEGY:
# 1. Create an zpool with reserver for boot
# 2. Attach disk
# 3. Verify slice
#

verify_runnable "global"

function cleanup
{
	# Clean up the pool created if we failed to abort.
	poolexists $TESTPOOL && destroy_pool $TESTPOOL
}

log_assert "zpool attach with bootable pool"
log_onexit cleanup

poolexists $TESTPOOL && destroy_pool $TESTPOOL

log_must zpool create -f -B $TESTPOOL $DISK0
zpool list -v $TESTPOOL
log_must zpool attach -f $TESTPOOL $DISK0 $DISK1

# Check slice, it must be 1

checkslice $TESTPOOL $DISK1 "s1"

log_must destroy_pool $TESTPOOL

log_pass "zpool attach with bootable pool"
