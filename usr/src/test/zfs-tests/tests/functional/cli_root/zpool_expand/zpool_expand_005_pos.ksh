#! /bin/ksh -p
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the "License").
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
# or https://opensource.org/licenses/CDDL-1.0.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/OPENSOLARIS.LICENSE.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets "[]" replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

. $STF_SUITE/include/libtest.shlib
. $STF_SUITE/tests/functional/cli_root/zpool_expand/zpool_expand.cfg

#
# DESCRIPTION:
# Once zpool expand after replace vdev 
#
#
# STRATEGY:
# 1) Create first vdev (loopback)
# 2) Create pool by using this device
# 3) Creane second bigger vdev (loopback)
# 4) Replace the first device by the second device
# 5) Expand
# 6) Check that the pool size was expanded
#

verify_runnable "global"

function cleanup
{
	poolexists $TESTPOOL1 && destroy_pool $TESTPOOL1

	if [[ -n $DEV1 ]]; then
		lofiadm -d /dev/dsk/${DEV1}p0 >/dev/null 2>&1
		log_must devfsadm -C
	fi

	if [[ -n $DEV2 ]]; then
		lofiadm -d /dev/dsk/${DEV2}p0 >/dev/null 2>&1
		log_must devfsadm -C
	fi

	rm -f $FILE_LO $FILE_RAW

	block_device_wait
}

log_onexit cleanup

log_assert "zpool can expand after replace a device"

log_note "Setting up loopback"
log_must truncate -s $org_size $FILE_LO
log_must truncate -s $exp_size $FILE_RAW

DEV1=$(lofiadm -la $FILE_LO|sed 's,.*/,,g;s,p0,,g')
DEV2=$(lofiadm -la $FILE_RAW|sed 's,.*/,,g;s,p0,,g')
log_must devfsadm -C

log_must zpool create -o ashift=12 -o cachefile=none $TESTPOOL1 $DEV1

typeset prev_size=$(get_pool_prop size $TESTPOOL1)
typeset zfs_prev_size=$(get_prop avail $TESTPOOL1)

log_must zpool replace $TESTPOOL1 $DEV1 $DEV2
log_must zpool wait $TESTPOOL1
log_must zpool online -e $TESTPOOL1 $DEV2
log_must zpool sync $TESTPOOL1

typeset expand_size=$(get_pool_prop size $TESTPOOL1)
typeset zfs_expand_size=$(get_prop avail $TESTPOOL1)

log_note "$TESTPOOL1 $type has previous size: $prev_size and " \
    "expanded size: $expand_size"

if ! [[ $zfs_expand_size -gt $zfs_prev_size ]]; then
	log_fail "pool $TESTPOOL1 is not xpanded after replace." \
	    "Previous size: $zfs_prev_size and expanded " \
	    "size: $zfs_expand_size"
fi

cleanup

log_pass "zpool can autoexpand if autoexpand=on after vdev expansion"
