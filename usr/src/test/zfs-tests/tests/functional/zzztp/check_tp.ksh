#!/bin/ksh -p
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the "License").
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
# or https://opensource.org/licenses/CDDL-1.0.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/OPENSOLARIS.LICENSE.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets "[]" replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

. $STF_SUITE/tests/functional/aaatp/tunable_parameters.cfg

#
# DESCRIPTION:
#       Check tunable parameters
#

log_assert "Check tunable parameters."

[[ -f $SAVED_TP_FILE ]] || log_fail "Tunable parameter file is not created."
before=$(cat $SAVED_TP_FILE)
rm -f $SAVED_TP_FILE

after=$(dump_tunables $EXCLUDE_TP_LIST)

log_note "Tunable parameters from $SAVED_TP_FILE"
log_note "$before"
log_note "Current tunable parameters"
log_note "$after"

[[ "$before" == "$after" ]] || log_fail "Tunable parameter is different."

log_pass "Check tunable parameters."
