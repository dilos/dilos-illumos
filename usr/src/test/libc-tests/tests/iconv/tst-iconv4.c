#include <errno.h>
#include <iconv.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "iconv_test.h"

void
do_test_iconv4(void)
{
	iconv_t cd = iconv_open("ISO-8859-1", "UNICODE");
	if (cd == (iconv_t) -1) {
		FAIL("iconv_open failed: %d", errno);
	}

	const char instr[] = "a";
	const char *inptr = instr;
	size_t inlen = strlen (instr);
	char buf[200];
	char *outptr = buf;
	size_t outlen = sizeof (outptr);

	errno = 0;
	size_t n = iconv(cd, &inptr, &inlen, &outptr, &outlen);
	if (n != (size_t) -1) {
		FAIL("n (= %zu) != (size_t) -1", n);
	}
	if (errno != EINVAL) {
		FAIL("errno = %d, not EINVAL", errno);
	}

	iconv_close(cd);
	PASS();
}
