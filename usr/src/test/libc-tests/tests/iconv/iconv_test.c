/*
 * This file and its contents are supplied under the terms of the
 * Common Development and Distribution License ("CDDL"), version 1.0.
 * You may only use this file in accordance with the terms of version
 * 1.0 of the CDDL.
 *
 * A full copy of the text of the CDDL should have accompanied this
 * source.  A copy of the CDDL is also available via the Internet at
 * http://www.illumos.org/license/CDDL.
 */

/*
 * Copyright (c) 2022 DilOS
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <assert.h>

#include "iconv_test.h"

typedef void (*test)(void);

test iconv_tests[]={
	&do_test_iconv4,
	&do_test_iconv6,
	&do_test_loading
};

#define	ICONV_TESTS_SIZE	(sizeof (iconv_tests) / sizeof (iconv_tests[0]))

void
msg(const char *func, size_t err, const char *mess, ...)
{
	static const char *serr[] = {
	    [TEST_PASS] = "PASS",
	    [TEST_FAIL] = "FAIL",
	    [TEST_SKIP] = "SKIP"
	};

	va_list valist;

	assert(err < TEST_LAST);
	printf("%s: test in %s", serr[err], func);

	if (mess != NULL) {
		printf(": ");
		va_start(valist, mess);
		vprintf(mess, valist);
		va_end(valist);
	}

	printf("\n");

	if (err == TEST_FAIL)
		exit(1);
}

static void
do_tests(void)
{
	for (size_t i = 0; i < ICONV_TESTS_SIZE; i++)
		iconv_tests[i]();
}

int
main(void)
{
	do_tests();
	return (0);
}