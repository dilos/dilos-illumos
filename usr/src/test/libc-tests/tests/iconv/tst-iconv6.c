/* BZ #2569 */

#include <iconv.h>
#include <stdio.h>

#include "iconv_test.h"

void
do_test_iconv6(void)
{
	iconv_t cd0 = iconv_open("ISO-8859-7", "UTF-16LE");
	if (cd0 == (iconv_t) -1) {
		FAIL("first iconv_open failed");
	}
	iconv_t cd1 = iconv_open("ISO-8859-7", "UTF-16LE");
	if (cd1 == (iconv_t) -1) {
		FAIL("second iconv_open failed");
	}
	if (iconv_close(cd0) != 0) {
		FAIL("first iconv_close failed");
	}
	if (iconv_close(cd1) != 0) {
		FAIL("second iconv_close failed");
	}
	PASS();
}

