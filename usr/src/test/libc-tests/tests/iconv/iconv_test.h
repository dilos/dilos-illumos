/*
 * This file and its contents are supplied under the terms of the
 * Common Development and Distribution License ("CDDL"), version 1.0.
 * You may only use this file in accordance with the terms of version
 * 1.0 of the CDDL.
 *
 * A full copy of the text of the CDDL should have accompanied this
 * source.  A copy of the CDDL is also available via the Internet at
 * http://www.illumos.org/license/CDDL.
 */

/*
 * Copyright (c) 2022 DilOS
 */

#ifndef	_ICONV_TESTS_H
#define	_ICONV_TESTS_H

void do_test_iconv4(void);
void do_test_iconv6(void);
void do_test_loading(void);

enum test_result {TEST_PASS, TEST_FAIL, TEST_SKIP, TEST_LAST};

void msg(const char *func, size_t err, const char *message, ...);

#define	PASS()		msg(__FUNCTION__, TEST_PASS, NULL)
#define	FAIL(...)	msg(__FUNCTION__, TEST_FAIL, __VA_ARGS__)
#define	SKIP(...)	msg(__FUNCTION__, TEST_SKIP, __VA_ARGS__)

#endif /* _ICONV_TESTS_H */