/*
 * This file and its contents are supplied under the terms of the
 * Common Development and Distribution License ("CDDL"), version 1.0.
 * You may only use this file in accordance with the terms of version
 * 1.0 of the CDDL.
 *
 * A full copy of the text of the CDDL should have accompanied this
 * source.  A copy of the CDDL is also available via the Internet at
 * http://www.illumos.org/license/CDDL.
 */

/*
 * Copyright 2024 DilOS.
 */
#pragma GCC optimize("O2")

#include <sys/time.h>

#include <stdlib.h>
#include <stdio.h>

int
main(void)
{
	timespec_t	ts;
	struct tm	lt;
	time_t		t;

	clock_gettime(CLOCK_REALTIME, &ts);

	localtime_r(&ts.tv_sec, &lt);
	t = timegm(&lt);

	if (t < 0) {
		printf("FAIL\n");
		return (EXIT_FAILURE);
	}

	printf("PASS\n");
	return (EXIT_SUCCESS);
}
