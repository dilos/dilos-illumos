#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <err.h>
#include <stdlib.h>
#include <stdio.h>


#define THE_COOKIE ((void *) 0xdeadbeeful)

#define NOTE_CALL(coocke) \
	printf("'%s' called with cookie %#lx\n", __FUNCTION__, \
	    (unsigned long int) cookie)

#define CHECK_COOKIE(cookie) \
do { \
	if ((cookie) != THE_COOKIE) \
		errx(EXIT_FAILURE, "TEST FAILED: '%s' get incorect 'cookie'", \
		    __FUNCTION__); \
} while (0)

static int cookieread_called = 0;
static ssize_t
cookieread(void *cookie, char *buf, size_t count)
{
	(void) buf;

	NOTE_CALL(cookie);
	CHECK_COOKIE(cookie);
	cookieread_called = 1;

	return (count);
}

static int cookiewrite_called = 0;
static ssize_t
cookiewrite(void *cookie, const char *buf, size_t count)
{
	(void) buf;

	NOTE_CALL(cookie);
	CHECK_COOKIE(cookie);
	cookiewrite_called = 1;

	return (count);
}

static int cookieseek_called = 0;
static int
cookieseek(void *cookie, off_t *offset, int whence)
{
	(void) offset, (void) whence;

	NOTE_CALL(cookie);
	CHECK_COOKIE(cookie);
	cookieseek_called = 1;

	return (0);
}

static int cookieclose_called = 0;
static int
cookieclose(void *cookie)
{
	NOTE_CALL(cookie);
	CHECK_COOKIE(cookie);
	cookieclose_called = 1;

	return (0);
}

int
main(void)
{
	char buf[1];

	cookie_io_functions_t fcts = {
		.read = cookieread,
		.seek = cookieseek,
		.close = cookieclose,
		.write = cookiewrite
	};

	FILE *f = fopencookie(THE_COOKIE, "r+", fcts);
	if (f == NULL)
		errx(EXIT_FAILURE, "TEST FAILED: fopencookie is failed: %d", errno);

	fread(buf, 1, 1, f);
	fwrite(buf, 1, 1, f);
	fflush(f);
	fseek(f, 0, SEEK_CUR);
	fclose(f);

	if (cookieread_called == 0)
		err(EXIT_FAILURE, "TEST FAILED: cookieread was not called");

	if (cookiewrite_called == 0)
		err(EXIT_FAILURE, "TEST FAILED: cookiewrite was not called");

	if (cookieseek_called == 0)
		err(EXIT_FAILURE, "TEST FAILED: cookieseek was not called");

	if (cookieclose_called == 0)
		err(EXIT_FAILURE, "TEST FAILED: cookieclose was not called");

	return (0);
}
