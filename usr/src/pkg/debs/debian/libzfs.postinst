#!/bin/bash

# postinst script for libzfs
#
# see: dh_installdeb(1)

#set -e

# summary of how this script can be called:
#        * <postinst> \`configure' <most-recently-configured-version>
#        * <old-postinst> \`abort-upgrade' <new version>
#        * <conflictor's-postinst> \`abort-remove' \`in-favour' <package>
#          <new-version>
#        * <postinst> \`abort-remove'
#        * <deconfigured's-postinst> \`abort-deconfigure' \`in-favour'
#          <failed-install-package> <version> \`removing'
#          <conflicting-package> <version>
# for details, see http://www.debian.org/doc/debian-policy/ or
# the debian-policy package

PATH=/usr/bin:/sbin:/usr/sbin:/usr/local/bin:/usr/local/sbin
export PATH

if [ "${BASEDIR:=/}" = "/" ]; then
    BASEDIR=""
else
    BASEDIR_OPT="-b $BASEDIR"
fi

case "$1" in
    configure)
        chown root:bin $BASEDIR/usr/lib/zfs
	chown root:bin $BASEDIR/usr/share/zfs
	chown root:bin $BASEDIR/usr/share/zfs/compatibility.d
	chown root:sys $BASEDIR/etc/fs/zfs
	chown root:sys $BASEDIR/etc/zfs
	chown root:sys $BASEDIR/usr/lib/fs/zfs
	chmod 0555 "$BASEDIR/sbin/zfs"
	chown root:bin "$BASEDIR/sbin/zfs"
	chmod 0555 "$BASEDIR/sbin/zpool"
	chown root:bin "$BASEDIR/sbin/zpool"
	chmod 0555 "$BASEDIR/usr/lib/fs/zfs/fstyp.so.1"
	chown root:bin "$BASEDIR/usr/lib/fs/zfs/fstyp.so.1"
	chmod 0555 "$BASEDIR/usr/sbin/zdb"
	chown root:bin "$BASEDIR/usr/sbin/zdb"
	chmod 0555 "$BASEDIR/usr/sbin/zstream"
	chown root:bin "$BASEDIR/usr/sbin/zstream"
	chmod 0555 "$BASEDIR/usr/sbin/zstreamdump"
	chown root:bin "$BASEDIR/usr/sbin/zstreamdump"
	chmod 0644 "$BASEDIR/usr/share/zfs/compatibility.d/compat-2018"
	chown root:bin "$BASEDIR/usr/share/zfs/compatibility.d/compat-2018"
	chmod 0644 "$BASEDIR/usr/share/zfs/compatibility.d/compat-2019"
	chown root:bin "$BASEDIR/usr/share/zfs/compatibility.d/compat-2019"
	chmod 0644 "$BASEDIR/usr/share/zfs/compatibility.d/compat-2020"
	chown root:bin "$BASEDIR/usr/share/zfs/compatibility.d/compat-2020"
	chmod 0644 "$BASEDIR/usr/share/zfs/compatibility.d/compat-2021"
	chown root:bin "$BASEDIR/usr/share/zfs/compatibility.d/compat-2021"
	chmod 0644 "$BASEDIR/usr/share/zfs/compatibility.d/freebsd-11.0"
	chown root:bin "$BASEDIR/usr/share/zfs/compatibility.d/freebsd-11.0"
	chmod 0644 "$BASEDIR/usr/share/zfs/compatibility.d/freebsd-11.2"
	chown root:bin "$BASEDIR/usr/share/zfs/compatibility.d/freebsd-11.2"
	chmod 0644 "$BASEDIR/usr/share/zfs/compatibility.d/freebsd-11.3"
	chown root:bin "$BASEDIR/usr/share/zfs/compatibility.d/freebsd-11.3"
	chmod 0644 "$BASEDIR/usr/share/zfs/compatibility.d/freenas-9.10.2"
	chown root:bin "$BASEDIR/usr/share/zfs/compatibility.d/freenas-9.10.2"
	chmod 0644 "$BASEDIR/usr/share/zfs/compatibility.d/grub2"
	chown root:bin "$BASEDIR/usr/share/zfs/compatibility.d/grub2"
	chmod 0644 "$BASEDIR/usr/share/zfs/compatibility.d/openzfs-2.0-freebsd"
	chown root:bin "$BASEDIR/usr/share/zfs/compatibility.d/openzfs-2.0-freebsd"
	chmod 0644 "$BASEDIR/usr/share/zfs/compatibility.d/openzfs-2.0-linux"
	chown root:bin "$BASEDIR/usr/share/zfs/compatibility.d/openzfs-2.0-linux"
	chmod 0644 "$BASEDIR/usr/share/zfs/compatibility.d/openzfs-2.1-freebsd"
	chown root:bin "$BASEDIR/usr/share/zfs/compatibility.d/openzfs-2.1-freebsd"
	chmod 0644 "$BASEDIR/usr/share/zfs/compatibility.d/openzfs-2.1-linux"
	chown root:bin "$BASEDIR/usr/share/zfs/compatibility.d/openzfs-2.1-linux"
	chmod 0644 "$BASEDIR/usr/share/zfs/compatibility.d/openzfsonosx-1.7.0"
	chown root:bin "$BASEDIR/usr/share/zfs/compatibility.d/openzfsonosx-1.7.0"
	chmod 0644 "$BASEDIR/usr/share/zfs/compatibility.d/openzfsonosx-1.8.1"
	chown root:bin "$BASEDIR/usr/share/zfs/compatibility.d/openzfsonosx-1.8.1"
	chmod 0644 "$BASEDIR/usr/share/zfs/compatibility.d/openzfsonosx-1.9.3"
	chown root:bin "$BASEDIR/usr/share/zfs/compatibility.d/openzfsonosx-1.9.3"
	chmod 0644 "$BASEDIR/usr/share/zfs/compatibility.d/zol-0.6.1"
	chown root:bin "$BASEDIR/usr/share/zfs/compatibility.d/zol-0.6.1"
	chmod 0644 "$BASEDIR/usr/share/zfs/compatibility.d/zol-0.6.4"
	chown root:bin "$BASEDIR/usr/share/zfs/compatibility.d/zol-0.6.4"
	chmod 0644 "$BASEDIR/usr/share/zfs/compatibility.d/zol-0.6.5"
	chown root:bin "$BASEDIR/usr/share/zfs/compatibility.d/zol-0.6.5"
	chmod 0644 "$BASEDIR/usr/share/zfs/compatibility.d/zol-0.7"
	chown root:bin "$BASEDIR/usr/share/zfs/compatibility.d/zol-0.7"
	chmod 0644 "$BASEDIR/usr/share/zfs/compatibility.d/zol-0.8"
	chown root:bin "$BASEDIR/usr/share/zfs/compatibility.d/zol-0.8"
	chmod 0755 "$BASEDIR/lib/amd64/libzdoor.so.1"
	chown root:bin "$BASEDIR/lib/amd64/libzdoor.so.1"
	chmod 0755 "$BASEDIR/lib/amd64/libzfs.so.1"
	chown root:bin "$BASEDIR/lib/amd64/libzfs.so.1"
	chmod 0755 "$BASEDIR/lib/amd64/libzfs_core.so.1"
	chown root:bin "$BASEDIR/lib/amd64/libzfs_core.so.1"
	chmod 0755 "$BASEDIR/lib/amd64/libzutil.so.1"
	chown root:bin "$BASEDIR/lib/amd64/libzutil.so.1"
	chmod 0755 "$BASEDIR/usr/lib/amd64/libzfsbootenv.so.1"
	chown root:bin "$BASEDIR/usr/lib/amd64/libzfsbootenv.so.1"
	chmod 0755 "$BASEDIR/usr/lib/amd64/libzpool.so.1"
	chown root:bin "$BASEDIR/usr/lib/amd64/libzpool.so.1"
    ;;

    abort-upgrade|abort-remove|abort-deconfigure)
    ;;

    *)
        echo "postinst called with unknown argument '$1'" >&2
        exit 1
    ;;
esac



# dh_installdeb will replace this with shell code automatically
# generated by other debhelper scripts.

#DEBHELPER#

exit 0

