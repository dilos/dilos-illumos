#!/bin/bash

# postinst script for driver-usb
#
# see: dh_installdeb(1)

#set -e

# summary of how this script can be called:
#        * <postinst> \`configure' <most-recently-configured-version>
#        * <old-postinst> \`abort-upgrade' <new version>
#        * <conflictor's-postinst> \`abort-remove' \`in-favour' <package>
#          <new-version>
#        * <postinst> \`abort-remove'
#        * <deconfigured's-postinst> \`abort-deconfigure' \`in-favour'
#          <failed-install-package> <version> \`removing'
#          <conflicting-package> <version>
# for details, see http://www.debian.org/doc/debian-policy/ or
# the debian-policy package

PATH=/usr/bin:/sbin:/usr/sbin:/usr/local/bin:/usr/local/sbin
export PATH

if [ "${BASEDIR:=/}" = "/" ]; then
    BASEDIR=""
else
    BASEDIR_OPT="-b $BASEDIR"
fi

case "$1" in
    configure)
	chown root:bin $BASEDIR/usr/lib/xhci
	chown root:sys $BASEDIR/etc/usb
	if [ "$ZONEINST" = "1" ]; then
	    [ -f $BASEDIR/kernel/kmdb/amd64/xhci ] && rm -f $BASEDIR/kernel/kmdb/amd64/xhci
	else
	    chmod 0555 "$BASEDIR/kernel/kmdb/amd64/xhci"
	    chown root:sys "$BASEDIR/kernel/kmdb/amd64/xhci"
	fi
	chmod 0555 "$BASEDIR/usr/lib/mdb/kvm/amd64/xhci.so"
	chown root:sys "$BASEDIR/usr/lib/mdb/kvm/amd64/xhci.so"
	if [ "$ZONEINST" = "1" ]; then
	    [ -f $BASEDIR/usr/lib/xhci/xhci_portsc ] && rm -f $BASEDIR/usr/lib/xhci/xhci_portsc
	else
	    chmod 0555 "$BASEDIR/usr/lib/xhci/xhci_portsc"
	    chown root:sys "$BASEDIR/usr/lib/xhci/xhci_portsc"
	fi
	if [ "$ZONEINST" = "1" ]; then
	    [ -f $BASEDIR/etc/usb/config_map.conf ] && rm -f $BASEDIR/etc/usb/config_map.conf
	else
	    [ -f $BASEDIR/etc/usb/config_map.conf ] || cp -ax $BASEDIR/usr/share/doc/driver-usb/examples/config_map.conf $BASEDIR/etc/usb/config_map.conf
	    chmod 0644 "$BASEDIR/etc/usb/config_map.conf"
	    chown root:sys "$BASEDIR/etc/usb/config_map.conf"
	fi
	if [ "$ZONEINST" = "1" ]; then
	    [ -f $BASEDIR/kernel/drv/scsa2usb.conf ] && rm -f $BASEDIR/kernel/drv/scsa2usb.conf
	else
	    [ -f $BASEDIR/kernel/drv/scsa2usb.conf ] || cp -ax $BASEDIR/usr/share/doc/driver-usb/examples/scsa2usb.conf $BASEDIR/kernel/drv/scsa2usb.conf
	    chmod 0644 "$BASEDIR/kernel/drv/scsa2usb.conf"
	    chown root:sys "$BASEDIR/kernel/drv/scsa2usb.conf"
	fi
	if [ "$ZONEINST" = "1" ]; then
	    [ -f $BASEDIR/kernel/drv/ehci.conf ] && rm -f $BASEDIR/kernel/drv/ehci.conf
	else
	    chmod 0644 "$BASEDIR/kernel/drv/ehci.conf"
	    chown root:sys "$BASEDIR/kernel/drv/ehci.conf"
	fi
	if [ "$ZONEINST" = "1" ]; then
	    [ -f $BASEDIR/kernel/drv/ohci.conf ] && rm -f $BASEDIR/kernel/drv/ohci.conf
	else
	    chmod 0644 "$BASEDIR/kernel/drv/ohci.conf"
	    chown root:sys "$BASEDIR/kernel/drv/ohci.conf"
	fi
	if [ "$ZONEINST" = "1" ]; then
	    [ -f $BASEDIR/kernel/drv/uhci.conf ] && rm -f $BASEDIR/kernel/drv/uhci.conf
	else
	    chmod 0644 "$BASEDIR/kernel/drv/uhci.conf"
	    chown root:sys "$BASEDIR/kernel/drv/uhci.conf"
	fi
	if [ "$ZONEINST" = "1" ]; then
	    [ -f $BASEDIR/kernel/drv/usb_ac.conf ] && rm -f $BASEDIR/kernel/drv/usb_ac.conf
	else
	    chmod 0644 "$BASEDIR/kernel/drv/usb_ac.conf"
	    chown root:sys "$BASEDIR/kernel/drv/usb_ac.conf"
	fi
	if [ "$ZONEINST" = "1" ]; then
	    [ -f $BASEDIR/kernel/drv/xhci.conf ] && rm -f $BASEDIR/kernel/drv/xhci.conf
	else
	    chmod 0644 "$BASEDIR/kernel/drv/xhci.conf"
	    chown root:sys "$BASEDIR/kernel/drv/xhci.conf"
	fi
	if [ "$ZONEINST" = "1" ]; then
	    [ -f $BASEDIR/kernel/drv/amd64/ehci ] && rm -f $BASEDIR/kernel/drv/amd64/ehci
	else
	    chmod 0755 "$BASEDIR/kernel/drv/amd64/ehci"
	    chown root:sys "$BASEDIR/kernel/drv/amd64/ehci"
	fi
	if [ "$ZONEINST" = "1" ]; then
	    [ -f $BASEDIR/kernel/drv/amd64/hid ] && rm -f $BASEDIR/kernel/drv/amd64/hid
	else
	    chmod 0755 "$BASEDIR/kernel/drv/amd64/hid"
	    chown root:sys "$BASEDIR/kernel/drv/amd64/hid"
	fi
	if [ "$ZONEINST" = "1" ]; then
	    [ -f $BASEDIR/kernel/drv/amd64/hubd ] && rm -f $BASEDIR/kernel/drv/amd64/hubd
	else
	    chmod 0755 "$BASEDIR/kernel/drv/amd64/hubd"
	    chown root:sys "$BASEDIR/kernel/drv/amd64/hubd"
	fi
	if [ "$ZONEINST" = "1" ]; then
	    [ -f $BASEDIR/kernel/drv/amd64/ohci ] && rm -f $BASEDIR/kernel/drv/amd64/ohci
	else
	    chmod 0755 "$BASEDIR/kernel/drv/amd64/ohci"
	    chown root:sys "$BASEDIR/kernel/drv/amd64/ohci"
	fi
	if [ "$ZONEINST" = "1" ]; then
	    [ -f $BASEDIR/kernel/drv/amd64/scsa2usb ] && rm -f $BASEDIR/kernel/drv/amd64/scsa2usb
	else
	    chmod 0755 "$BASEDIR/kernel/drv/amd64/scsa2usb"
	    chown root:sys "$BASEDIR/kernel/drv/amd64/scsa2usb"
	fi
	if [ "$ZONEINST" = "1" ]; then
	    [ -f $BASEDIR/kernel/drv/amd64/uhci ] && rm -f $BASEDIR/kernel/drv/amd64/uhci
	else
	    chmod 0755 "$BASEDIR/kernel/drv/amd64/uhci"
	    chown root:sys "$BASEDIR/kernel/drv/amd64/uhci"
	fi
	if [ "$ZONEINST" = "1" ]; then
	    [ -f $BASEDIR/kernel/drv/amd64/usb_ac ] && rm -f $BASEDIR/kernel/drv/amd64/usb_ac
	else
	    chmod 0755 "$BASEDIR/kernel/drv/amd64/usb_ac"
	    chown root:sys "$BASEDIR/kernel/drv/amd64/usb_ac"
	fi
	if [ "$ZONEINST" = "1" ]; then
	    [ -f $BASEDIR/kernel/drv/amd64/usb_as ] && rm -f $BASEDIR/kernel/drv/amd64/usb_as
	else
	    chmod 0755 "$BASEDIR/kernel/drv/amd64/usb_as"
	    chown root:sys "$BASEDIR/kernel/drv/amd64/usb_as"
	fi
	if [ "$ZONEINST" = "1" ]; then
	    [ -f $BASEDIR/kernel/drv/amd64/usb_ia ] && rm -f $BASEDIR/kernel/drv/amd64/usb_ia
	else
	    chmod 0755 "$BASEDIR/kernel/drv/amd64/usb_ia"
	    chown root:sys "$BASEDIR/kernel/drv/amd64/usb_ia"
	fi
	if [ "$ZONEINST" = "1" ]; then
	    [ -f $BASEDIR/kernel/drv/amd64/usb_mid ] && rm -f $BASEDIR/kernel/drv/amd64/usb_mid
	else
	    chmod 0755 "$BASEDIR/kernel/drv/amd64/usb_mid"
	    chown root:sys "$BASEDIR/kernel/drv/amd64/usb_mid"
	fi
	if [ "$ZONEINST" = "1" ]; then
	    [ -f $BASEDIR/kernel/drv/amd64/usbprn ] && rm -f $BASEDIR/kernel/drv/amd64/usbprn
	else
	    chmod 0755 "$BASEDIR/kernel/drv/amd64/usbprn"
	    chown root:sys "$BASEDIR/kernel/drv/amd64/usbprn"
	fi
	if [ "$ZONEINST" = "1" ]; then
	    [ -f $BASEDIR/kernel/drv/amd64/xhci ] && rm -f $BASEDIR/kernel/drv/amd64/xhci
	else
	    chmod 0755 "$BASEDIR/kernel/drv/amd64/xhci"
	    chown root:sys "$BASEDIR/kernel/drv/amd64/xhci"
	fi
	if [ "$ZONEINST" = "1" ]; then
	    [ -f $BASEDIR/kernel/misc/amd64/hidparser ] && rm -f $BASEDIR/kernel/misc/amd64/hidparser
	else
	    chmod 0755 "$BASEDIR/kernel/misc/amd64/hidparser"
	    chown root:sys "$BASEDIR/kernel/misc/amd64/hidparser"
	fi
	if [ "$ZONEINST" = "1" ]; then
	    [ -f $BASEDIR/kernel/misc/amd64/usba ] && rm -f $BASEDIR/kernel/misc/amd64/usba
	else
	    chmod 0755 "$BASEDIR/kernel/misc/amd64/usba"
	    chown root:sys "$BASEDIR/kernel/misc/amd64/usba"
	fi
	if [ "$ZONEINST" = "1" ]; then
	    [ -f $BASEDIR/kernel/misc/amd64/usba10 ] && rm -f $BASEDIR/kernel/misc/amd64/usba10
	else
	    chmod 0755 "$BASEDIR/kernel/misc/amd64/usba10"
	    chown root:sys "$BASEDIR/kernel/misc/amd64/usba10"
	fi
	if [ "$ZONEINST" = "1" ]; then
	    [ -f $BASEDIR/kernel/strmod/amd64/usb_ah ] && rm -f $BASEDIR/kernel/strmod/amd64/usb_ah
	else
	    chmod 0755 "$BASEDIR/kernel/strmod/amd64/usb_ah"
	    chown root:sys "$BASEDIR/kernel/strmod/amd64/usb_ah"
	fi
	if [ "$ZONEINST" = "1" ]; then
	    [ -f $BASEDIR/kernel/strmod/amd64/usbkbm ] && rm -f $BASEDIR/kernel/strmod/amd64/usbkbm
	else
	    chmod 0755 "$BASEDIR/kernel/strmod/amd64/usbkbm"
	    chown root:sys "$BASEDIR/kernel/strmod/amd64/usbkbm"
	fi
	if [ "$ZONEINST" = "1" ]; then
	    [ -f $BASEDIR/kernel/strmod/amd64/usbms ] && rm -f $BASEDIR/kernel/strmod/amd64/usbms
	else
	    chmod 0755 "$BASEDIR/kernel/strmod/amd64/usbms"
	    chown root:sys "$BASEDIR/kernel/strmod/amd64/usbms"
	fi
	if [ "$ZONEINST" = "1" ]; then
	    [ -f $BASEDIR/kernel/strmod/amd64/usbwcm ] && rm -f $BASEDIR/kernel/strmod/amd64/usbwcm
	else
	    chmod 0755 "$BASEDIR/kernel/strmod/amd64/usbwcm"
	    chown root:sys "$BASEDIR/kernel/strmod/amd64/usbwcm"
	fi
	[ "$ZONEINST" = "1" ] || (grep -c "^uhci " $BASEDIR/etc/name_to_major >/dev/null || ( add_drv -n  $BASEDIR_OPT  -i '"pciclass,0c0300"' -m '* 0644 root sys' uhci ) )
	[ "$ZONEINST" = "1" ] || (grep -c "^ohci " $BASEDIR/etc/name_to_major >/dev/null || ( add_drv -n  $BASEDIR_OPT  -i '"pciclass,0c0310"' -m '* 0644 root sys' ohci ) )
	[ "$ZONEINST" = "1" ] || (grep -c "^ehci " $BASEDIR/etc/name_to_major >/dev/null || ( add_drv -n  $BASEDIR_OPT  -i '"pciclass,0c0320"' -m '* 0644 root sys' ehci ) )
	[ "$ZONEINST" = "1" ] || (grep -c "^xhci " $BASEDIR/etc/name_to_major >/dev/null || ( add_drv -n  $BASEDIR_OPT  -i '"pciclass,0c0330"' -m '* 0644 root sys' xhci ) )
	[ "$ZONEINST" = "1" ] || (grep -c "^usb_mid " $BASEDIR/etc/name_to_major >/dev/null || ( add_drv -n  $BASEDIR_OPT  -i '"usb,device"' usb_mid ) )
	[ "$ZONEINST" = "1" ] || (grep -c "^usb_ia " $BASEDIR/etc/name_to_major >/dev/null || ( add_drv -n  $BASEDIR_OPT  -i '"usb,ia"' usb_ia ) )
	[ "$ZONEINST" = "1" ] || (grep -c "^scsa2usb " $BASEDIR/etc/name_to_major >/dev/null || ( add_drv -n  $BASEDIR_OPT  -i '"usb584,222" "usbif,class8"' scsa2usb ) )
	[ "$ZONEINST" = "1" ] || (grep -c "^usb_ac " $BASEDIR/etc/name_to_major >/dev/null || ( add_drv -n  $BASEDIR_OPT  -i '"usbif,class1.1"' -m '* 0600 root sys' usb_ac ) )
	[ "$ZONEINST" = "1" ] || (grep -c "^usb_as " $BASEDIR/etc/name_to_major >/dev/null || ( add_drv -n  $BASEDIR_OPT  -i '"usbif,class1.2"' -m '* 0600 root sys' usb_as ) )
	[ "$ZONEINST" = "1" ] || (grep -c "^hid " $BASEDIR/etc/name_to_major >/dev/null || ( add_drv -n  $BASEDIR_OPT  -i '"usbif,class3"' -m '* 0600 root sys' hid ) )
	[ "$ZONEINST" = "1" ] || (grep -c "^usbprn " $BASEDIR/etc/name_to_major >/dev/null || ( add_drv -n  $BASEDIR_OPT  -i '"usbif,class7.1"' -m '* 0666 root sys' usbprn ) )
	[ "$ZONEINST" = "1" ] || (grep -c "^hubd " $BASEDIR/etc/name_to_major >/dev/null || ( add_drv -n  $BASEDIR_OPT  -i '"usbif,class9"' -m '* 0644 root sys' hubd ) )
	
    ;;

    abort-upgrade|abort-remove|abort-deconfigure)
    ;;

    *)
        echo "postinst called with unknown argument '$1'" >&2
        exit 1
    ;;
esac



# dh_installdeb will replace this with shell code automatically
# generated by other debhelper scripts.

#DEBHELPER#

exit 0

