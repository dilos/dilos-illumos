#
# This file and its contents are supplied under the terms of the
# Common Development and Distribution License ("CDDL"), version 1.0.
# You may only use this file in accordance with the terms of version
# 1.0 of the CDDL.
#
# A full copy of the text of the CDDL should have accompanied this
# source.  A copy of the CDDL is also available via the Internet
# at http://www.illumos.org/license/CDDL.
#

#
# Copyright 2011, Richard Lowe
# Copyright 2018 Nexenta Systems, Inc.
# Copyright 2021 OmniOS Community Edition (OmniOSce) Association.
#

file path=usr/share/man/man1/allocate.1
file path=usr/share/man/man1/amt.1
file path=usr/share/man/man1/asa.1
file path=usr/share/man/man1/auths.1
file path=usr/share/man/man1/bdiff.1
file path=usr/share/man/man1/ckdate.1
file path=usr/share/man/man1/ckgid.1
file path=usr/share/man/man1/ckint.1
file path=usr/share/man/man1/ckitem.1
file path=usr/share/man/man1/ckkeywd.1
file path=usr/share/man/man1/ckpath.1
file path=usr/share/man/man1/ckrange.1
file path=usr/share/man/man1/ckstr.1
file path=usr/share/man/man1/cktime.1
file path=usr/share/man/man1/ckuid.1
file path=usr/share/man/man1/ckyorn.1
file path=usr/share/man/man1/crle.1
file path=usr/share/man/man1/crypt.1
file path=usr/share/man/man1/ctrun.1
file path=usr/share/man/man1/ctstat.1
file path=usr/share/man/man1/ctwatch.1
file path=usr/share/man/man1/deallocate.1
link path=usr/share/man/man1/decrypt.1 target=encrypt.1
file path=usr/share/man/man1/dhcpinfo.1
file path=usr/share/man/man1/digest.1
file path=usr/share/man/man1/disown.1
file path=usr/share/man/man1/dispgid.1
file path=usr/share/man/man1/dispuid.1
file path=usr/share/man/man1/dumpcs.1
link path=usr/share/man/man1/dumpkeys.1 target=loadkeys.1
file path=usr/share/man/man1/eject.1
file path=usr/share/man/man1/encrypt.1
link path=usr/share/man/man1/errange.1 target=ckrange.1
link path=usr/share/man/man1/errdate.1 target=ckdate.1
link path=usr/share/man/man1/errgid.1 target=ckgid.1
link path=usr/share/man/man1/errint.1 target=ckint.1
link path=usr/share/man/man1/erritem.1 target=ckitem.1
link path=usr/share/man/man1/errpath.1 target=ckpath.1
link path=usr/share/man/man1/errstr.1 target=ckstr.1
link path=usr/share/man/man1/errtime.1 target=cktime.1
link path=usr/share/man/man1/erruid.1 target=ckuid.1
link path=usr/share/man/man1/erryorn.1 target=ckyorn.1
link path=usr/share/man/man1/eval.1 target=exec.1
file path=usr/share/man/man1/exec.1
file path=usr/share/man/man1/exit.1
file path=usr/share/man/man1/fmtmsg.1
#file path=usr/share/man/man1/fold.1
file path=usr/share/man/man1/getfacl.1
file path=usr/share/man/man1/getoptcvt.1
file path=usr/share/man/man1/glob.1
file path=usr/share/man/man1/hash.1
link path=usr/share/man/man1/hashstat.1 target=hash.1
link path=usr/share/man/man1/helpdate.1 target=ckdate.1
link path=usr/share/man/man1/helpgid.1 target=ckgid.1
link path=usr/share/man/man1/helpint.1 target=ckint.1
link path=usr/share/man/man1/helpitem.1 target=ckitem.1
link path=usr/share/man/man1/helppath.1 target=ckpath.1
link path=usr/share/man/man1/helprange.1 target=ckrange.1
link path=usr/share/man/man1/helpstr.1 target=ckstr.1
link path=usr/share/man/man1/helptime.1 target=cktime.1
link path=usr/share/man/man1/helpuid.1 target=ckuid.1
link path=usr/share/man/man1/helpyorn.1 target=ckyorn.1
file path=usr/share/man/man1/isainfo.1
file path=usr/share/man/man1/isalist.1
file path=usr/share/man/man1/kbd.1
file path=usr/share/man/man1/kmfcfg.1
file path=usr/share/man/man1/kvmstat.1
file path=usr/share/man/man1/ld.so.1.1
file path=usr/share/man/man1/let.1
file path=usr/share/man/man1/lgrpinfo.1
file path=usr/share/man/man1/limit.1
file path=usr/share/man/man1/line.1
file path=usr/share/man/man1/list_devices.1
file path=usr/share/man/man1/listusers.1
file path=usr/share/man/man1/loadkeys.1
#file path=usr/share/man/man1/logger.1
file path=usr/share/man/man1/logout.1
file path=usr/share/man/man1/mac.1
file path=usr/share/man/man1/mach.1
file path=usr/share/man/man1/madv.so.1.1
file path=usr/share/man/man1/makekey.1
file path=usr/share/man/man1/mesg.1
file path=usr/share/man/man1/mkcsmapper.1
file path=usr/share/man/man1/mkesdb.1
file path=usr/share/man/man1/moe.1
file path=usr/share/man/man1/mpss.so.1.1
file path=usr/share/man/man1/newform.1
#file path=usr/share/man/man1/more.1
file path=usr/share/man/man1/news.1
file path=usr/share/man/man1/optisa.1
file path=usr/share/man/man1/pack.1
file path=usr/share/man/man1/pagesize.1
file path=usr/share/man/man1/pargs.1
link path=usr/share/man/man1/pauxv.1 target=pargs.1
link path=usr/share/man/man1/pcat.1 target=pack.1
link path=usr/share/man/man1/pcred.1 target=proc.1
link path=usr/share/man/man1/penv.1 target=pargs.1
link path=usr/share/man/man1/pfcsh.1 target=pfexec.1
file path=usr/share/man/man1/pfexec.1
link path=usr/share/man/man1/pfiles.1 target=proc.1
link path=usr/share/man/man1/pflags.1 target=proc.1
link path=usr/share/man/man1/pfsh.1 target=pfexec.1
file path=usr/share/man/man1/pgrep.1
link path=usr/share/man/man1/pkill.1 target=pgrep.1
file path=usr/share/man/man1/pktool.1
file path=usr/share/man/man1/plgrp.1
file path=usr/share/man/man1/pmadvise.1
file path=usr/share/man/man1/pmap.1
file path=usr/share/man/man1/ppgsz.1
file path=usr/share/man/man1/ppriv.1
#file path=usr/share/man/man1/pr.1
file path=usr/share/man/man1/prctl.1
file path=usr/share/man/man1/preap.1
file path=usr/share/man/man1/priocntl.1
file path=usr/share/man/man1/proc.1
#file path=usr/share/man/man1/profiles.1
file path=usr/share/man/man1/projects.1
link path=usr/share/man/man1/prun.1 target=proc.1
file path=usr/share/man/man1/ps.1
file path=usr/share/man/man1/psecflags.1
link path=usr/share/man/man1/psig.1 target=proc.1
link path=usr/share/man/man1/pstack.1 target=proc.1
link path=usr/share/man/man1/pstop.1 target=proc.1
link path=usr/share/man/man1/ptime.1 target=proc.1
file path=usr/share/man/man1/ptree.1
link path=usr/share/man/man1/pwait.1 target=proc.1
link path=usr/share/man/man1/pwdx.1 target=proc.1
#file path=usr/share/man/man1/pwd.1
file path=usr/share/man/man1/read.1
file path=usr/share/man/man1/readonly.1
link path=usr/share/man/man1/rehash.1 target=hash.1
file path=usr/share/man/man1/rev.1
file path=usr/share/man/man1/roles.1
file path=usr/share/man/man1/runat.1
file path=usr/share/man/man1/script.1
file path=usr/share/man/man1/setfacl.1
file path=usr/share/man/man1/setpgrp.1
file path=usr/share/man/man1/strchg.1
link path=usr/share/man/man1/strconf.1 target=strchg.1
file path=usr/share/man/man1/suspend.1
file path=usr/share/man/man1/svcprop.1
file path=usr/share/man/man1/svcs.1
file path=usr/share/man/man1/tcopy.1
file path=usr/share/man/man1/times.1
file path=usr/share/man/man1/tip.1
file path=usr/share/man/man1/trap.1
file path=usr/share/man/man1/type.1
file path=usr/share/man/man1/typeset.1
link path=usr/share/man/man1/ulimit.1 target=limit.1
file path=usr/share/man/man1/uname.1
link path=usr/share/man/man1/unhash.1 target=hash.1
link path=usr/share/man/man1/unlimit.1 target=limit.1
link path=usr/share/man/man1/unpack.1 target=pack.1
link path=usr/share/man/man1/valdate.1 target=ckdate.1
link path=usr/share/man/man1/valgid.1 target=ckgid.1
link path=usr/share/man/man1/valint.1 target=ckint.1
link path=usr/share/man/man1/valpath.1 target=ckpath.1
link path=usr/share/man/man1/valrange.1 target=ckrange.1
link path=usr/share/man/man1/valstr.1 target=ckstr.1
link path=usr/share/man/man1/valtime.1 target=cktime.1
link path=usr/share/man/man1/valuid.1 target=ckuid.1
link path=usr/share/man/man1/valyorn.1 target=ckyorn.1
file path=usr/share/man/man1/vtfontcvt.1
file path=usr/share/man/man1/w.1
file path=usr/share/man/man1/wait.1
link path=usr/share/man/man1/whence.1 target=typeset.1
file path=usr/share/man/man1/xstr.1
