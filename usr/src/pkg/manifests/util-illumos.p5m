#
# Copyright (c) 2018-2019 DilOS
#

<include SUNWcs.man1.inc>
<include SUNWcs.man4d.inc>
<include SUNWcs.man4fs.inc>
<include SUNWcs.man5.inc>
<include SUNWcs.man7.inc>
<include SUNWcs.man8.inc>
set name=pkg.fmri value=pkg:/util-illumos@$(PKGVERS)
set name=pkg.summary value="illumos utils"
set name=pkg.description \
    value="core software for a specific instruction-set architecture"
set name=info.classification value=org.opensolaris.category.2008:System/Core
set name=variant.arch value=$(ARCH)
file path=etc/.login group=sys preserve=renamenew
link path=etc/autopush target=../sbin/autopush
#link path=etc/cfgadm target=../usr/sbin/cfgadm
link path=etc/clri target=../usr/sbin/clri
dir  path=etc/crypto group=sys
dir  path=etc/crypto/crls group=sys
file path=etc/crypto/kmf.conf group=sys preserve=true
file path=etc/crypto/pkcs11.conf group=sys preserve=renameold
file path=etc/datemsk group=sys mode=0444
link path=etc/dcopy target=../usr/sbin/dcopy
file path=etc/default/fs group=sys preserve=true
#file path=etc/default/keyserv group=sys preserve=true
#file path=etc/default/login group=sys preserve=true
file path=etc/default/nss group=sys preserve=true
#file path=etc/default/su group=sys preserve=true
file path=etc/default/utmpd group=sys preserve=true
dir  path=etc/dev group=sys
file path=etc/dev/reserved_devnames group=sys preserve=true
file path=etc/device.tab group=root mode=0444 preserve=true
dir  path=etc/devices group=sys
dir  path=etc/dfs group=sys
file path=etc/dfs/dfstab group=sys preserve=true
file path=etc/dfs/fstypes group=root preserve=true
file path=etc/dfs/sharetab group=root mode=0444 preserve=true
file path=etc/dgroup.tab group=sys mode=0444 preserve=true
dir  path=etc/dhcp
file path=etc/dhcp/inittab group=sys preserve=true
file path=etc/dhcp/inittab6 group=sys preserve=true
file path=etc/dumpdates group=sys mode=0664 preserve=true
link path=etc/ff target=../usr/sbin/ff
$(i386_ONLY)dir path=etc/flash group=sys
$(i386_ONLY)dir path=etc/flash/postcreation group=sys mode=0700
$(i386_ONLY)dir path=etc/flash/precreation group=sys mode=0700
$(i386_ONLY)file path=etc/flash/precreation/caplib group=sys mode=0500
$(i386_ONLY)dir path=etc/flash/preexit group=sys mode=0700
link path=etc/fmthard target=../usr/sbin/fmthard
link path=etc/format target=../usr/sbin/format
file path=etc/format.dat group=sys preserve=true
dir  path=etc/fs group=sys
dir  path=etc/fs/dev group=sys
file path=etc/fs/dev/mount mode=0555
dir  path=etc/fs/hsfs group=sys
file path=etc/fs/hsfs/mount mode=0555
dir  path=etc/fs/ufs group=sys
file path=etc/fs/ufs/mount mode=0555
link path=etc/fsck target=../usr/sbin/fsck
link path=etc/fsdb target=../usr/sbin/fsdb
link path=etc/getty target=../usr/lib/saf/ttymon
dir  path=etc/inet group=sys
#file path=etc/group group=sys preserve=true
file path=etc/inet/inetd.conf group=sys preserve=true
link path=etc/inetd.conf target=./inet/inetd.conf
file path=etc/ioctl.syscon group=sys preserve=true
link path=etc/killall target=../usr/sbin/killall
link path=etc/labelit target=../usr/sbin/labelit
file path=etc/logadm.conf group=sys preserve=true timestamp=19700101T000000Z
#dir path=etc/lib group=sys
dir  path=etc/logadm.d group=sys
#link path=etc/lib/nss_files.so.1 target=../../lib/nss_files.so.1
link path=etc/mkfs target=../usr/sbin/mkfs
#file path=etc/logindevperm group=sys preserve=true
file path=etc/mnttab group=root mode=0444 preserve=true
link path=etc/mount target=../sbin/mount
link path=etc/mountall target=../sbin/mountall
link path=etc/ncheck target=../usr/sbin/ncheck
dir  path=etc/net group=sys
dir  path=etc/net/ticlts group=sys
#file path=etc/motd group=sys
file path=etc/net/ticlts/hosts group=sys
file path=etc/net/ticlts/services group=sys preserve=true
dir  path=etc/net/ticots group=sys
file path=etc/net/ticots/hosts group=sys
file path=etc/net/ticots/services group=sys preserve=true
dir  path=etc/net/ticotsord group=sys
file path=etc/net/ticotsord/hosts group=sys
file path=etc/net/ticotsord/services group=sys preserve=true
file path=etc/netconfig group=sys preserve=true
file path=etc/nscd.conf group=sys preserve=true
file path=etc/nsswitch.ad group=sys
file path=etc/nsswitch.conf group=sys preserve=true
file path=etc/nsswitch.dns group=sys
file path=etc/nsswitch.files group=sys
file path=etc/nsswitch.ldap group=sys
#file path=etc/nsswitch.nis group=sys
#file path=etc/pam.conf group=sys preserve=true
#file path=etc/passwd group=sys preserve=true
file path=etc/project group=sys preserve=true
link path=etc/prtvtoc target=../usr/sbin/prtvtoc
file path=etc/release_diff group=sys
file path=etc/release_revision group=sys
file path=etc/remote preserve=true
dir  path=etc/rpcsec group=sys
dir  path=etc/saf group=sys
file path=etc/saf/_sactab group=sys preserve=true
file path=etc/saf/_sysconfig group=sys preserve=true
dir  path=etc/saf/zsmon group=sys
file path=etc/saf/zsmon/_pmtab group=sys preserve=true
dir  path=etc/security group=sys
dir  path=etc/security/audit group=sys
dir  path=etc/security/audit/localhost group=sys
link path=etc/security/audit/localhost/files target=../../../../var/audit
file path=etc/security/audit_class group=sys preserve=renamenew
file path=etc/security/audit_event group=sys preserve=renamenew
file path=etc/security/audit_warn group=sys mode=0740 preserve=renamenew
file path=etc/security/auth_attr group=sys preserve=true \
    timestamp=19700101T000000Z
dir  path=etc/security/auth_attr.d group=sys
file path=etc/security/auth_attr.d/SUNWcs group=sys
file path=etc/security/crypt.conf group=sys preserve=renamenew
dir  path=etc/security/dev group=sys
file path=etc/security/dev/audio mode=0400
file path=etc/security/dev/fd0 mode=0400
file path=etc/security/dev/sr0 mode=0400
file path=etc/security/dev/st0 mode=0400
file path=etc/security/dev/st1 mode=0400
file path=etc/security/exec_attr group=sys preserve=true \
    timestamp=19700101T000000Z
dir  path=etc/security/exec_attr.d group=sys
file path=etc/security/exec_attr.d/SUNWcs group=sys
file path=etc/security/kmfpolicy.xml
dir  path=etc/security/lib group=sys
file path=etc/security/lib/audio_clean group=sys mode=0555
file path=etc/security/lib/fd_clean group=sys mode=0555
file path=etc/security/lib/sr_clean group=sys mode=0555
file path=etc/security/lib/st_clean group=sys mode=0555
file path=etc/security/policy.conf group=sys preserve=true
file path=etc/security/priv_names group=sys preserve=renameold
file path=etc/security/prof_attr group=sys preserve=true \
    timestamp=19700101T000000Z
dir  path=etc/security/prof_attr.d group=sys
file path=etc/security/prof_attr.d/SUNWcs group=sys
link path=etc/setmnt target=../usr/sbin/setmnt
link path=etc/swap target=../usr/sbin/swap
link path=etc/swapadd target=../sbin/swapadd
link path=etc/sysdef target=../usr/sbin/sysdef
dir  path=etc/sysevent group=sys
dir  path=etc/sysevent/config group=sys
#file path=etc/shadow group=sys mode=0400 preserve=true
file path=etc/sysevent/config/README group=sys mode=0444
file path=etc/sysevent/config/SUNW,EC_dr,ESC_dr_req,sysevent.conf group=sys
dir  path=etc/tm group=sys
file path=etc/ttydefs group=sys preserve=true
file path=etc/ttysrch group=sys preserve=true
#link path=etc/uadmin target=../sbin/uadmin
link path=etc/umount target=../sbin/umount
link path=etc/umountall target=../sbin/umountall
file path=etc/user_attr group=sys preserve=true timestamp=19700101T000000Z
dir  path=etc/user_attr.d group=sys
file path=etc/user_attr.d/SUNWcs group=sys
file path=etc/vfstab group=sys preserve=true
file etc/vfstab path=etc/vfstab.example group=sys
link path=etc/volcopy target=../usr/sbin/volcopy
link path=etc/whodo target=../usr/sbin/whodo
dir  path=lib/inet
file path=lib/svc/manifest/system/boot-archive-update.xml group=sys mode=0444
file path=lib/svc/manifest/system/boot-archive.xml group=sys mode=0444
file path=lib/svc/manifest/system/boot-config.xml group=sys mode=0444
# should be moved to hostid binary package
$(i386_ONLY)file path=lib/svc/manifest/system/hostid.xml group=sys mode=0444
file path=lib/svc/manifest/system/idmap.xml group=sys mode=0444
file path=lib/svc/manifest/system/name-service-cache.xml group=sys mode=0444
file path=lib/svc/manifest/system/pfexecd.xml group=sys mode=0444
file path=lib/svc/manifest/system/utmp.xml group=sys mode=0444
file path=lib/svc/method/boot-archive mode=0555
file path=lib/svc/method/boot-archive-update mode=0555
$(i386_ONLY)file path=lib/svc/method/svc-hostid mode=0555
file path=lib/svc/method/svc-utmpd mode=0555
file path=sbin/autopush mode=0555
$(i386_ONLY)file path=sbin/biosdev mode=0555
file path=sbin/bootparams mode=0555
file path=sbin/cryptoadm mode=0555
file path=sbin/devprop mode=0555
file path=sbin/dhcpagent mode=0555
file path=sbin/dhcpinfo mode=0555
file path=sbin/fdisk mode=0555
file path=sbin/fiocompress mode=0555
file path=sbin/hostconfig mode=0555
file path=sbin/ipmpstat mode=0555
file path=sbin/mount mode=0555
file path=sbin/mountall group=sys mode=0555
file path=sbin/netstrategy mode=0555
link path=sbin/pfsh target=../usr/bin/pfexec
file path=sbin/soconfig mode=0555
file path=sbin/swapadd group=sys mode=0744
file path=sbin/tzreload mode=0555
file path=sbin/uadmin group=sys mode=0555
file path=sbin/umount mode=0555
file path=sbin/umountall group=sys mode=0555
file path=sbin/uname mode=0555
dir  path=system
dir  path=system/bin mode=0755
file path=system/bin/cat mode=0555
file path=system/bin/chgrp mode=0555
file path=system/bin/chmod mode=0555
file path=system/bin/chown mode=0555
file path=system/bin/chroot mode=0555
file path=system/bin/clear mode=0555
file path=system/bin/compress mode=0555
file path=system/bin/cp mode=0555
file path=system/bin/cpio mode=0555
file path=system/bin/date mode=0555
file path=system/bin/dd mode=0555
file path=system/bin/df mode=0555
file path=system/bin/echo mode=0555
file path=system/bin/file mode=0555
file path=system/bin/find mode=0555
file path=system/bin/getopt mode=0555
file path=system/bin/grep mode=0555
file path=system/bin/groupadd mode=0555
file path=system/bin/groupdel mode=0555
file path=system/bin/groupmod mode=0555
file path=system/bin/groups mode=0555
file path=system/bin/head mode=0555
file path=system/bin/id mode=0555
file path=system/bin/install mode=0555
link path=system/bin/ln target=cp
file path=system/bin/logger mode=0555
file path=system/bin/ls mode=0555
file path=system/bin/mkdir mode=0555
file path=system/bin/mktemp mode=0555
file path=system/bin/more mode=0555
link path=system/bin/mv target=cp
file path=system/bin/nawk mode=0555
file path=system/bin/pg mode=0555
file path=system/bin/profiles mode=0555
file path=system/bin/renice mode=0555
link path=system/bin/roleadd target=useradd
link path=system/bin/roledel target=userdel
link path=system/bin/rolemod target=usermod
#file path=system/bin/su group=sys mode=4555
#file path=system/bin/sulogin mode=0555
file path=system/bin/tabs mode=0555
file path=system/bin/tar mode=0555
link path=system/bin/uncompress target=compress
file path=system/bin/useradd mode=0555
file path=system/bin/userdel mode=0555
file path=system/bin/usermod mode=0555
file path=system/bin/wall group=tty mode=2555
dir  path=system/boot group=root mode=0555
dir  path=system/contract group=root mode=0555
dir  path=system/lib
dir  path=system/lib/$(ARCH64)
dir  path=system/object group=root mode=0555
$(i386_ONLY)file path=usr/bin/addbadsec mode=0555
file path=usr/bin/amt mode=0555
#file path=usr/bin/arch mode=0555
file path=usr/bin/asa mode=0555
file path=usr/bin/attr mode=0555
file path=usr/bin/auths mode=0555
file path=usr/bin/banner mode=0555
file path=usr/bin/bdiff mode=0755
file path=usr/bin/busstat mode=0555
file path=usr/bin/ckdate mode=0555
file path=usr/bin/ckgid mode=0555
file path=usr/bin/ckint mode=0555
file path=usr/bin/ckitem mode=0555
file path=usr/bin/ckkeywd mode=0555
file path=usr/bin/ckpath mode=0555
file path=usr/bin/ckrange mode=0555
file path=usr/bin/ckstr mode=0555
file path=usr/bin/cktime mode=0555
file path=usr/bin/ckuid mode=0555
file path=usr/bin/ckyorn mode=0555
file path=usr/bin/crypt mode=0555
file path=usr/bin/ctrun mode=0555
file path=usr/bin/ctstat mode=0555
file path=usr/bin/ctwatch mode=0555
file path=usr/bin/decrypt mode=0555
file path=usr/bin/decrypt mode=0555
file path=usr/bin/devattr mode=0555
file path=usr/bin/devfree mode=0555
file path=usr/bin/devreserv mode=0555
file path=usr/bin/digest mode=0555
$(i386_ONLY)file path=usr/bin/diskscan mode=0555
link path=usr/bin/dispgid target=ckgid
link path=usr/bin/dispuid target=ckuid
link path=usr/bin/dmesg target=../sbin/dmesg
file path=usr/bin/dumpcs mode=0555
file path=usr/bin/eject mode=0555
link path=usr/bin/encrypt target=decrypt
file path=usr/bin/fdetach mode=0555
file path=usr/bin/fdformat mode=4555
file path=usr/bin/fmtmsg mode=0555
file path=usr/bin/fsstat mode=0555
file path=usr/bin/getdev mode=0555
file path=usr/bin/getdgrp mode=0555
file path=usr/bin/getvol mode=0555
file path=usr/bin/iostat mode=0555
file path=usr/bin/kbd mode=0555
#file path=usr/bin/keylogin mode=0555
#file path=usr/bin/keylogout mode=0555
file path=usr/bin/kmfcfg mode=0555
file path=usr/bin/kstat mode=0555
file path=usr/bin/kvmstat mode=0555
file path=usr/bin/last mode=0555
file path=usr/bin/lgrpinfo mode=0555
file path=usr/bin/line mode=0555
file path=usr/bin/listdgrp mode=0555
file path=usr/bin/listusers mode=0555
file path=usr/bin/logins mode=0750
link path=usr/bin/mac target=digest
file path=usr/bin/mach mode=0555
file path=usr/bin/makedev mode=0555
file path=usr/bin/mesg mode=0555
file path=usr/bin/mpstat mode=0555
file path=usr/bin/newform mode=0555
file path=usr/bin/newgrp mode=4755
file path=usr/bin/news mode=0555
file path=usr/bin/newtask mode=4555
file path=usr/bin/optisa mode=0555
file path=usr/bin/pack mode=0555
file path=usr/bin/pagesize mode=0555
file path=usr/bin/pargs mode=0555
link path=usr/bin/pauxv target=pargs
link path=usr/bin/pcat target=./unpack
file path=usr/bin/pcred mode=0555
link path=usr/bin/penv target=pargs
link path=usr/bin/pfbash target=pfexec
link path=usr/bin/pfcsh target=pfexec
link path=usr/bin/pfdash target=pfexec
file path=usr/bin/pfexec mode=0555
file path=usr/bin/pfiles mode=0555
link path=usr/bin/pfksh target=pfexec
link path=usr/bin/pfksh93 target=pfexec
file path=usr/bin/pflags mode=0555
link path=usr/bin/pfrksh target=pfexec
link path=usr/bin/pfrksh93 target=pfexec
link path=usr/bin/pfsh target=pfexec
link path=usr/bin/pftcsh target=pfexec
link path=usr/bin/pfzsh target=pfexec
file path=usr/bin/pginfo mode=0555
file path=usr/bin/pgrep mode=0555
file path=usr/bin/pgstat mode=0555
link path=usr/bin/pkill target=pgrep
file path=usr/bin/pktool mode=0555
file path=usr/bin/plgrp mode=0555
file path=usr/bin/pmadvise mode=0555
file path=usr/bin/pmap mode=0555
file path=usr/bin/ppgsz mode=0555
file path=usr/bin/ppriv mode=0555
#file path=usr/bin/prctl mode=0555
file path=usr/bin/preap mode=0555
file path=usr/bin/projects mode=0555
file path=usr/bin/prstat mode=0555
file path=usr/bin/prun mode=0555
file path=usr/bin/ps mode=0555
file path=usr/bin/psecflags mode=0555
file path=usr/bin/psig mode=0555
file path=usr/bin/pstack mode=0555
file path=usr/bin/pstop mode=0555
file path=usr/bin/ptime mode=0555
file path=usr/bin/ptree mode=0555
file path=usr/bin/putdev mode=0555
file path=usr/bin/putdgrp mode=0555
file path=usr/bin/pwait mode=0555
file path=usr/bin/pwdx mode=0555
file path=usr/bin/roles mode=0555
file path=usr/bin/rpcinfo mode=0555
file path=usr/bin/runat mode=0555
file path=usr/bin/script mode=0555
file path=usr/bin/setpgrp group=sys mode=0555
link path=usr/bin/strace target=../sbin/strace
file path=usr/bin/strchg group=root mode=0555
link path=usr/bin/strclean target=../sbin/strclean
file path=usr/bin/strconf group=root mode=0555
link path=usr/bin/strerr target=../sbin/strerr
file path=usr/bin/tcopy mode=0555
file path=usr/bin/tip-illumos owner=uucp mode=4511
link path=usr/bin/uname target=../../sbin/uname
file path=usr/bin/unpack mode=0555
file path=usr/bin/userattr mode=0555
file path=usr/bin/vmstat mode=0555
file path=usr/bin/vtfontcvt mode=0555
file path=usr/bin/w mode=4555
file path=usr/bin/xstr mode=0555
#file path=usr/kvm/README group=sys
file path=usr/lib/$(ARCH64)/madv.so.1
file path=usr/lib/$(ARCH64)/mpss.so.1
dir  path=usr/lib/adb group=sys
dir  path=usr/lib/adb/$(ARCH64) group=sys
file path=usr/lib/adb/$(ARCH64)/adbsub.o group=sys
file path=usr/lib/adb/adbgen group=sys mode=0755
file path=usr/lib/adb/adbgen1 group=sys mode=0755
file path=usr/lib/adb/adbgen3 group=sys mode=0755
file path=usr/lib/adb/adbgen4 group=sys mode=0755
file path=usr/lib/adb/adbsub.o group=sys
dir  path=usr/lib/audit
file path=usr/lib/audit/audit_record_attr mode=0444
dir  path=usr/lib/crypto group=sys
dir  path=usr/lib/fs group=sys
dir  path=usr/lib/fs/autofs group=sys
dir  path=usr/lib/fs/autofs/$(ARCH64) group=sys
dir  path=usr/lib/fs/ctfs group=sys
file path=usr/lib/fs/ctfs/mount mode=0555
dir  path=usr/lib/fs/dev group=sys
link path=usr/lib/fs/dev/mount target=../../../../etc/fs/dev/mount
dir  path=usr/lib/fs/fd group=sys
file path=usr/lib/fs/fd/mount mode=0555
dir  path=usr/lib/fs/hsfs group=sys
link path=usr/lib/fs/hsfs/fstyp target=../../../sbin/fstyp
file path=usr/lib/fs/hsfs/fstyp.so.1 mode=0555
file path=usr/lib/fs/hsfs/labelit mode=0555
link path=usr/lib/fs/hsfs/mount target=../../../../etc/fs/hsfs/mount
$(i386_ONLY)dir path=usr/lib/fs/hyprlofs group=sys
$(i386_ONLY)file path=usr/lib/fs/hyprlofs/hlcfg mode=0555
$(i386_ONLY)file path=usr/lib/fs/hyprlofs/mount mode=0555
dir  path=usr/lib/fs/lofs group=sys
file path=usr/lib/fs/lofs/mount mode=0555
dir  path=usr/lib/fs/lxproc group=sys
file path=usr/lib/fs/lxproc/mount mode=0555
dir  path=usr/lib/fs/mntfs group=sys
file path=usr/lib/fs/mntfs/mount mode=0555
dir  path=usr/lib/fs/nfs group=sys
#dir path=usr/lib/fs/nfs/$(ARCH64) group=sys
dir  path=usr/lib/fs/objfs group=sys
file path=usr/lib/fs/objfs/mount mode=0555
dir  path=usr/lib/fs/pcfs group=sys
file path=usr/lib/fs/pcfs/fsck mode=0555
link path=usr/lib/fs/pcfs/fstyp target=../../../sbin/fstyp
file path=usr/lib/fs/pcfs/fstyp.so.1 mode=0555
file path=usr/lib/fs/pcfs/mkfs mode=0555
file path=usr/lib/fs/pcfs/mount mode=0555
dir  path=usr/lib/fs/proc group=sys
file path=usr/lib/fs/proc/mount mode=0555
dir  path=usr/lib/fs/sharefs group=sys
file path=usr/lib/fs/sharefs/mount mode=0555
dir  path=usr/lib/fs/tmpfs group=sys
file path=usr/lib/fs/tmpfs/mount mode=0555
dir  path=usr/lib/fs/ufs group=sys
file path=usr/lib/fs/ufs/clri mode=0555
link path=usr/lib/fs/ufs/dcopy target=clri
file path=usr/lib/fs/ufs/df mode=0555
file path=usr/lib/fs/ufs/edquota mode=0555
file path=usr/lib/fs/ufs/ff mode=0555
file path=usr/lib/fs/ufs/fsck mode=0555
file path=usr/lib/fs/ufs/fsckall mode=0555
file path=usr/lib/fs/ufs/fsdb mode=0555
file path=usr/lib/fs/ufs/fsirand mode=0555
file path=usr/lib/fs/ufs/fssnap mode=0555
link path=usr/lib/fs/ufs/fstyp target=../../../sbin/fstyp
file path=usr/lib/fs/ufs/fstyp.so.1 mode=0555
file path=usr/lib/fs/ufs/labelit mode=0555
file path=usr/lib/fs/ufs/lockfs mode=0555
file path=usr/lib/fs/ufs/mkfs mode=0555
link path=usr/lib/fs/ufs/mount target=../../../../etc/fs/ufs/mount
file path=usr/lib/fs/ufs/ncheck mode=0555
file path=usr/lib/fs/ufs/newfs mode=0555
file path=usr/lib/fs/ufs/quot mode=0555
file path=usr/lib/fs/ufs/quota mode=4555
file path=usr/lib/fs/ufs/quotacheck mode=0555
link path=usr/lib/fs/ufs/quotaoff target=quotaon
file path=usr/lib/fs/ufs/quotaon mode=0555
file path=usr/lib/fs/ufs/repquota mode=0555
file path=usr/lib/fs/ufs/tunefs mode=0555
file path=usr/lib/fs/ufs/ufsdump mode=4555
file path=usr/lib/fs/ufs/ufsrestore mode=4555
file path=usr/lib/fs/ufs/volcopy mode=0555
file path=usr/lib/getoptcvt mode=0555
file path=usr/lib/hotplugd mode=0555
file path=usr/lib/idmapd mode=0555
dir  path=usr/lib/inet
dir  path=usr/lib/inet/$(ARCH64)
file path=usr/lib/inet/inetd mode=0555
dir  path=usr/lib/locale
file path=usr/lib/madv.so.1
file path=usr/lib/makekey mode=0555
file path=usr/lib/mpss.so.1
dir  path=usr/lib/netsvc group=sys
file path=usr/lib/passmgmt group=sys mode=0555
dir  path=usr/lib/pci
file path=usr/lib/pci/pcidr mode=0555
file path=usr/lib/pci/pcidr_plugin.so
file path=usr/lib/pfexecd mode=0555
file path=usr/lib/platexec mode=0555
dir  path=usr/lib/rcm
dir  path=usr/lib/rcm/modules
file path=usr/lib/rcm/modules/SUNW_aggr_rcm.so mode=0555
file path=usr/lib/rcm/modules/SUNW_cluster_rcm.so mode=0555
file path=usr/lib/rcm/modules/SUNW_dump_rcm.so mode=0555
file path=usr/lib/rcm/modules/SUNW_filesys_rcm.so mode=0555
file path=usr/lib/rcm/modules/SUNW_ibpart_rcm.so mode=0555
file path=usr/lib/rcm/modules/SUNW_ip_anon_rcm.so mode=0555
file path=usr/lib/rcm/modules/SUNW_ip_rcm.so mode=0555
file path=usr/lib/rcm/modules/SUNW_mpxio_rcm.so mode=0555
file path=usr/lib/rcm/modules/SUNW_network_rcm.so mode=0555
file path=usr/lib/rcm/modules/SUNW_swap_rcm.so mode=0555
file path=usr/lib/rcm/modules/SUNW_vlan_rcm.so mode=0555
file path=usr/lib/rcm/modules/SUNW_vnic_rcm.so mode=0555
file path=usr/lib/rcm/rcm_daemon mode=0555
dir  path=usr/lib/rcm/scripts
dir  path=usr/lib/reparse
file path=usr/lib/reparse/reparsed group=sys mode=0555
dir  path=usr/lib/saf
file path=usr/lib/saf/listen group=sys mode=0755
file path=usr/lib/saf/nlps_server group=sys mode=0755
file path=usr/lib/saf/sac group=sys mode=0555
file path=usr/lib/saf/ttymon group=sys mode=0555
dir  path=usr/lib/security
dir  path=usr/lib/security/$(ARCH64)
dir  path=usr/lib/spd
file path=usr/lib/spd/spd mode=0555
dir  path=usr/lib/sysevent
dir  path=usr/lib/sysevent/modules
file path=usr/lib/sysevent/modules/datalink_mod.so
file path=usr/lib/sysevent/modules/sysevent_conf_mod.so
file path=usr/lib/sysevent/modules/sysevent_reg_mod.so
file path=usr/lib/sysevent/syseventconfd mode=0555
file path=usr/lib/sysevent/syseventd mode=0555
file path=usr/lib/utmp_update mode=4555
file path=usr/lib/vtdaemon mode=0555
file path=usr/lib/vtinfo mode=0555
file path=usr/lib/vtxlock mode=0555
dir  path=usr/net group=sys
dir  path=usr/net/nls group=sys
link path=usr/net/nls/listen target=../../lib/saf/listen
link path=usr/net/nls/nlps_server target=../../lib/saf/nlps_server
dir  path=usr/net/servers group=sys
dir  path=usr/sadm
dir  path=usr/sadm/bin
link path=usr/sadm/bin/dispgid target=../../../usr/bin/ckgid
link path=usr/sadm/bin/dispuid target=../../../usr/bin/ckuid
link path=usr/sadm/bin/errange target=../../../usr/bin/ckrange
link path=usr/sadm/bin/errdate target=../../../usr/bin/ckdate
link path=usr/sadm/bin/errgid target=../../../usr/bin/ckgid
link path=usr/sadm/bin/errint target=../../../usr/bin/ckint
link path=usr/sadm/bin/erritem target=../../../usr/bin/ckitem
link path=usr/sadm/bin/errpath target=../../../usr/bin/ckpath
link path=usr/sadm/bin/errstr target=../../../usr/bin/ckstr
link path=usr/sadm/bin/errtime target=../../../usr/bin/cktime
link path=usr/sadm/bin/erruid target=../../../usr/bin/ckuid
link path=usr/sadm/bin/erryorn target=../../../usr/bin/ckyorn
link path=usr/sadm/bin/helpdate target=../../../usr/bin/ckdate
link path=usr/sadm/bin/helpgid target=../../../usr/bin/ckgid
link path=usr/sadm/bin/helpint target=../../../usr/bin/ckint
link path=usr/sadm/bin/helpitem target=../../../usr/bin/ckitem
link path=usr/sadm/bin/helppath target=../../../usr/bin/ckpath
link path=usr/sadm/bin/helprange target=../../../usr/bin/ckrange
link path=usr/sadm/bin/helpstr target=../../../usr/bin/ckstr
link path=usr/sadm/bin/helptime target=../../../usr/bin/cktime
link path=usr/sadm/bin/helpuid target=../../../usr/bin/ckuid
link path=usr/sadm/bin/helpyorn target=../../../usr/bin/ckyorn
file path=usr/sadm/bin/puttext mode=0555
link path=usr/sadm/bin/valdate target=../../../usr/bin/ckdate
link path=usr/sadm/bin/valgid target=../../../usr/bin/ckgid
link path=usr/sadm/bin/valint target=../../../usr/bin/ckint
link path=usr/sadm/bin/valpath target=../../../usr/bin/ckpath
link path=usr/sadm/bin/valrange target=../../../usr/bin/ckrange
link path=usr/sadm/bin/valstr target=../../../usr/bin/ckstr
link path=usr/sadm/bin/valtime target=../../../usr/bin/cktime
link path=usr/sadm/bin/valuid target=../../../usr/bin/ckuid
link path=usr/sadm/bin/valyorn target=../../../usr/bin/ckyorn
dir  path=usr/sadm/install
file path=usr/sadm/install/miniroot.db group=sys mode=0444
dir  path=usr/sadm/install/scripts
file path=usr/sadm/install/scripts/i.ipsecalgs group=sys mode=0555
file path=usr/sadm/install/scripts/i.kcfconf group=sys mode=0555
file path=usr/sadm/install/scripts/i.kmfconf group=sys mode=0555
file path=usr/sadm/install/scripts/i.manifest group=sys mode=0555
file path=usr/sadm/install/scripts/i.pkcs11conf group=sys mode=0555
file path=usr/sadm/install/scripts/i.rbac group=sys mode=0555
file path=usr/sadm/install/scripts/r.ipsecalgs group=sys mode=0555
file path=usr/sadm/install/scripts/r.kcfconf group=sys mode=0555
file path=usr/sadm/install/scripts/r.kmfconf group=sys mode=0555
file path=usr/sadm/install/scripts/r.manifest group=sys mode=0555
file path=usr/sadm/install/scripts/r.pkcs11conf group=sys mode=0555
file path=usr/sadm/install/scripts/r.rbac group=sys mode=0555
file path=usr/sadm/ugdates mode=0444
file path=usr/sbin/allocate mode=4555
file path=usr/sbin/audit mode=0555
file path=usr/sbin/auditconfig mode=0555
file path=usr/sbin/auditd mode=0555
file path=usr/sbin/auditrecord mode=0555
file path=usr/sbin/auditreduce mode=0555
file path=usr/sbin/auditstat mode=0555
link path=usr/sbin/autopush target=../../sbin/autopush
file path=usr/sbin/cfgadm mode=0555
file path=usr/sbin/clear_locks mode=0555
file path=usr/sbin/clinfo mode=0555
file path=usr/sbin/clri mode=0555
file path=usr/sbin/consadm group=sys mode=0555
link path=usr/sbin/consadmd target=consadm
link path=usr/sbin/cryptoadm target=../../sbin/cryptoadm
link path=usr/sbin/dcopy target=./clri
link path=usr/sbin/deallocate target=allocate
link path=usr/sbin/devnm target=../../system/bin/df
link path=usr/sbin/dfmounts target=dfshares
file path=usr/sbin/dfshares mode=0555
file path=usr/sbin/dmesg mode=0555
file path=usr/sbin/dminfo mode=0555
link path=usr/sbin/edquota target=../lib/fs/ufs/edquota
file path=usr/sbin/eeprom group=sys mode=2555
link path=usr/sbin/fdisk target=../../sbin/fdisk
file path=usr/sbin/ff mode=0555
link path=usr/sbin/fiocompress target=../../sbin/fiocompress
file path=usr/sbin/fmthard group=sys mode=0555
file path=usr/sbin/format mode=0555
file path=usr/sbin/fsck mode=0555
link path=usr/sbin/fsdb target=./clri
link path=usr/sbin/fsirand target=../lib/fs/ufs/fsirand
link path=usr/sbin/fssnap target=./clri
file path=usr/sbin/fstyp group=sys mode=0555
file path=usr/sbin/fuser mode=0555
file path=usr/sbin/getdevpolicy group=sys mode=0555
file path=usr/sbin/growfs mode=0555
link path=usr/sbin/hostconfig target=../../sbin/hostconfig
file path=usr/sbin/hotplug mode=0555
file path=usr/sbin/idmap mode=0555
file path=usr/sbin/if_mpadm mode=0555
file path=usr/sbin/inetadm mode=0555
file path=usr/sbin/inetconv mode=0555
link path=usr/sbin/inetd target=../lib/inet/inetd
link path=usr/sbin/ipmpstat target=../../sbin/ipmpstat
file path=usr/sbin/ipsecalgs mode=0555
#file path=usr/sbin/keyserv group=sys mode=0555
file path=usr/sbin/killall mode=0555
link path=usr/sbin/labelit target=./clri
link path=usr/sbin/list_devices target=allocate
link path=usr/sbin/lockfs target=../lib/fs/ufs/lockfs
file path=usr/sbin/lofiadm mode=0555
file path=usr/sbin/logadm mode=0555
#file path=usr/sbin/makedbm mode=0555
file path=usr/sbin/mkdevalloc mode=0555
link path=usr/sbin/mkdevmaps target=mkdevalloc
file path=usr/sbin/mkfile mode=0555
link path=usr/sbin/mkfs target=./clri
link path=usr/sbin/mount target=../../sbin/mount
file path=usr/sbin/mountall group=sys mode=0555
file path=usr/sbin/msgid mode=0555
file path=usr/sbin/mvdir mode=0555
link path=usr/sbin/ncheck target=./ff
file path=usr/sbin/ndp mode=0555
link path=usr/sbin/newfs target=../lib/fs/ufs/newfs
file path=usr/sbin/nlsadmin group=adm mode=0755
#file path=usr/sbin/nltest mode=0555
file path=usr/sbin/nscd mode=0555
file path=usr/sbin/pbind group=sys mode=0555
file path=usr/sbin/pmadm group=sys mode=0555
file path=usr/sbin/praudit mode=0555
file path=usr/sbin/projadd group=sys mode=0555
file path=usr/sbin/projdel group=sys mode=0555
file path=usr/sbin/projmod group=sys mode=0555
file path=usr/sbin/prtvtoc group=sys mode=0555
file path=usr/sbin/psradm group=sys mode=0555
file path=usr/sbin/psrinfo group=sys mode=0555
file path=usr/sbin/psrset group=sys mode=0555
link path=usr/sbin/quot target=../lib/fs/ufs/quot
link path=usr/sbin/quota target=../lib/fs/ufs/quota
link path=usr/sbin/quotacheck target=../lib/fs/ufs/quotacheck
link path=usr/sbin/quotaoff target=../lib/fs/ufs/quotaoff
link path=usr/sbin/quotaon target=../lib/fs/ufs/quotaon
file path=usr/sbin/raidctl mode=0555
file path=usr/sbin/ramdiskadm mode=0555
file path=usr/sbin/rctladm mode=0555
link path=usr/sbin/repquota target=../lib/fs/ufs/repquota
file path=usr/sbin/root_archive group=sys mode=0555
file path=usr/sbin/rpcbind mode=0555
$(i386_ONLY)file path=usr/sbin/rtc mode=0555
file path=usr/sbin/sacadm group=sys mode=4755
file path=usr/sbin/setmnt mode=0555
link path=usr/sbin/share target=sharemgr
file path=usr/sbin/shareall mode=0555
file path=usr/sbin/sharectl mode=0555
file path=usr/sbin/sharemgr mode=0555
file path=usr/sbin/smbios mode=0555
file path=usr/sbin/stmsboot mode=0555
file path=usr/sbin/strace group=sys mode=0555
file path=usr/sbin/strclean group=sys mode=0555
file path=usr/sbin/strerr group=sys mode=0555
file path=usr/sbin/sttydefs group=sys mode=0755
file path=usr/sbin/swap group=sys mode=2555
file path=usr/sbin/syncinit mode=0555
file path=usr/sbin/syncloop mode=0555
file path=usr/sbin/syncstat mode=0555
file path=usr/sbin/sysdef group=sys mode=2555
file path=usr/sbin/syseventadm group=sys mode=0555
$(sparc_ONLY)file path=usr/sbin/trapstat mode=0555
$(i386_ONLY)link path=usr/sbin/trapstat target=../../usr/lib/platexec
file path=usr/sbin/ttyadm group=sys mode=0755
link path=usr/sbin/tunefs target=../lib/fs/ufs/tunefs
link path=usr/sbin/tzreload target=../../sbin/tzreload
link path=usr/sbin/uadmin target=../../sbin/uadmin
$(i386_ONLY)file path=usr/sbin/ucodeadm mode=0555
link path=usr/sbin/ufsdump target=../lib/fs/ufs/ufsdump
link path=usr/sbin/ufsrestore target=../lib/fs/ufs/ufsrestore
link path=usr/sbin/umount target=../../sbin/umount
file path=usr/sbin/umountall group=sys mode=0555
link path=usr/sbin/unshare target=sharemgr
file path=usr/sbin/unshareall mode=0555
file path=usr/sbin/utmpd mode=0555
$(sparc_ONLY)file path=usr/sbin/virtinfo mode=0555
file path=usr/sbin/volcopy mode=0555
file path=usr/sbin/whodo mode=4555
dir  path=usr/share/lib
dir  path=usr/share/lib/pub
dir  path=usr/share/lib/xml group=sys
dir  path=usr/share/lib/xml/dtd group=sys
#file etc/group path=usr/share/base-files/group.example group=sys
#file etc/passwd path=usr/share/base-files/passwd.example group=sys
#file etc/shadow path=usr/share/base-files/shadow.example group=sys mode=0400
file path=usr/share/lib/xml/dtd/adt_record.dtd.1
file path=usr/share/lib/xml/dtd/kmfpolicy.dtd
dir  path=usr/share/lib/xml/style group=sys
file path=usr/share/lib/xml/style/adt_record.xsl.1
dir  path=var/audit group=sys
dir  path=var/idmap owner=daemon group=daemon
dir  path=var/inet group=sys
dir  path=var/logadm
dir  path=var/news
dir  path=var/preserve mode=1777
dir  path=var/saf
dir  path=var/saf/zsmon group=sys
#file path=var/adm/aculog mode=0600 owner=uucp preserve=true
#file path=var/log/authlog group=sys mode=0600 preserve=true
file path=var/saf/zsmon/log group=sys preserve=true
legacy pkg=SUNWesu desc="additional UNIX system utilities" \
    name="Extended System Utilities"
license cr_ATT license=cr_ATT
license cr_Sun license=cr_Sun
license lic_CDDL license=lic_CDDL
license usr/src/cmd/eeprom/THIRDPARTYLICENSE \
    license=usr/src/cmd/eeprom/THIRDPARTYLICENSE
license usr/src/cmd/fs.d/ufs/THIRDPARTYLICENSE \
    license=usr/src/cmd/fs.d/ufs/THIRDPARTYLICENSE
license usr/src/cmd/mt/THIRDPARTYLICENSE \
    license=usr/src/cmd/mt/THIRDPARTYLICENSE
license usr/src/cmd/script/THIRDPARTYLICENSE \
    license=usr/src/cmd/script/THIRDPARTYLICENSE
license usr/src/cmd/stat/vmstat/THIRDPARTYLICENSE \
    license=usr/src/cmd/stat/vmstat/THIRDPARTYLICENSE
license usr/src/cmd/tip/THIRDPARTYLICENSE \
    license=usr/src/cmd/tip/THIRDPARTYLICENSE
license usr/src/cmd/xstr/THIRDPARTYLICENSE \
    license=usr/src/cmd/xstr/THIRDPARTYLICENSE
depend type=require fmri=acl
depend type=require fmri=base-files
#depend fmri=base-passwd type=require
depend type=require fmri=bsd-mailx
#depend fmri=coreadm type=require
depend type=require fmri=coreutils
depend type=require fmri=cpio
depend type=require fmri=dash
#depend fmri=diffutils type=require
depend type=require fmri=findutils
depend type=require fmri=g-build-version
#depend fmri=ftpd type=require
depend type=require fmri=gawk
depend type=require fmri=grep
#depend fmri=iputils-ping type=require
depend type=require fmri=kbd
depend type=require fmri=lib0at0@$(DEB_VERSION)
depend type=require fmri=libads1@$(DEB_VERSION)
depend type=require fmri=libaio@$(DEB_VERSION)
depend type=require fmri=libbsdmalloc@$(DEB_VERSION)
depend type=require fmri=libc-bin
depend type=require fmri=libcommputil@$(DEB_VERSION)
depend type=require fmri=libcrle@$(DEB_VERSION)
depend type=require fmri=libdoor@$(DEB_VERSION)
depend type=require fmri=libextendedfile@$(DEB_VERSION)
depend type=require fmri=libnsswitch@$(DEB_VERSION)
depend type=require fmri=libsip@$(DEB_VERSION)
depend type=require fmri=libsmp@$(DEB_VERSION)
depend type=require fmri=libstraddr@$(DEB_VERSION)
depend type=require fmri=login
#depend fmri=mime-support type=require
#depend fmri=ncurses-term type=require
depend type=require fmri=net-tools
depend type=require fmri=netbase
depend type=require fmri=passwd
#depend fmri=scheduler type=require
depend type=require fmri=sed
depend type=require fmri=smf
#depend fmri=smf-tools type=require
depend type=require fmri=sysinit-core
#depend fmri=system-data-console-fonts type=require
depend type=require fmri=traceroute
depend type=require fmri=zonename
