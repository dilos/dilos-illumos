#
# This file and its contents are supplied under the terms of the
# Common Development and Distribution License ("CDDL"), version 1.0.
# You may only use this file in accordance with the terms of version
# 1.0 of the CDDL.
#
# A full copy of the text of the CDDL should have accompanied this
# source.  A copy of the CDDL is also available via the Internet
# at http://www.illumos.org/license/CDDL.
#

#
# Copyright 2011, Richard Lowe
# Copyright 2016 Nexenta Systems, Inc.
# Copyright 2019 Peter Tribble.
# Copyright 2019 Joyent, Inc.
#

link path=usr/share/man/man4d/allkmem.4d target=mem.4d
$(i386_ONLY)file path=usr/share/man/man4d/amdnbtemp.4d
$(i386_ONLY)file path=usr/share/man/man4d/amdzen.4d
$(i386_ONLY)link path=usr/share/man/man4d/amdzen_stub.4d target=amdzen.4d
$(i386_ONLY)file path=usr/share/man/man4d/asy.4d
$(i386_ONLY)file path=usr/share/man/man4d/cmdk.4d
file path=usr/share/man/man4d/console.4d
$(i386_ONLY)file path=usr/share/man/man4d/coretemp.4d
file path=usr/share/man/man4d/cpuid.4d
file path=usr/share/man/man4d/devinfo.4d
$(i386_ONLY)file path=usr/share/man/man4d/ecpp.4d
file path=usr/share/man/man4d/fd.4d
file path=usr/share/man/man4d/full.4d
file path=usr/share/man/man4d/gld.4d
$(i386_ONLY)file path=usr/share/man/man4d/imc.4d
$(i386_ONLY)file path=usr/share/man/man4d/imcstub.4d
file path=usr/share/man/man4d/ipnet.4d
file path=usr/share/man/man4d/kmdb.4d
link path=usr/share/man/man4d/kmem.4d target=mem.4d
file path=usr/share/man/man4d/ksensor.4d
file path=usr/share/man/man4d/llc1.4d
link path=usr/share/man/man4d/lo0.4d target=ipnet.4d
file path=usr/share/man/man4d/lofi.4d
file path=usr/share/man/man4d/log.4d
file path=usr/share/man/man4d/mem.4d
file path=usr/share/man/man4d/msglog.4d
file path=usr/share/man/man4d/mt.4d
file path=usr/share/man/man4d/null.4d
file path=usr/share/man/man4d/nulldriver.4d
file path=usr/share/man/man4d/openprom.4d
$(i386_ONLY)file path=usr/share/man/man4d/pchtemp.4d
file path=usr/share/man/man4d/physmem.4d
file path=usr/share/man/man4d/poll.4d
file path=usr/share/man/man4d/pty.4d
file path=usr/share/man/man4d/ramdisk.4d
file path=usr/share/man/man4d/random.4d
file path=usr/share/man/man4d/sad.4d
file path=usr/share/man/man4d/sata.4d
file path=usr/share/man/man4d/sd.4d
file path=usr/share/man/man4d/sgen.4d
$(i386_ONLY)file path=usr/share/man/man4d/smntemp.4d
file path=usr/share/man/man4d/st.4d
file path=usr/share/man/man4d/sysmsg.4d
file path=usr/share/man/man4d/ticlts.4d
link path=usr/share/man/man4d/ticots.4d target=ticlts.4d
link path=usr/share/man/man4d/ticotsord.4d target=ticlts.4d
file path=usr/share/man/man4d/tty.4d
file path=usr/share/man/man4d/tzmon.4d
file path=usr/share/man/man4d/ufm.4d
link path=usr/share/man/man4d/urandom.4d target=random.4d
file path=usr/share/man/man4d/virtualkm.4d
file path=usr/share/man/man4d/vnd.4d
file path=usr/share/man/man4d/vni.4d
file path=usr/share/man/man4d/wscons.4d
file path=usr/share/man/man4d/zero.4d
file path=usr/share/man/man4d/zfd.4d
