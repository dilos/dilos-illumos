/*
 * Copyright 2011 Nexenta Systems, Inc.  All rights reserved.
 * Copyright 2001-2003 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

/*
 * The contents of this file are subject to the Netscape Public
 * License Version 1.1 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of
 * the License at http://www.mozilla.org/NPL/
 *
 * Software distributed under the License is distributed on an "AS
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
 * implied. See the License for the specific language governing
 * rights and limitations under the License.
 *
 * The Original Code is Mozilla Communicator client code, released
 * March 31, 1998.
 *
 * The Initial Developer of the Original Code is Netscape
 * Communications Corporation. Portions created by Netscape are
 * Copyright (C) 1998-1999 Netscape Communications Corporation. All
 * Rights Reserved.
 *
 * Contributor(s):
 */

#ifndef	_SOLARIS_LDAP_H
#define	_SOLARIS_LDAP_H

#ifdef	__cplusplus
extern "C" {
#endif

#ifndef	_SOLARIS_SDK
#define	_SOLARIS_SDK
#endif

#include <ldap.h>


#define	LDAP_ROOT_DSE		""		/* API extension */
#define	LDAP_NO_ATTRS		"1.1"
#define	LDAP_ALL_USER_ATTRS	"*"

/* Password information sent back to client */
#define LDAP_CONTROL_PWEXPIRED          "2.16.840.1.113730.3.4.4"
#define LDAP_CONTROL_PWEXPIRING         "2.16.840.1.113730.3.4.5"


int ldap_parse_sort_control(LDAP *, LDAPControl **, unsigned long *, char **);

/*int ldap_create_sort_keylist(LDAPsortkey ***, const char *);*/


/*
 * Virtual list view (an LDAPv3 extension -- LDAP_API_FEATURE_VIRTUAL_LIST_VIEW)
 */
/*
 * structure that describes a VirtualListViewRequest control.
 * note that ldvlist_index and ldvlist_size are only relevant to
 * ldap_create_virtuallist_control() if ldvlist_attrvalue is NULL.
 */
typedef struct ldapvirtuallist {
    unsigned long	ldvlist_before_count;	/* # entries before target */
    unsigned long   ldvlist_after_count;	/* # entries after target */
    char	    *ldvlist_attrvalue;		/* jump to this value */
    unsigned long   ldvlist_index;		/* list offset */
    unsigned long   ldvlist_size;		/* number of items in vlist */
    void	*ldvlist_extradata;		/* for use by application */
} LDAPVirtualList;

/*
 * VLV functions:
 */
LDAP_F(int)
ldap_create_virtuallist_control
LDAP_P((LDAP *, LDAPVirtualList *, LDAPControl **));

LDAP_F(int)
ldap_parse_virtuallist_control
LDAP_P((LDAP *, LDAPControl **, unsigned long *, unsigned long *, int *));


/*
 * Routines for creating persistent search controls and for handling
 * "entry changed notification" controls (an LDAPv3 extension --
 * LDAP_API_FEATURE_PERSISTENT_SEARCH)
 */
#define	LDAP_CHANGETYPE_ADD		1
#define	LDAP_CHANGETYPE_DELETE		2
#define	LDAP_CHANGETYPE_MODIFY		4
#define	LDAP_CHANGETYPE_MODDN		8
#define	LDAP_CHANGETYPE_ANY		(1|2|4|8)
LDAP_F(int)
ldap_create_persistentsearch_control
LDAP_P((LDAP *, int, int, int, int, LDAPControl **));

#if	0
LDAP_F(int)
ldap_parse_entrychange_control
LDAP_P((LDAP *, LDAPControl **, int *, char **, int *, ber_int_t *));
#endif

#if	0
/*
 * Routines for creating Proxied Authorization controls (an LDAPv3
 * extension -- LDAP_API_FEATURE_PROXY_AUTHORIZATION)
 * ldap_create_proxyauth_control() is for the old (version 1) control.
 * ldap_create_proxiedauth_control() is for the newer (version 2) control.
 * Version 1 is supported by iPlanet Directory Server 4.1 and later.
 * Version 2 is supported by iPlanet Directory Server 5.0 and later.
 */
int LDAP_CALL ldap_create_proxyauth_control(LDAP *ld,
	const char *dn, const char ctl_iscritical, LDAPControl **ctrlp);
int LDAP_CALL ldap_create_proxiedauth_control(LDAP *ld,
	const char *authzid, LDAPControl **ctrlp);


/*
 * Functions to get and set LDAP error information (API extension --
 * LDAP_API_FEATURE_X_LDERRNO )
 */
int LDAP_CALL ldap_get_lderrno(LDAP *ld, char **m, char **s);
int LDAP_CALL ldap_set_lderrno(LDAP *ld, int e, char *m, char *s);


/*
 * LDAP URL functions and definitions (an API extension --
 * LDAP_API_FEATURE_X_URL_FUNCTIONS)
 */
/*
 * types for ldap URL handling
 */
typedef struct ldap_url_desc {
    char		*lud_host;
    int			lud_port;
    char		*lud_dn;
    char		**lud_attrs;
    int			lud_scope;
    char		*lud_filter;
    unsigned long	lud_options;
#define	LDAP_URL_OPT_SECURE	0x01
    char	*lud_string;    /* for internal use only */
} LDAPURLDesc;

#define	NULLLDAPURLDESC ((LDAPURLDesc *)NULL)

/*
 * possible errors returned by ldap_url_parse()
 */
#define	LDAP_URL_ERR_NOTLDAP	1	/* URL doesn't begin with "ldap://" */
#define	LDAP_URL_ERR_NODN	2	/* URL has no DN (required) */
#define	LDAP_URL_ERR_BADSCOPE	3	/* URL scope string is invalid */
#define	LDAP_URL_ERR_MEM	4	/* can't allocate memory space */
#define	LDAP_URL_ERR_PARAM	5	/* bad parameter to an URL function */
#define	LDAP_URL_ERR_HOSTPORT	6	/* URL hostcode is invalid */

/*
 * URL functions:
 */
int LDAP_CALL ldap_is_ldap_url(const char *url);
int LDAP_CALL ldap_url_parse(const char *url, LDAPURLDesc **ludpp);
int LDAP_CALL ldap_url_parse_nodn(const char *url, LDAPURLDesc **ludpp);
LDAP_API(void) LDAP_CALL ldap_free_urldesc(LDAPURLDesc *ludp);
int LDAP_CALL ldap_url_search(LDAP *ld, const char *url,
	int attrsonly);
int LDAP_CALL ldap_url_search_s(LDAP *ld, const char *url,
	int attrsonly, LDAPMessage **res);
int LDAP_CALL ldap_url_search_st(LDAP *ld, const char *url,
	int attrsonly, struct timeval *timeout, LDAPMessage **res);

#ifdef	_SOLARIS_SDK
/*
 * Additional URL functions plus Character set, Search Preference
 * and Display Template functions moved from internal header files
 */

/*
 * URL functions
 */
char *ldap_dns_to_url(LDAP *ld, char *dns_name, char *attrs,
	char *scope, char *filter);
char *ldap_dn_to_url(LDAP *ld, char *dn, int nameparts);

/*
 * Character set functions
 */
#ifdef	STR_TRANSLATION
void ldap_set_string_translators(LDAP *ld,
	BERTranslateProc encode_proc, BERTranslateProc decode_proc);
int ldap_translate_from_t61(LDAP *ld, char **bufp,
	unsigned long *lenp, int free_input);
int ldap_translate_to_t61(LDAP *ld, char **bufp,
	unsigned long *lenp, int free_input);
void ldap_enable_translation(LDAP *ld, LDAPMessage *entry,
	int enable);
#ifdef	LDAP_CHARSET_8859
int ldap_t61_to_8859(char **bufp, unsigned long *buflenp,
	int free_input);
int ldap_8859_to_t61(char **bufp, unsigned long *buflenp,
	int free_input);
#endif	/* LDAP_CHARSET_8859 */
#endif	/* STR_TRANSLATION */

/*
 * Display Temple functions/structures
 */
/*
 * display template item structure
 */
struct ldap_tmplitem {
    unsigned long		ti_syntaxid;
    unsigned long		ti_options;
    char			*ti_attrname;
    char			*ti_label;
    char			**ti_args;
    struct ldap_tmplitem	*ti_next_in_row;
    struct ldap_tmplitem	*ti_next_in_col;
    void			*ti_appdata;
};

#define	NULLTMPLITEM	((struct ldap_tmplitem *)0)

#define	LDAP_SET_TMPLITEM_APPDATA(ti, datap)  \
	(ti)->ti_appdata = (void *)(datap)

#define	LDAP_GET_TMPLITEM_APPDATA(ti, type)   \
	(type)((ti)->ti_appdata)

#define	LDAP_IS_TMPLITEM_OPTION_SET(ti, option)       \
	(((ti)->ti_options & option) != 0)

/*
 * object class array structure
 */
struct ldap_oclist {
    char		**oc_objclasses;
    struct ldap_oclist	*oc_next;
};

#define	NULLOCLIST	((struct ldap_oclist *)0)


/*
 * add defaults list
 */
struct ldap_adddeflist {
    int			ad_source;
#define	LDAP_ADSRC_CONSTANTVALUE	1
#define	LDAP_ADSRC_ADDERSDN		2
    char		*ad_attrname;
    char		*ad_value;
    struct ldap_adddeflist	*ad_next;
};

#define	NULLADLIST	((struct ldap_adddeflist *)0)


/*
 * display template global options
 * if this bit is set in dt_options, it applies.
 */
/*
 * users should be allowed to try to add objects of these entries
 */
#define	LDAP_DTMPL_OPT_ADDABLE		0x00000001L

/*
 * users should be allowed to do "modify RDN" operation of these entries
 */
#define	LDAP_DTMPL_OPT_ALLOWMODRDN	0x00000002L

/*
 * this template is an alternate view, not a primary view
 */
#define	LDAP_DTMPL_OPT_ALTVIEW	0x00000004L


/*
 * display template structure
 */
struct ldap_disptmpl {
    char			*dt_name;
    char			*dt_pluralname;
    char			*dt_iconname;
    unsigned long		dt_options;
    char			*dt_authattrname;
    char			*dt_defrdnattrname;
    char			*dt_defaddlocation;
    struct ldap_oclist		*dt_oclist;
    struct ldap_adddeflist	*dt_adddeflist;
    struct ldap_tmplitem	*dt_items;
    void			*dt_appdata;
    struct ldap_disptmpl	*dt_next;
};

#define	NULLDISPTMPL	((struct ldap_disptmpl *)0)

#define	LDAP_SET_DISPTMPL_APPDATA(dt, datap)  \
	(dt)->dt_appdata = (void *)(datap)

#define	LDAP_GET_DISPTMPL_APPDATA(dt, type)   \
	(type)((dt)->dt_appdata)

#define	LDAP_IS_DISPTMPL_OPTION_SET(dt, option)       \
	(((dt)->dt_options & option) != 0)

#define	LDAP_TMPL_ERR_VERSION   1
#define	LDAP_TMPL_ERR_MEM	2
#define	LDAP_TMPL_ERR_SYNTAX    3
#define	LDAP_TMPL_ERR_FILE	4

/*
 * buffer size needed for entry2text and vals2text
 */
#define	LDAP_DTMPL_BUFSIZ	8192

typedef int (*writeptype)(void *writeparm, char *p, int len);

LDAP_API(int) LDAP_CALL ldap_init_templates(char *file,
    struct ldap_disptmpl **tmpllistp);

LDAP_API(int) LDAP_CALL ldap_init_templates_buf(char *buf, long buflen,
    struct ldap_disptmpl **tmpllistp);

LDAP_API(void) LDAP_CALL ldap_free_templates(struct ldap_disptmpl *tmpllist);

LDAP_API(struct ldap_disptmpl *) LDAP_CALL ldap_first_disptmpl(
    struct ldap_disptmpl *tmpllist);

LDAP_API(struct ldap_disptmpl *) LDAP_CALL ldap_next_disptmpl(
    struct ldap_disptmpl *tmpllist,
    struct ldap_disptmpl *tmpl);

LDAP_API(struct ldap_disptmpl *) LDAP_CALL ldap_name2template(char *name,
    struct ldap_disptmpl *tmpllist);

LDAP_API(struct ldap_disptmpl *) LDAP_CALL ldap_oc2template(char **oclist,
    struct ldap_disptmpl *tmpllist);

LDAP_API(char **) LDAP_CALL ldap_tmplattrs(struct ldap_disptmpl *tmpl,
    char **includeattrs, int exclude,
    unsigned long syntaxmask);

LDAP_API(struct ldap_tmplitem *) LDAP_CALL ldap_first_tmplrow(
    struct ldap_disptmpl *tmpl);

LDAP_API(struct ldap_tmplitem *) LDAP_CALL ldap_next_tmplrow(
    struct ldap_disptmpl *tmpl, struct ldap_tmplitem *row);

LDAP_API(struct ldap_tmplitem *) LDAP_CALL ldap_first_tmplcol(
    struct ldap_disptmpl *tmpl, struct ldap_tmplitem *row);

LDAP_API(struct ldap_tmplitem *) LDAP_CALL ldap_next_tmplcol(
    struct ldap_disptmpl *tmpl, struct ldap_tmplitem *row,
    struct ldap_tmplitem *col);

LDAP_API(int) LDAP_CALL ldap_entry2text(LDAP *ld, char *buf, LDAPMessage *entry,
    struct ldap_disptmpl *tmpl, char **defattrs, char ***defvals,
    writeptype writeproc, void *writeparm, char *eol, int rdncount,
    unsigned long opts);

LDAP_API(int) LDAP_CALL ldap_vals2text(LDAP *ld, char *buf, char **vals,
    char *label, int labelwidth,
    unsigned long syntaxid, writeptype writeproc, void *writeparm,
    char *eol, int rdncount);

LDAP_API(int) LDAP_CALL ldap_entry2text_search(LDAP *ld, char *dn, char *base,
    LDAPMessage *entry,
    struct ldap_disptmpl *tmpllist, char **defattrs, char ***defvals,
    writeptype writeproc, void *writeparm, char *eol, int rdncount,
    unsigned long opts);

LDAP_API(int) LDAP_CALL ldap_entry2html(LDAP *ld, char *buf, LDAPMessage *entry,
    struct ldap_disptmpl *tmpl, char **defattrs, char ***defvals,
    writeptype writeproc, void *writeparm, char *eol, int rdncount,
    unsigned long opts, char *urlprefix, char *base);

LDAP_API(int) LDAP_CALL ldap_vals2html(LDAP *ld, char *buf, char **vals,
    char *label, int labelwidth,
    unsigned long syntaxid, writeptype writeproc, void *writeparm,
    char *eol, int rdncount, char *urlprefix);

LDAP_API(int) LDAP_CALL ldap_entry2html_search(LDAP *ld, char *dn, char *base,
    LDAPMessage *entry,
    struct ldap_disptmpl *tmpllist, char **defattrs, char ***defvals,
    writeptype writeproc, void *writeparm, char *eol, int rdncount,
    unsigned long opts, char *urlprefix);

/*
 * Search Preference Definitions
 */

struct ldap_searchattr {
	char				*sa_attrlabel;
	char				*sa_attr;
					/* max 32 matchtypes for now */
	unsigned long			sa_matchtypebitmap;
	char				*sa_selectattr;
	char				*sa_selecttext;
	struct ldap_searchattr		*sa_next;
};

struct ldap_searchmatch {
	char				*sm_matchprompt;
	char				*sm_filter;
	struct ldap_searchmatch		*sm_next;
};

struct ldap_searchobj {
	char				*so_objtypeprompt;
	unsigned long			so_options;
	char				*so_prompt;
	short				so_defaultscope;
	char				*so_filterprefix;
	char				*so_filtertag;
	char				*so_defaultselectattr;
	char				*so_defaultselecttext;
	struct ldap_searchattr		*so_salist;
	struct ldap_searchmatch		*so_smlist;
	struct ldap_searchobj		*so_next;
};

#define	NULLSEARCHOBJ			((struct ldap_searchobj *)0)

/*
 * global search object options
 */
#define	LDAP_SEARCHOBJ_OPT_INTERNAL	0x00000001

#define	LDAP_IS_SEARCHOBJ_OPTION_SET(so, option)      \
	(((so)->so_options & option) != 0)

#define	LDAP_SEARCHPREF_VERSION_ZERO    0
#define	LDAP_SEARCHPREF_VERSION		1

#define	LDAP_SEARCHPREF_ERR_VERSION	1
#define	LDAP_SEARCHPREF_ERR_MEM		2
#define	LDAP_SEARCHPREF_ERR_SYNTAX	3
#define	LDAP_SEARCHPREF_ERR_FILE	4

LDAP_API(int) LDAP_CALL ldap_init_searchprefs(char *file,
    struct ldap_searchobj **solistp);

LDAP_API(int) LDAP_CALL ldap_init_searchprefs_buf(char *buf, long buflen,
    struct ldap_searchobj **solistp);

LDAP_API(void) LDAP_CALL ldap_free_searchprefs(struct ldap_searchobj *solist);

LDAP_API(struct ldap_searchobj *) LDAP_CALL ldap_first_searchobj(
    struct ldap_searchobj *solist);

LDAP_API(struct ldap_searchobj *) LDAP_CALL ldap_next_searchobj(
    struct ldap_searchobj *sollist, struct ldap_searchobj *so);

/*
 * specific LDAP instantiations of BER types we know about
 */

/* general stuff */
#define	LDAP_TAG_MESSAGE	0x30   /* tag is 16 + constructed bit */
#define	LDAP_TAG_MSGID		0x02   /* INTEGER */
#define	LDAP_TAG_CONTROLS	0xa0   /* context specific + constructed + 0 */
#define	LDAP_TAG_REFERRAL	0xa3   /* context specific + constructed + 3 */
#define	LDAP_TAG_NEWSUPERIOR    0x80   /* context specific + primitive + 0 */
#define	LDAP_TAG_SASL_RES_CREDS 0x87   /* context specific + primitive + 7 */
#define	LDAP_TAG_VLV_BY_INDEX   0xa0   /* context specific + constructed + 0 */
#define	LDAP_TAG_VLV_BY_VALUE   0x81   /* context specific + primitive + 1 */
/* tag for sort control */
#define	LDAP_TAG_SK_MATCHRULE   0x80L   /* context specific + primitive + 0 */
#define	LDAP_TAG_SK_REVERSE	0x81L   /* context specific + primitive + 1 */
#define	LDAP_TAG_SR_ATTRTYPE    0x80L   /* context specific + primitive + 0 */

/* possible operations a client can invoke */
#define	LDAP_REQ_BIND	0x60   /* application + constructed + 0 */
#define	LDAP_REQ_UNBIND		0x42   /* application + primitive   + 2 */
#define	LDAP_REQ_SEARCH		0x63   /* application + constructed + 3 */
#define	LDAP_REQ_MODIFY		0x66   /* application + constructed + 6 */
#define	LDAP_REQ_ADD		0x68   /* application + constructed + 8 */
#define	LDAP_REQ_DELETE		0x4a   /* application + primitive   + 10 */
#define	LDAP_REQ_MODRDN		0x6c   /* application + constructed + 12 */
#define	LDAP_REQ_MODDN		0x6c   /* application + constructed + 12 */
#define	LDAP_REQ_RENAME		0x6c   /* application + constructed + 12 */
#define	LDAP_REQ_COMPARE	0x6e   /* application + constructed + 14 */
#define	LDAP_REQ_ABANDON	0x50   /* application + primitive   + 16 */
#define	LDAP_REQ_EXTENDED	0x77   /* application + constructed + 23 */

/* U-M LDAP release 3.0 compatibility stuff */
#define	LDAP_REQ_UNBIND_30	0x62
#define	LDAP_REQ_DELETE_30	0x6a
#define	LDAP_REQ_ABANDON_30	0x70

/* U-M LDAP 3.0 compatibility auth methods */
#define	LDAP_AUTH_SIMPLE_30	0xa0   /* context specific + constructed */
#define	LDAP_AUTH_KRBV41_30	0xa1   /* context specific + constructed */
#define	LDAP_AUTH_KRBV42_30	0xa2   /* context specific + constructed */

/* filter types */
#define	LDAP_FILTER_AND		0xa0   /* context specific + constructed + 0 */
#define	LDAP_FILTER_OR		0xa1   /* context specific + constructed + 1 */
#define	LDAP_FILTER_NOT		0xa2   /* context specific + constructed + 2 */
#define	LDAP_FILTER_EQUALITY	0xa3   /* context specific + constructed + 3 */
#define	LDAP_FILTER_SUBSTRINGS	0xa4   /* context specific + constructed + 4 */
#define	LDAP_FILTER_GE		0xa5   /* context specific + constructed + 5 */
#define	LDAP_FILTER_LE		0xa6   /* context specific + constructed + 6 */
#define	LDAP_FILTER_PRESENT	0x87   /* context specific + primitive   + 7 */
#define	LDAP_FILTER_APPROX	0xa8   /* context specific + constructed + 8 */
#define	LDAP_FILTER_EXTENDED	0xa9   /* context specific + constructed + 0 */

/* U-M LDAP 3.0 compatibility filter types */
#define	LDAP_FILTER_PRESENT_30	0xa7   /* context specific + constructed */

/* substring filter component types */
#define	LDAP_SUBSTRING_INITIAL	0x80   /* context specific + primitive + 0 */
#define	LDAP_SUBSTRING_ANY	0x81   /* context specific + primitive + 1 */
#define	LDAP_SUBSTRING_FINAL    0x82   /* context specific + primitive + 2 */

/* U-M LDAP 3.0 compatibility substring filter component types */
#define	LDAP_SUBSTRING_INITIAL_30	0xa0   /* context specific */
#define	LDAP_SUBSTRING_ANY_30		0xa1   /* context specific */
#define	LDAP_SUBSTRING_FINAL_30		0xa2   /* context specific */

#endif	/* _SOLARIS_SDK */

/*
 * Function to dispose of an array of LDAPMod structures (an API extension).
 * Warning: don't use this unless the mods array was allocated using the
 * same memory allocator as is being used by libldap.
 */
LDAP_API(void) LDAP_CALL ldap_mods_free(LDAPMod **mods, int freemods);

/*
 * Preferred language and get_lang_values (an API extension --
 * LDAP_API_FEATURE_X_GETLANGVALUES)
 *
 * The following two APIs are deprecated
 */

char **LDAP_CALL ldap_get_lang_values(LDAP *ld, LDAPMessage *entry,
	const char *target, char **type);
struct berval **LDAP_CALL ldap_get_lang_values_len(LDAP *ld,
	LDAPMessage *entry, const char *target, char **type);


/*
 * Rebind callback function (an API extension)
 */
#define	LDAP_OPT_REBIND_FN		0x06	/* 6 - API extension */
#define	LDAP_OPT_REBIND_ARG		0x07	/* 7 - API extension */
typedef int (LDAP_CALL LDAP_CALLBACK LDAP_REBINDPROC_CALLBACK)(LDAP *ld,
	char **dnp, char **passwdp, int *authmethodp, int freeit, void *arg);
LDAP_API(void) LDAP_CALL ldap_set_rebind_proc(LDAP *ld,
    LDAP_REBINDPROC_CALLBACK *rebindproc, void *arg);

/*
 * Thread function callbacks (an API extension --
 * LDAP_API_FEATURE_X_THREAD_FUNCTIONS).
 */
#define	LDAP_OPT_THREAD_FN_PTRS		0x05	/* 5 - API extension */

/*
 * Thread callback functions:
 */
typedef void *(LDAP_C LDAP_CALLBACK LDAP_TF_MUTEX_ALLOC_CALLBACK)(void);
typedef void (LDAP_C LDAP_CALLBACK LDAP_TF_MUTEX_FREE_CALLBACK)(void *m);
typedef int (LDAP_C LDAP_CALLBACK LDAP_TF_MUTEX_LOCK_CALLBACK)(void *m);
typedef int (LDAP_C LDAP_CALLBACK LDAP_TF_MUTEX_UNLOCK_CALLBACK)(void *m);
typedef int (LDAP_C LDAP_CALLBACK LDAP_TF_GET_ERRNO_CALLBACK)(void);
typedef void (LDAP_C LDAP_CALLBACK LDAP_TF_SET_ERRNO_CALLBACK)(int e);
typedef int (LDAP_C LDAP_CALLBACK LDAP_TF_GET_LDERRNO_CALLBACK)(
	char **matchedp, char **errmsgp, void *arg);
typedef void    (LDAP_C LDAP_CALLBACK LDAP_TF_SET_LDERRNO_CALLBACK)(int err,
	char *matched, char *errmsg, void *arg);

/*
 * Structure to hold thread function pointers:
 */
struct ldap_thread_fns {
	LDAP_TF_MUTEX_ALLOC_CALLBACK *ltf_mutex_alloc;
	LDAP_TF_MUTEX_FREE_CALLBACK *ltf_mutex_free;
	LDAP_TF_MUTEX_LOCK_CALLBACK *ltf_mutex_lock;
	LDAP_TF_MUTEX_UNLOCK_CALLBACK *ltf_mutex_unlock;
	LDAP_TF_GET_ERRNO_CALLBACK *ltf_get_errno;
	LDAP_TF_SET_ERRNO_CALLBACK *ltf_set_errno;
	LDAP_TF_GET_LDERRNO_CALLBACK *ltf_get_lderrno;
	LDAP_TF_SET_LDERRNO_CALLBACK *ltf_set_lderrno;
	void    *ltf_lderrno_arg;
};

/*
 * Client side sorting of entries (an API extension --
 * LDAP_API_FEATURE_X_CLIENT_SIDE_SORT)
 */
/*
 * Client side sorting callback functions:
 */
typedef const struct berval *(LDAP_C LDAP_CALLBACK
	LDAP_KEYGEN_CALLBACK)(void *arg, LDAP *ld, LDAPMessage *entry);
typedef int (LDAP_C LDAP_CALLBACK
	LDAP_KEYCMP_CALLBACK)(void *arg, const struct berval *,
	const struct berval *);
typedef void (LDAP_C LDAP_CALLBACK
	LDAP_KEYFREE_CALLBACK)(void *arg, const struct berval *);
typedef int (LDAP_C LDAP_CALLBACK
	LDAP_CMP_CALLBACK)(const char *val1, const char *val2);
typedef int (LDAP_C LDAP_CALLBACK
	LDAP_VALCMP_CALLBACK)(const char **val1p, const char **val2p);

/*
 * Client side sorting functions:
 */
int LDAP_CALL ldap_multisort_entries(LDAP *ld, LDAPMessage **chain,
	char **attr, LDAP_CMP_CALLBACK *cmp);
int LDAP_CALL ldap_sort_entries(LDAP *ld, LDAPMessage **chain,
	char *attr, LDAP_CMP_CALLBACK *cmp);
int LDAP_CALL ldap_sort_values(LDAP *ld, char **vals,
	LDAP_VALCMP_CALLBACK *cmp);
int LDAP_C LDAP_CALLBACK ldap_sort_strcasecmp(const char **a,
	const char **b);


/*
 * Filter functions and definitions (an API extension --
 * LDAP_API_FEATURE_X_FILTER_FUNCTIONS)
 */
/*
 * Structures, constants, and types for filter utility routines:
 */
typedef struct ldap_filt_info {
	char			*lfi_filter;
	char			*lfi_desc;
	int			lfi_scope;	/* LDAP_SCOPE_BASE, etc */
	int			lfi_isexact;    /* exact match filter? */
	struct ldap_filt_info   *lfi_next;
} LDAPFiltInfo;

#define	LDAP_FILT_MAXSIZ	1024

typedef struct ldap_filt_list LDAPFiltList; /* opaque filter list handle */
typedef struct ldap_filt_desc LDAPFiltDesc; /* opaque filter desc handle */

/*
 * Filter utility functions:
 */
LDAP_API(LDAPFiltDesc *) LDAP_CALL ldap_init_getfilter(char *fname);
LDAP_API(LDAPFiltDesc *) LDAP_CALL ldap_init_getfilter_buf(char *buf,
    ssize_t buflen);
LDAP_API(LDAPFiltInfo *) LDAP_CALL ldap_getfirstfilter(LDAPFiltDesc *lfdp,
    char *tagpat, char *value);
LDAP_API(LDAPFiltInfo *) LDAP_CALL ldap_getnextfilter(LDAPFiltDesc *lfdp);
int LDAP_CALL ldap_set_filter_additions(LDAPFiltDesc *lfdp,
	char *prefix, char *suffix);
int LDAP_CALL ldap_create_filter(char *buf, unsigned long buflen,
	char *pattern, char *prefix, char *suffix, char *attr,
	char *value, char **valwords);
LDAP_API(void) LDAP_CALL ldap_getfilter_free(LDAPFiltDesc *lfdp);


/*
 * Friendly mapping structure and routines (an API extension)
 */
typedef struct friendly {
	char    *f_unfriendly;
	char    *f_friendly;
} *FriendlyMap;
char *LDAP_CALL ldap_friendly_name(char *filename, char *name,
	FriendlyMap *map);
LDAP_API(void) LDAP_CALL ldap_free_friendlymap(FriendlyMap *map);


/*
 * In Memory Cache (an API extension -- LDAP_API_FEATURE_X_MEMCACHE)
 */
typedef struct ldapmemcache  LDAPMemCache;  /* opaque in-memory cache handle */

int LDAP_CALL ldap_memcache_init(unsigned long ttl,
	unsigned long size, char **baseDNs, struct ldap_thread_fns *thread_fns,
	LDAPMemCache **cachep);
int LDAP_CALL ldap_memcache_set(LDAP *ld, LDAPMemCache *cache);
int LDAP_CALL ldap_memcache_get(LDAP *ld, LDAPMemCache **cachep);
LDAP_API(void) LDAP_CALL ldap_memcache_flush(LDAPMemCache *cache, char *dn,
    int scope);
LDAP_API(void) LDAP_CALL ldap_memcache_destroy(LDAPMemCache *cache);
LDAP_API(void) LDAP_CALL ldap_memcache_update(LDAPMemCache *cache);

/*
 * Server reconnect (an API extension).
 */
#define	LDAP_OPT_RECONNECT		0x62    /* 98 - API extension */

/*
 * Asynchronous I/O (an API extension).
 */
/*
 * This option enables completely asynchronous IO.  It works by using ioctl()
 * on the fd, (or tlook())
 */
#define	LDAP_OPT_ASYNC_CONNECT		0x63    /* 99 - API extension */

/*
 * I/O function callbacks option (an API extension --
 * LDAP_API_FEATURE_X_IO_FUNCTIONS).
 * Use of the extended I/O functions instead is recommended; see above.
 */
#define	LDAP_OPT_IO_FN_PTRS		0x0B    /* 11 - API extension */

/*
 * Extended I/O function callbacks option (an API extension --
 * LDAP_API_FEATURE_X_EXTIO_FUNCTIONS).
 */
#define	LDAP_X_OPT_EXTIO_FN_PTRS   (LDAP_OPT_PRIVATE_EXTENSION_BASE + 0x0F00)
	/* 0x4000 + 0x0F00 = 0x4F00 = 20224 - API extension */



/*
 * generalized bind
 */
/*
 * Authentication methods:
 */
#define	LDAP_AUTH_NONE		0x00
#define	LDAP_AUTH_SIMPLE	0x80
#define	LDAP_AUTH_SASL		0xa3
int LDAP_CALL ldap_bind(LDAP *ld, const char *who,
	const char *passwd, int authmethod);
int LDAP_CALL ldap_bind_s(LDAP *ld, const char *who,
	const char *cred, int method);

/*
 * experimental DN format support
 */
char **LDAP_CALL ldap_explode_dns(const char *dn);
int LDAP_CALL ldap_is_dns_dn(const char *dn);

#ifdef	_SOLARIS_SDK
char *ldap_dns_to_dn(char *dns_name, int *nameparts);
#endif


/*
 * user friendly naming/searching routines
 */
typedef int (LDAP_C LDAP_CALLBACK LDAP_CANCELPROC_CALLBACK)(void *cl);
int LDAP_CALL ldap_ufn_search_c(LDAP *ld, char *ufn,
	char **attrs, int attrsonly, LDAPMessage **res,
	LDAP_CANCELPROC_CALLBACK *cancelproc, void *cancelparm);
int LDAP_CALL ldap_ufn_search_ct(LDAP *ld, char *ufn,
	char **attrs, int attrsonly, LDAPMessage **res,
	LDAP_CANCELPROC_CALLBACK *cancelproc, void *cancelparm,
	char *tag1, char *tag2, char *tag3);
int LDAP_CALL ldap_ufn_search_s(LDAP *ld, char *ufn,
	char **attrs, int attrsonly, LDAPMessage **res);
LDAP_API(LDAPFiltDesc *) LDAP_CALL ldap_ufn_setfilter(LDAP *ld, char *fname);
LDAP_API(void) LDAP_CALL ldap_ufn_setprefix(LDAP *ld, char *prefix);
int LDAP_C ldap_ufn_timeout(void *tvparam);

/*
 * functions and definitions that have been replaced by new improved ones
 */
/*
 * Use ldap_get_option() with LDAP_OPT_API_INFO and an LDAPAPIInfo structure
 * instead of ldap_version(). The use of this API is deprecated.
 */
typedef struct _LDAPVersion {
	int sdk_version;	/* Version of the SDK, * 100 */
	int protocol_version;	/* Highest protocol version supported, * 100 */
	int SSL_version;	/* SSL version if this SDK supports it, * 100 */
	int security_level;	/* highest level available */
	int reserved[4];
} LDAPVersion;
#define	LDAP_SECURITY_NONE	0
int LDAP_CALL ldap_version(LDAPVersion *ver);

/* use ldap_create_filter() instead of ldap_build_filter() */
LDAP_API(void) LDAP_CALL ldap_build_filter(char *buf, size_t buflen,
    char *pattern, char *prefix, char *suffix, char *attr,
    char *value, char **valwords);
/* use ldap_set_filter_additions() instead of ldap_setfilteraffixes() */
LDAP_API(void) LDAP_CALL ldap_setfilteraffixes(LDAPFiltDesc *lfdp,
    char *prefix, char *suffix);

/* older result types a server can return -- use LDAP_RES_MODDN instead */
#define	LDAP_RES_MODRDN			LDAP_RES_MODDN
#define	LDAP_RES_RENAME			LDAP_RES_MODDN

/* older error messages */
#define	LDAP_AUTH_METHOD_NOT_SUPPORTED  LDAP_STRONG_AUTH_NOT_SUPPORTED

/* end of unsupported functions */

#ifdef	_SOLARIS_SDK

/* SSL Functions */

/*
 * these three defines resolve the SSL strength
 * setting auth weak, diables all cert checking
 * the CNCHECK tests for the man in the middle hack
 */
#define	LDAPSSL_AUTH_WEAK	0
#define	LDAPSSL_AUTH_CERT	1
#define	LDAPSSL_AUTH_CNCHECK    2

/*
 * Initialize LDAP library for SSL
 */
LDAP * LDAP_CALL ldapssl_init(const char *defhost, int defport,
	int defsecure);

/*
 * Install I/O routines to make SSL over LDAP possible.
 * Use this after ldap_init() or just use ldapssl_init() instead.
 */
int LDAP_CALL ldapssl_install_routines(LDAP *ld);


/*
 * The next three functions initialize the security code for SSL
 * The first one ldapssl_client_init() does initialization for SSL only
 * The next one supports ldapssl_clientauth_init() intializes security
 * for SSL for client authentication. The third function initializes
 * security for doing SSL with client authentication, and PKCS, that is,
 * the third function initializes the security module database(secmod.db).
 * The parameters are as follows:
 * const char *certdbpath - path to the cert file.  This can be a shortcut
 * to the directory name, if so cert7.db will be postfixed to the string.
 * void *certdbhandle - Normally this is NULL.  This memory will need
 * to be freed.
 * int needkeydb - boolean.  Must be ! = 0 if client Authentification
 * is required
 * char *keydbpath - path to the key database.  This can be a shortcut
 * to the directory name, if so key3.db will be postfixed to the string.
 * void *keydbhandle - Normally this is NULL, This memory will need
 * to be freed
 * int needsecmoddb - boolean.  Must be ! = 0 to assure that the correct
 * security module is loaded into memory
 * char *secmodpath - path to the secmod.  This can be a shortcut to the
 * directory name, if so secmod.db will be postfixed to the string.
 *
 * These three functions are mutually exclusive.  You can only call
 * one.  This means that, for a given process, you must call the
 * appropriate initialization function for the life of the process.
 */


/*
 * Initialize the secure parts (Security and SSL) of the runtime for use
 * by a client application.  This is only called once.
 */
int LDAP_CALL ldapssl_client_init(
    const char *certdbpath, void *certdbhandle);

/*
 * Initialize the secure parts (Security and SSL) of the runtime for use
 * by a client application that may want to do SSL client authentication.
 */
int LDAP_CALL ldapssl_clientauth_init(
    const char *certdbpath, void *certdbhandle,
    const int needkeydb, const char *keydbpath, void *keydbhandle);

/*
 * Initialize the secure parts (Security and SSL) of the runtime for use
 * by a client application that may want to do SSL client authentication.
 */
int LDAP_CALL ldapssl_advclientauth_init(
    const char *certdbpath, void *certdbhandle,
    const int needkeydb, const char *keydbpath, void *keydbhandle,
    const int needsecmoddb, const char *secmoddbpath,
    const int sslstrength);

/*
 * get a meaningful error string back from the security library
 * this function should be called, if ldap_err2string doesn't
 * identify the error code.
 */
const char *LDAP_CALL ldapssl_err2string(const int prerrno);

/*
 * Enable SSL client authentication on the given ld.
 */
int LDAP_CALL ldapssl_enable_clientauth(LDAP *ld, char *keynickname,
	char *keypasswd, char *certnickname);

typedef int (LDAP_C LDAP_CALLBACK LDAP_PKCS_GET_TOKEN_CALLBACK)
	(void *context, char **tokenname);
typedef int (LDAP_C LDAP_CALLBACK LDAP_PKCS_GET_PIN_CALLBACK)
	(void *context, const char *tokenname, char **tokenpin);
typedef int (LDAP_C LDAP_CALLBACK LDAP_PKCS_GET_CERTPATH_CALLBACK)
	(void *context, char **certpath);
typedef int (LDAP_C LDAP_CALLBACK LDAP_PKCS_GET_KEYPATH_CALLBACK)
	(void *context, char **keypath);
typedef int (LDAP_C LDAP_CALLBACK LDAP_PKCS_GET_MODPATH_CALLBACK)
	(void *context, char **modulepath);
typedef int (LDAP_C LDAP_CALLBACK LDAP_PKCS_GET_CERTNAME_CALLBACK)
	(void *context, char **certname);
typedef int (LDAP_C LDAP_CALLBACK LDAP_PKCS_GET_DONGLEFILENAME_CALLBACK)
	(void *context, char **filename);

#define	PKCS_STRUCTURE_ID 1
struct ldapssl_pkcs_fns {
    int local_structure_id;
    void *local_data;
    LDAP_PKCS_GET_CERTPATH_CALLBACK *pkcs_getcertpath;
    LDAP_PKCS_GET_CERTNAME_CALLBACK *pkcs_getcertname;
    LDAP_PKCS_GET_KEYPATH_CALLBACK *pkcs_getkeypath;
    LDAP_PKCS_GET_MODPATH_CALLBACK *pkcs_getmodpath;
    LDAP_PKCS_GET_PIN_CALLBACK *pkcs_getpin;
    LDAP_PKCS_GET_TOKEN_CALLBACK *pkcs_gettokenname;
    LDAP_PKCS_GET_DONGLEFILENAME_CALLBACK *pkcs_getdonglefilename;

};


int LDAP_CALL ldapssl_pkcs_init(const struct ldapssl_pkcs_fns *pfns);

/* end of SSL functions */
#endif	/* _SOLARIS_SDK */

/* SASL options */
#define	LDAP_OPT_X_SASL_MECH		0x6100
#define	LDAP_OPT_X_SASL_REALM		0x6101
#define	LDAP_OPT_X_SASL_AUTHCID		0x6102
#define	LDAP_OPT_X_SASL_AUTHZID		0x6103
#define	LDAP_OPT_X_SASL_SSF		0x6104 /* read-only */
#define	LDAP_OPT_X_SASL_SSF_EXTERNAL	0x6105 /* write-only */
#define	LDAP_OPT_X_SASL_SECPROPS	0x6106 /* write-only */
#define	LDAP_OPT_X_SASL_SSF_MIN		0x6107
#define	LDAP_OPT_X_SASL_SSF_MAX		0x6108
#define	LDAP_OPT_X_SASL_MAXBUFSIZE	0x6109

/*
 * ldap_interactive_bind_s Interaction flags
 *  Interactive: prompt always - REQUIRED
 */
#define	LDAP_SASL_INTERACTIVE		1U

/*
 * V3 SASL Interaction Function Callback Prototype
 *      when using SASL, interact is pointer to sasl_interact_t
 *  should likely passed in a control (and provided controls)
 */
typedef int (LDAP_SASL_INTERACT_PROC)
	(LDAP *ld, unsigned flags, void* defaults, void *interact);

int LDAP_CALL ldap_sasl_interactive_bind_s(LDAP *ld, const char *dn,
	const char *saslMechanism, LDAPControl **serverControls,
	LDAPControl **clientControls, unsigned flags,
	LDAP_SASL_INTERACT_PROC *proc, void *defaults);
#endif	/* 0 */

#ifdef	__cplusplus
}
#endif

#endif	/* _SOLARIS_LDAP_H */
