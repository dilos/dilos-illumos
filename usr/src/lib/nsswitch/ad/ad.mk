LIB_NAME= nss_ad
SHLIB_VERSION= .1

SRCS= getpwnam.c getspent.c getgrent.c ad_common.c


#LIBRARY =	libnss_ad.a
#VERS =		.1
#OBJECTS =	getpwnam.o getspent.o getgrent.o ad_common.o
SRC=../../..
IDMAP_PROT_DIR =	$(SRC)/head/rpcsvc

# include common nsswitch library definitions.
#include		../../Makefile.com

CPPFLAGS +=	-I../../../libadutils/common -I../../../libidmap/common \
		-I$(IDMAP_PROT_DIR)
LDADD +=	-ladutils -lidmap
#DYNLIB1 =	nss_ad.so$(VERS)
