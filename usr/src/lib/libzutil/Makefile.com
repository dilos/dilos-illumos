#
# This file and its contents are supplied under the terms of the
# Common Development and Distribution License ("CDDL"), version 1.0.
# You may only use this file in accordance with the terms of version
# 1.0 of the CDDL.
#
# A full copy of the text of the CDDL should have accompanied this
# source.  A copy of the CDDL is also available via the Internet at
# http://www.illumos.org/license/CDDL.
#

#
# Copyright 2019 DilOS Team
#

LIBRARY= libzutil.a
VERS= .1

OBJS_COMMON=			\
	gethostid.o		\
	zutil_device_path.o	\
	zutil_device_path_os.o	\
	zutil_highlowbits.o	\
	zutil_import.o		\
	zutil_import_os.o	\
	zutil_nicenum.o		\
	zutil_pool.o		\
	zutil_syslog.o

OBJECTS= $(OBJS_COMMON)

include ../../Makefile.lib

# libzutil must be installed in the root filesystem for mount(8)
include ../../Makefile.rootfs

LIBS=	$(DYNLIB)

SRCDIR =	../common

INCS += -I$(SRCDIR)
INCS += -I$(SRC)/lib/libzpool/common
INCS += -I$(SRC)/uts/common/fs/zfs
INCS += -I../../../common/zfs
INCS += -I../../libc/inc

CSTD=	$(CSTD_GNU99)
LDLIBS +=	-lnvpair -lavl -lefi
LDLIBS +=	-ladm
LDLIBS +=	-lc
CPPFLAGS += $(INCS) -D_REENTRANT
$(NOT_RELEASE_BUILD)CPPFLAGS += -DDEBUG -DZFS_DEBUG

CERRWARN += -_gcc=-Wwrite-strings

# not linted
SMATCH=off

SRCS=	$(OBJS_COMMON:%.o=$(SRCDIR)/%.c)

.KEEP_STATE:

all: $(LIBS)

pics/%.o: ../../../common/zfs/%.c
	$(COMPILE.c) -o $@ $<
	$(POST_PROCESS_O)

include ../../Makefile.targ
