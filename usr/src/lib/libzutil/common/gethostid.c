#include <stdlib.h>
#include <unistd.h>

ulong_t
get_system_hostid(void)
{
	char *env;

	/*
	 * Allow the hostid to be subverted for testing.
	 */
	env = getenv("ZFS_HOSTID");
	if (env) {
		ulong_t hostid = strtoull(env, NULL, 16);
		return (hostid & 0xFFFFFFFF);
	}

	return (gethostid());
}
