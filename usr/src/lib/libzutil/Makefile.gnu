#
# This file and its contents are supplied under the terms of the
# Common Development and Distribution License ("CDDL"), version 1.0.
# You may only use this file in accordance with the terms of version
# 1.0 of the CDDL.
#
# A full copy of the text of the CDDL should have accompanied this
# source.  A copy of the CDDL is also available via the Internet at
# http://www.illumos.org/license/CDDL.
#

#
# Copyright 2019 DilOS Team
#

LIB=libzutil.so.1
VPATH=common
SRCTREE=../../../..
# ../../common/zfs
SRCS=zutil_device_path.c zutil_device_path_os.c zutil_import.c \
zutil_import_os.c zutil_nicenum.c zutil_pool.c gethostid.c \
zutil_syslog.c zutil_highlowbits.c zutil_compat.c


OBJS=$(SRCS:%.c=%.o)
CC=gcc
CFLAGS=-fno-builtin -D__sun -D__dilos__ -D__dilos \
-O0 -ggdb3 -m64 -Ui386 -U__i386 -fpic \
-std=gnu99 -DTEXT_DOMAIN=\"SUNW_OST_OSLIB\" -D_TS_ERRNO \
-I$(SRCTREE)/proto/root_i386/usr/include \
-Icommon -I$(SRCTREE)/usr/src/uts/common/fs/zfs \
-I../../common/zfs -I../libc/inc -D_LARGEFILE64_SOURCE=1 -D_REENTRANT \
-DPIC

LDFLAGS= -Wl,-zdefs\
-L$(SRCTREE)/proto/root_i386/lib/amd64 \
-L$(SRCTREE)/proto/root_i386/usr/lib/amd64 \
-ladm -lnvpair -lavl -lefi -lblkid -lc

all: $(LIB)

$(LIB): $(OBJS)
	$(CC) -o amd64/$@ -shared -h $@ $^ $(LDFLAGS)

clean:
	$(RM) $(OBJS) amd64/$(LIB)
