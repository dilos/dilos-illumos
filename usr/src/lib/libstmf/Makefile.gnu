LIB=libstmf.so.1

SRCTREE= ../..
PROTODIR= $(SRCTREE)/../../proto/root_i386
VPATH= common
SRCS= stmf.c store.c

OBJS= $(SRCS:%.c=%.o)
CC= gcc

CFLAGS+= -D__sun -D__dilos__ -D__dilos -O0 -ggdb3 -Ui386 -U__i386 -fpic -std=gnu99 \
-DTEXT_DOMAIN=\"SUNW_OST_OSLIB\" -D_TS_ERRNO \
-I$(PROTODIR)/usr/include -Icommon -D_REENTRANT -DPIC -D_REENTRANT

LDFLAGS+= -nodefaultlibs -Wl,-ztext -Wl,-zdefs \
-L$(PROTODIR)/usr/lib/amd64 -L$(PROTODIR)/lib/amd64 \
-lc -lnvpair -lscf

all: $(LIB)

$(LIB): $(OBJS)
	$(CC) -o amd64/$@ -shared -h $@ $^ $(LDFLAGS)

clean:
	$(RM) $(LIB) $(OBJS)
