/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the "License").
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
 * or https://opensource.org/licenses/CDDL-1.0.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/OPENSOLARIS.LICENSE.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets "[]" replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright (c) 2024 DilOS Team
 */

#ifndef	SQLLITE_ADAP_H
#define	SQLLITE_ADAP_H

#include <sqlite3.h>

/* Objects */
#define sqlite_temp_directory		sqlite3_temp_directory /* deprecated */

/* Types */
#define	sqlite				sqlite3
#define sqlite_callback			sqlite3_callback

/* Routines */
#define	sqlite_freemem(PTR)		sqlite3_free(PTR)
#define	sqlite_close(DB)		sqlite3_close(DB)
#define	sqlite_exec(DB, SQL, CB, ARG, ERR)		\
	sqlite3_exec(DB, SQL, CB, ARG, ERR)
#define	sqlite_vmprintf(F, AP)		sqlite3_vmprintf(F, AP)
#define	sqlite_mprintf(F, ...)		sqlite3_mprintf(F, __VA_ARGS__)
#define	sqlite_trace(DB, A, B)		sqlite3_trace(DB, A, B)
#define	sqlite_busy_timeout(DB, MS)	sqlite3_busy_timeout(DB, MS)
#define	sqlite_get_table(DB, SQL, RES, R, C, ERR)	\
	sqlite3_get_table(DB, SQL, RES, R, C, ERR)
#define	sqlite_free_table(RES)		sqlite3_free_table(RES)

sqlite *sqlite_open(const char *, int, char **);

int sqlite_exec_printf(
    sqlite *db,			/* An open database */
    const char *,		/* printf-style format string for the SQL */
    sqlite_callback,		/* Callback function */
    void *,			/* 1st argument to callback function */
    char **,			/* Error msg written here */
    ...				/* Arguments to the format string. */
);

int sqlite_exec_vprintf(
    sqlite *db,			/* An open database */
    const char *sqlFormat,	/* printf-style format string for the SQL */
    sqlite_callback,		/* Callback function */
    void *,			/* 1st argument to callback function */
    char **errmsg,		/* Error msg written here */
    va_list ap			/* Arguments to the format string. */
);

struct sqlite_vm {
	sqlite3_stmt	*vm_stmt;
	sqlite3		*vm_db;
	size_t		vm_cols;
	const char	**vm_value;
};

typedef struct sqlite_vm sqlite_vm;

int sqlite_compile(
    sqlite *,			/* An open database */
    const char *,		/* The SQL to be compiled */
    const char **,		/* OUT: Part of zSQL not compiled */
    sqlite_vm **,		/* OUT: Virtual machine written here */
    char **			/* OUT: Error message written here */
);

int sqlite_step(
    sqlite_vm *,		/* The virtual machine to execute */
    int *,			/* OUT: Number of columns in result */
    const char ***,		/* OUT: Column data */
    const char ***		/* OUT: Column names and datatypes */
);

int sqlite_finalize(sqlite_vm *, char **);

void sqlite_busy_handler(sqlite*, int(*)(void*,const char*,int), void*);

#endif	/* SQLLITE_ADAP_H */
