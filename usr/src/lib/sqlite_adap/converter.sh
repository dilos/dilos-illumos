#!/bin/bash

# Restore  db-files from dump file

# Parameters:
# $1 - list of masks for search
#

if [ $# -lt 1 ]; then
	echo "Use:"
	echo "$0 \"/dir1/* /dir2/*.db\""
	exit 1
fi 

MASKS="$1"

DEBUG=1

# Use util
SQLITE3=sqlite3

#
# Output debug message
#
function debug_msg {
	local msg="$1"

	if [ $DEBUG -ne 0 ]; then
		echo "$msg"
	fi
}

#
# Converter
# $1 - full path of db file
# return:
# 0 - converted
# 1 - no db ver 2
#
function convert {
	local dbfile=$1
	local dumpfile=$dbfile.dump
	local tmpdbfile=$dbfile.$$
	local dbfile_type

	case $dbfile in *.dump) return 1 ;; esac			# skip *.dump file
	[ -L $dbfile ] && return 1					# skip symlink
	[ -f $dumpfile ] || return 1					# skip no dump file
	[ "** This file co" = "`head -c 15 $dbfile`" ] || return 1	# skip no db v2 file

	cat $dumpfile | sed 's/lower_utf8/lower/' | \
	    sed -r 's/INSERT INTO value_tbl VALUES\((.*),(.*),([0-9]+),(.*)\)\;/INSERT INTO value_tbl VALUES\(\1,\2,"\3",\4\)\;/' | \
	    $SQLITE3 $tmpdbfile || return 1	# error of convert

	chmod --reference=$dbfile $tmpdbfile
	chown --reference=$dbfile $tmpdbfile
	mv $tmpdbfile $dbfile
	rm -f $dumpfile

	return 0
}

#
# Converter with debug output 
#
function convert_debug {
	local dbfile=$1
	local res

	convert "$dbfile"
	res=$?
	if [ $res -eq 0 ]; then
		debug_msg "Converted $dbfile"
	elif [ $res -eq 1 ]; then
		debug_msg "Skip $dbfile, no db file of version 2"
	else
		debug_msg "Unknow error of $dbfile"
	fi
}

debug_msg "sqlite converter DEBUG is ON"

for mask in $MASKS; do
	for f in $mask; do
		if [ -f $f ]; then
			convert_debug "$f"
		fi
	done
done

exit 0
