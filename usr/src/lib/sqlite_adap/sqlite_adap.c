/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the "License").
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
 * or https://opensource.org/licenses/CDDL-1.0.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/OPENSOLARIS.LICENSE.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets "[]" replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright (c) 2024 DilOS Team
 */

#include <assert.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include "sqlite_adap.h"	/* includes sqlite3.h */

/*
 * strdup via sqlite3_malloc
 * free with sqlite_free
 */
static char *
sqlite3_strdup(const char *str)
{
	size_t len = strlen(str) + 1;
	char *s = sqlite3_malloc(len);
	memcpy(s, str, len);

	return (s);
}

sqlite *
sqlite_open(const char *path, int mask, char **err)
{
	struct sqlite3 *db;
	struct stat buf;

	/* change mask only for create */
	if (stat(path, &buf) == 0)
		mask = 0;

	int r = sqlite3_open_v2(path, &db,
	    SQLITE_OPEN_READWRITE|SQLITE_OPEN_CREATE|SQLITE_OPEN_FULLMUTEX,
	     NULL);
	if (r != SQLITE_OK) {
		if (err != NULL)
			*err = sqlite3_strdup(sqlite3_errmsg(db));
		sqlite3_close(db);
		db = NULL;
	}

	if (mask != 0 && stat(path, &buf) == 0 && mask != buf.st_mode)
		(void)chmod(path, mask);

	return (db);
}

int
sqlite_exec_printf(sqlite *db, const char *sqlFormat, sqlite_callback cb,
    void *arg, char **errmsg, ...)
{
	va_list args;

	va_start(args, errmsg);
	int ret = sqlite_exec_vprintf(db, sqlFormat, cb, arg, errmsg, args);
	va_end(args);

	return (ret);
}

int
sqlite_exec_vprintf(sqlite *db, const char *sqlFormat, sqlite_callback cb,
    void *arg, char **errmsg, va_list ap)
{
	char *sql = sqlite3_vmprintf(sqlFormat, ap);
	int ret = sqlite3_exec(db, sql, cb, arg, errmsg);
	sqlite3_free(sql);

	return (ret);
}

int
sqlite_compile(sqlite *db, const char *sql, const char **tail,
    sqlite_vm **pvm, char **err)
{
	sqlite3_stmt *stmt = NULL;

	/* This adapter is't supported returning tail */
	assert(tail == NULL);

	int res = sqlite3_prepare_v2(db, sql, -1, &stmt, tail);
	if (res != SQLITE_OK) {
		if (err != NULL)
			*err = sqlite3_strdup(sqlite3_errmsg(db));

		*pvm = NULL;

		return (res);
	}

	sqlite_vm *vm = malloc(sizeof (*vm));
	assert(vm != NULL);
	vm->vm_stmt = stmt;
	vm->vm_db = db;
	vm->vm_value = NULL;
	vm->vm_cols = sqlite3_column_count(stmt);
	*pvm = vm;

	return (res);
}

int
sqlite_step(sqlite_vm *vm, int *pcols, const char ***pvalue,
    const char ***pname)
{
	int res = sqlite3_step(vm->vm_stmt);
	if (res == SQLITE_ROW || res == SQLITE_DONE) {
		if (vm->vm_value == NULL) {
			vm->vm_value = calloc(2 * vm->vm_cols + 1,
			    sizeof (const char *));
			assert(vm->vm_value != NULL);
			for (size_t i = 0; i < vm->vm_cols; i++)
				vm->vm_value[i] = (const char *)
				    sqlite3_column_name(vm->vm_stmt, i);
		}
		if (pname != NULL)
			*pname = vm->vm_value;
	}

	if (res == SQLITE_ROW) {
		for (size_t i = 0; i < vm->vm_cols; i++)
			vm->vm_value[i + vm->vm_cols] = (const char *)
			    sqlite3_column_text(vm->vm_stmt, i);
		if (pvalue != NULL)
			*pvalue = &vm->vm_value[vm->vm_cols];
	}

	if (pcols != NULL)
		*pcols = vm->vm_cols;

	return (res);
}

int
sqlite_finalize(sqlite_vm *vm, char **err)
{
	free(vm->vm_value);

	int res = sqlite3_finalize(vm->vm_stmt);
	if (res != SQLITE_OK) {
		if (err != NULL)
			*err = sqlite3_strdup(sqlite3_errmsg(vm->vm_db));
	}
	free(vm);

	return (res);
}

typedef struct sqlite3_busy_handler_cb_t {
	int (*bh_cb)(void *, const char *, int);
	void* bh_arg;
} sqlite3_busy_handler_cb_t;

static int
sqlite3_busy_handler_cb(void *arg, int count)
{
	sqlite3_busy_handler_cb_t *busy = arg;

	/* This adapter does not use second argument (table name) */
	return (busy->bh_cb(busy->bh_arg, NULL, count));
}

void
sqlite_busy_handler(sqlite *db, int (*cb)(void *, const char *, int), void *arg)
{
	sqlite3_busy_handler_cb_t busy = {
		.bh_cb = cb,
		.bh_arg = arg
	};

	sqlite3_busy_handler(db, sqlite3_busy_handler_cb, &busy);
}
