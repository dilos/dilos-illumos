LIB=ses.so

VPATH= ../disk
SRCS= ses.c ses_facility.c disk_common.c

OBJS=$(SRCS:%.c=%.o)

SRCTREE=../../../../../../../..
PROTO= $(SRCTREE)/proto/root_i386
CC= gcc


CFLAGS+= -ggdb3 -O0 -fpic -nodefaultlibs -D__sun -D__dilos__ -D__dilos \
-Ui386 -U__i386 -std=gnu99 -DTEXT_DOMAIN=\"SUNW_OST_OSLIB\" -D_TS_ERRNO \
-I$(PROTO)/usr/include -I. -D_POSIX_PTHREAD_SEMANTICS -D_REENTRANT -I../disk

LDFLAGS+= -nodefaultlibs -shared -Wl,-zignore -h -Wl,-ztext -Wl,-zdefs \
-R/usr/lib/fm/amd64 \
-L$(PROTO)/lib/amd64 -L$(PROTO)/usr/lib/amd64 \
-L$(PROTO)/usr/lib/fm/amd64 -ltopo -lnvpair -lc \
-L$(PROTO)/usr/lib/scsi/amd64 -R/usr/lib/scsi/amd64 \
-lses -ldevinfo -ldevid -ldiskstatus -lcontract -lsysevent -ldiskmgt

all: $(LIB)


$(LIB): $(OBJS)
	$(CC) -o $@ $^ $(LDFLAGS)

clean:
	$(RM) $(OBJS) $(LIB)
