%{
/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License, Version 1.0 only
 * (the "License").  You may not use this file except in compliance
 * with the License.
 *
 * You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/OPENSOLARIS.LICENSE.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets "[]" replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 *
 * Copyright (c) 2000 by Sun Microsystems, Inc.
 * All rights reserved.
 */

#include <string.h>

/* this is the lexer for the libfru NamingSyntax */
/* parser.h MUST occur before nameSyntax.tab.h */
#include "Parser.h"

extern char *gParserString;

#define YY_INPUT(buf, result, max_size) do { \
    int c = *gParserString++; \
    result = (c) ? (buf[0] = c, 1) : YY_NULL; \
    } while (0)

#ifdef  __cplusplus
extern "C" {
#endif
extern void fruerror(const char *msg);
extern int frulex(void);
extern int fruparse(void);
#ifdef  __cplusplus

#include "nameSyntaxYacc.hh"
}
#endif

/* Keep the separator symbols here because the lexer is the only one who knows
 * about the string being parsed. */

%}

%option noinput
%option nounput
%option noyywrap
%option prefix="fru"

%%

"$" { return LAST; }
"+" { return ADD; }
"/" { return SEPIDENT; }
"[" { return ITERBEGIN; }
"]" { return ITEREND; }

[0-9]+ { frulval.num = atoi (frutext); return NUMBER; }
[_a-zA-Z0-9]+ { frulval.name = strdup(frutext); return NAME; }

%%
