#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the "License").
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/OPENSOLARIS.LICENSE.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets "[]" replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#
#
# Copyright (c) 2005, 2010, Oracle and/or its affiliates. All rights reserved.
# Copyright 2016 Igor Kozhukhov <ikozhukhov@gmail.com>
# Copyright (c) 2011, 2017 by Delphix. All rights reserved.
# Copyright (c) 2018, Joyent, Inc.
#

LIBRARY= libzfs.a
VERS= .1

amd64_OBJS_SHARED=		\
	zfs_fletcher_intel.o	\
	zfs_fletcher_sse.o	\
	zfs_fletcher_avx512.o

OBJS_SHARED=			\
	cityhash.o		\
	zfeature_common.o	\
	zfs_comutil.o		\
	zfs_deleg.o		\
	zfs_fletcher.o		\
	zfs_fletcher_superscalar.o \
	zfs_fletcher_superscalar4.o \
	zfs_namecheck.o		\
	zfs_prop.o		\
	zpool_prop.o		\
	zprop_common.o		\
	$($(MACH64)_OBJS_SHARED)

OBJS_COMMON=			\
	libzfs_changelist.o	\
	libzfs_config.o		\
	libzfs_crypto.o		\
	libzfs_dataset.o	\
	libzfs_diff.o		\
	libzfs_fru.o		\
	libzfs_import.o		\
	libzfs_iter.o		\
	libzfs_mount.o		\
	libzfs_mount_os.o	\
	libzfs_pool.o		\
	libzfs_pool_os.o	\
	libzfs_sendrecv.o	\
	libzfs_status.o		\
	libzfs_util.o		\
	libzfs_util_os.o	\
	libzfs_taskq.o

OBJECTS= $(OBJS_COMMON) $(OBJS_SHARED)

include ../../Makefile.lib

# libzfs must be installed in the root filesystem for mount(8)
include ../../Makefile.rootfs

LIBS=	$(DYNLIB)

SRCDIR =	../common

INCS += -I$(SRCDIR)
#INCS += -I$(SRC)/lib/libzpool/common
INCS += -I$(SRC)/uts/common/fs/zfs
INCS += -I../../../common/zfs
INCS += -I../../libc/inc

CSTD =	$(CSTD_GNU99)
LDLIBS +=	-ldevid -lgen -lnvpair -luutil -lavl \
	-lidmap -ltsol -lumem -lzfs_core
LDLIBS += -lefi -lz
LDLIBS += -lcrypto
LDLIBS += -lzutil
LDLIBS += -lc

# To support the http protocol in crypto,
# the developer version libcurl has to be installed
HAVE_CURL:sh= test -f /usr/include/curl/curl.h || echo \#
${HAVE_CURL}CPPFLAGS += -DLIBFETCH_DYNAMIC=1 -DLIBFETCH_IS_LIBCURL=1 \
	-DLIBFETCH_SONAME=\"libcurl.so.4\"
${HAVE_CURL}INCS += -I$(ADJUNCT_PROTO)/usr/include

CPPFLAGS += $(INCS) -D_REENTRANT

amd64_CPPFLAGS_OPTS= -DHAVE_SSE2 -DHAVE_SSSE3 \
	-DHAVE_AVX -DHAVE_AVX2 \
	-DHAVE_AVX512F -DHAVE_AVX512BW

CPPFLAGS += $($(MACH64)_CPPFLAGS_OPTS)

$(NOT_RELEASE_BUILD)CPPFLAGS += -DDEBUG -DZFS_DEBUG
$(RELEASE_BUILD)CPPFLAGS +=     -DNDEBUG

CERRWARN += -_gcc=-Wwrite-strings

# not linted
SMATCH=off

# There's no lint library for zlib, so only include this when building
#$(DYNLIB) := LDLIBS +=	-lz

SRCS=	$(OBJS_COMMON:%.o=$(SRCDIR)/%.c)	\
	$(OBJS_SHARED:%.o=$(SRC)/common/zfs/%.c)

.KEEP_STATE:

all: $(LIBS)

lint: lintcheck

pics/%.o: ../../../common/zfs/%.c
	$(COMPILE.c) -o $@ $<
	$(POST_PROCESS_O)

include ../../Makefile.targ
