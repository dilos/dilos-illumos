LIB=libzfs.so.1
VPATH=common ../../common/zfs
SRCTREE=../../../..

SRCS=libzfs_changelist.c libzfs_config.c libzfs_crypto.c libzfs_dataset.c \
libzfs_diff.c libzfs_fru.c libzfs_import.c libzfs_iter.c libzfs_mount.c \
libzfs_pool.c libzfs_sendrecv.c libzfs_status.c libzfs_util.c libzfs_taskq.c \
zfeature_common.c zfs_comutil.c zfs_deleg.c zfs_fletcher.c zfs_namecheck.c \
zfs_prop.c zpool_prop.c zprop_common.c zfs_fletcher_superscalar.c \
zfs_fletcher_superscalar4.c

OBJS=$(SRCS:%.c=%.o)
CC=gcc
CFLAGS=-fno-builtin -nodefaultlibs -D__sun -D__dilos__ -D__dilos \
-O0 -ggdb3 -m64 -Ui386 -U__i386 -fpic \
-std=gnu99 -DTEXT_DOMAIN=\"SUNW_OST_OSLIB\" -D_TS_ERRNO \
-I$(SRCTREE)/proto/root_i386/usr/include \
-Icommon -I$(SRCTREE)/usr/src/uts/common/fs/zfs \
-I../../common/zfs -I../libzutil/common -I../libc/inc \
-D_LARGEFILE64_SOURCE=1 -D_REENTRANT -DDEBUG -DZFS_DEBUG -DPIC

LDFLAGS=-m64 -Wl,-zdefs -Wl,-ztext\
-L$(SRCTREE)/proto/root_i386/lib/amd64 \
-L$(SRCTREE)/proto/root_i386/usr/lib/amd64 \
-lc -ldevid -lgen -lnvpair -luutil -lavl -lefi -ladm -lidmap -ltsol \
-lcryptoutil -lpkcs11 -lumem -lzfs_core -lzutil -lcrypto -lz

all: $(LIB)

$(LIB): $(OBJS)
	$(CC) -o amd64/$@ -shared -h $@ $^ $(LDFLAGS)

clean:
	$(RM) $(OBJS) $(LIB)
