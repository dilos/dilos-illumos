/*
 * This file and its contents are supplied under the terms of the
 * Common Development and Distribution License ("CDDL"), version 1.0.
 * You may only use this file in accordance with the terms of version
 * 1.0 of the CDDL.
 *
 * A full copy of the text of the CDDL should have accompanied this
 * source.  A copy of the CDDL is also available via the Internet at
 * http://www.illumos.org/license/CDDL.
 */

/*
 * Copyright (c) 2018 DilOS
 */

#include <sys/kmem.h>

boolean_t
kmem_cache_reap_active(void)
{
	return (B_FALSE);
}

/* ARGSUSED */
void
kmem_cache_reap_soon(kmem_cache_t *kc)
{
}
