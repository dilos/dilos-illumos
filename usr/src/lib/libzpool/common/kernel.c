/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the "License").
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
 * or https://opensource.org/licenses/CDDL-1.0.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/OPENSOLARIS.LICENSE.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets "[]" replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/*
 * Copyright (c) 2005, 2010, Oracle and/or its affiliates. All rights reserved.
 * Copyright (c) 2013, Joyent, Inc.  All rights reserved.
 * Copyright (c) 2012, 2018 by Delphix. All rights reserved.
 * Copyright (c) 2016 Actifio, Inc. All rights reserved.
 */

#include <assert.h>
#include <fcntl.h>
#include <libgen.h>
#include <poll.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <libzutil.h>
#include <sys/processor.h>
#include <sys/rrwlock.h>
#include <sys/spa.h>
#include <sys/stat.h>
#include <sys/systeminfo.h>
#include <sys/utsname.h>
#include <sys/zfs_context.h>
#include <sys/zfs_file.h>
#include <sys/zmod.h>
#include <sys/zfs_vfsops.h>
#include <sys/zstd/zstd.h>
#include <sys/vdev_raidz.h>
#include <sys/vdev_raidz_impl.h>
#include <zfs_fletcher.h>
#include <sys/crypto/common.h>
#include <sys/crypto/impl.h>
#include <sys/crypto/api.h>
#include <sys/sha2.h>
#include <crypto/aes/aes_impl.h>
#include <libzfs.h>

//#ifndef	_KTHREAD_INVALID
//#define	_KTHREAD_INVALID	((void *)(uintptr_t)-1)
//#endif

#define VNTOZFS(vp) ((zfs_file_t *)((vp)->v_data))

extern void system_taskq_init(void);
extern void system_taskq_fini(void);

/*
 * Emulation of kernel services in userland.
 */

int aok;
uint64_t physmem;
vnode_t *rootdir = (vnode_t *)0xabcd1234;
char hw_serial[HW_HOSTID_LEN];
kmutex_t cpu_lock;
vmem_t *zio_arena = NULL;

/* If set, all blocks read will be copied to the specified directory. */
char *vn_dumpdir = NULL;

struct utsname hw_utsname = {
	"userland", "libzpool", "1", "1", "na"
};

/* this only exists to have its address taken */
struct proc p0;

/*
 * =========================================================================
 * threads
 * =========================================================================
 *
 * TS_STACK_MIN is dictated by the minimum allowed pthread stack size.  While
 * TS_STACK_MAX is somewhat arbitrary, it was selected to be large enough for
 * the expected stack depth while small enough to avoid exhausting address
 * space with high thread counts.
 */
/*
 * FIXME: PTHREAD_STACK_MIN is undefined.
 * limits.h has unfulfilled conditions
 */
// #define	TS_STACK_MIN	MAX(PTHREAD_STACK_MIN, 32768)
// #define	TS_STACK_MAX	(256 * 1024)

struct zk_thread_wrapper {
	void (*func)(void *);
	void *arg;
};

static void *
zk_thread_wrapper(void *arg)
{
	struct zk_thread_wrapper ztw;
	memcpy(&ztw, arg, sizeof (ztw));
	free(arg);
	ztw.func(ztw.arg);
	return (NULL);
}

kthread_t *
zk_thread_create(void (*func)(void *), void *arg, size_t stksize, int state)
{
	pthread_attr_t attr;
	pthread_t tid;
//	char *stkstr;
	struct zk_thread_wrapper *ztw;
	int detachstate = PTHREAD_CREATE_DETACHED;

	VERIFY0(pthread_attr_init(&attr));

	if (state & TS_JOINABLE)
		detachstate = PTHREAD_CREATE_JOINABLE;

	VERIFY0(pthread_attr_setdetachstate(&attr, detachstate));

	/*
	 * We allow the default stack size in user space to be specified by
	 * setting the ZFS_STACK_SIZE environment variable.  This allows us
	 * the convenience of observing and debugging stack overruns in
	 * user space.  Explicitly specified stack sizes will be honored.
	 * The usage of ZFS_STACK_SIZE is discussed further in the
	 * ENVIRONMENT VARIABLES sections of the ztest(1) man page.
	 */
//	if (stksize == 0) {
//		stkstr = getenv("ZFS_STACK_SIZE");
//
//		if (stkstr == NULL)
//			stksize = TS_STACK_MAX;
//		else
//			stksize = MAX(atoi(stkstr), TS_STACK_MIN);
//	}

//	VERIFY3S(stksize, >, 0);
//	stksize = P2ROUNDUP(MAX(stksize, TS_STACK_MIN), PAGESIZE);

	/*
	 * If this ever fails, it may be because the stack size is not a
	 * multiple of system page size.
	 */
//	VERIFY0(pthread_attr_setstacksize(&attr, stksize));
//	VERIFY0(pthread_attr_setguardsize(&attr, PAGESIZE));

	VERIFY(ztw = malloc(sizeof (*ztw)));
	ztw->func = func;
	ztw->arg = arg;
	VERIFY0(pthread_create(&tid, &attr, zk_thread_wrapper, ztw));
	VERIFY0(pthread_attr_destroy(&attr));

	return ((void *)(uintptr_t)tid);
}

/*
 * =========================================================================
 * kstats
 * =========================================================================
 */
kstat_t *
kstat_create(const char *module, int instance, const char *name,
    const char *class, uchar_t type, ulong_t ndata, uchar_t ks_flag)
{
	(void) module, (void) instance, (void) name, (void) class, (void) type,
	    (void) ndata, (void) ks_flag;
	return (NULL);
}

void
kstat_named_init(kstat_named_t *knp, const char *name, uchar_t type)
{
	(void) knp, (void) name, (void) type;
}

void
kstat_install(kstat_t *ksp)
{
	(void) ksp;
}

void
kstat_delete(kstat_t *ksp)
{
	(void) ksp;
}

void
kstat_waitq_enter(kstat_io_t *kiop)
{
	(void) kiop;
}

void
kstat_waitq_exit(kstat_io_t *kiop)
{
	(void) kiop;
}

void
kstat_runq_enter(kstat_io_t *kiop)
{
	(void) kiop;
}

void
kstat_runq_exit(kstat_io_t *kiop)
{
	(void) kiop;
}

void
kstat_waitq_to_runq(kstat_io_t *kiop)
{
	(void) kiop;
}

void
kstat_runq_back_to_waitq(kstat_io_t *kiop)
{
	(void) kiop;
}

/*
 * =========================================================================
 * mutexes
 * =========================================================================
 */

void
zmutex_init(kmutex_t *mp)
{
	VERIFY0(pthread_mutex_init(&mp->m_lock, NULL));
	memset(&mp->m_owner, 0, sizeof (pthread_t));
}

void
zmutex_destroy(kmutex_t *mp)
{
	VERIFY0(pthread_mutex_destroy(&mp->m_lock));
}

void
zmutex_enter(kmutex_t *mp)
{
	VERIFY0(pthread_mutex_lock(&mp->m_lock));
	mp->m_owner = pthread_self();
}

int
mutex_tryenter(kmutex_t *mp)
{
	int error = pthread_mutex_trylock(&mp->m_lock);
	if (error == 0) {
		mp->m_owner = pthread_self();
		return (1);
	} else {
		VERIFY3S(error, ==, EBUSY);
		return (0);
	}
}

void
zmutex_exit(kmutex_t *mp)
{
	memset(&mp->m_owner, 0, sizeof (pthread_t));
	VERIFY0(pthread_mutex_unlock(&mp->m_lock));
}

void *
mutex_owner(kmutex_t *mp)
{
	return ((void *)(uintptr_t)mp->m_owner);
}

/*
 * =========================================================================
 * rwlocks
 * =========================================================================
 */

void
rw_init(krwlock_t *rwlp, char *name, int type, void *arg)
{
	(void) name, (void) type, (void) arg;

	_krwlock_t *rwl;

	VERIFY(rwl = malloc(sizeof (_krwlock_t)));
	VERIFY0(pthread_rwlock_init(&rwl->rw_lock, NULL));
	rwl->rw_readers = 0;
	rwl->rw_owner = 0;
	rwlp->_opaque[0] = rwl;
}

void
rw_destroy(krwlock_t *rwlp)
{
	_krwlock_t *rwl = rwlp->_opaque[0];
	VERIFY3P(rwl, !=, NULL);
	VERIFY0(pthread_rwlock_destroy(&rwl->rw_lock));
	free(rwl);
	rwlp->_opaque[0] = NULL;
}

void
rw_enter(krwlock_t *rwlp, krw_t rw)
{
	_krwlock_t *rwl = rwlp->_opaque[0];
	VERIFY3P(rwl, !=, NULL);
	if (rw == RW_READER) {
		VERIFY0(pthread_rwlock_rdlock(&rwl->rw_lock));
		atomic_inc_uint(&rwl->rw_readers);
	} else {
		VERIFY0(pthread_rwlock_wrlock(&rwl->rw_lock));
		rwl->rw_owner = pthread_self();
	}
}

void
rw_exit(krwlock_t *rwlp)
{
	_krwlock_t *rwl = rwlp->_opaque[0];
	VERIFY3P(rwl, !=, NULL);
	if (RW_READ_HELD(rwlp))
		atomic_dec_uint(&rwl->rw_readers);
	else
		rwl->rw_owner = 0;

	VERIFY0(pthread_rwlock_unlock(&rwl->rw_lock));
}

int
rw_tryenter(krwlock_t *rwlp, krw_t rw)
{
	int error;

	_krwlock_t *rwl = rwlp->_opaque[0];
	VERIFY3P(rwl, !=, NULL);
	if (rw == RW_READER)
		error = pthread_rwlock_tryrdlock(&rwl->rw_lock);
	else
		error = pthread_rwlock_trywrlock(&rwl->rw_lock);

	if (error == 0) {
		if (rw == RW_READER)
			atomic_inc_uint(&rwl->rw_readers);
		else
			rwl->rw_owner = pthread_self();

		return (1);
	}

	VERIFY3S(error, ==, EBUSY);

	return (0);
}

int
rw_tryupgrade(krwlock_t *rwlp)
{
	(void) rwlp;
	return (0);
}

/*
 * =========================================================================
 * condition variables
 * =========================================================================
 */

void
cv_init(kcondvar_t *cv, char *name, int type, void *arg)
{
	(void) name, (void) type, (void) arg;
	VERIFY0(pthread_cond_init(cv, NULL));
}

void
cv_destroy(kcondvar_t *cv)
{
	VERIFY0(pthread_cond_destroy(cv));
}

void
cv_wait(kcondvar_t *cv, kmutex_t *mp)
{
	memset(&mp->m_owner, 0, sizeof (pthread_t));
	VERIFY0(pthread_cond_wait(cv, &mp->m_lock));
	mp->m_owner = pthread_self();
}

int
cv_wait_sig(kcondvar_t *cv, kmutex_t *mp)
{
	cv_wait(cv, mp);
	return (1);
}

int
cv_timedwait(kcondvar_t *cv, kmutex_t *mp, clock_t abstime)
{
	int error;
	struct timeval tv;
	struct timespec ts;
	clock_t delta;

	delta = abstime - ddi_get_lbolt();
	if (delta <= 0)
		return (-1);

	VERIFY(gettimeofday(&tv, NULL) == 0);

	ts.tv_sec = tv.tv_sec + delta / hz;
	ts.tv_nsec = tv.tv_usec * NSEC_PER_USEC + (delta % hz) * (NANOSEC / hz);
	if (ts.tv_nsec >= NANOSEC) {
		ts.tv_sec++;
		ts.tv_nsec -= NANOSEC;
	}

	memset(&mp->m_owner, 0, sizeof (pthread_t));
	error = pthread_cond_timedwait(cv, &mp->m_lock, &ts);
	mp->m_owner = pthread_self();

	if (error == ETIMEDOUT)
		return (-1);

	VERIFY0(error);

	return (1);
}

int
cv_timedwait_hires(kcondvar_t *cv, kmutex_t *mp, hrtime_t tim, hrtime_t res,
    int flag)
{
	(void) res;
	int error;
	struct timeval tv;
	struct timespec ts;
	hrtime_t delta;

	ASSERT(flag == 0 || flag == CALLOUT_FLAG_ABSOLUTE);

	delta = tim;
	if (flag & CALLOUT_FLAG_ABSOLUTE)
		delta -= gethrtime();

	if (delta <= 0)
		return (-1);

	VERIFY0(gettimeofday(&tv, NULL));

	ts.tv_sec = tv.tv_sec + delta / NANOSEC;
	ts.tv_nsec = tv.tv_usec * NSEC_PER_USEC + (delta % NANOSEC);
	if (ts.tv_nsec >= NANOSEC) {
		ts.tv_sec++;
		ts.tv_nsec -= NANOSEC;
	}

	memset(&mp->m_owner, 0, sizeof (pthread_t));
	error = pthread_cond_timedwait(cv, &mp->m_lock, &ts);
	mp->m_owner = pthread_self();

	if (error == ETIMEDOUT)
		return (-1);

	ASSERT0(error);

	return (1);
}

void
cv_signal(kcondvar_t *cv)
{
	VERIFY0(pthread_cond_signal(cv));
}

void
cv_broadcast(kcondvar_t *cv)
{
	VERIFY0(pthread_cond_broadcast(cv));
}

#ifdef ZFS_DEBUG

/*
 * =========================================================================
 * Figure out which debugging statements to print
 * =========================================================================
 */

static char *dprintf_string;
static int dprintf_print_all;

int
dprintf_find_string(const char *string)
{
	char *tmp_str = dprintf_string;
	int len = strlen(string);

	/*
	 * Find out if this is a string we want to print.
	 * String format: file1.c,function_name1,file2.c,file3.c
	 */

	while (tmp_str != NULL) {
		if (strncmp(tmp_str, string, len) == 0 &&
		    (tmp_str[len] == ',' || tmp_str[len] == '\0'))
			return (1);
		tmp_str = strchr(tmp_str, ',');
		if (tmp_str != NULL)
			tmp_str++; /* Get rid of , */
	}
	return (0);
}

void
dprintf_setup(int *argc, char **argv)
{
	int i, j;

	/*
	 * Debugging can be specified two ways: by setting the
	 * environment variable ZFS_DEBUG, or by including a
	 * "debug=..."  argument on the command line.  The command
	 * line setting overrides the environment variable.
	 */

	for (i = 1; i < *argc; i++) {
		int len = strlen("debug=");
		/* First look for a command line argument */
		if (strncmp("debug=", argv[i], len) == 0) {
			dprintf_string = argv[i] + len;
			/* Remove from args */
			for (j = i; j < *argc; j++)
				argv[j] = argv[j+1];
			argv[j] = NULL;
			(*argc)--;
		}
	}

	if (dprintf_string == NULL) {
		/* Look for ZFS_DEBUG environment variable */
		dprintf_string = getenv("ZFS_DEBUG");
	}

	/*
	 * Are we just turning on all debugging?
	 */
	if (dprintf_find_string("on"))
		dprintf_print_all = 1;

	if (dprintf_string != NULL)
		zfs_flags |= ZFS_DEBUG_DPRINTF;
}

/*
 * =========================================================================
 * debug printfs
 * =========================================================================
 */
void
__dprintf(boolean_t dprint, const char *file, const char *func,
    int line, const char *fmt, ...)
{
	/* Get rid of annoying "../common/" prefix to filename. */
	const char *newfile = zfs_basename(file);

	va_list adx;
	if (dprint) {
		/* dprintf messages are printed immediately */

		if (!dprintf_print_all &&
		    !dprintf_find_string(newfile) &&
		    !dprintf_find_string(func))
			return;

		/* Print out just the function name if requested */
		flockfile(stdout);
		if (dprintf_find_string("pid"))
			(void) printf("%d ", getpid());
		if (dprintf_find_string("tid"))
			(void) printf("%ju ",
			    (uintmax_t)(uintptr_t)pthread_self());
		if (dprintf_find_string("cpu"))
			(void) printf("%u ", getcpuid());
		if (dprintf_find_string("time"))
			(void) printf("%llu ", gethrtime());
		if (dprintf_find_string("long"))
			(void) printf("%s, line %d: ", newfile, line);
		(void) printf("dprintf: %s: ", func);
		va_start(adx, fmt);
		(void) vprintf(fmt, adx);
		va_end(adx);
		funlockfile(stdout);
	} else {
		/* zfs_dbgmsg is logged for dumping later */
		size_t size;
		char *buf;
		int i;

		size = 1024;
		buf = umem_alloc(size, UMEM_NOFAIL);
		i = snprintf(buf, size, "%s:%d:%s(): ", newfile, line, func);

		if (i < size) {
			va_start(adx, fmt);
			(void) vsnprintf(buf + i, size - i, fmt, adx);
			va_end(adx);
		}

		__zfs_dbgmsg(buf);

		umem_free(buf, size);
	}
}
#endif /* ZFS_DEBUG */

void
__set_error(const char *file, const char *func, int line, int err)
{
	/*
	 * To enable this:
	 *
	 * $ export ZFS_DEBUG_SET_ERROR=1
	 */
#ifdef ZFS_DEBUG
	if (getenv("ZFS_DEBUG_SET_ERROR") != NULL)
		__dprintf(B_FALSE, file, func, line, "error %lu",
		    (ulong_t)err);
#else
	(void) file, (void) func, (void) line, (void) err;
#endif
}

/*
 * =========================================================================
 * cmn_err() and panic()
 * =========================================================================
 */
static char ce_prefix[CE_IGNORE][10] = { "", "NOTICE: ", "WARNING: ", "" };
static char ce_suffix[CE_IGNORE][2] = { "", "\n", "\n", "" };

__attribute__((noreturn)) void
vpanic(const char *fmt, va_list adx)
{
	char buf[512];
	(void) vsnprintf(buf, 512, fmt, adx);
	assfail(buf, NULL, 0);

	abort();	/* think of it as a "user-level crash dump" */
}

__attribute__((noreturn)) void
panic(const char *fmt, ...)
{
	va_list adx;

	va_start(adx, fmt);
	vpanic(fmt, adx);
	va_end(adx);
}

void
vcmn_err(int ce, const char *fmt, va_list adx)
{
	if (ce == CE_PANIC)
		vpanic(fmt, adx);
	if (ce != CE_NOTE) {	/* suppress noise in userland stress testing */
		(void) fprintf(stderr, "%s", ce_prefix[ce]);
		(void) vfprintf(stderr, fmt, adx);
		(void) fprintf(stderr, "%s", ce_suffix[ce]);
	}
}

void
cmn_err(int ce, const char *fmt, ...)
{
	va_list adx;

	va_start(adx, fmt);
	vcmn_err(ce, fmt, adx);
	va_end(adx);
}

/*
 * =========================================================================
 * misc routines
 * =========================================================================
 */

void
delay(clock_t ticks)
{
	(void) poll(0, 0, ticks * (1000 / hz));
}

const char *random_path = "/dev/random";
const char *urandom_path = "/dev/urandom";
static int random_fd = -1, urandom_fd = -1;

void
random_init(void)
{
	VERIFY((random_fd = open(random_path, O_RDONLY | O_CLOEXEC)) != -1);
	VERIFY((urandom_fd = open(urandom_path, O_RDONLY | O_CLOEXEC)) != -1);
}

void
random_fini(void)
{
	close(random_fd);
	close(urandom_fd);

	random_fd = -1;
	urandom_fd = -1;
}

static int
random_get_bytes_common(uint8_t *ptr, size_t len, int fd)
{
	size_t resid = len;
	ssize_t bytes;

	ASSERT(fd != -1);

	while (resid != 0) {
		bytes = read(fd, ptr, resid);
		ASSERT3S(bytes, >=, 0);
		ptr += bytes;
		resid -= bytes;
	}

	return (0);
}

int
random_get_bytes(uint8_t *ptr, size_t len)
{
	return (random_get_bytes_common(ptr, len, random_fd));
}

int
random_get_pseudo_bytes(uint8_t *ptr, size_t len)
{
	return (random_get_bytes_common(ptr, len, urandom_fd));
}

int
ddi_strtoul(const char *hw_serial, char **nptr, int base, unsigned long *result)
{
	errno = 0;
	*result = strtoul(hw_serial, nptr, base);
	if (*result == 0)
		return (errno);
	return (0);
}

int
ddi_strtoull(const char *str, char **nptr, int base, u_longlong_t *result)
{
	errno = 0;
	*result = strtoull(str, nptr, base);
	if (*result == 0)
		return (errno);
	return (0);
}

cyclic_id_t
cyclic_add(cyc_handler_t *hdlr, cyc_time_t *when)
{
	(void) hdlr, (void) when;
	return (1);
}

void
cyclic_remove(cyclic_id_t id)
{
	(void) id;
}

int
cyclic_reprogram(cyclic_id_t id, hrtime_t expiration)
{
	(void) id, (void) expiration;
	return (1);
}

/*
 * =========================================================================
 * kernel emulation setup & teardown
 * =========================================================================
 */
static int
umem_out_of_memory(void)
{
	char errmsg[] = "out of memory -- generating core dump\n";

	write(fileno(stderr), errmsg, sizeof (errmsg) - 1);
	abort();
	return (0);
}

void
kernel_init(int mode)
{
	extern uint_t rrw_tsd_key;

	umem_nofail_callback(umem_out_of_memory);

	physmem = sysconf(_SC_PHYS_PAGES);

	dprintf("physmem = %llu pages (%.2f GB)\n", (u_longlong_t)physmem,
	    (double)physmem * sysconf(_SC_PAGE_SIZE) / (1ULL << 30));

	(void) snprintf(hw_serial, sizeof (hw_serial), "%ld",
	    (mode & SPA_MODE_WRITE) ? get_system_hostid() : 0);

	random_init();

	system_taskq_init();

	mutex_init(&cpu_lock, NULL, MUTEX_DEFAULT, NULL);

	zstd_init();

	spa_init((spa_mode_t)mode);

	tsd_create(&rrw_tsd_key, rrw_tsd_destroy);

	VERIFY0(vdev_raidz_impl_set("scalar"));
}

void
kernel_fini(void)
{
	spa_fini();

	zstd_fini();

	mutex_destroy(&cpu_lock);

	/* icp_fini(); */
	system_taskq_fini();

	random_fini();
}

uint32_t
zone_get_hostid(void *zonep)
{
	(void) zonep;
	/*
	 * We're emulating the system's hostid in userland.
	 */
	return (strtoul(hw_serial, NULL, 10));
}

uid_t
crgetuid(cred_t *cr)
{
	(void) cr;
	return (0);
}

uid_t
crgetruid(cred_t *cr)
{
	(void) cr;
	return (0);
}

gid_t
crgetgid(cred_t *cr)
{
	(void) cr;
	return (0);
}

int
crgetngroups(cred_t *cr)
{
	(void) cr;
	return (0);
}

gid_t *
crgetgroups(cred_t *cr)
{
	(void) cr;
	return (NULL);
}

int
zfs_secpolicy_snapshot_perms(const char *name, cred_t *cr)
{
	(void) name, (void) cr;
	return (0);
}

int
zfs_secpolicy_rename_perms(const char *from, const char *to, cred_t *cr)
{
	(void) from, (void) to, (void) cr;
	return (0);
}

int
zfs_secpolicy_destroy_perms(const char *name, cred_t *cr)
{
	(void) name, (void) cr;
	return (0);
}

int
secpolicy_zfs(const cred_t *cr)
{
	(void) cr;
	return (0);
}

int
secpolicy_zfs_proc(const cred_t *cr, proc_t *proc)
{
	(void) cr, (void) proc;
	return (0);
}

ksiddomain_t *
ksid_lookupdomain(const char *dom)
{
	ksiddomain_t *kd;

	kd = umem_zalloc(sizeof (ksiddomain_t), UMEM_NOFAIL);
	kd->kd_name = spa_strdup(dom);
	return (kd);
}

void
ksiddomain_rele(ksiddomain_t *ksid)
{
	spa_strfree(ksid->kd_name);
	umem_free(ksid, sizeof (ksiddomain_t));
}

/*
 * Do not change the length of the returned string; it must be freed
 * with strfree().
 */
char *
kmem_asprintf(const char *fmt, ...)
{
	int size;
	va_list adx;
	char *buf;

	va_start(adx, fmt);
	size = vsnprintf(NULL, 0, fmt, adx) + 1;
	va_end(adx);

	buf = kmem_alloc(size, KM_SLEEP);

	va_start(adx, fmt);
	size = vsnprintf(buf, size, fmt, adx);
	va_end(adx);

	return (buf);
}

zfs_file_t *
zfs_onexit_fd_hold(int fd, minor_t *minorp)
{
	(void) fd;
	*minorp = 0;
	return (NULL);
}

void
zfs_onexit_fd_rele(int fd, zfs_file_t *fp)
{
	(void) fp, (void) fp;
}

int
zfs_onexit_add_cb(minor_t minor, void (*func)(void *), void *data,
    uint64_t *action_handle)
{
	(void) minor, (void) func, (void) data, (void) action_handle;
	return (0);
}

int
zfs_onexit_del_cb(minor_t minor, uint64_t action_handle, boolean_t fire)
{
	(void) minor, (void) action_handle, (void) fire;
	return (0);
}

int
zfs_onexit_cb_data(minor_t minor, uint64_t action_handle, void **data)
{
	(void) minor, (void) action_handle, (void) data;
	return (0);
}

void
bioinit(buf_t *bp)
{
	bzero(bp, sizeof (buf_t));
}

void
biodone(buf_t *bp)
{
	if (bp->b_iodone != NULL) {
		(*(bp->b_iodone))(bp);
		return;
	}
	ASSERT((bp->b_flags & B_DONE) == 0);
	bp->b_flags |= B_DONE;
}

void
bioerror(buf_t *bp, int error)
{
	ASSERT(bp != NULL);
	ASSERT(error >= 0);

	if (error != 0) {
		bp->b_flags |= B_ERROR;
	} else {
		bp->b_flags &= ~B_ERROR;
	}
	bp->b_error = error;
}


int
geterror(struct buf *bp)
{
	int error = 0;

	if (bp->b_flags & B_ERROR) {
		error = bp->b_error;
		if (!error)
			error = EIO;
	}
	return (error);
}

int
crypto_create_ctx_template(crypto_mechanism_t *mech,
    crypto_key_t *key, crypto_ctx_template_t *tmpl, int kmflag)
{
	return (0);
}

crypto_mech_type_t
crypto_mech2id(const char *name)
{
	return (CRYPTO_MECH_INVALID);
}

int
crypto_mac(crypto_mechanism_t *mech, crypto_data_t *data,
    crypto_key_t *key, crypto_ctx_template_t tmpl,
    crypto_data_t *mac, crypto_call_req_t *cr)
{
	(void) mech, (void) data, (void) key, (void) tmpl, (void) mac,
	    (void) cr;
	return (0);
}

int
crypto_encrypt(crypto_mechanism_t *mech, crypto_data_t *plaintext,
    crypto_key_t *key, crypto_ctx_template_t tmpl,
    crypto_data_t *ciphertext, crypto_call_req_t *cr)
{
	(void) mech, (void) plaintext, (void) key, (void) tmpl,
	    (void) ciphertext, (void) cr;
	return (0);
}

/* This could probably be a weak reference */
int
crypto_decrypt(crypto_mechanism_t *mech, crypto_data_t *plaintext,
    crypto_key_t *key, crypto_ctx_template_t tmpl,
    crypto_data_t *ciphertext, crypto_call_req_t *cr)
{
	(void) mech, (void) plaintext, (void) key, (void) tmpl,
	    (void) ciphertext, (void) cr;
	return (0);
}


int
crypto_digest_final(crypto_context_t context, crypto_data_t *digest,
    crypto_call_req_t *cr)
{
	(void) context, (void) digest, (void) cr;
	return (0);
}

int
crypto_digest_update(crypto_context_t context, crypto_data_t *data,
    crypto_call_req_t *cr)
{
	(void) context, (void) data, (void) cr;
	return (0);
}

int
crypto_digest_init(crypto_mechanism_t *mech, crypto_context_t *ctxp,
    crypto_call_req_t *crq)
{
	(void) mech, (void) ctxp, (void) crq;
	return (0);
}

void
crypto_destroy_ctx_template(crypto_ctx_template_t tmpl)
{
	(void) tmpl;
}

extern int
crypto_mac_init(crypto_mechanism_t *mech, crypto_key_t *key,
    crypto_ctx_template_t tmpl, crypto_context_t *ctxp,
    crypto_call_req_t *cr)
{
	(void) mech, (void) key, (void) tmpl, (void) ctxp, (void) cr;
	return (0);
}

extern int
crypto_mac_update(crypto_context_t ctx, crypto_data_t *data,
    crypto_call_req_t *cr)
{
	(void) ctx, (void) data, (void) cr;
	return (0);
}

extern int
crypto_mac_final(crypto_context_t ctx, crypto_data_t *data,
    crypto_call_req_t *cr)
{
	(void) ctx, (void) data, (void) cr;
	return (0);
}

#if 0
/*
 * Factored out implementation of all the cv_*timedwait* functions.
 * Note that the delta passed in is relative to the (simulated)
 * current time reported by ddi_get_lbolt().  Convert that to
 * timespec format and keep calling _lwp_cond_reltimedwait,
 * which (NB!) decrements that delta in-place!
 */
static clock_t
cv__twait(kcondvar_t *cv, kmutex_t *mp, clock_t delta, int sigok, int hires)
{
	timestruc_t ts;
	int err;

	if (delta <= 0)
		return (-1);

	if (hires) {
		ts.tv_sec = delta / NANOSEC;
		ts.tv_nsec = delta % NANOSEC;
	} else {
		ts.tv_sec = delta / hz;
		ts.tv_nsec = (delta % hz) * (NANOSEC / hz);
	}

top:
	if (ts.tv_sec == 0 && ts.tv_nsec == 0)
		return (-1);

	ASSERT(mp->m_owner == curthread);
	mp->m_owner = _KTHREAD_INVALID;
	err = _lwp_cond_reltimedwait(cv, &mp->m_lock, &ts);
	mp->m_owner = curthread;

	switch (err) {
	case 0:
		return (1);
	case EINTR:
		if (sigok)
			return (0);
		goto top;
	default:
		ASSERT(0);
		/* FALLTHROUGH */
	case ETIME:
		break;
	}

	return (-1);
}
#endif

/*
 * Open file
 *
 * path - fully qualified path to file
 * flags - file attributes O_READ / O_WRITE / O_EXCL
 * fpp - pointer to return file pointer
 *
 * Returns 0 on success underlying error on failure.
 */
int
zfs_file_open(const char *path, int flags, int mode, zfs_file_t **fpp)
{
	int fd = -1;
	int dump_fd = -1;
	int err;
	int old_umask = 0;
	zfs_file_t *fp;
	char realpath[MAXPATHLEN];
	struct stat64 st;

#ifdef __dilos__
	/*
	 * If we're accessing a real disk from userland, we need to use
	 * the character interface to avoid caching.  This is particularly
	 * important if we're trying to look at a real in-kernel storage
	 * pool from userland, e.g. via zdb, because otherwise we won't
	 * see the changes occurring under the segmap cache.
	 * On the other hand, the stupid character device returns zero
	 * for its size.  So -- gag -- we open the block device to get
	 * its size, and remember it for subsequent VOP_GETATTR().
	 */
	if (strncmp(path, "/dev/", 5) == 0) {
		char *dsk;
		fd = open64(path, O_RDONLY);
		if (fd == -1) {
			err = errno;
			return (err);
		}
		if (fstat64(fd, &st) == -1) {
			err = errno;
			close(fd);
			return (err);
		}
		close(fd);
		(void) sprintf(realpath, "%s", path);
		dsk = strstr(path, "/dsk/");
		if (dsk != NULL)
			(void) sprintf(realpath + (dsk - path) + 1, "r%s",
			    dsk + 1);
		path = (const char *)realpath;
	}
#endif

	if (!(flags & O_CREAT) && stat64(path, &st) == -1)
		return (errno);

#ifndef __dilos__
	if (!(flags & O_CREAT) && S_ISBLK(st.st_mode))
		flags |= O_DIRECT;
#endif /* !__dilos__ */

	if (flags & O_CREAT)
		old_umask = umask(0);

	fd = open64(path, flags, mode);
	if (fd == -1)
		return (errno);

	if (flags & O_CREAT)
		(void) umask(old_umask);

	if (vn_dumpdir != NULL) {
		char *dumppath = umem_zalloc(MAXPATHLEN, UMEM_NOFAIL);
		char *inpath = basename((char *)(uintptr_t)path);

		(void) snprintf(dumppath, MAXPATHLEN,
		    "%s/%s", vn_dumpdir, inpath);
		dump_fd = open64(dumppath, O_CREAT | O_WRONLY, 0666);
		umem_free(dumppath, MAXPATHLEN);
		if (dump_fd == -1) {
			err = errno;
			close(fd);
			return (err);
		}
	} else {
		dump_fd = -1;
	}

	if (fstat64(fd, &st) == -1) {
		err = errno;
		close(fd);
		if (dump_fd != -1)
			close(dump_fd);
		return (err);
	}

	(void) fcntl(fd, F_SETFD, FD_CLOEXEC);

	fp = umem_zalloc(sizeof (zfs_file_t), UMEM_NOFAIL);
	fp->f_fd = fd;
	fp->f_dump_fd = dump_fd;
	*fpp = fp;

	return (0);
}

void
zfs_file_close(zfs_file_t *fp)
{
	close(fp->f_fd);
	if (fp->f_dump_fd != -1)
		close(fp->f_dump_fd);

	umem_free(fp, sizeof (zfs_file_t));
}

/*
 * Stateful write - use os internal file pointer to determine where to
 * write and update on successful completion.
 *
 * fp -  pointer to file (pipe, socket, etc) to write to
 * buf - buffer to write
 * count - # of bytes to write
 * resid -  pointer to count of unwritten bytes  (if short write)
 *
 * Returns 0 on success errno on failure.
 */
int
zfs_file_write(zfs_file_t *fp, const void *buf, size_t count, ssize_t *resid)
{
	ssize_t rc;

	rc = write(fp->f_fd, buf, count);
	if (rc < 0)
		return (errno);

	if (resid) {
		*resid = count - rc;
	} else if (rc != count) {
		return (EIO);
	}

	return (0);
}

/*
 * Stateless write - os internal file pointer is not updated.
 *
 * fp -  pointer to file (pipe, socket, etc) to write to
 * buf - buffer to write
 * count - # of bytes to write
 * off - file offset to write to (only valid for seekable types)
 * resid -  pointer to count of unwritten bytes
 *
 * Returns 0 on success errno on failure.
 */
int
zfs_file_pwrite(zfs_file_t *fp, const void *buf,
    size_t count, loff_t pos, ssize_t *resid)
{
	ssize_t rc, split, done;
	int sectors;

	/*
	 * To simulate partial disk writes, we split writes into two
	 * system calls so that the process can be killed in between.
	 * This is used by ztest to simulate realistic failure modes.
	 */
	sectors = count >> SPA_MINBLOCKSHIFT;
	split = (sectors > 0 ? rand() % sectors : 0) << SPA_MINBLOCKSHIFT;
	rc = pwrite64(fp->f_fd, buf, split, pos);
	if (rc != -1) {
		done = rc;
		rc = pwrite64(fp->f_fd, (char *)buf + split,
		    count - split, pos + split);
	}
#ifdef __linux__
	if (rc == -1 && errno == EINVAL) {
		/*
		 * Under Linux, this most likely means an alignment issue
		 * (memory or disk) due to O_DIRECT, so we abort() in order
		 * to catch the offender.
		 */
		abort();
	}
#endif

	if (rc < 0)
		return (errno);

	done += rc;

	if (resid) {
		*resid = count - done;
	} else if (done != count) {
		return (EIO);
	}

	return (0);
}

/*
 * Stateful read - use os internal file pointer to determine where to
 * read and update on successful completion.
 *
 * fp -  pointer to file (pipe, socket, etc) to read from
 * buf - buffer to write
 * count - # of bytes to read
 * resid -  pointer to count of unread bytes (if short read)
 *
 * Returns 0 on success errno on failure.
 */
int
zfs_file_read(zfs_file_t *fp, void *buf, size_t count, ssize_t *resid)
{
	int rc;

	rc = read(fp->f_fd, buf, count);
	if (rc < 0)
		return (errno);

	if (resid) {
		*resid = count - rc;
	} else if (rc != count) {
		return (EIO);
	}

	return (0);
}

/*
 * Stateless read - os internal file pointer is not updated.
 *
 * fp -  pointer to file (pipe, socket, etc) to read from
 * buf - buffer to write
 * count - # of bytes to write
 * off - file offset to read from (only valid for seekable types)
 * resid -  pointer to count of unwritten bytes (if short write)
 *
 * Returns 0 on success errno on failure.
 */
int
zfs_file_pread(zfs_file_t *fp, void *buf, size_t count, loff_t off,
    ssize_t *resid)
{
	ssize_t rc;

	rc = pread64(fp->f_fd, buf, count, off);
	if (rc < 0) {
#ifdef __linux__
		/*
		 * Under Linux, this most likely means an alignment issue
		 * (memory or disk) due to O_DIRECT, so we abort() in order to
		 * catch the offender.
		 */
		if (errno == EINVAL)
			abort();
#endif
		return (errno);
	}

	if (fp->f_dump_fd != -1) {
		int status __unused;

		status = pwrite64(fp->f_dump_fd, buf, rc, off);
		ASSERT(status != -1);
	}

	if (resid) {
		*resid = count - rc;
	} else if (rc != count) {
		return (EIO);
	}

	return (0);
}

/*
 * lseek - set / get file pointer
 *
 * fp -  pointer to file (pipe, socket, etc) to read from
 * offp - value to seek to, returns current value plus passed offset
 * whence - see man pages for standard lseek whence values
 *
 * Returns 0 on success errno on failure (ESPIPE for non seekable types)
 */
int
zfs_file_seek(zfs_file_t *fp, loff_t *offp, int whence)
{
	loff_t rc;

	rc = lseek(fp->f_fd, *offp, whence);
	if (rc < 0)
		return (errno);

	*offp = rc;

	return (0);
}

/*
 * Get file attributes
 *
 * filp - file pointer
 * zfattr - pointer to file attr structure
 *
 * Currently only used for fetching size and file mode
 *
 * Returns 0 on success or error code of underlying getattr call on failure.
 */
int
zfs_file_getattr(zfs_file_t *fp, zfs_file_attr_t *zfattr)
{
	struct stat64 st;

#ifdef __dilos__
	if (fstat64(fp->f_fd, &st) == -1)
#else
	if (fstat64_blk(fp->f_fd, &st) == -1)
#endif /* __dilos__ */
		return (errno);

	zfattr->zfa_size = st.st_size;
	zfattr->zfa_mode = st.st_mode;

	return (0);
}

/*
 * Sync file to disk
 *
 * filp - file pointer
 * flags - O_SYNC and or O_DSYNC
 *
 * Returns 0 on success or error code of underlying sync call on failure.
 */
int
zfs_file_fsync(zfs_file_t *fp, int flags)
{
	(void) flags;

	if (fsync(fp->f_fd) < 0)
		return (errno);

	return (0);
}

/*
 * fallocate - allocate or free space on disk
 *
 * fp - file pointer
 * mode (non-standard options for hole punching etc)
 * offset - offset to start allocating or freeing from
 * len - length to free / allocate
 *
 * OPTIONAL
 */
int
zfs_file_fallocate(zfs_file_t *fp, int mode, loff_t offset, loff_t len)
{
#ifdef __dilos__
	return (posix_fallocate(fp->f_fd, offset, len));
#elif __linux__
	return (fallocate(fp->f_fd, mode, offset, len));
#else
	(void) fp, (void) mode, (void) offset, (void) len;
	return (EOPNOTSUPP);
#endif
}

/*
 * Request current file pointer offset
 *
 * fp - pointer to file
 *
 * Returns current file offset.
 */
loff_t
zfs_file_off(zfs_file_t *fp)
{
	return (lseek(fp->f_fd, 0, SEEK_CUR));
}

/*
 * unlink file
 *
 * path - fully qualified file path
 *
 * Returns 0 on success.
 *
 * OPTIONAL
 */
int
zfs_file_unlink(const char *path)
{
	return (remove(path));
}

/*
 * Get reference to file pointer
 *
 * fd - input file descriptor
 *
 * Returns pointer to file struct or NULL.
 * Unsupported in user space.
 */
zfs_file_t *
zfs_file_get(int fd)
{
	(void) fd;
	abort();
	return (NULL);
}
/*
 * Drop reference to file pointer
 *
 * fd - input file descriptor
 * fp - pointer to file struct
 *
 * Unsupported in user space.
 */
void
zfs_file_put(int fd, zfs_file_t *fp)
{
	(void) fd, (void) fp;
	abort();
}

void
zfsvfs_update_fromname(const char *oldname, const char *newname)
{
}
