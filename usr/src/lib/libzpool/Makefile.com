#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the "License").
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/OPENSOLARIS.LICENSE.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets "[]" replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#
#
# Copyright (c) 2005, 2010, Oracle and/or its affiliates. All rights reserved.
# Copyright (c) 2013, 2016 by Delphix. All rights reserved.
# Copyright 2019, Joyent, Inc.
#

LIBRARY= libzpool.a
VERS= .1

# include the list of ZFS sources
include ../../../uts/common/Makefile.files
KERNEL_OBJS = kernel.o taskq.o util.o kmem_cache_reap_stub.o
DTRACE_OBJS = zfs.o
ZUTIL_SHARED = zutil_nicenum.o gethostid.o zutil_highlowbits.o

OBJECTS=$(LUA_OBJS) $(ZFS_COMMON_OBJS) $(ZFS_SHARED_OBJS) $(KERNEL_OBJS)
OBJECTS += $(ZSTD_COMMON_OBJS)
OBJECTS += $(ZUTIL_SHARED)

# include library definitions
include ../../Makefile.lib

LUA_SRCS=		$(LUA_OBJS:%.o=../../../uts/common/fs/zfs/lua/%.c)
ZFS_COMMON_SRCS=	$(ZFS_COMMON_OBJS:%.o=../../../uts/common/fs/zfs/%.c)
ZFS_SHARED_SRCS=	$(ZFS_SHARED_OBJS:%.o=../../../common/zfs/%.c)
KERNEL_SRCS=		$(KERNEL_OBJS:%.o=../common/%.c)
ZSTD_COMMON_SRCS=	$(ZSTD_COMMON_OBJS:%.o=../../../uts/common/zstd/%.c)

SRCS=$(LUA_SRCS) $(ZFS_COMMON_SRCS) $(ZFS_SHARED_SRCS) $(KERNEL_SRCS) $(ZSTD_COMMON_SRCS)
SRCDIR=		../common

# There should be a mapfile here
MAPFILES =

LIBS +=		$(DYNLIB)

INCS += -I.
INCS += -I../common
INCS += -I$(SRC)/uts/common/fs/zfs
INCS += -I$(SRC)/uts/common/fs/zfs/lua
INCS += -I$(SRC)/uts/common/zstd/include
INCS += -I$(SRC)/common/zfs
INCS += -I$(SRC)/common

CLEANFILES += ../common/zfs.h
CLEANFILES += $(EXTPICS)

$(LIBS): ../common/zfs.h

# FIXME: temp solution because optimization issue
#COPTFLAG64=-xO0

CSTD=	$(CSTD_GNU99)

#CFLAGS +=	$(CCGDEBUG) $(CCVERBOSE) $(CNOGLOBAL)
CFLAGS64 +=	$(CCGDEBUG) $(CCVERBOSE) $(CNOGLOBAL)
ZFS_ZSTD_FLAGS = -_gcc=-include $(SRC)/uts/common/zstd/include/zstd_compat_wrapper.h
ZFS_ZSTD_FLAGS += -_gcc=-U__BMI__ -_gcc=-fno-tree-vectorize
LDLIBS +=	-lumem -lnvpair -lsysevent
LDLIBS +=	-lavl
LDLIBS +=	-lcmdutils
LDLIBS +=	-lzutil
LDLIBS +=	-lz
#LDLIBS +=	-lm
LDLIBS +=	-lc
CPPFLAGS +=	$(INCS) -DZFS_DEBUG
$(NOT_RELEASE_BUILD)CPPFLAGS += -DDEBUG

amd64_CPPFLAGS_OPTS= \
	-DHAVE_SSE2 -DHAVE_SSSE3 \
	-DHAVE_AVX -DHAVE_AVX2 \
	-DHAVE_AVX512F -DHAVE_AVX512BW

CPPFLAGS += $($(MACH64)_CPPFLAGS_OPTS)
AS_CPPFLAGS += $($(MACH64)_CPPFLAGS_OPTS)

#CERRWARN +=	-_gcc=-Wno-unused-variable
CERRWARN +=	-_gcc4=-Wno-type-limits
#CERRWARN +=	-_gcc=-Wwrite-strings

pics/vdev_raidz.o := CERRWARN += -_gcc=-Wno-unused-variable
pics/taskq.o := CERRWARN += -_gcc=-Wno-unused-variable

# ZSTD CFLAGS64 += $(ZFS_ZSTD_FLAGS)
pics/zfs_zstd.o := CFLAGS64 += $(ZFS_ZSTD_FLAGS)
pics/zstd_sparc.o := CFLAGS64 += $(ZFS_ZSTD_FLAGS)
pics/entropy_common.o := CFLAGS64 += $(ZFS_ZSTD_FLAGS)
pics/error_private.o := CFLAGS64 += $(ZFS_ZSTD_FLAGS)
pics/fse_decompress.o := CFLAGS64 += $(ZFS_ZSTD_FLAGS)
pics/zstd_pool.o := CFLAGS64 += $(ZFS_ZSTD_FLAGS)
pics/zstd_common.o := CFLAGS64 += $(ZFS_ZSTD_FLAGS)
pics/fse_compress.o := CFLAGS64 += $(ZFS_ZSTD_FLAGS)
pics/hist.o := CFLAGS64 += $(ZFS_ZSTD_FLAGS)
pics/huf_compress.o := CFLAGS64 += $(ZFS_ZSTD_FLAGS)
pics/zstd_compress.o := CFLAGS64 += $(ZFS_ZSTD_FLAGS)
pics/zstd_compress_literals.o := CFLAGS64 += $(ZFS_ZSTD_FLAGS)
pics/zstd_compress_sequences.o := CFLAGS64 += $(ZFS_ZSTD_FLAGS)
pics/zstd_compress_superblock.o := CFLAGS64 += $(ZFS_ZSTD_FLAGS)
pics/zstd_double_fast.o := CFLAGS64 += $(ZFS_ZSTD_FLAGS)
pics/zstd_fast.o := CFLAGS64 += $(ZFS_ZSTD_FLAGS)
pics/zstd_lazy.o := CFLAGS64 += $(ZFS_ZSTD_FLAGS)
pics/zstd_ldm.o := CFLAGS64 += $(ZFS_ZSTD_FLAGS)
pics/zstd_opt.o := CFLAGS64 += $(ZFS_ZSTD_FLAGS)
pics/huf_decompress.o := CFLAGS64 += $(ZFS_ZSTD_FLAGS)
pics/zstd_ddict.o := CFLAGS64 += $(ZFS_ZSTD_FLAGS)
pics/zstd_decompress.o := CFLAGS64 += $(ZFS_ZSTD_FLAGS)
pics/zstd_decompress_block.o := CFLAGS64 += $(ZFS_ZSTD_FLAGS)

# not linted
SMATCH=off

.KEEP_STATE:

all: $(LIBS)


include ../../Makefile.targ

EXTPICS= $(DTRACE_OBJS:%=pics/%)

pics/%.o: ../../../lib/libzutil/common/%.c ../common/zfs.h
	$(COMPILE.c) -o $@ $<
	$(POST_PROCESS_O)

pics/%.o: ../../../uts/common/zstd/%.c ../common/zfs.h
	$(COMPILE.c) -o $@ $<
	$(POST_PROCESS_O)

pics/%.o: ../../../uts/common/zstd/lib/common/%.c ../common/zfs.h
	$(COMPILE.c) -o $@ $<
	$(POST_PROCESS_O)

pics/%.o: ../../../uts/common/zstd/lib/compress/%.c ../common/zfs.h
	$(COMPILE.c) -o $@ $<
	$(POST_PROCESS_O)

pics/%.o: ../../../uts/common/zstd/lib/decompress/%.c ../common/zfs.h
	$(COMPILE.c) -o $@ $<
	$(POST_PROCESS_O)

pics/%.o: ../../../uts/common/fs/zfs/%.c ../common/zfs.h
	$(COMPILE.c) -o $@ $<
	$(POST_PROCESS_O)

pics/%.o: ../../../uts/common/fs/zfs/lua/%.c ../common/zfs.h
	$(COMPILE.c) -o $@ $<
	$(POST_PROCESS_O)

pics/%.o: ../../../common/zfs/%.c ../common/zfs.h
	$(COMPILE.c) -o $@ $<
	$(POST_PROCESS_O)

pics/%.o: ../../../common/util/%.c ../common/zfs.h
	$(COMPILE.c) -o $@ $<
	$(POST_PROCESS_O)

pics/%.o: ../../../common/crypto/blake3/%.c ../../../uts/common/sys/blake3.h
	$(COMPILE.c) -o $@ $<
	$(POST_PROCESS_O)

pics/%.o: ../../../common/crypto/blake3/$(MACH64)/%.S
	$(COMPILE.s) -o $@ $<

pics/%.o: ../common/%.d $(PICS)
	$(COMPILE.d) -C -s $< -o $@ $(PICS)
	$(POST_PROCESS_O)

../common/%.h: ../common/%.d
	$(DTRACE) -xnolibs -h -s $< -o $@
