LIB=zpool
SHLIB=lib$(LIB).so.1

CC=gcc
DTRACE=/usr/sbin/dtrace
VPATH=../../uts/common/fs/zfs:../../uts/common/fs/zfs/lua:common:../../common/zfs
SRCTREE=../../../..
SRCS=vdev_cache.c vdev_file.c vdev_indirect.c vdev_indirect_births.c \
vdev_indirect_mapping.c vdev_initialize.c vdev_label.c vdev_mirror.c \
vdev_missing.c vdev_queue.c vdev_raidz.c vdev_removal.c vdev_root.c \
vdev_trim.c zap.c zap_leaf.c zap_micro.c zcp.c zcp_get.c zcp_global.c \
zcp_iter.c zcp_synctask.c zfs_byteswap.c zfs_debug.c zfs_fm.c zfs_fuid.c \
zfs_sa.c zfs_znode.c zfs_zone.c zil.c zio.c zio_checksum.c zio_compress.c \
zio_crypt.c zio_inject.c zle.c zrlock.c zthr.c zfeature_common.c zfs_comutil.c \
zfs_deleg.c zfs_fletcher.c zfs_fletcher_superscalar.c \
zfs_fletcher_superscalar4.c zfs_namecheck.c zfs_prop.c zpool_prop.c \
zprop_common.c kernel.c taskq.c util.c kmem_cache_reap_stub.c\
dsl_dataset.c dmu_recv.c dbuf.c dmu.c dmu_os.c dsl_destroy.c ddt.c dsl_pool.c\
spa_misc.c lcorolib.c blkptr.c bqueue.c range_tree.c dsl_crypt.c dmu_objset.c \
sa.c multilist.c vdev.c dsl_scan.c lapi.c dnode.c lstring.c metaslab.c \
dsl_deleg.c arc.c dsl_synctask.c refcount.c lvm.c lfunc.c ldebug.c lauxlib.c \
dsl_dir.c spa_checkpoint.c dmu_tx.c txg.c abd.c abd_os.c dsl_bookmark.c lstate.c ltm.c \
ltable.c lobject.c zfeature.c lgc.c edonr_zfs.c ldo.c bpobj.c uberblock.c \
space_map.c dmu_object.c dsl_prop.c bplist.c spa.c spa_history.c bptree.c \
lz4.c space_reftree.c rrwlock.c lstrlib.c lbaselib.c lcompat.c spa_config.c \
objlist.c mmp.c aggsum.c cityhash.c unique.c sha256.c gzip.c dsl_deadlist.c \
dnode_sync.c spa_errlog.c skein_zfs.c dsl_userhold.c dmu_zfetch.c lzio.c \
lmem.c hkdf.c dmu_traverse.c llex.c lundump.c lzjb.c ldump.c lparser.c \
lcode.c lctype.c ltablib.c lopcodes.c ddt_zap.c spa_log_spacemap.c btree.c \
spa_stats.c vdev_draid.c vdev_draid_rand.c vdev_rebuild.c zcp_set.c arc_os.c \
vdev_raidz_math_scalar.c vdev_raidz_math.c vdev_raidz_math_sse2.c \
vdev_raidz_math_ssse3.c vdev_raidz_math_avx2.c

OBJS=$(SRCS:%.c=%.o)

CFLAGS= -O0 -ggdb3 -nodefaultlibs -D__sun -D__dilos__ -D__dilos \
-m64 -Ui386 -U__i386 -fpic -std=gnu99 \
-DTEXT_DOMAIN=\"SUNW_OST_OSLIB\" -D_TS_ERRNO \
-I$(SRCTREE)/proto/root_i386/usr/include \
-I. -Icommon \
-I$(SRCTREE)/usr/src/uts/common/fs/zfs \
-I$(SRCTREE)/usr/src/uts/common/fs/zfs/lua \
-I../../common/zfs -I../../common \
-DDEBUG -DZFS_DEBUG -DPIC -D_REENTRANT

LDFLAGS+=-nodefaultlibs -m64 -zdefs\
-L$(SRCTREE)/proto/root_i386/lib/amd64 \
-L$(SRCTREE)/proto/root_i386/usr/lib/amd64 \
-lcmdutils -lumem -lavl -lnvpair -lz -lc -lsysevent -lzfs -lzutil

all: $(SHLIB)

$(SHLIB): zfs.o $(OBJS)
	$(DTRACE) -xnolibs -G -64 -C -s common/zfs.d $(OBJS)
	$(CC) -o amd64/$@ $(OBJS) -shared -h $@ $(LDFLAGS)

clean:
	rm -f $(OBJS) zfs.h zfs.o

distclean: clean
	rm -f amd64/$(SHLIB)

zfs.o: zfs.h

zfs.h: zfs.d
	$(DTRACE) -xnolibs -xnolibs -h -s $^ -o $@



#/usr/sbin/dtrace -xnolibs -G -64 -C -s ../common/zfs.d -o pics/zfs.o pics/ldo.o			 pics/lvm.o			 pics/lbitlib.o		 pics/lopcodes.o		 pics/lstring.o		 pics/ltable.o		 pics/ltm.o			 pics/lcorolib.o		 pics/lauxlib.o		 pics/ldebug.o		 pics/lstate.o		 pics/lgc.o			 pics/lmem.o			 pics/lctype.o		 pics/lfunc.o			 pics/ldump.o			 pics/lundump.o		 pics/lstrlib.o		 pics/ltablib.o		 pics/lapi.o			 pics/lobject.o		 pics/lbaselib.o		 pics/lcompat.o		 pics/lzio.o			 pics/lcode.o			 pics/llex.o			 pics/lparser.o pics/abd.o			 pics/aggsum.o		 pics/arc.o			 pics/blkptr.o		 pics/bplist.o		 pics/bpobj.o			 pics/bptree.o		 pics/bqueue.o		 pics/cityhash.o		 pics/dbuf.o			 pics/ddt.o			 pics/ddt_zap.o		 pics/dmu.o			 pics/dmu_os.o			 pics/dmu_diff.o		 pics/dmu_recv.o		 pics/dmu_send.o		 pics/dmu_object.o		 pics/dmu_objset.o		 pics/dmu_traverse.o		 pics/dmu_tx.o		 pics/dnode.o			 pics/dnode_sync.o		 pics/dsl_bookmark.o		 pics/dsl_dir.o		 pics/dsl_crypt.o		 pics/dsl_dataset.o		 pics/dsl_deadlist.o		 pics/dsl_destroy.o		 pics/dsl_pool.o		 pics/dsl_synctask.o		 pics/dsl_userhold.o		 pics/dmu_redact.o		 pics/dmu_zfetch.o		 pics/dsl_deleg.o		 pics/dsl_prop.o		 pics/dsl_scan.o		 pics/zfeature.o		 pics/gzip.o			 pics/hkdf.o			 pics/lz4.o			 pics/lzjb.o			 pics/metaslab.o		 pics/mmp.o			 pics/multilist.o		 pics/objlist.o		 pics/range_tree.o		 pics/refcount.o		 pics/rrwlock.o		 pics/sa.o			 pics/sha256.o		 pics/edonr_zfs.o		 pics/skein_zfs.o		 pics/spa.o			 pics/spa_checkpoint.o	 pics/spa_config.o		 pics/spa_errlog.o		 pics/spa_history.o		 pics/spa_misc.o		 pics/space_map.o		 pics/space_reftree.o		 pics/txg.o			 pics/uberblock.o		 pics/unique.o		 pics/vdev.o			 pics/vdev_cache.o		 pics/vdev_file.o		 pics/vdev_indirect.o		 pics/vdev_indirect_births.o	 pics/vdev_indirect_mapping.o	 pics/vdev_initialize.o	 pics/vdev_label.o		 pics/vdev_mirror.o		 pics/vdev_missing.o		 pics/vdev_queue.o		 pics/vdev_raidz.o		 pics/vdev_removal.o		 pics/vdev_root.o		 pics/vdev_trim.o		 pics/zap.o			 pics/zap_leaf.o		 pics/zap_micro.o		 pics/zcp.o			 pics/zcp_get.o		 pics/zcp_global.o		 pics/zcp_iter.o		 pics/zcp_synctask.o		 pics/zfs_byteswap.o		 pics/zfs_debug.o		 pics/zfs_fm.o		 pics/zfs_fuid.o		 pics/zfs_sa.o		 pics/zfs_znode.o		 pics/zfs_zone.o		 pics/zil.o			 pics/zio.o			 pics/zio_checksum.o		 pics/zio_compress.o		 pics/zio_crypt.o		 pics/zio_inject.o		 pics/zle.o			 pics/zrlock.o		 pics/zthr.o pics/zfeature_common.o	 pics/zfs_comutil.o		 pics/zfs_deleg.o		 pics/zfs_fletcher.o		 pics/zfs_fletcher_superscalar.o  pics/zfs_fletcher_superscalar4.o  pics/zfs_namecheck.o		 pics/zfs_prop.o		 pics/zpool_prop.o		 pics/zprop_common.o pics/kernel.o pics/taskq.o pics/util.o pics/kmem_cache_reap_stub.o
#+ /usr/lib/ccache/gcc-6 -fident -finline -fno-inline-functions -fno-builtin -fno-asm -fdiagnostics-show-option -nodefaultlibs -D__sun -D__dilos__ -D__dilos -fno-strict-aliasing -fno-unit-at-a-time -fno-optimize-sibling-calls -O2 -m64 -mtune=opteron -Ui386 -U__i386 -Wall -Wextra -Werror -Wno-missing-braces -Wno-sign-compare -Wno-unknown-pragmas -Wno-unused-parameter -Wno-missing-field-initializers -Wno-array-bounds -Wno-parentheses -Wno-switch -Wno-unused-variable -Wno-empty-body -Wno-unused-function -Wno-unused-label -Wno-unused-but-set-variable -std=gnu99 -fno-inline-small-functions -fno-inline-functions-called-once -fno-ipa-cp -fno-ipa-icf -fno-ipa-sra -fno-clone-functions -fno-reorder-functions -fno-aggressive-loop-optimizations -g -gdwarf-2 -o libzpool.so.1 -shared -hlibzpool.so.1 -Wl,-ztext -Wl,-zdefs -Wl,-Bdirect -Wl,-M/export/home/denis/projects/dilos/illumos/usr/src/common/mapfiles/common/map.pagealign -Wl,-M/export/home/denis/projects/dilos/illumos/usr/src/common/mapfiles/common/map.noexdata pics/ldo.o pics/lvm.o pics/lbitlib.o pics/lopcodes.o pics/lstring.o pics/ltable.o pics/ltm.o pics/lcorolib.o pics/lauxlib.o pics/ldebug.o pics/lstate.o pics/lgc.o pics/lmem.o pics/lctype.o pics/lfunc.o pics/ldump.o pics/lundump.o pics/lstrlib.o pics/ltablib.o pics/lapi.o pics/lobject.o pics/lbaselib.o pics/lcompat.o pics/lzio.o pics/lcode.o pics/llex.o pics/lparser.o pics/abd.o pics/aggsum.o pics/arc.o pics/blkptr.o pics/bplist.o pics/bpobj.o pics/bptree.o pics/bqueue.o pics/cityhash.o pics/dbuf.o pics/ddt.o pics/ddt_zap.o pics/dmu.o pics/dmu_os.o pics/dmu_diff.o pics/dmu_recv.o pics/dmu_send.o pics/dmu_object.o pics/dmu_objset.o pics/dmu_traverse.o pics/dmu_tx.o pics/dnode.o pics/dnode_sync.o pics/dsl_bookmark.o pics/dsl_dir.o pics/dsl_crypt.o pics/dsl_dataset.o pics/dsl_deadlist.o pics/dsl_destroy.o pics/dsl_pool.o pics/dsl_synctask.o pics/dsl_userhold.o pics/dmu_redact.o pics/dmu_zfetch.o pics/dsl_deleg.o pics/dsl_prop.o pics/dsl_scan.o pics/zfeature.o pics/gzip.o pics/hkdf.o pics/lz4.o pics/lzjb.o pics/metaslab.o pics/mmp.o pics/multilist.o pics/objlist.o pics/range_tree.o pics/refcount.o pics/rrwlock.o pics/sa.o pics/sha256.o pics/edonr_zfs.o pics/skein_zfs.o pics/spa.o pics/spa_checkpoint.o pics/spa_config.o pics/spa_errlog.o pics/spa_history.o pics/spa_misc.o pics/space_map.o pics/space_reftree.o pics/txg.o pics/uberblock.o pics/unique.o pics/vdev.o pics/vdev_cache.o pics/vdev_file.o pics/vdev_indirect.o pics/vdev_indirect_births.o pics/vdev_indirect_mapping.o pics/vdev_initialize.o pics/vdev_label.o pics/vdev_mirror.o pics/vdev_missing.o pics/vdev_queue.o pics/vdev_raidz.o pics/vdev_removal.o pics/vdev_root.o pics/vdev_trim.o pics/zap.o pics/zap_leaf.o pics/zap_micro.o pics/zcp.o pics/zcp_get.o pics/zcp_global.o pics/zcp_iter.o pics/zcp_synctask.o pics/zfs_byteswap.o pics/zfs_debug.o pics/zfs_fm.o pics/zfs_fuid.o pics/zfs_sa.o pics/zfs_znode.o pics/zfs_zone.o pics/zil.o pics/zio.o pics/zio_checksum.o pics/zio_compress.o pics/zio_crypt.o pics/zio_inject.o pics/zle.o pics/zrlock.o pics/zthr.o pics/zfeature_common.o pics/zfs_comutil.o pics/zfs_deleg.o pics/zfs_fletcher.o pics/zfs_fletcher_superscalar.o pics/zfs_fletcher_superscalar4.o pics/zfs_namecheck.o pics/zfs_prop.o pics/zpool_prop.o pics/zprop_common.o pics/kernel.o pics/taskq.o pics/util.o pics/kmem_cache_reap_stub.o pics/zfs.o -L/export/home/denis/projects/dilos/illumos/proto/root_i386/lib/amd64 -L/export/home/denis/projects/dilos/illumos/proto/root_i386/usr/lib/amd64 -lcmdutils -lumem -lavl -lnvpair -lz -lc -lsysevent -lmd -lzfs 
