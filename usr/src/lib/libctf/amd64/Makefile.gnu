LIB= libctf.so.1
SRCTREE= ../../..
PROTODIR= $(SRCTREE)/../../proto/root_i386

VPATH+= $(SRCTREE)/common/ctf:$(SRCTREE)/common/list:$(SRCTREE)/lib/mergeq:../common
SRCS= ctf_create.c ctf_decl.c ctf_dwarf.c ctf_error.c ctf_hash.c ctf_labels.c \
ctf_lookup.c ctf_open.c ctf_types.c ctf_util.c ctf_convert.c ctf_elfwrite.c \
ctf_diff.c ctf_lib.c ctf_merge.c ctf_subr.c list.c mergeq.c workq.c 

OBJS= $(SRCS:%.c=%.o)
CC= gcc

CFLAGS+= -D__sun -D__dilos__ -D__dilos -O0 -ggdb3 -Ui386 -U__i386 -fpic -std=gnu99 \
-DTEXT_DOMAIN=\"SUNW_OST_OSLIB\" -D_TS_ERRNO \
-I$(PROTODIR)/usr/include -I../common -I$(SRCTREE)/common/ctf \
-I$(SRCTREE)/lib/mergeq -DCTF_OLD_VERSIONS -DPIC -D_REENTRANT

LDFLAGS+= -nodefaultlibs -Wl,-ztext -Wl,-zdefs \
-L$(PROTODIR)/lib/amd64 -L$(PROTODIR)/usr/lib/amd64 \
-lc -lelf -ldwarf -lavl 

all: $(LIB)

$(LIB): $(OBJS)
	$(CC) -o $@ -shared -h $@ $^ $(LDFLAGS)

clean:
	$(RM) $(OBJS) $(LIB)
