#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the "License").
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/OPENSOLARIS.LICENSE.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets "[]" replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#
#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# Copyright (c) 2018, Joyent, Inc.

LIBRARY=	audit_binfile.a
VERS=		.1
OBJECTS=	binfile.o

LIBBSM=		$(SRC)/lib/libbsm/common

include		$(SRC)/lib/Makefile.lib

LIBS=		$(DYNLIB)
LDLIBS		+= -lbsm -lsecdb -lc

CFLAGS		+= $(CCVERBOSE)
CFLAGS		+= -DTM_GMTOFF=tm_gmtoff -DTM_ZONE=tm_zone -DSTD_INSPIRED -DPCTS
CFLAGS		+= -DTZDIR=\"/usr/share/zoneinfo\"
CFLAGS += -DUSG_COMPAT=1 -DALTZONE=1

CPPFLAGS	+= -D_REENTRANT -I$(LIBBSM)
CPPFLAGS	+= -D_LARGEFILE_SOURCE -D_FILE_OFFSET_BITS=64
CPPFLAGS	+= -I$(SRC)/common/tzcode/stdtime

CERRWARN	+= -_gcc=-Wno-parentheses

# not linted
SMATCH=off

ROOTLIBDIR=	$(ROOT)/usr/lib/security
ROOTLIBDIR64=	$(ROOT)/usr/lib/security/$(MACH64)

.KEEP_STATE:

all:	$(LIBS)


include		../../../Makefile.targ
