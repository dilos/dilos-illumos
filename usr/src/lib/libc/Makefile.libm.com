#
# This file and its contents are supplied under the terms of the
# Common Development and Distribution License ("CDDL"), version 1.0.
# You may only use this file in accordance with the terms of version
# 1.0 of the CDDL.
#
# A full copy of the text of the CDDL should have accompanied this
# source.  A copy of the CDDL is also available via the Internet at
# http://www.illumos.org/license/CDDL.
#

#
# Copyright 2011 Nexenta Systems, Inc.  All rights reserved.
#


MATH_LDBLDIR_sparc	= Q
MATH_LDBLDIR_i386	= LD
MATH_LDBLDIR		= $(MATH_LDBLDIR_$(MACH))

sparc_MATH_CFLAGS	+= -Wa,-xarch=v8plus

MATH_CPPFLAGS	= \
		$(C_PICFLAGS) $(XSTRCONST) \
		-I$(LIBCBASE)/../port/m/C \
		-I$(LIBCBASE)/../port/m/$(MATH_LDBLDIR) \
		-I$(LIBCBASE)/../$(TARGET_ARCH)/m

MATH_CFLAGS_i386	= -_gcc=-ffloat-store

MATH_CPPFLAGS	+= \
		$(MATH_CFLAGS_$(TARGET_ARCH))
		_gcc=-D__C99FEATURES__ \

# libm depends on integer overflow characteristics
MATH_CPPFLAGS	+= -_gcc=-fno-strict-overflow

# sparse currently has no _Complex support
#MATH_CPPFLAGS	+= -_smatch=off

MATH_FPDEF_amd64	= -DARCH_amd64
MATH_FPDEF_sparc	= -DCG89 -DARCH_v8plus -DFPADD_TRAPS_INCOMPLETE_ON_NAN
MATH_FPDEF_sparcv9	= -DARCH_v9 -DFPADD_TRAPS_INCOMPLETE_ON_NAN
MATH_FPDEF		= $(MATH_FPDEF_$(TARGET_ARCH))

MATH_ASFLAGS		= -D_ASM $(MATH_FPDEF)

MATH_XARCH_sparc	= v8plus
MATH_XARCH_sparcv9	= v9
MATH_XARCH_i386		= f80387
MATH_XARCH_amd64	= amd64
MATH_XARCH		= $(MATH_XARCH_$(TARGET_ARCH))

MATH_ASOPT_sparc	= -xarch=$(MATH_XARCH) $(AS_PICFLAGS)
MATH_ASOPT_sparcv9	= -xarch=$(MATH_XARCH) $(AS_PICFLAGS)
MATH_ASOPT_i386 	=
MATH_ASOPT_amd64	= -xarch=$(MATH_XARCH) $(AS_PICFLAGS)
MATH_ASOPT		= $(MATH_ASOPT_$(TARGET_ARCH))
MATH_ASFLAGS		= $(MATH_ASOPT)

#$(OBJS_M9XSSE)  := CFLAGS += -xarch=sse2
MATH_XARCH_M9XSSE_i386	= -xarch=sse2
MATH_XARCH_M9XSSE	= $(MATH_XARCH_M9XSSE_$(TARGET_ARCH))

#$(MATH_OBJS_M9XSSE:%=pics/% ) := MATH_CPPFLAGS += $(MATH_XARCH_M9XSSE)

MATH_CPPFLAGS		+= $(MATH_CFLAGS)
MATH_ASFLAGS		+= $(MATH_CPPFLAGS)

m9xsseMATH_OBJS_i386 = \
		__fex_hdlr.o \
		__fex_i386.o \
		__fex_sse.o \
		__fex_sym.o \
		fex_log.o

m9xsseMATH_OBJS	= $(m9xsseMATH_OBJS_$(TARGET_ARCH))

m9xMATH_OBJS_amd64 = \
		__fex_sse.o \
		feprec.o

m9xMATH_OBJS_sparc = \
		lrint.o \
		lrintf.o \
		lrintl.o \
		lround.o \
		lroundf.o \
		lroundl.o

m9xMATH_OBJS_i386 = \
		__fex_sse.o \
		feprec.o \
		lrint.o \
		lrintf.o \
		lrintl.o \
		lround.o \
		lroundf.o \
		lroundl.o

#
# lrint.o, lrintf.o, lrintl.o, lround.o, lroundf.o & lroundl.o are 32-bit only
#
m9xMATH_OBJS	= \
		$(m9xMATH_OBJS_$(TARGET_ARCH)) \
		__fex_$(MACH).o \
		__fex_hdlr.o \
		__fex_sym.o \
		fdim.o \
		fdimf.o \
		fdiml.o \
		feexcept.o \
		fenv.o \
		feround.o \
		fex_handler.o \
		fex_log.o \
		fma.o \
		fmaf.o \
		fmal.o \
		fmax.o \
		fmaxf.o \
		fmaxl.o \
		fmin.o \
		fminf.o \
		fminl.o \
		frexp.o \
		frexpf.o \
		frexpl.o \
		ldexp.o \
		ldexpf.o \
		ldexpl.o \
		llrint.o \
		llrintf.o \
		llrintl.o \
		llround.o \
		llroundf.o \
		llroundl.o \
		modf.o \
		modff.o \
		modfl.o \
		nan.o \
		nanf.o \
		nanl.o \
		nearbyint.o \
		nearbyintf.o \
		nearbyintl.o \
		nexttoward.o \
		nexttowardf.o \
		nexttowardl.o \
		remquo.o \
		remquof.o \
		remquol.o \
		round.o \
		roundf.o \
		roundl.o \
		scalbln.o \
		scalblnf.o \
		scalblnl.o \
		tgamma.o \
		tgammaf.o \
		tgammal.o \
		trunc.o \
		truncf.o \
		truncl.o

MATH_OBJS_M9XSSE = $(m9xsseMATH_OBJS:%=pics/%)

MATH_COBJS_i386	= \
		__libx_errno.o

MATH_COBJS_sparc = \
		$(MATH_COBJS_i386) \
		_TBL_atan.o \
		_TBL_exp2.o \
		_TBL_log.o \
		_TBL_log2.o \
		_TBL_tan.o \
		__tan.o \
		__tanf.o

#
# atan2pi.o and sincospi.o is for internal use only
#

MATH_COBJS_amd64 = \
		_TBL_atan.o \
		_TBL_exp2.o \
		_TBL_log.o \
		_TBL_log2.o \
		__tan.o \
		__tanf.o \
		_TBL_tan.o \
		copysign.o \
		exp.o \
		fabs.o \
		fmod.o \
		ilogb.o \
		isnan.o \
		nextafter.o \
		remainder.o \
		rint.o \
		scalbn.o

MATH_COBJS_sparcv9 = $(MATH_COBJS_amd64)

MATH_COBJS	= \
		$(MATH_COBJS_$(TARGET_ARCH)) \
		__cos.o \
		__lgamma.o \
		__rem_pio2.o \
		__rem_pio2m.o \
		__sin.o \
		__sincos.o \
		_lib_version.o \
		_SVID_error.o \
		_TBL_ipio2.o \
		_TBL_sin.o \
		acos.o \
		acosh.o \
		asin.o \
		asinh.o \
		atan.o \
		atan2.o \
		atan2pi.o \
		atanh.o \
		cbrt.o \
		ceil.o \
		cos.o \
		cosh.o \
		erf.o \
		exp10.o \
		exp2.o \
		expm1.o \
		floor.o \
		gamma.o \
		gamma_r.o \
		hypot.o \
		j0.o \
		j1.o \
		jn.o \
		lgamma.o \
		lgamma_r.o \
		log.o \
		log10.o \
		log1p.o \
		log2.o \
		logb.o \
		matherr.o \
		pow.o \
		scalb.o \
		signgam.o \
		significand.o \
		sin.o \
		sincos.o \
		sincospi.o \
		sinh.o \
		sqrt.o \
		tan.o \
		tanh.o

#
# LSARC/2003/658 adds isnanl
#
MATH_QOBJS_sparc = \
		_TBL_atanl.o \
		_TBL_expl.o \
		_TBL_expm1l.o \
		_TBL_logl.o \
		finitel.o \
		isnanl.o

MATH_QOBJS_sparcv9 = $(MATH_QOBJS_sparc)

MATH_QOBJS_amd64 = \
		finitel.o \
		isnanl.o

#
# atan2pil.o, ieee_funcl.o, rndintl.o, sinpil.o, sincospil.o
# are for internal use only
#
# LSARC/2003/279 adds the following:
#		gammal.o	1
#		gammal_r.o	1
#		j0l.o		2
#		j1l.o		2
#		jnl.o		2
#		lgammal_r.o	1
#		scalbl.o	1
#		significandl.o	1
#
MATH_QOBJS	= \
		$(MATH_QOBJS_$(TARGET_ARCH)) \
		__cosl.o \
		__lgammal.o \
		__poly_libmq.o \
		__rem_pio2l.o \
		__sincosl.o \
		__sinl.o \
		__tanl.o \
		_TBL_cosl.o \
		_TBL_ipio2l.o \
		_TBL_sinl.o \
		_TBL_tanl.o \
		acoshl.o \
		acosl.o \
		asinhl.o \
		asinl.o \
		atan2l.o \
		atan2pil.o \
		atanhl.o \
		atanl.o \
		cbrtl.o \
		copysignl.o \
		coshl.o \
		cosl.o \
		erfl.o \
		exp10l.o \
		exp2l.o \
		expl.o \
		expm1l.o \
		fabsl.o \
		floorl.o \
		fmodl.o \
		gammal.o \
		gammal_r.o \
		hypotl.o \
		ieee_funcl.o \
		ilogbl.o \
		j0l.o \
		j1l.o \
		jnl.o \
		lgammal.o \
		lgammal_r.o \
		log10l.o \
		log1pl.o \
		log2l.o \
		logbl.o \
		logl.o \
		nextafterl.o \
		powl.o \
		remainderl.o \
		rintl.o \
		rndintl.o \
		scalbl.o \
		scalbnl.o \
		signgaml.o \
		significandl.o \
		sincosl.o \
		sincospil.o \
		sinhl.o \
		sinl.o \
		sinpil.o \
		sqrtl.o \
		tanhl.o \
		tanl.o

#
# LSARC/2003/658 adds isnanf
#
MATH_ROBJS_sparc = \
		__cosf.o \
		__sincosf.o \
		__sinf.o \
		isnanf.o

MATH_ROBJS_sparcv9 = $(MATH_ROBJS_sparc)

MATH_ROBJS_amd64 = \
		isnanf.o \
		__cosf.o \
		__sincosf.o \
		__sinf.o

#
# atan2pif.o, sincosf.o, sincospif.o are for internal use only
#
# LSARC/2003/279 adds the following:
#		besself.o	6
#		scalbf.o	1
#		gammaf.o	1
#		gammaf_r.o	1
#		lgammaf_r.o	1
#		significandf.o	1
#
MATH_ROBJS	= \
		$(MATH_ROBJS_$(TARGET_ARCH)) \
		_TBL_r_atan_.o \
		acosf.o \
		acoshf.o \
		asinf.o \
		asinhf.o \
		atan2f.o \
		atan2pif.o \
		atanf.o \
		atanhf.o \
		besself.o \
		cbrtf.o \
		copysignf.o \
		cosf.o \
		coshf.o \
		erff.o \
		exp10f.o \
		exp2f.o \
		expf.o \
		expm1f.o \
		fabsf.o \
		floorf.o \
		fmodf.o \
		gammaf.o \
		gammaf_r.o \
		hypotf.o \
		ilogbf.o \
		lgammaf.o \
		lgammaf_r.o \
		log10f.o \
		log1pf.o \
		log2f.o \
		logbf.o \
		logf.o \
		nextafterf.o \
		powf.o \
		remainderf.o \
		rintf.o \
		scalbf.o \
		scalbnf.o \
		signgamf.o \
		significandf.o \
		sinf.o \
		sinhf.o \
		sincosf.o \
		sincospif.o \
		sqrtf.o \
		tanf.o \
		tanhf.o

#
# LSARC/2003/658 adds isnanf/isnanl
#

MATH_SOBJS_sparc = \
		copysign.o \
		exp.o \
		fabs.o \
		fmod.o \
		ilogb.o \
		isnan.o \
		nextafter.o \
		remainder.o \
		rint.o \
		scalbn.o

MATH_SOBJS_i386	= \
		__reduction.o \
		finitef.o \
		finitel.o \
		isnanf.o \
		isnanl.o \
		$(MATH_SOBJS_sparc)

MATH_SOBJS_amd64 = \
		__swapFLAGS.o
#		_xtoll.o \
#		_xtoull.o \


MATH_SOBJS	= \
		$(MATH_SOBJS_$(TARGET_ARCH))

complexMATH_OBJS = \
		cabs.o \
		cabsf.o \
		cabsl.o \
		cacos.o \
		cacosf.o \
		cacosh.o \
		cacoshf.o \
		cacoshl.o \
		cacosl.o \
		carg.o \
		cargf.o \
		cargl.o \
		casin.o \
		casinf.o \
		casinh.o \
		casinhf.o \
		casinhl.o \
		casinl.o \
		catan.o \
		catanf.o \
		catanh.o \
		catanhf.o \
		catanhl.o \
		catanl.o \
		ccos.o \
		ccosf.o \
		ccosh.o \
		ccoshf.o \
		ccoshl.o \
		ccosl.o \
		cexp.o \
		cexpf.o \
		cexpl.o \
		cimag.o \
		cimagf.o \
		cimagl.o \
		clog.o \
		clogf.o \
		clogl.o \
		conj.o \
		conjf.o \
		conjl.o \
		cpow.o \
		cpowf.o \
		cpowl.o \
		cproj.o \
		cprojf.o \
		cprojl.o \
		creal.o \
		crealf.o \
		creall.o \
		csin.o \
		csinf.o \
		csinh.o \
		csinhf.o \
		csinhl.o \
		csinl.o \
		csqrt.o \
		csqrtf.o \
		csqrtl.o \
		ctan.o \
		ctanf.o \
		ctanh.o \
		ctanhf.o \
		ctanhl.o \
		ctanl.o \
		k_atan2.o \
		k_atan2l.o \
		k_cexp.o \
		k_cexpl.o \
		k_clog_r.o \
		k_clog_rl.o

PORTM		= \
		$(MATH_COBJS) \
		$(MATH_ROBJS) \
		$(MATH_QOBJS) \
		$(MATH_SOBJS) \
		$(m9xMATH_OBJS) \
		$(complexMATH_OBJS)

#include		$(SRC)/lib/Makefile.lib
#include		$(SRC)/lib/Makefile.rootfs

MATH_SRCDIR	= ../port/m
#LIBS		= $(DYNLIB)

CFLAGS		+= $(C_BIGPICFLAGS)
CFLAGS64	+= $(C_BIGPICFLAGS)

MATH_SRCS_LD_i386_amd64 = \
	$(MATH_SRCDIR)LD/finitel.c \
	$(MATH_SRCDIR)LD/isnanl.c \
	$(MATH_SRCDIR)LD/nextafterl.c

MATH_SRCS_LD = \
	$(MATH_SRCS_LD_i386_$(TARGET_ARCH)) \
	$(MATH_SRCDIR)LD/__cosl.c \
	$(MATH_SRCDIR)LD/__lgammal.c \
	$(MATH_SRCDIR)LD/__poly_libmq.c \
	$(MATH_SRCDIR)LD/__rem_pio2l.c \
	$(MATH_SRCDIR)LD/__sincosl.c \
	$(MATH_SRCDIR)LD/__sinl.c \
	$(MATH_SRCDIR)LD/__tanl.c \
	$(MATH_SRCDIR)LD/_TBL_cosl.c \
	$(MATH_SRCDIR)LD/_TBL_ipio2l.c \
	$(MATH_SRCDIR)LD/_TBL_sinl.c \
	$(MATH_SRCDIR)LD/_TBL_tanl.c \
	$(MATH_SRCDIR)LD/acoshl.c \
	$(MATH_SRCDIR)LD/asinhl.c \
	$(MATH_SRCDIR)LD/atan2pil.c \
	$(MATH_SRCDIR)LD/atanhl.c \
	$(MATH_SRCDIR)LD/cbrtl.c \
	$(MATH_SRCDIR)LD/coshl.c \
	$(MATH_SRCDIR)LD/cosl.c \
	$(MATH_SRCDIR)LD/erfl.c \
	$(MATH_SRCDIR)LD/gammal.c \
	$(MATH_SRCDIR)LD/gammal_r.c \
	$(MATH_SRCDIR)LD/hypotl.c \
	$(MATH_SRCDIR)LD/j0l.c \
	$(MATH_SRCDIR)LD/j1l.c \
	$(MATH_SRCDIR)LD/jnl.c \
	$(MATH_SRCDIR)LD/lgammal.c \
	$(MATH_SRCDIR)LD/lgammal_r.c \
	$(MATH_SRCDIR)LD/log1pl.c \
	$(MATH_SRCDIR)LD/logbl.c \
	$(MATH_SRCDIR)LD/scalbl.c \
	$(MATH_SRCDIR)LD/signgaml.c \
	$(MATH_SRCDIR)LD/significandl.c \
	$(MATH_SRCDIR)LD/sincosl.c \
	$(MATH_SRCDIR)LD/sincospil.c \
	$(MATH_SRCDIR)LD/sinhl.c \
	$(MATH_SRCDIR)LD/sinl.c \
	$(MATH_SRCDIR)LD/sinpil.c \
	$(MATH_SRCDIR)LD/tanhl.c \
	$(MATH_SRCDIR)LD/tanl.c

MATH_SRCS_LD_i386 = \
	$(MATH_SRCS_LD)

MATH_SRCS_R_amd64 = \
	$(MATH_SRCDIR)R/__tanf.c \
	$(MATH_SRCDIR)R/isnanf.c \
	$(MATH_SRCDIR)R/__cosf.c \
	$(MATH_SRCDIR)R/__sincosf.c \
	$(MATH_SRCDIR)R/__sinf.c \
	$(MATH_SRCDIR)R/acosf.c \
	$(MATH_SRCDIR)R/asinf.c \
	$(MATH_SRCDIR)R/atan2f.c \
	$(MATH_SRCDIR)R/copysignf.c \
	$(MATH_SRCDIR)R/exp10f.c \
	$(MATH_SRCDIR)R/exp2f.c \
	$(MATH_SRCDIR)R/expm1f.c \
	$(MATH_SRCDIR)R/fabsf.c \
	$(MATH_SRCDIR)R/hypotf.c \
	$(MATH_SRCDIR)R/ilogbf.c \
	$(MATH_SRCDIR)R/log10f.c \
	$(MATH_SRCDIR)R/log2f.c \
	$(MATH_SRCDIR)R/nextafterf.c \
	$(MATH_SRCDIR)R/powf.c \
	$(MATH_SRCDIR)R/rintf.c \
	$(MATH_SRCDIR)R/scalbnf.c

# sparc + sparcv9
MATH_SRCS_R_sparc = \
	$(MATH_SRCDIR)R/__tanf.c \
	$(MATH_SRCDIR)R/__cosf.c \
	$(MATH_SRCDIR)R/__sincosf.c \
	$(MATH_SRCDIR)R/__sinf.c \
	$(MATH_SRCDIR)R/isnanf.c \
	$(MATH_SRCDIR)R/acosf.c \
	$(MATH_SRCDIR)R/asinf.c \
	$(MATH_SRCDIR)R/atan2f.c \
	$(MATH_SRCDIR)R/copysignf.c \
	$(MATH_SRCDIR)R/exp10f.c \
	$(MATH_SRCDIR)R/exp2f.c \
	$(MATH_SRCDIR)R/expm1f.c \
	$(MATH_SRCDIR)R/fabsf.c \
	$(MATH_SRCDIR)R/fmodf.c \
	$(MATH_SRCDIR)R/hypotf.c \
	$(MATH_SRCDIR)R/ilogbf.c \
	$(MATH_SRCDIR)R/log10f.c \
	$(MATH_SRCDIR)R/log2f.c \
	$(MATH_SRCDIR)R/nextafterf.c \
	$(MATH_SRCDIR)R/powf.c \
	$(MATH_SRCDIR)R/remainderf.c \
	$(MATH_SRCDIR)R/rintf.c \
	$(MATH_SRCDIR)R/scalbnf.c

MATH_SRCS_R = \
	$(MATH_SRCS_R_$(MACH)) \
	$(MATH_SRCS_R_$(TARGET_ARCH)) \
	$(MATH_SRCDIR)R/_TBL_r_atan_.c \
	$(MATH_SRCDIR)R/acoshf.c \
	$(MATH_SRCDIR)R/asinhf.c \
	$(MATH_SRCDIR)R/atan2pif.c \
	$(MATH_SRCDIR)R/atanf.c \
	$(MATH_SRCDIR)R/atanhf.c \
	$(MATH_SRCDIR)R/besself.c \
	$(MATH_SRCDIR)R/cbrtf.c \
	$(MATH_SRCDIR)R/cosf.c \
	$(MATH_SRCDIR)R/coshf.c \
	$(MATH_SRCDIR)R/erff.c \
	$(MATH_SRCDIR)R/expf.c \
	$(MATH_SRCDIR)R/floorf.c \
	$(MATH_SRCDIR)R/gammaf.c \
	$(MATH_SRCDIR)R/gammaf_r.c \
	$(MATH_SRCDIR)R/lgammaf.c \
	$(MATH_SRCDIR)R/lgammaf_r.c \
	$(MATH_SRCDIR)R/log1pf.c \
	$(MATH_SRCDIR)R/logbf.c \
	$(MATH_SRCDIR)R/logf.c \
	$(MATH_SRCDIR)R/scalbf.c \
	$(MATH_SRCDIR)R/signgamf.c \
	$(MATH_SRCDIR)R/significandf.c \
	$(MATH_SRCDIR)R/sinf.c \
	$(MATH_SRCDIR)R/sinhf.c \
	$(MATH_SRCDIR)R/sincosf.c \
	$(MATH_SRCDIR)R/sincospif.c \
	$(MATH_SRCDIR)R/sqrtf.c \
	$(MATH_SRCDIR)R/tanf.c \
	$(MATH_SRCDIR)R/tanhf.c

MATH_SRCS_Q = \
	$(MATH_SRCDIR)Q/_TBL_atanl.c \
	$(MATH_SRCDIR)Q/_TBL_expl.c \
	$(MATH_SRCDIR)Q/_TBL_expm1l.c \
	$(MATH_SRCDIR)Q/_TBL_logl.c \
	$(MATH_SRCDIR)Q/finitel.c \
	$(MATH_SRCDIR)Q/isnanl.c \
	$(MATH_SRCDIR)Q/__cosl.c \
	$(MATH_SRCDIR)Q/__lgammal.c \
	$(MATH_SRCDIR)Q/__poly_libmq.c \
	$(MATH_SRCDIR)Q/__rem_pio2l.c \
	$(MATH_SRCDIR)Q/__sincosl.c \
	$(MATH_SRCDIR)Q/__sinl.c \
	$(MATH_SRCDIR)Q/__tanl.c \
	$(MATH_SRCDIR)Q/_TBL_cosl.c \
	$(MATH_SRCDIR)Q/_TBL_ipio2l.c \
	$(MATH_SRCDIR)Q/_TBL_sinl.c \
	$(MATH_SRCDIR)Q/_TBL_tanl.c \
	$(MATH_SRCDIR)Q/acoshl.c \
	$(MATH_SRCDIR)Q/acosl.c \
	$(MATH_SRCDIR)Q/asinhl.c \
	$(MATH_SRCDIR)Q/asinl.c \
	$(MATH_SRCDIR)Q/atan2l.c \
	$(MATH_SRCDIR)Q/atan2pil.c \
	$(MATH_SRCDIR)Q/atanhl.c \
	$(MATH_SRCDIR)Q/atanl.c \
	$(MATH_SRCDIR)Q/cbrtl.c \
	$(MATH_SRCDIR)Q/copysignl.c \
	$(MATH_SRCDIR)Q/coshl.c \
	$(MATH_SRCDIR)Q/cosl.c \
	$(MATH_SRCDIR)Q/erfl.c \
	$(MATH_SRCDIR)Q/exp10l.c \
	$(MATH_SRCDIR)Q/exp2l.c \
	$(MATH_SRCDIR)Q/expl.c \
	$(MATH_SRCDIR)Q/expm1l.c \
	$(MATH_SRCDIR)Q/fabsl.c \
	$(MATH_SRCDIR)Q/floorl.c \
	$(MATH_SRCDIR)Q/fmodl.c \
	$(MATH_SRCDIR)Q/gammal.c \
	$(MATH_SRCDIR)Q/gammal_r.c \
	$(MATH_SRCDIR)Q/hypotl.c \
	$(MATH_SRCDIR)Q/ieee_funcl.c \
	$(MATH_SRCDIR)Q/ilogbl.c \
	$(MATH_SRCDIR)Q/j0l.c \
	$(MATH_SRCDIR)Q/j1l.c \
	$(MATH_SRCDIR)Q/jnl.c \
	$(MATH_SRCDIR)Q/lgammal.c \
	$(MATH_SRCDIR)Q/lgammal_r.c \
	$(MATH_SRCDIR)Q/log10l.c \
	$(MATH_SRCDIR)Q/log1pl.c \
	$(MATH_SRCDIR)Q/log2l.c \
	$(MATH_SRCDIR)Q/logbl.c \
	$(MATH_SRCDIR)Q/logl.c \
	$(MATH_SRCDIR)Q/nextafterl.c \
	$(MATH_SRCDIR)Q/powl.c \
	$(MATH_SRCDIR)Q/remainderl.c \
	$(MATH_SRCDIR)Q/rintl.c \
	$(MATH_SRCDIR)Q/rndintl.c \
	$(MATH_SRCDIR)Q/scalbl.c \
	$(MATH_SRCDIR)Q/scalbnl.c \
	$(MATH_SRCDIR)Q/signgaml.c \
	$(MATH_SRCDIR)Q/significandl.c \
	$(MATH_SRCDIR)Q/sincosl.c \
	$(MATH_SRCDIR)Q/sincospil.c \
	$(MATH_SRCDIR)Q/sinhl.c \
	$(MATH_SRCDIR)Q/sinl.c \
	$(MATH_SRCDIR)Q/sinpil.c \
	$(MATH_SRCDIR)Q/sqrtl.c \
	$(MATH_SRCDIR)Q/tanhl.c \
	$(MATH_SRCDIR)Q/tanl.c

MATH_SRCS_Q_sparc = \
	$(MATH_SRCS_Q)

MATH_SRCS_complex = \
	$(MATH_SRCDIR)complex/cabs.c \
	$(MATH_SRCDIR)complex/cabsf.c \
	$(MATH_SRCDIR)complex/cabsl.c \
	$(MATH_SRCDIR)complex/cacos.c \
	$(MATH_SRCDIR)complex/cacosf.c \
	$(MATH_SRCDIR)complex/cacosh.c \
	$(MATH_SRCDIR)complex/cacoshf.c \
	$(MATH_SRCDIR)complex/cacoshl.c \
	$(MATH_SRCDIR)complex/cacosl.c \
	$(MATH_SRCDIR)complex/carg.c \
	$(MATH_SRCDIR)complex/cargf.c \
	$(MATH_SRCDIR)complex/cargl.c \
	$(MATH_SRCDIR)complex/casin.c \
	$(MATH_SRCDIR)complex/casinf.c \
	$(MATH_SRCDIR)complex/casinh.c \
	$(MATH_SRCDIR)complex/casinhf.c \
	$(MATH_SRCDIR)complex/casinhl.c \
	$(MATH_SRCDIR)complex/casinl.c \
	$(MATH_SRCDIR)complex/catan.c \
	$(MATH_SRCDIR)complex/catanf.c \
	$(MATH_SRCDIR)complex/catanh.c \
	$(MATH_SRCDIR)complex/catanhf.c \
	$(MATH_SRCDIR)complex/catanhl.c \
	$(MATH_SRCDIR)complex/catanl.c \
	$(MATH_SRCDIR)complex/ccos.c \
	$(MATH_SRCDIR)complex/ccosf.c \
	$(MATH_SRCDIR)complex/ccosh.c \
	$(MATH_SRCDIR)complex/ccoshf.c \
	$(MATH_SRCDIR)complex/ccoshl.c \
	$(MATH_SRCDIR)complex/ccosl.c \
	$(MATH_SRCDIR)complex/cexp.c \
	$(MATH_SRCDIR)complex/cexpf.c \
	$(MATH_SRCDIR)complex/cexpl.c \
	$(MATH_SRCDIR)complex/cimag.c \
	$(MATH_SRCDIR)complex/cimagf.c \
	$(MATH_SRCDIR)complex/cimagl.c \
	$(MATH_SRCDIR)complex/clog.c \
	$(MATH_SRCDIR)complex/clogf.c \
	$(MATH_SRCDIR)complex/clogl.c \
	$(MATH_SRCDIR)complex/conj.c \
	$(MATH_SRCDIR)complex/conjf.c \
	$(MATH_SRCDIR)complex/conjl.c \
	$(MATH_SRCDIR)complex/cpow.c \
	$(MATH_SRCDIR)complex/cpowf.c \
	$(MATH_SRCDIR)complex/cpowl.c \
	$(MATH_SRCDIR)complex/cproj.c \
	$(MATH_SRCDIR)complex/cprojf.c \
	$(MATH_SRCDIR)complex/cprojl.c \
	$(MATH_SRCDIR)complex/creal.c \
	$(MATH_SRCDIR)complex/crealf.c \
	$(MATH_SRCDIR)complex/creall.c \
	$(MATH_SRCDIR)complex/csin.c \
	$(MATH_SRCDIR)complex/csinf.c \
	$(MATH_SRCDIR)complex/csinh.c \
	$(MATH_SRCDIR)complex/csinhf.c \
	$(MATH_SRCDIR)complex/csinhl.c \
	$(MATH_SRCDIR)complex/csinl.c \
	$(MATH_SRCDIR)complex/csqrt.c \
	$(MATH_SRCDIR)complex/csqrtf.c \
	$(MATH_SRCDIR)complex/csqrtl.c \
	$(MATH_SRCDIR)complex/ctan.c \
	$(MATH_SRCDIR)complex/ctanf.c \
	$(MATH_SRCDIR)complex/ctanh.c \
	$(MATH_SRCDIR)complex/ctanhf.c \
	$(MATH_SRCDIR)complex/ctanhl.c \
	$(MATH_SRCDIR)complex/ctanl.c \
	$(MATH_SRCDIR)complex/k_atan2.c \
	$(MATH_SRCDIR)complex/k_atan2l.c \
	$(MATH_SRCDIR)complex/k_cexp.c \
	$(MATH_SRCDIR)complex/k_cexpl.c \
	$(MATH_SRCDIR)complex/k_clog_r.c \
	$(MATH_SRCDIR)complex/k_clog_rl.c

MATH_SRCS_m9x_i386 = \
	$(MATH_SRCDIR)m9x/__fex_sse.c \
	$(MATH_SRCDIR)m9x/feprec.c \
	$(MATH_SRCDIR)m9x/__fex_i386.c

MATH_SRCS_m9x_i386_i386 = \
	$(MATH_SRCDIR)m9x/lroundf.c

MATH_SRCS_m9x_i386_amd64 = \
	$(MATH_SRCDIR)m9x/llrint.c \
	$(MATH_SRCDIR)m9x/llrintf.c \
	$(MATH_SRCDIR)m9x/llrintl.c \
	$(MATH_SRCDIR)m9x/nexttowardl.c \
	$(MATH_SRCDIR)m9x/remquo.c \
	$(MATH_SRCDIR)m9x/remquof.c \
	$(MATH_SRCDIR)m9x/round.c \
	$(MATH_SRCDIR)m9x/roundl.c \
	$(MATH_SRCDIR)m9x/scalbln.c \
	$(MATH_SRCDIR)m9x/scalblnf.c \
	$(MATH_SRCDIR)m9x/scalblnl.c \
	$(MATH_SRCDIR)m9x/trunc.c \
	$(MATH_SRCDIR)m9x/truncl.c

# sparc
MATH_SRCS_m9x_sparc_sparc = \
	$(MATH_SRCDIR)m9x/lrint.c \
	$(MATH_SRCDIR)m9x/lrintf.c \
	$(MATH_SRCDIR)m9x/lrintl.c \
	$(MATH_SRCDIR)m9x/lround.c \
	$(MATH_SRCDIR)m9x/lroundf.c \
	$(MATH_SRCDIR)m9x/lroundl.c

MATH_SRCS_m9x_sparc = \
	$(MATH_SRCDIR)m9x/__fex_sparc.c \
	$(MATH_SRCDIR)m9x/llrint.c \
	$(MATH_SRCDIR)m9x/llrintf.c \
	$(MATH_SRCDIR)m9x/llrintl.c \
	$(MATH_SRCDIR)m9x/nexttowardl.c \
	$(MATH_SRCDIR)m9x/remquo.c \
	$(MATH_SRCDIR)m9x/remquof.c \
	$(MATH_SRCDIR)m9x/remquol.c \
	$(MATH_SRCDIR)m9x/round.c \
	$(MATH_SRCDIR)m9x/roundl.c \
	$(MATH_SRCDIR)m9x/scalbln.c \
	$(MATH_SRCDIR)m9x/scalblnf.c \
	$(MATH_SRCDIR)m9x/scalblnl.c \
	$(MATH_SRCDIR)m9x/trunc.c \
	$(MATH_SRCDIR)m9x/truncl.c

MATH_SRCS_m9x = \
	$(MATH_SRCS_m9x_$(MACH)) \
	$(MATH_SRCS_m9x_sparc_$(TARGET_ARCH)) \
	$(MATH_SRCS_m9x_i386_$(TARGET_ARCH)) \
	$(MATH_SRCDIR)m9x/__fex_hdlr.c \
	$(MATH_SRCDIR)m9x/__fex_sym.c \
	$(MATH_SRCDIR)m9x/fdim.c \
	$(MATH_SRCDIR)m9x/fdimf.c \
	$(MATH_SRCDIR)m9x/fdiml.c \
	$(MATH_SRCDIR)m9x/feexcept.c \
	$(MATH_SRCDIR)m9x/fenv.c \
	$(MATH_SRCDIR)m9x/feround.c \
	$(MATH_SRCDIR)m9x/fex_handler.c \
	$(MATH_SRCDIR)m9x/fex_log.c \
	$(MATH_SRCDIR)m9x/fma.c \
	$(MATH_SRCDIR)m9x/fmaf.c \
	$(MATH_SRCDIR)m9x/fmal.c \
	$(MATH_SRCDIR)m9x/fmax.c \
	$(MATH_SRCDIR)m9x/fmaxf.c \
	$(MATH_SRCDIR)m9x/fmaxl.c \
	$(MATH_SRCDIR)m9x/fmin.c \
	$(MATH_SRCDIR)m9x/fminf.c \
	$(MATH_SRCDIR)m9x/fminl.c \
	$(MATH_SRCDIR)m9x/frexp.c \
	$(MATH_SRCDIR)m9x/frexpf.c \
	$(MATH_SRCDIR)m9x/frexpl.c \
	$(MATH_SRCDIR)m9x/ldexp.c \
	$(MATH_SRCDIR)m9x/ldexpf.c \
	$(MATH_SRCDIR)m9x/ldexpl.c \
	$(MATH_SRCDIR)m9x/llround.c \
	$(MATH_SRCDIR)m9x/llroundf.c \
	$(MATH_SRCDIR)m9x/llroundl.c \
	$(MATH_SRCDIR)m9x/modf.c \
	$(MATH_SRCDIR)m9x/modff.c \
	$(MATH_SRCDIR)m9x/modfl.c \
	$(MATH_SRCDIR)m9x/nan.c \
	$(MATH_SRCDIR)m9x/nanf.c \
	$(MATH_SRCDIR)m9x/nanl.c \
	$(MATH_SRCDIR)m9x/nearbyint.c \
	$(MATH_SRCDIR)m9x/nearbyintf.c \
	$(MATH_SRCDIR)m9x/nearbyintl.c \
	$(MATH_SRCDIR)m9x/nexttoward.c \
	$(MATH_SRCDIR)m9x/nexttowardf.c \
	$(MATH_SRCDIR)m9x/roundf.c \
	$(MATH_SRCDIR)m9x/tgamma.c \
	$(MATH_SRCDIR)m9x/tgammaf.c \
	$(MATH_SRCDIR)m9x/tgammal.c \
	$(MATH_SRCDIR)m9x/truncf.c

MATH_SRCS_C_sparc = \
	$(MATH_SRCDIR)C/__tan.c \
	$(MATH_SRCDIR)C/_TBL_atan.c \
	$(MATH_SRCDIR)C/_TBL_exp2.c \
	$(MATH_SRCDIR)C/_TBL_log.c \
	$(MATH_SRCDIR)C/_TBL_log2.c \
	$(MATH_SRCDIR)C/_TBL_tan.c \
	$(MATH_SRCDIR)C/acos.c \
	$(MATH_SRCDIR)C/asin.c \
	$(MATH_SRCDIR)C/atan.c \
	$(MATH_SRCDIR)C/atan2.c \
	$(MATH_SRCDIR)C/ceil.c \
	$(MATH_SRCDIR)C/cos.c \
	$(MATH_SRCDIR)C/exp.c \
	$(MATH_SRCDIR)C/exp10.c \
	$(MATH_SRCDIR)C/exp2.c \
	$(MATH_SRCDIR)C/expm1.c \
	$(MATH_SRCDIR)C/floor.c \
	$(MATH_SRCDIR)C/fmod.c \
	$(MATH_SRCDIR)C/hypot.c \
	$(MATH_SRCDIR)C/ilogb.c \
	$(MATH_SRCDIR)C/isnan.c \
	$(MATH_SRCDIR)C/log.c \
	$(MATH_SRCDIR)C/log10.c \
	$(MATH_SRCDIR)C/log2.c \
	$(MATH_SRCDIR)C/pow.c \
	$(MATH_SRCDIR)C/remainder.c \
	$(MATH_SRCDIR)C/rint.c \
	$(MATH_SRCDIR)C/scalbn.c \
	$(MATH_SRCDIR)C/sin.c \
	$(MATH_SRCDIR)C/sincos.c \
	$(MATH_SRCDIR)C/tan.c

MATH_SRCS_i386_i386	= \
	$(MATH_SRCDIR)C/__libx_errno.c

MATH_SRCS_sparc_sparc = \
	$(MATH_SRCS_i386_i386)

MATH_SRCS_sparc_sparcv9 = \
	$(MATH_SRCDIR)C/copysign.c \
	$(MATH_SRCDIR)C/fabs.c \
	$(MATH_SRCDIR)C/nextafter.c

MATH_SRCS_i386_amd64 = \
	$(MATH_SRCDIR)C/_TBL_atan.c \
	$(MATH_SRCDIR)C/_TBL_exp2.c \
	$(MATH_SRCDIR)C/_TBL_log.c \
	$(MATH_SRCDIR)C/_TBL_log2.c \
	$(MATH_SRCDIR)C/__tan.c \
	$(MATH_SRCDIR)C/_TBL_tan.c \
	$(MATH_SRCDIR)C/copysign.c \
	$(MATH_SRCDIR)C/exp.c \
	$(MATH_SRCDIR)C/fabs.c \
	$(MATH_SRCDIR)C/ilogb.c \
	$(MATH_SRCDIR)C/isnan.c \
	$(MATH_SRCDIR)C/nextafter.c \
	$(MATH_SRCDIR)C/rint.c \
	$(MATH_SRCDIR)C/scalbn.c \
	$(MATH_SRCDIR)C/acos.c \
	$(MATH_SRCDIR)C/asin.c \
	$(MATH_SRCDIR)C/atan.c \
	$(MATH_SRCDIR)C/atan2.c \
	$(MATH_SRCDIR)C/ceil.c \
	$(MATH_SRCDIR)C/cos.c \
	$(MATH_SRCDIR)C/exp10.c \
	$(MATH_SRCDIR)C/exp2.c \
	$(MATH_SRCDIR)C/expm1.c \
	$(MATH_SRCDIR)C/floor.c \
	$(MATH_SRCDIR)C/hypot.c \
	$(MATH_SRCDIR)C/log.c \
	$(MATH_SRCDIR)C/log10.c \
	$(MATH_SRCDIR)C/log2.c \
	$(MATH_SRCDIR)C/pow.c \
	$(MATH_SRCDIR)C/sin.c \
	$(MATH_SRCDIR)C/sincos.c \
	$(MATH_SRCDIR)C/tan.c

MATH_SRCS_C = \
	$(MATH_SRCS_C_$(MACH)) \
	$(MATH_SRCS_C_i386_$(TARGET_ARCH)) \
	$(MATH_SRCDIR)C/__cos.c \
	$(MATH_SRCDIR)C/__lgamma.c \
	$(MATH_SRCDIR)C/__rem_pio2.c \
	$(MATH_SRCDIR)C/__rem_pio2m.c \
	$(MATH_SRCDIR)C/__sin.c \
	$(MATH_SRCDIR)C/__sincos.c \
	$(MATH_SRCDIR)C/_lib_version.c \
	$(MATH_SRCDIR)C/_SVID_error.c \
	$(MATH_SRCDIR)C/_TBL_ipio2.c \
	$(MATH_SRCDIR)C/_TBL_sin.c \
	$(MATH_SRCDIR)C/acosh.c \
	$(MATH_SRCDIR)C/asinh.c \
	$(MATH_SRCDIR)C/atan2pi.c \
	$(MATH_SRCDIR)C/atanh.c \
	$(MATH_SRCDIR)C/cbrt.c \
	$(MATH_SRCDIR)C/cosh.c \
	$(MATH_SRCDIR)C/erf.c \
	$(MATH_SRCDIR)C/gamma.c \
	$(MATH_SRCDIR)C/gamma_r.c \
	$(MATH_SRCDIR)C/j0.c \
	$(MATH_SRCDIR)C/j1.c \
	$(MATH_SRCDIR)C/jn.c \
	$(MATH_SRCDIR)C/lgamma.c \
	$(MATH_SRCDIR)C/lgamma_r.c \
	$(MATH_SRCDIR)C/log1p.c \
	$(MATH_SRCDIR)C/logb.c \
	$(MATH_SRCDIR)C/matherr.c \
	$(MATH_SRCDIR)C/scalb.c \
	$(MATH_SRCDIR)C/signgam.c \
	$(MATH_SRCDIR)C/significand.c \
	$(MATH_SRCDIR)C/sincospi.c \
	$(MATH_SRCDIR)C/sinh.c \
	$(MATH_SRCDIR)C/sqrt.c \
	$(MATH_SRCDIR)C/tanh.c

MATH_SRCS	= \
	$(MATH_SRCS_Q_$(MACH)) \
	$(MATH_SRCS_LD_$(MACH)) \
	$(MATH_SRCS_R) \
	$(MATH_SRCS_complex) \
	$(MATH_SRCS_C)
