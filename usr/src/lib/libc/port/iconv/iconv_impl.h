/*
 * This file and its contents are supplied under the terms of the
 * Common Development and Distribution License ("CDDL"), version 1.0.
 * You may only use this file in accordance with the terms of version
 * 1.0 of the CDDL.
 *
 * A full copy of the text of the CDDL should have accompanied this
 * source.  A copy of the CDDL is also available via the Internet at
 * http://www.illumos.org/license/CDDL.
 */

/*
 * Copyright (c) 2022 DilOS
 */

#ifndef _ICONV_IMPL_H_
#define _ICONV_IMPL_H_

#include <iconv.h>

#include <sys/cdefs.h>
#include <sys/types.h>

#include <wchar.h>

#ifdef __cplusplus
typedef	bool	__iconv_bool;
#elif __STDC_VERSION__ >= 199901L
typedef	_Bool	__iconv_bool;
#else
typedef	int	__iconv_bool;
#endif

__BEGIN_DECLS

#define __ICONV_F_HIDE_INVALID	0x0001

typedef void (*iconv_unicode_char_hook) (unsigned int mbr, void *data);
typedef void (*iconv_wide_char_hook) (wchar_t wc, void *data);

struct iconv_hooks {
	iconv_unicode_char_hook		 uc_hook;
	iconv_wide_char_hook		 wc_hook;
	void				*data;
};

/*
 * Fallbacks aren't supported but type definitions are provided for
 * source compatibility.
 */
typedef void (*iconv_unicode_mb_to_uc_fallback) (const char*,
		size_t, void (*write_replacement) (const unsigned int *,
		size_t, void*),	void*, void*);
typedef void (*iconv_unicode_uc_to_mb_fallback) (unsigned int,
		void (*write_replacement) (const char *, size_t, void*),
		void*, void*);
typedef void (*iconv_wchar_mb_to_wc_fallback) (const char*, size_t,
		void (*write_replacement) (const wchar_t *, size_t, void*),
		void*, void*);
typedef void (*iconv_wchar_wc_to_mb_fallback) (wchar_t,
		void (*write_replacement) (const char *, size_t, void*),
		void*, void*);

struct iconv_fallbacks {
	iconv_unicode_mb_to_uc_fallback	 mb_to_uc_fallback;
	iconv_unicode_uc_to_mb_fallback  uc_to_mb_fallback;
	iconv_wchar_mb_to_wc_fallback	 mb_to_wc_fallback;
	iconv_wchar_wc_to_mb_fallback	 wc_to_mb_fallback;
	void				*data;
};

__END_DECLS

#endif /* !_ICONV_IMPL_H_ */
