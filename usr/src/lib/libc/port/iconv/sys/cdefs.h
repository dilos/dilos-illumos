/*
 * This file and its contents are supplied under the terms of the
 * Common Development and Distribution License ("CDDL"), version 1.0.
 * You may only use this file in accordance with the terms of version
 * 1.0 of the CDDL.
 *
 * A full copy of the text of the CDDL should have accompanied this
 * source.  A copy of the CDDL is also available via the Internet at
 * http://www.illumos.org/license/CDDL.
 */

/*
 * Copyright (c) 2022 DilOS
 */

#ifndef	_SYS_DEFS_H
#define	_SYS_DEFS_H

#include <sys/types.h>

#define	_PATH_CSMAPPER  "/usr/share/i18n/csmapper"
#define	_PATH_ESDB      "/usr/share/i18n/esdb"

#ifdef _LP64
#define _PATH_I18NMODULE        "/usr/lib/i18n/64"
#else
#define _PATH_I18NMODULE        "/usr/lib/i18n"
#endif

#ifndef	__DECONST
#define	__DECONST(type, var)	((type)(uintptr_t)(const void *)(var))
#endif

#define	__CONCAT1(x,y)		x ## y
#define	__CONCAT(x,y)		__CONCAT1(x,y)

#define	__INT_MIN	(-2147483647-1) /* min value of an "int" */
#define	__INT_MAX	2147483647      /* max value of an "int" */
#define	__UINT_MAX	4294967295U     /* max value of an "unsigned int" */

typedef uintptr_t	__uintptr_t;
typedef uint32_t	__uint32_t;
typedef uint8_t		__uint8_t;
typedef boolean_t	bool;

#define	EFTYPE		79		/* Inappropriate file type or format */

#endif /* _SYS_DEFS_H */
