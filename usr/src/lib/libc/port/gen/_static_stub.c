
#include <sys/types.h>
#include <dlfcn.h>

/*static char sorry[] = "Service unavailable";*/

//#pragma weak dlopen
void *
dlopen(const char *name __unused, int mode __unused)
{

	//_rtld_error(sorry);
	return (NULL);
}

//#pragma weak dlsym
void *
dlsym(void * handle __unused, const char *name __unused)
{

	//_rtld_error(sorry);
	return (NULL);
}

//#pragma weak dlclose
int
dlclose(void *handle __unused)
{

	//_rtld_error(sorry);
	return (-1);
}

//#pragma weak dlinfo
int
dlinfo(void * handle __unused, int request __unused,
    void * p __unused)
{

	//_rtld_error(sorry);
	return (0);
}

//#pragma weak dlamd64getunwind
Dl_amd64_unwindinfo *
dlamd64getunwind(void *pc __unused, Dl_amd64_unwindinfo *unwindinfo __unused)
{
	return (NULL);
}

//#pragma weak _ld_libc
void
_ld_libc(void *ptr __unused)
{
}
