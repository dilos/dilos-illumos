
#include "thr_uberdata.h"

ulong_t
caller(void)
{
	register ulong_t __value __asm__("i7");
	return (__value);
}

ulong_t
getfp(void)
{
	register ulong_t __value __asm__("fp");
	return (__value);
}

ulwp_t *
_curthread(void)
{
	register ulwp_t *__value __asm__("g7");
	return (__value);
}
