/*
 * This file and its contents are supplied under the terms of the
 * Common Development and Distribution License ("CDDL"), version 1.0.
 * You may only use this file in accordance with the terms of version
 * 1.0 of the CDDL.
 *
 * A full copy of the text of the CDDL should have accompanied this
 * source.  A copy of the CDDL is also available via the Internet at
 * http://www.illumos.org/license/CDDL.
 */

/*
 * Copyright 2023 (c) DilOS
 */

/*
 * Implements fopencookie(3C).
 */

#include "file64.h"
#include <stdio.h>
#include "stdiom.h"
#include <errno.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/sysmacros.h>

struct fopencookie_thunk {
	void			*foc_cookie;
	cookie_io_functions_t	foc_io;
};

static ssize_t fopencookie_read(FILE *iop, char *buf, size_t nbytes);
static ssize_t fopencookie_write(FILE *iop, const char *buf, size_t nbytes);
static off_t fopencookie_seek(FILE *iop, off_t off, int whence);
static int fopencookie_close(FILE *iop);

FILE *
fopencookie(void *cookie, const char *mode, cookie_io_functions_t io_funcs)
{
	int oflags, fflags;
	FILE *iop;


	if (_stdio_flags(mode, &oflags, &fflags) != 0) {
		/* errno set for us */
		return (NULL);
	}

	struct fopencookie_thunk *thunk = malloc(sizeof(*thunk));
	if (thunk == NULL)
		return (NULL);

	thunk->foc_cookie = cookie;
	thunk->foc_io = io_funcs;

	iop = _findiop();
	if (iop == NULL) {
		goto cleanup;
	}

#ifdef	_LP64
	iop->_flag = (iop->_flag & ~_DEF_FLAG_MASK) | fflags;
#else
	iop->_flag = fflags;
#endif

	/*
	 * Update the user pointers now, in case a call to fflush() happens
	 * immediately.
	 */

	if (_xassoc(iop, fopencookie_read, fopencookie_write,
	    fopencookie_seek, fopencookie_close, thunk) != 0) {
		goto cleanup;
	}
	_setorientation(iop, _BYTE_MODE);
	SET_SEEKABLE(iop);

	return (iop);

cleanup:
	free(thunk);
	return (NULL);
}

static ssize_t
fopencookie_read(FILE *iop, char *buf, size_t nbytes)
{
	struct fopencookie_thunk *thunk = _xdata(iop);

	/* Reads from a stream with NULL read return EOF. */
	if (thunk->foc_io.read == NULL)
		return (0);

	return (thunk->foc_io.read(thunk->foc_cookie, buf, nbytes));
}

static ssize_t
fopencookie_write(FILE *iop, const char *buf, size_t nbytes)
{
	struct fopencookie_thunk *thunk = _xdata(iop);

	/* Writes to a stream with NULL write discard data. */
	if (thunk->foc_io.write == NULL)
		return (nbytes);

	return (thunk->foc_io.write(thunk->foc_cookie, buf, nbytes));
}

static off_t
fopencookie_seek(FILE *iop, off_t off, int whence)
{
	struct fopencookie_thunk *thunk = _xdata(iop);

	switch (whence) {
	case SEEK_SET:
	case SEEK_CUR:
	case SEEK_END:
		break;
	default:
		/* fopencookie(3) only allows these three seek modes. */
		errno = EINVAL;
		return (-1);
	}

	/*
	 * If seek is NULL, it is not possible to perform seek operations on
	 * the stream.
	 */
	if (thunk->foc_io.seek == NULL) {
		errno = ENOTSUP;
		return (-1);
	}

	int res = thunk->foc_io.seek(thunk->foc_cookie, &off, whence);
	if (res < 0)
		return (res);

	return (off);
}

static int
fopencookie_close(FILE *iop)
{
	struct fopencookie_thunk *thunk = _xdata(iop);

	int ret = 0;
	if (thunk->foc_io.close != NULL)
		ret = thunk->foc_io.close(thunk->foc_cookie);

	int serrno = errno;
	free(thunk);
	_xunassoc(iop);
	errno = serrno;
	return (ret);
}
