#!/usr/bin/perl

# VERSION 1.1

use strict;

my $fName1 = $ARGV[0];
my $fName2 = $ARGV[1];

#print "f1:$fName1, f2:$fName2\n";

my %f1 = ();
my %f2 = ();

my @buff = ();
open (IN, "<", $fName1) or die "Error: '$fName1':$!\n";
while (<IN>)
{
    chomp;
    @buff = split(/ /);
    $f1{$buff[1]} = $buff[0];
}
close(IN);

open (IN, "<", $fName2) or die "Error: '$fName2':$!\n";
while (<IN>)
{
    chomp;
    my @buff = split(/ /);
    $f2{$buff[1]} = $buff[0];
}
close(IN);

my %out;
my $str;
foreach my $item (sort(keys %f2))
{
    next if defined($out{$item});
    if (defined($f1{$item}))
    {
	unless ($f1{$item} eq $f2{$item})
	{
	    $out{$item} = 1 unless ($f1{$item} eq $f2{$item});
#print "==DEBUG1:item=$item\n";
	    unless ($item =~ /\-dev$/)
	    {
		$str = $item."-dev";
		$out{$str} = 1 if defined($f2{$str});
#print "==DEBUG11:str=$str\n" if defined($f2{$str});
	    }
	}
    }
    else
    {
	$out{$item} = 1;
    }

    if ($item =~ /\-dev$/)
    {
        $str = $item =~ s/\-dev$//;
#print "==DEBUG2:str=$str\n";
        $out{$str} = 1 if defined($f2{$str});
    }
}

foreach my $line (sort (keys %out))
{
    print "$line\n";
}
