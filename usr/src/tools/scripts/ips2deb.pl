#!/usr/bin/perl
#
# This file and its contents are supplied under the terms of the
# Common Development and Distribution License ("CDDL"), version 1.0.
# You may only use this file in accordance with the terms of version
# 1.0 of the CDDL.
#
# A full copy of the text of the CDDL should have accompanied this
# source.  A copy of the CDDL is also available via the Internet at
# http://www.illumos.org/license/CDDL.
#

#
# Copyright (c) 2010-2018 Igor Kozhukhov <igor@dilos.org>
#

#
# VERSION 1.79
#

use FindBin;
use lib "$FindBin::Bin/";

use File::Path;
use File::Copy;
#use File::Find;
use File::Basename;
use Getopt::Long;
use Env;

use strict;

use MFT;

#-----------------------------------------------------------------------------#
my $ignorePkgs = {};
$$ignorePkgs{'consolidation-osnet-osnet-message-files'} = 1;
$$ignorePkgs{'consolidation-osnet-osnet-incorporation'} = 1;
$$ignorePkgs{'consolidation-osnet-osnet-redistributable'} = 1;

my $specialPkgs = {};
#$$specialPkgs{'system-file-system-zfs'} = 1;
#$$specialPkgs{'system-file-system-zfs-tests'} = 1;
#$$specialPkgs{'system-file-system-udfs'} = 1;
#$$specialPkgs{'system-floating-point-scrubber'} = 1;
#$$specialPkgs{'system-tnf'} = 1;
#$$specialPkgs{'diagnostic-cpu-counters'} = 1;
#$$specialPkgs{'driver-network-eri'} = 1;
#$$specialPkgs{'compatibility-ucb'} = 1;
#$$specialPkgs{'diagnostic-powertop'} = 1;
#$$specialPkgs{'diagnostic-latencytop'} = 1;
#$$specialPkgs{'network-ipfilter'} = 1;
#$$specialPkgs{'developer-debug-mdb'} = 1;
#$$specialPkgs{'system-extended-system-utilities'} = 1;
#$$specialPkgs{'storage-library-network-array'} = 1;
#$$specialPkgs{'service-resource-cap'} = 1;
#$$specialPkgs{'developer-linker'} = 1;
#$$specialPkgs{'developer-dtrace'} = 1;
#$$specialPkgs{'driver-network-eri'} = 1;


#-----------------------------------------------------------------------------#
my $tmpl = {};
my $modulesList = [];
my @dirs = "";
my $verbouse = 0;
my $gatePath = '';
my $local_flag = 0;
#-----------------------------------------------------------------------------#
#------------------------------
# main
#------------------------------
{
    $$tmpl{'MAINTAINER'} = 'Igor Kozhukhov <igor@dilos.org>';
    $$tmpl{'MANIFESTGATE'} = $gatePath.'/usr/src/pkg/manifests';
    $$tmpl{'DESTANATION'} = "$FindBin::Bin/packages";
    $$tmpl{'MFEXT'} = 'mog';
    $$tmpl{'CATEGORY'} = 'undef';
    $$tmpl{'PKGVER'} = '1.0.1.1';
    $$tmpl{'IPSPKG'} = '';
    $$tmpl{'ISDEV'} = 0;
    $$tmpl{'PKGNAME'} = '';
    $$tmpl{'ARCH'} = '';
    $$tmpl{'ARCHITECTURE'} = '';
    $$tmpl{'BASEGATE'} = '';
    undef $$tmpl{'COMPONENT_VERSION'};
    my @pkgName = "";
    $pkgName[0] = 'all';
    my $result = GetOptions ( "p|pkg|package=s" => \@pkgName,
				'v|verbose!' => \$verbouse,
				'l|local!' => \$local_flag,
				'i|ips|ips_package=s' => \$$tmpl{'IPSPKG'},
				"basegate=s" => \$$tmpl{'BASEGATE'},
				"pv|pversion|package_version=s" => \$$tmpl{'PKGVER'},
				"cv|component_version=s" => \$$tmpl{'COMPONENT_VERSION'},
				"o|out|destination=s" => \$$tmpl{'DESTANATION'},
				"manifest_gate|mg|mfg=s" => \$$tmpl{'MANIFESTGATE'},
				"maintainer=s" => \$$tmpl{'MAINTAINER'},
				"manifest_extention|me|mfe=s" => \$$tmpl{'MFEXT'},
				"category|pkgtype=s" => \$$tmpl{'CATEGORY'},
				"arch|architecture=s" => \$$tmpl{'ARCHITECTURE'},
				"d|dir=s" => \@dirs);

    my $specIgnoreDev = {};
    $specIgnoreDev = readSpecFile("$FindBin::Bin/ips2deb.ignore_dev") if (-f "$FindBin::Bin/ips2deb.ignore_dev");


    my $arch = `uname -p`;
    chomp $arch;
    $$tmpl{'ARCH'} = $arch;

    unless ($$tmpl{'IPSPKG'} eq "")
    {
	$dirs[0] = "root";
	$$tmpl{'DESTANATION'} = ".";
	my $debPkg = $$tmpl{'IPSPKG'};
	$debPkg =~ s/\//-/g;
	$debPkg =~ s/_/-/g;
	$debPkg = lc($debPkg);
	$debPkg .= ".mf";
	print "pkg contents -m $$tmpl{'IPSPKG'} > $debPkg\n";
	`pkg contents -m $$tmpl{'IPSPKG'} > $debPkg`;
	$pkgName[0] = $debPkg;
    }

#    print "$result, $pkg\n";
#    exit 0;

    my $templates = &initTemplates();

#    find( \&d, $ENV{'MANIFESTGATE'});
#    find( { wanted => \&d }, $ENV{'MANIFESTGATE'});

    my $str;
    if ($pkgName[0] eq 'all' && scalar(@pkgName) < 2)
    {
	&listModules($$tmpl{'MANIFESTGATE'});
    }
    else
    {
	foreach my $pkgNameOne (@pkgName)
	{
	    $$tmpl{'MANIFESTGATE'} = dirname ($pkgNameOne);
	    $pkgNameOne = basename ($pkgNameOne);
	    $$tmpl{'MFEXT'} = $pkgNameOne;
	    $$tmpl{'MFEXT'} =~ s/.*\.//;
	    $str = "\.".$$tmpl{'MFEXT'}."\$";
	    $pkgNameOne =~ s/$str//;
#print "=DEBUG:'$pkgNameOne'\n";
#print "=DEBUG:'$$tmpl{'MFEXT'}'\n";
	    push(@$modulesList, $pkgNameOne) unless ($pkgNameOne =~ /\.\// || $pkgNameOne eq 'all');
	}
    }

#print "== @pkgName\n";
#print "== @$modulesList\n";

#    my $ARCH = system("uname -p");
#    &genPackage('sunwcs');
#    &saveDepends('developer-gcc-3');
#    &saveDepends('library-gmp');
#exit 0;

    my $pkgFailed = [];
    my $pkg;
    foreach $pkg (sort @$modulesList)
    {
#        next if (defined($$ignorePkgs{$pkg}));
        next if ($pkg =~ /^consolidation-/i);
#print "== '$pkg'\n";
        print "Generating structure from file: $pkg ... " if $verbouse;
        my $oPackage = &genPackage($pkg, 0);
#print "== DEBUG1:$$tmpl{'PKGNAME'}\n";
#        my $res = ($oPackage) ? $oPackage : 'OK';
        my $res = ($oPackage) ? 'FAILED' : 'OK';
        print "- $res\n" if $verbouse;
        print "-------------------------------------------------------------\n" if $verbouse;
        push(@$pkgFailed, "$pkg ($oPackage)") if ($res eq 'FAILED');
        unless (defined($$specIgnoreDev{$$tmpl{'PKGNAME'}}))
        {
	    &genPackage($pkg, 1) if ($$tmpl{'ISDEV'});
        }
    }

    if (scalar(@$pkgFailed) > 0)
    {
        open(LOG, ">", "$pkgName[0].failed.txt") or die "Can't open '$pkgName[0].failed.txt' : $!";
        my $str = "\n";
        $str .= "1 - absent 'dir'\n";
        $str .= "2 - absent 'file'\n";
        $str .= "3 - absent 'hardlink'\n";
        $str .= "4 - absent 'driver'\n";
        $str .= "5 - absent 'link'\n";
        $str .= "6 - absent 'user'\n";
        $str .= "7 - absent 'group'\n";
        $str .= "8 - arch != $$tmpl{'ARCH'}\n";
        $str .= "9 - PkgObsolete\n";
        $str .= "0 - PkgRenamed\n";
        $str .= "a - absent depends\n";
        $str .= "b - ignored packages\n";
        print LOG "Help: \n$str\n\n";
        print LOG "Failed packages:".scalar(@$pkgFailed)."\n\t".join("\n\t", @$pkgFailed);
        close(LOG);
    }

}

sub d
{
    my $str = "\.".$$tmpl{'MFEXT'}."\$";
#    /\.mf$/ or return;
    /$str/ or return;

    my $mod = basename($File::Find::name);
#    $mod =~ s/\..*$//;
    $mod =~ s/$str//;
    if ($mod =~ /^SUNW/i)
    {
        return unless ($mod =~ /^SUNWcsd$/i || $mod =~ /^SUNWcs$/i);
    }
#    print "$str, $mod\n";
    push(@$modulesList, $mod);
}

sub listModules ()
{
    my $dir = shift;

    my $str = "\.".$$tmpl{'MFEXT'}."\$";

    my @files = <$dir/*>;
    foreach my $line (@files)
    {
	next unless ($line =~ /$str/);
	my $mod = basename($line);
	$mod =~ s/$str//;
	if ($mod =~ /^SUNW/i)
	{
	    next unless ($mod =~ /^SUNWcsd$/i || $mod =~ /^SUNWcs$/i);
	}
	push(@$modulesList, $mod) unless defined($$ignorePkgs{$mod});
    }
}


#------------------------------
# genPackage
#------------------------------
sub genPackage
{
    my ($package, $dev) = @_;

    my $packageFile;
    $packageFile = $package.".".$$tmpl{'MFEXT'};
    $packageFile =~ /^SUNWcs\./ if ($package eq 'sunwcs');
    $packageFile = /^SUNWcsd\./ if ($package eq 'sunwcsd');
    $packageFile = $$tmpl{'MANIFESTGATE'}."/".$packageFile;

    $$tmpl{'DEV'} = $dev;
    $$tmpl{'DEPENDENCES'} = '${shlibs:Depends}, ${misc:Depends}';
    $$tmpl{'FIXPERMS'} = '';
    $$tmpl{'POSTINST_CONFIGURE'} = '';
    $$tmpl{'POSTINST_ADDITIONS'} = '';
    $$tmpl{'PREINST_INSTALL'} = '';
    $$tmpl{'PRERM_UPGRADE'} = '';
    $$tmpl{'CONFFILES'} = '';
    $$tmpl{'ZONE'} = '';
    $$tmpl{'ORIGINALVERSION'} = '';
    $$tmpl{'ORIGINALNAME'} = 'PKGNAMEremove';
    $$tmpl{'ORIGINALDEPENDNAME'} = '';
    $$tmpl{'REPLACES'} = 'PKGNAMEremove';
    $$tmpl{'PRIORITY'} = '';
    $$tmpl{'MAKESHLIBS'} = '';
    
    my $replaces = {};
    $replaces = readSpecFile("$FindBin::Bin/ips2deb.replaces") if (-f "$FindBin::Bin/ips2deb.replaces");
    my $specVersions = {};
    $specVersions = readSpecFile("$FindBin::Bin/ips2deb.versions") if (-f "$FindBin::Bin/ips2deb.versions");
    my $specPriorities = {};
    $specPriorities = readSpecFile("$FindBin::Bin/ips2deb.priorities") if (-f "$FindBin::Bin/ips2deb.priorities");
    my $specDefaults = {};
    $specDefaults = readSpecFile("$FindBin::Bin/ips2deb.defaults") if (-f "$FindBin::Bin/ips2deb.defaults");
    my $specIgnoreDepends = {};
    $specIgnoreDepends = readSpecFile("$FindBin::Bin/ips2deb.ignore_depends") if (-f "$FindBin::Bin/ips2deb.ignore_depends");
    my $specIgnoreDev = {};
    $specIgnoreDev = readSpecFile("$FindBin::Bin/ips2deb.ignore_dev") if (-f "$FindBin::Bin/ips2deb.ignore_dev");
    my $specIgnoreMakeShlibs = {};
    $specIgnoreMakeShlibs = readSpecFile("$FindBin::Bin/ips2deb.ignore_makeshlibs") if (-f "$FindBin::Bin/ips2deb.ignore_makeshlibs");
    my $specEssential = {};
    $specEssential = readSpecFile("$FindBin::Bin/ips2deb.essential") if (-f "$FindBin::Bin/ips2deb.essential");

    my $mft = new MFT();
    my $str = '';
    my $file = '';
    my $dep = [];

    $mft->init();
    $mft->setIps(1) unless ($$tmpl{'IPSPKG'} eq "");
    $mft->setHostArch($$tmpl{'ARCH'});
    $mft->{_specDefaults} = $specDefaults;
    $mft->{_ignoreDepends} = $specIgnoreDepends;
    $mft->{_ignoreDev} = $specIgnoreDev;
#print "== pkgFile = $packageFile\n";
    $mft->readMfFile($packageFile);
#print "== 2\n";
    $mft->setDev($dev);
    $mft->readMfFile($packageFile) if ($dev);
    
    my $originalName = $mft->getOrigName();

    $$tmpl{'PKGNAME'} = $mft->getModuleName();
    $$tmpl{'ORIGINALDEPENDNAME'} = $$tmpl{'PKGNAME'};
    $$tmpl{'PKGNAME'} .= '-dev' if ($$tmpl{'DEV'});
    $$tmpl{'PKGSHORTDESCRIPTION'} = $mft->getShortDescription();
    $$tmpl{'PKGDESCRIPTION'} = $mft->getDescription();
    $$tmpl{'ORIGINALVERSION'} = $mft->getOrigVersion();
    $$tmpl{'ORIGINALVERSION'} = $$tmpl{'COMPONENT_VERSION'} if (defined($$tmpl{'COMPONENT_VERSION'}));
    
    unless ($originalName eq $$tmpl{'PKGNAME'}) {
        $$tmpl{'ORIGINALNAME'} = "Provides: ".$originalName;
	$$tmpl{'ORIGINALNAME'} .= '-dev' if ($dev);
    }

    $$tmpl{'SAVETO'} = $$tmpl{'DESTANATION'}.'/'.$$tmpl{'PKGNAME'}.'/debian';
    $$tmpl{'PKGVER'} = $$specVersions{"$$tmpl{'PKGNAME'}"} if (defined($$specVersions{"$$tmpl{'PKGNAME'}"}));
    $mft->setVersion($$tmpl{'PKGVER'});
    $$tmpl{'PRIORITY'} = 'Optional';
    $$tmpl{'PRIORITY'} = $$specPriorities{"$$tmpl{'PKGNAME'}"} if (defined($$specPriorities{"$$tmpl{'PKGNAME'}"}));
    $$tmpl{'ESSENTIAL'} = '';
    $$tmpl{'ESSENTIAL'} = "Essential: yes\n" if (defined($$specEssential{"$$tmpl{'PKGNAME'}"}));

#    return '8' unless (defined($$noArchPkgs{$$tmpl{'PKGNAME'}})) || $mft->getArch() eq 'i386';
    return '8' unless ($mft->getArch() eq $$tmpl{'ARCH'});
    return '9' if ($mft->getPkgObsolete() eq 'true');
    return '0' if (($mft->getPkgRenamed() eq 'true') && ($$tmpl{'CATEGORY'} eq 'illumos'));
    return 'b' if (defined($$ignorePkgs{$$tmpl{'PKGNAME'}}));
    return 'b' if ($$tmpl{'PKGNAME'} =~ /^consolidation-/i);

#    $$tmpl{'MAKESHLIBS'} = 'dh_makeshlibs -Xfstyp' unless (defined($$specIgnoreMakeShlibs{$$tmpl{'PKGNAME'}}));
    $$tmpl{'MAKESHLIBS'} = 'dh_makeshlibs -a' unless (defined($$specIgnoreMakeShlibs{$$tmpl{'PKGNAME'}}));

    $str = "test -d $$tmpl{'DESTANATION'}/$$tmpl{'PKGNAME'} && rm -rf $$tmpl{'DESTANATION'}/$$tmpl{'PKGNAME'}";
#print "== $str == ";
    system($str);

    my $res='';
    my $oDirs = &saveDirs($mft);
    $res = '1' if ($oDirs);
#print "== DEBUG: saveDirs\n";
    my $oFiles = &saveFiles($mft);
    $res .= '2' if ($oFiles);
#print "== DEBUG: saveFiles\n";
    my $oHardLinks = &saveHardLinks($mft);
    $res .= '3' if ($oHardLinks);
#print "== DEBUG: saveHardLinks\n";
    my $oDrivers = &saveDrivers($mft);
    $res .= '4' if ($oDrivers);
#print "== DEBUG: saveDrivers\n";
    my $oLinks = &saveLinks($mft);
    $res .= '5' if ($oLinks);
#print "== DEBUG: saveLinks\n";
#    my $oGroups = &saveGroups($mft);
#    $res .= '7' if ($oGroups);
#print "== DEBUG: saveGroups\n";
#    my $oUsers = &saveUsers($mft);
#    $res .= '6' if ($oUsers);
#print "== DEBUG: saveUsers\n";
    $dep = $mft->getDepend();
    $res .= 'a' unless (defined($dep) && scalar($dep) > 0);
    my $tempDep = [];
    my @tmpDepBuff;
    if (defined($dep) && scalar($dep) > 0)
    {
	foreach my $tmpDep (@$dep)
	{
	    @tmpDepBuff = split(/\@/, $tmpDep);
	    $tmpDep = $tmpDepBuff[0];
#	    $tmpDep .= " (= $$tmpl{'PKGVER'})" if ($$tmpl{'DEV'} && ($tmpDep =~ /$$tmpl{'ORIGINALDEPENDNAME'}$/));
	    $tmpDep .= " (= $tmpDepBuff[1])" if (scalar(@tmpDepBuff) > 1);
#print "== DEBUG2:$tmpDepBuff[0], $tmpDepBuff[1] ==\n" if (scalar(@tmpDepBuff) > 1);
#print "== DEBUG1:$tmpDep ==\n";
	    push(@$tempDep, $tmpDep);
	}
    }

#    return $res if ($oDirs && $oFiles && $oHardLinks && $oUsers && $oGroups && ($res =~ /^a/));
    return $res if ($oDirs && $oFiles && $oHardLinks && ($res =~ /^a/));

# fill scripts and conffiles
    my $templates = &initTemplates();
    my $output = $mft->getScripts();

    my $operations;
    my $operation;
# postinst
    $operation = 'postinst';
    undef $operations;
    $operations = $$output{$operation} if defined($$output{$operation});
    if ((defined ($operations) && scalar(@$operations) > 0) || \
		-f "$$tmpl{'PKGNAME'}.$operation.adds")
    {
        $$tmpl{'POSTINST_CONFIGURE'} = join("\n\t",@$operations) if (defined ($operations) && scalar(@$operations) > 0);
        if (-f "$$tmpl{'PKGNAME'}.$operation.adds")
        {
	    $$tmpl{'POSTINST_ADDITIONS'} = `cat "$$tmpl{'PKGNAME'}.$operation.adds"`;
        }
        &saveExtfiles($mft, $operation);
    }

# preinst
    $operation = 'preinst';
    undef $operations;
    $operations = $$output{$operation} if defined($$output{$operation});
    if ((defined ($operations) && scalar(@$operations) > 0) || \
		-f "$$tmpl{'PKGNAME'}.$operation")
    {
        unless (-f "$$tmpl{'PKGNAME'}.$operation")
        {
	    $$tmpl{'PREINST_INSTALL'} = join("\n\t",@$operations) if defined($$output{$operation});;
	}
        &saveExtfiles($mft, $operation);
    }

# prerm
    $operation = 'prerm';
    undef $operations;
    $operations = $$output{$operation} if defined($$output{$operation});
    if ((defined ($operations) && scalar(@$operations) > 0) || \
		-f "$$tmpl{'PKGNAME'}.$operation")
    {
        unless (-f "$$tmpl{'PKGNAME'}.$operation")
        {
	    $$tmpl{'PRERM_UPGRADE'} = join("\n\t",@$operations) if defined($$output{$operation});;
	}
        &saveExtfiles($mft, $operation);
    }

# fixperms
    $operation = 'fixperms';
    undef $operations;
    $operations = $$output{$operation} if defined($$output{$operation});
    if (defined ($operations) && scalar(@$operations) > 0)
    {
        $$tmpl{'PATH'} = '/usr/bin:/sbin:/usr/sbin';
        $$tmpl{'FIXPERMS'} = join("\n",@$operations);
        &saveExtfiles($mft, $operation);
    }
    else
    {
        my $str = fillTemplate($$templates{$operation});
        my $file = $$tmpl{'SAVETO'}.'/'.$$tmpl{'PKGNAME'}.'.'.$operation;
        &saveFinalFile($file, $str);
    }

# we need clean <pkg>.conffiles
# because we have scheme for update files by postinst operation
    $operation = 'conffiles';
    $$output{$operation} = "";
    $mft->{_scripts} = $output;
    $$tmpl{'CONFFILES'} = "";
    &saveExtfiles($mft, $operation);

#    undef $operations;
#    $operations = $$output{$operation} if defined($$output{$operation});
#    if (defined ($operations) && scalar(@$operations) > 0)
#    {
#        $$tmpl{'CONFFILES'} = join("\n",@$operations);
#        &saveExtfiles($mft, $operation);
#    }

#    $operation = 'zone';
#    undef $operations;
#    $operations = $$output{$operation} if defined($$output{$operation});
#    if (defined ($operations) && scalar(@$operations) > 0)
#    {
#        $$tmpl{'ZONE'} = @$operations[0];
#    }

    $operation = 'triggers';
    $file = $$tmpl{'SAVETO'}."/$operation";
    &saveExtfiles($mft, $operation) if (-f "$$tmpl{'PKGNAME'}.triggers");

    $operation = 'changelog';
    $str = fillTemplate($$templates{$operation});
    $file = $$tmpl{'SAVETO'}."/$operation";
    &saveFinalFile($file, $str);


##    $$tmpl{'DEPENDENCES'} .= ', sunwcs' if ($$specialPkgs{$$tmpl{'PKGNAME'}});
#    $dep = $mft->getDepend();
#print "== DEPENDENCIES: @$dep\n" if (defined($dep));
    $$tmpl{'DEPENDENCES'} .= ', '.join(', ', @$dep) if (defined($dep) && scalar(@$dep) > 0);

    $$tmpl{'REPLACES'} = "Replaces: ".$$replaces{$$tmpl{'PKGNAME'}} if (defined($$replaces{$$tmpl{'PKGNAME'}}));

    $operation = 'control';
    $str = fillTemplate($$templates{$operation});
    $file = $$tmpl{'SAVETO'}."/$operation";
    &saveFinalFile($file, $str);

    $operation = 'copyright';
    $str = fillTemplate($$templates{$operation});
    $file = $$tmpl{'SAVETO'}."/$operation";
    &saveFinalFile($file, $str);

    $operation = 'compat';
    $str = fillTemplate($$templates{$operation});
    $file = $$tmpl{'SAVETO'}."/$operation";
    &saveFinalFile($file, $str);

    $operation = 'rules';
    $str = fillTemplate($$templates{$operation});
    $file = $$tmpl{'SAVETO'}."/$operation";
    &saveFinalFile($file, $str);
    $str = "chmod 0755 $file";
    system($str);

    return 0;
}

#-----------------------------------------------------------------------------#

#------------------------------
# fillTemplate
#------------------------------
sub fillTemplate
{
#    my ( $file ) = @_;
    my ( $template ) = @_;
    
    my $str = '';

    $str = $template;

    my $curDate = qx(date "+%a, %d %h %Y %H:%M:%S %z");
    chomp($curDate);
    $$tmpl{'DATE'} = $curDate;

    $str =~ s/%%PKGNAME%%/$$tmpl{'PKGNAME'}/g;
    $str =~ s/%%PKGVER%%/$$tmpl{'PKGVER'}/g;
    $str =~ s/%%MAINTAINER%%/$$tmpl{'MAINTAINER'}/g;
    $str =~ s/%%PKGSHORTDESCRIPTION%%/$$tmpl{'PKGSHORTDESCRIPTION'}/g;
    $str =~ s/%%PKGDESCRIPTION%%/$$tmpl{'PKGDESCRIPTION'}/g;
    $str =~ s/%%DEPENDENCES%%/$$tmpl{'DEPENDENCES'}/g;
    $str =~ s/%%DATE%%/$$tmpl{'DATE'}/g;
    $str =~ s/%%FIXPERMS%%/$$tmpl{'FIXPERMS'}/g;
    $str =~ s/%%PATH%%/$$tmpl{'PATH'}/g;
    $str =~ s/%%POSTINST_CONFIGURE%%/$$tmpl{'POSTINST_CONFIGURE'}/g;
    $str =~ s/%%POSTINST_ADDITIONS%%/$$tmpl{'POSTINST_ADDITIONS'}/g;
    $str =~ s/%%PREINST_INSTALL%%/$$tmpl{'PREINST_INSTALL'}/g;
    $str =~ s/%%PRERM_UPGRADE%%/$$tmpl{'PRERM_UPGRADE'}/g;
    $str =~ s/%%CONFFILES%%/$$tmpl{'CONFFILES'}/g;
#    $str =~ s/%%SHLIBS%%/$$tmpl{'SHLIBS'}/g;
#    $str =~ s/%%ZONE%%/$$tmpl{'ZONE'}/g;
    $str =~ s/%%ORIGINALVERSION%%/$$tmpl{'ORIGINALVERSION'}/g;
    $str =~ s/%%CATEGORY%%/$$tmpl{'CATEGORY'}/g;
    $str =~ s/%%ARCHITECTURE%%/$$tmpl{'ARCHITECTURE'}/g;
    $str =~ s/%%PRIORITY%%/$$tmpl{'PRIORITY'}/g;
    $str =~ s/%%MAKESHLIBS%%/$$tmpl{'MAKESHLIBS'}/g;
    $str =~ s/%%BASEGATE%%/$$tmpl{'BASEGATE'}/g;
    $str =~ s/%%ESSENTIAL%%\n/$$tmpl{'ESSENTIAL'}/g;

    if ($$tmpl{'ORIGINALNAME'} eq 'PKGNAMEremove') {
	$str =~ s/%%ORIGINALNAME%%\n//g;
    }
    else {
	$str =~ s/%%ORIGINALNAME%%/$$tmpl{'ORIGINALNAME'}/g;
    }

    if ($$tmpl{'REPLACES'} eq 'PKGNAMEremove') {
	$str =~ s/%%REPLACES%%\n//g;
    }
    else {
	$str =~ s/%%REPLACES%%/$$tmpl{'REPLACES'}/g;
    }


    return $str;
}

#------------------------------
# saveFinalFile
#------------------------------
sub saveFinalFile
{
    my ( $file, $str ) = @_;

    my $dirName = dirname($file);
    system("mkdir -p $dirName");

    local *FILE;
    open (FILE, ">", $file);
    print FILE "$str\n";
    close (FILE);
}

#------------------------------
# saveExternalfiles
#------------------------------
sub saveExtfiles
{
    my ($mft, $operation) = @_;

    my $output = $mft->getScripts();
    my $templates = &initTemplates();
#    my $moduleName = $mft->getModuleName();
    my $operations = $$output{$operation} if defined($$output{$operation});
#    print join("\n", @$operations) if defined($$output{$operation});
##    return unless defined($$output{$operation});

#    my $str = fillTemplate('tmpl/'.$operation.'.tmpl');
    my $str = fillTemplate($$templates{$operation});
    my $file = $$tmpl{'SAVETO'}.'/'.$$tmpl{'PKGNAME'}.'.'.$operation;
    my $originalFile = './'.$$tmpl{'PKGNAME'}.'.'.$operation;

    if ( -f "$originalFile" )
    {
	copy("$originalFile", "$file") or die "Copy failed: $!";
    }
    else
    {
	&saveFinalFile($file, $str);
    }
}

#------------------------------
# saveDepends()
#------------------------------
sub saveDepends
{
#    my ($mft) = @_;
    my ($package) = @_;
    my $packageFile = "manifests/".$package.".mf";

    my $mft = new MFT();
    $mft->init();
    $mft->readMfFile($packageFile);
    my $res = [];
    $res = $mft->getDepend();

    print join(', ', @$res)."\n";
}

#------------------------------
# saveFiles()
#------------------------------
sub saveFiles
{
    my ($mft) = @_;

    my $mf = $mft->getManifest();
    return 1 unless defined($$mf{'file'});

    my $r = $$mf{'file'};
    my $moduleName = $mft->getModuleName();
    my $ignoreDev = {};
    $ignoreDev = $mft->{_ignoreDev};

    my $str;
    my $found = 0;
    my $output = {};
    $output = $mft->{_scripts} if (defined($mft->{_scripts}));

    my $postinst = [];
    my $preinst = [];
    my $postrm = [];
    my $prerm = [];
    my $conffiles = [];
    my $fixperms = [];
    my $zone = [];
#    my $svcChk = {};

    $postinst = $$output{'postinst'} if (defined($$output{'postinst'}));
    $preinst = $$output{'preinst'} if (defined($$output{'preinst'}));
    $postrm = $$output{'postrm'} if (defined($$output{'postrm'}));
    $prerm = $$output{'prerm'} if (defined($$output{'prerm'}));
    $conffiles = $$output{'conffiles'} if (defined($$output{'conffiles'}));
    $fixperms = $$output{'fixperms'} if (defined($$output{'fixperms'}));
    $zone = $$output{'zone'} if (defined($$output{'zone'}));

    my @buff;
    my @buffperm;
    my $file = $$tmpl{'SAVETO'}.'/'.$moduleName.".install";
    my $fileperm = $$tmpl{'SAVETO'}.'/'.$moduleName.".files.fixperm";

    foreach my $line (@$r)
    {
        my $facetdev = 1 if (defined($$line{'facet.devel'}) && !defined($$ignoreDev{$moduleName}));
        $$tmpl{'ISDEV'} = 1 if defined($facetdev);
#        (defined($$line{'facet.devel'}) && !defined($$ignoreDev{$moduleName}));
        if ($$tmpl{'DEV'})
        {
	    next unless defined($facetdev);
        }
        else
        {
	    next if defined($facetdev);
        }

        my $preserve = $$line{'preserve'}[0] if defined($$line{'preserve'});
#        my $restart_fmri = $$line{'restart_fmri'}[0] if defined($$line{'restart_fmri'});
        my $arch = $$line{'variant.arch'}[0] if defined($$line{'variant.arch'});
	next if (defined($arch) && ($arch ne $$tmpl{'ARCH'}));

        my $zonevariant = $$line{'variant.opensolaris.zone'}[0] if defined($$line{'variant.opensolaris.zone'});

        my $group = $$line{'group'}[0];
        my $mode = $$line{'mode'}[0];
        my $owner = $$line{'owner'}[0];

#        my $chash = $$line{'chash'}[0] if defined($$line{'chash'});
        my $path = $$line{'path'}[0]; # if defined($$line{'path'});
        $path =~ s/\"//g;
        my $path_dir = dirname($path);
        my $path_others = $$line{'others'}[0]; # if defined($$line{'others'});
        my $origPath = $path;

#will use set/motd from illumos build in sunwcs package
#        next if ($path =~ /etc\/motd/); # use etc/motd from base-files package

###        push(@$fixperms, "mkdir -p \$DEST/$path_dir");

	$found = 0;
	undef($path_others) if ($path_others =~ /NOHASH/ || defined($$line{'chash'}));
        if (defined($path_others))
        {
	    $str = dirname("$origPath");
#print "==DEBUG: $$tmpl{'SAVETO'}/$$tmpl{'PKGNAME'}/$str\n";
	    mkpath("$$tmpl{'SAVETO'}/$$tmpl{'PKGNAME'}/$str");

	    foreach my $dirOne (@dirs)
	    {
		next unless (-e "$dirOne/$path_others" && -f "$dirOne/$path_others");
		copy("$dirOne/$path_others", "$$tmpl{'SAVETO'}/$$tmpl{'PKGNAME'}/$origPath") or die "Copy failed:($dirOne/$path_others), $!";
		chown $owner, $group, "$$tmpl{'SAVETO'}/$$tmpl{'PKGNAME'}/$origPath";
		chmod oct("$mode"), "$$tmpl{'SAVETO'}/$$tmpl{'PKGNAME'}/$origPath";
#		last if ($? == 0);
		$found = 1;
	    }
        }
        else
        {
	    foreach my $myDirOne (@dirs)
	    {
		my $dirOne = $myDirOne;
#print "0 - == $dirOne, $path, $origPath, $arch, $$tmpl{'ARCH'}\n";
		next if (($dirOne =~ /^\.\//) || (length($dirOne) < 3));
		$dirOne = '' if ($myDirOne eq 'root');
#print "==DEBUG: '$dirOne'\n";
#print "1 - == $dirOne, $path, $origPath\n";
		next unless (-e "$dirOne/$origPath" && -f "$dirOne/$origPath");
#print "2 - == $dirOne/$path\n";
		$str = dirname("$origPath");
#print "==DEBUG2: $$tmpl{'SAVETO'}/$$tmpl{'PKGNAME'}/$str\n";
		mkpath("$$tmpl{'SAVETO'}/$$tmpl{'PKGNAME'}/$str");
		copy("$dirOne/$origPath", "$$tmpl{'SAVETO'}/$$tmpl{'PKGNAME'}/$origPath") or die "Copy failed: $!";
		chown $owner, $group, "$$tmpl{'SAVETO'}/$$tmpl{'PKGNAME'}/$origPath";
		chmod oct("$mode"), "$$tmpl{'SAVETO'}/$$tmpl{'PKGNAME'}/$origPath";
		$found = 1;
		last;
	    }
        }

        unless ($found)
        {
	    print "FAILED!\n==NOT found: $path\n"; 
	    exit 1;
        }

	unless ( $origPath =~ /^usr\/share\/man/ )
	{
	    push(@$fixperms, "test -f \"\$DEST/$origPath\" || echo '== Missing: $origPath'");
	    push(@$fixperms, "test -f \"\$DEST/$origPath\" || exit 1");
	    push(@$fixperms, "chmod $mode \"\$DEST/$origPath\"");
	    push(@$fixperms, "chown $owner:$group \"\$DEST/$origPath\"");
	}

        if (defined($$line{'preserve'}) && $preserve eq 'renamenew')
        {
            $str = "mv \$DEST/$origPath \$DEST/$origPath.new";
	    push(@$fixperms, $str);

	    $str = "([ -f \$BASEDIR/$path ] || mv -f \$BASEDIR/$path.new \$BASEDIR/$path)";
            push(@$postinst, $str);
        }

        if (defined($$line{'preserve'}) && $preserve eq 'renameold')
        {
            $str = "mv \$DEST/$origPath \$DEST/$origPath.$moduleName";
	    push(@$fixperms, $str);

	    $str = "([ -f \$BASEDIR/$path ] && mv -f \$BASEDIR/$path \$BASEDIR/$path.old )";
            push(@$postinst, $str);

	    $str = "([ -f \$BASEDIR/$path.$moduleName ] && mv -f \$BASEDIR/$path.$moduleName \$BASEDIR/$path )";
            push(@$postinst, $str);
        }

        if (defined($$line{'preserve'}) && $preserve eq 'legacy')
        {
            $str = "mv \$DEST/$origPath \$DEST/$origPath.$moduleName";
	    push(@$fixperms, $str);

	    $str = "([ -f \$BASEDIR/$path ] || rm -f \$BASEDIR/$path.$moduleName )";
            push(@$postinst, $str);

	    $str = "([ -f \$BASEDIR/$path ] && mv -f \$BASEDIR/$path \$BASEDIR/$path.legacy )";
            push(@$postinst, $str);

	    $str = "([ -f \$BASEDIR/$path.$moduleName ] && mv -f \$BASEDIR/$path.$moduleName \$BASEDIR/$path )";
            push(@$postinst, $str);
        }

        if (defined($$line{'preserve'}) && $preserve eq 'true')
        {
            $str = "mv \$DEST/$origPath \$DEST/$origPath.$moduleName";
	    push(@$fixperms, $str);

#	    $str = "([ -f \$BASEDIR/$path.saved ] && mv -f \$BASEDIR/$path.saved \$BASEDIR/$path )";
#            push(@$postinst, $str);
	    $str = "([ -f \$BASEDIR/$path ] || mv -f \$BASEDIR/$path.$moduleName \$BASEDIR/$path )";
            push(@$postinst, $str);
	    $str = "([ -f \$BASEDIR/$path ] && rm -f \$BASEDIR/$path.$moduleName)";
            push(@$postinst, $str);

#	    $str = "([ -f \$BASEDIR/$path ] && mv -f \$BASEDIR/$path \$BASEDIR/$path.saved)";
#            push(@$prerm, $str);
        }

        if (defined($zonevariant) && ($zonevariant eq 'global'))
        {
	    $str = "[ \"\$ZONEINST\" = \"1\" ] && ([ -f \$BASEDIR/$path ] && rm -f \$BASEDIR/$path)";
            push(@$postinst, $str);
        }

#        if (defined($restart_fmri))
#        {
#            unless(defined($$svcChk{"$restart_fmri"}))
#            {
#                $$svcChk{"$restart_fmri"} = 1;
#                push(@$postinst, "[ -z \"\${BASEDIR}\" ] && ( /usr/sbin/svcadm restart $restart_fmri || true )");
#            }
#        }
    }

    return 1 unless ($found);

    $$output{'postinst'} = $postinst if (scalar(@$postinst) > 0);
    $$output{'preinst'} = $preinst if (scalar(@$preinst) > 0);
    $$output{'postrm'} = $postrm if (scalar(@$postrm) > 0);
    $$output{'prerm'} = $prerm if (scalar(@$prerm) > 0);
    $$output{'conffiles'} = $conffiles if (scalar(@$conffiles) > 0);
    $$output{'fixperms'} = $fixperms if (scalar(@$fixperms) > 0);
    $$output{'zone'} = $zone if (scalar(@$zone) > 0);

    $mft->{_scripts} = $output;
    return 0;
}


#------------------------------
# saveDirs()
#------------------------------
sub saveDirs
{
    my ($mft) = @_;

    my $mf = $mft->getManifest();
    return 1 unless defined($$mf{'dir'});

    my $r = $$mf{'dir'};
    my $moduleName = $mft->getModuleName();
    my $ignoreDev = {};
    $ignoreDev = $mft->{_ignoreDev};

    my $str;
    my $found = 0;
    my $output = {};
    $output = $mft->{_scripts} if (defined($mft->{_scripts}));

    my $postinst = [];
    my $fixperms = [];
    
    $postinst = $$output{'postinst'} if (defined($$output{'postinst'}));
    $fixperms = $$output{'fixperms'} if (defined($$output{'fixperms'}));

    my @buff;
#    my @buffperm;
    my $file = $$tmpl{'SAVETO'}.'/'.$moduleName.".dirs";
#    my $fileperm = $$tmpl{'SAVETO'}.'/'.$moduleName.".dirs.fixperm";
    foreach my $line (@$r)
    {

        my $facetdev = 1 if (defined($$line{'facet.devel'}) && !defined($$ignoreDev{$moduleName}));
        $$tmpl{'ISDEV'} = 1 if defined($facetdev);
        if ($$tmpl{'DEV'})
        {
	    next unless defined($facetdev);
        }
        else
        {
	    next if defined($facetdev);
        }

        my $arch = $$line{'variant.arch'}[0] if defined($$line{'variant.arch'});
	next if (defined($arch) && $arch ne $$tmpl{'ARCH'});

        my $group = $$line{'group'}[0];
        my $mode = $$line{'mode'}[0];
        my $owner = $$line{'owner'}[0];

        my $path = $$line{'path'}[0];

	mkpath("$$tmpl{'SAVETO'}/$$tmpl{'PKGNAME'}/$path", 
		{mode => oct($mode), owner => $owner, group => $group});

        push(@$fixperms, "chmod $mode \$DEST/$path");
        push(@$fixperms, "chown $owner:$group \$DEST/$path");
        $found = 1;
    }
##    return 1 if (scalar(@buff) < 1);
#    return 1 if (scalar(@$fixperms) < 1);
    return 1 unless ($found);
##    &saveFinalFile($file, join("\n", @buff));
#    &saveFinalFile($fileperm, join("\n", @buffperm));

    $$output{'postinst'} = $postinst if (scalar(@$postinst) > 0);
    $$output{'fixperms'} = $fixperms if (scalar(@$fixperms) > 0);
    $mft->{_scripts} = $output;
    return 0;
}

#------------------------------
# saveUsers
#------------------------------
sub saveUsers
{
    my ($mft) = @_;

    my $mf = $mft->getManifest();
    return 1 unless defined($$mf{'user'});

    my $r = $$mf{'user'};
    my $moduleName = $mft->getModuleName();
    my $ignoreDev = {};
    $ignoreDev = $mft->{_ignoreDev};

    my $str;
    my $output = {};
    $output = $mft->{_scripts} if (defined($mft->{_scripts}));

    my $postinst = [];

    $postinst = $$output{'postinst'} if (defined($$output{'postinst'}));

    foreach my $user (@$r)
    {
        my $facetdev = 1 if (defined($$user{'facet.devel'}) && !defined($$ignoreDev{$moduleName}));
        $$tmpl{'ISDEV'} = 1 if defined($facetdev);
        if ($$tmpl{'DEV'})
        {
	    next unless defined($facetdev);
        }
        else
        {
	    next if defined($facetdev);
        }

        $str = '';
        push (@$postinst, $str);
        $str = "if [ \$(grep -c \"^$$user{'username'}[0]:\" /etc/passwd) == \"0\" ]; then";
        push (@$postinst, $str);
#        $str = "adduser --system --no-create-home ";
#        $str .= "--gecos $$user{'gcos-field'}[0] " if defined ($$user{'gcos-field'});
#        $str .= "--home $$user{'home-dir'}[0] " if defined ($$user{'home-dir'});
#        $str .= "--shell $$user{'login-shell'}[0] " if defined ($$user{'login-shell'});
#        $str .= "--uid $$user{'uid'}[0] ";
#        $str .= "--ingroup $$user{'group'}[0] " if defined ($$user{'group'});
#        $str .= " $$user{'username'}[0]";
        $str = "useradd ";
        $str .= " -c $$user{'gcos-field'}[0] " if defined ($$user{'gcos-field'});
        $str .= " -d $$user{'home-dir'}[0] " if defined ($$user{'home-dir'});
        $str .= " -s $$user{'login-shell'}[0] " if defined ($$user{'login-shell'});
        $str .= " -u $$user{'uid'}[0] " if defined ($$user{'uid'}[0]);;
        $str .= " -g $$user{'group'}[0] " if defined ($$user{'group'});
        $str .= " -m $$user{'username'}[0]";
        push (@$postinst, $str);

        $str = "if [ \"\${BASEDIR}\" != \"/\" ] ; then";
        push (@$postinst, $str);
        $str = "if [ \$(grep -c \"^$$user{'username'}[0]:\" \$BASEDIR/etc/passwd) == \"0\" ] ; then";
        push (@$postinst, $str);
        $str = "NEWUSER=`cat /etc/passwd | grep \"^$$user{'username'}[0]:\"`";
        push (@$postinst, $str);
        $str = "echo \"\$NEWUSER\" >> \$BASEDIR/etc/passwd";
        push (@$postinst, $str);
        $str = "fi";
        push (@$postinst, $str);
        $str = "fi";
        push (@$postinst, $str);

        $str = "else";
        push (@$postinst, $str);
        $str = "if [ \"\${BASEDIR}\" != \"/\" ] ; then";
        push (@$postinst, $str);
        $str = "if [ \$(grep -c \"^$$user{'username'}[0]:\" \$BASEDIR/etc/passwd) == \"0\" ] ; then";
        push (@$postinst, $str);
        $str = "NEWUSER=`cat /etc/passwd | grep \"^$$user{'username'}[0]:\"`";
        push (@$postinst, $str);
        $str = "echo \"\$NEWUSER\" >> \$BASEDIR/etc/passwd";
        push (@$postinst, $str);
        $str = "fi";
        push (@$postinst, $str);
        $str = "fi";
        push (@$postinst, $str);
        $str = "fi";
        push (@$postinst, $str);
    }

    $$output{'postinst'} = $postinst if (scalar(@$postinst) > 0);

    $mft->{_scripts} = $output;
    return 0;
}


#------------------------------
# saveGroups
#------------------------------
sub saveGroups
{
    my ($mft) = @_;

    my $mf = $mft->getManifest();
    return 1 unless defined($$mf{'group'});

    my $r = $$mf{'group'};
    my $moduleName = $mft->getModuleName();
    my $ignoreDev = {};
    $ignoreDev = $mft->{_ignoreDev};

    my $str;
    my $output = {};
    $output = $mft->{_scripts} if (defined($mft->{_scripts}));

    my $postinst = [];

    $postinst = $$output{'postinst'} if (defined($$output{'postinst'}));

    foreach my $grp (@$r)
    {
        my $facetdev = 1 if (defined($$grp{'facet.devel'}) && !defined($$ignoreDev{$moduleName}));
        $$tmpl{'ISDEV'} = 1 if defined($facetdev);
        if ($$tmpl{'DEV'})
        {
	    next unless defined($facetdev);
        }
        else
        {
	    next if defined($facetdev);
        }

        $str = '';
        push (@$postinst, $str);
        $str = "if [ \$(grep -c \"^$$grp{'groupname'}[0]:\" /etc/group) == \"0\" ] ; then";
        push (@$postinst, $str);
        if (defined($$grp{'gid'}[0])) {
	    $str = "groupadd -g $$grp{'gid'}[0] $$grp{'groupname'}[0]";
	}
	else {
	    $str = "groupadd $$grp{'groupname'}[0]";
	}
        push (@$postinst, $str);

        $str = "if [ \"\${BASEDIR}\" != \"/\" ] ; then";
        push (@$postinst, $str);
        $str = "if [ \$(grep -c \"^$$grp{'groupname'}[0]:\" \$BASEDIR/etc/group) == \"0\" ] ; then";
        push (@$postinst, $str);
        $str = "NEWGROUP=`cat /etc/group | grep \"^$$grp{'groupname'}[0]:\"`";
        push (@$postinst, $str);
        $str = "echo \"\$NEWGROUP\" >> \$BASEDIR/etc/group";
        push (@$postinst, $str);
        $str = "fi";
        push (@$postinst, $str);
        $str = "fi";
        push (@$postinst, $str);

        $str = "else";
        push (@$postinst, $str);
        $str = "if [ \"\${BASEDIR}\" != \"/\" ] ; then";
        push (@$postinst, $str);
        $str = "if [ \$(grep -c \"^$$grp{'groupname'}[0]:\" \$BASEDIR/etc/group) == \"0\" ] ; then";
        push (@$postinst, $str);
        $str = "NEWGOUP=`cat /etc/group | grep \"^$$grp{'groupname'}[0]:\"`";
        push (@$postinst, $str);
        $str = "echo \"\$NEWGROUP\" >> \$BASEDIR/etc/group";
        push (@$postinst, $str);
        $str = "fi";
        push (@$postinst, $str);
        $str = "fi";
        push (@$postinst, $str);
        $str = "fi";
        push (@$postinst, $str);
    }

    $$output{'postinst'} = $postinst if (scalar(@$postinst) > 0);

    $mft->{_scripts} = $output;
    return 0;
}

#------------------------------
# saveLinks
#------------------------------
sub saveLinks
{
    my ($mft) = @_;

    my $mf = $mft->getManifest();
    return 1 unless defined($$mf{'link'});

    my $r = $$mf{'link'};
    my $moduleName = $mft->getModuleName();
    my $ignoreDev = {};
    $ignoreDev = $mft->{_ignoreDev};

    my $str;
    my $output = {};
    $output = $mft->{_scripts} if (defined($mft->{_scripts}));

    my $postinst = [];
    my $preinst = [];
    my $postrm = [];
    my $prerm = [];
    my $fixperms = [];

    $postinst = $$output{'postinst'} if (defined($$output{'postinst'}));
    $preinst = $$output{'preinst'} if (defined($$output{'preinst'}));
    $postrm = $$output{'postrm'} if (defined($$output{'postrm'}));
    $prerm = $$output{'prerm'} if (defined($$output{'prerm'}));
    $fixperms = $$output{'fixperms'} if (defined($$output{'fixperms'}));

#    my $file = $$tmpl{'SAVETO'}.'/'.$mft->getModuleName().".links";
    my @buff;
    foreach my $line (@$r)
    {
        my $facetdev = 1 if (defined($$line{'facet.devel'}) && !defined($$ignoreDev{$moduleName}));
        $$tmpl{'ISDEV'} = 1 if defined($facetdev);
        if ($$tmpl{'DEV'})
        {
	    next unless defined($facetdev);
        }
        else
        {
	    next if defined($facetdev);
        }

        my $path = $$line{'path'}[0];
        my $target = $$line{'target'}[0];
        my $mediator = $$line{'mediator'}[0] if defined($$line{'mediator'});
        my $mediator_ver = $$line{'mediator-version'}[0] if defined($$line{'mediator-version'});
        my $mediator_priority = $$line{'mediator-priority'}[0] if defined($$line{'mediator-priority'});
        
        my $alternative_priority = 0;
        $alternative_priority = 10 if (defined($mediator));
        $alternative_priority = 99 if (defined($mediator_priority) && ($mediator_priority eq 'vendor'));

        my $zonevariant;
        $zonevariant = $$line{'variant.opensolaris.zone'}[0] if defined($$line{'variant.opensolaris.zone'});
#        push(@buff, "$target $path");

        my $dir;
        my $alternativesName = $mediator."_" if (defined($mediator));
        $dir = dirname("$path");
        if (defined($zonevariant) && ($zonevariant eq 'global'))
        {
	    $str = "[ \"\$ZONEINST\" = \"1\" ] || (mkdir -p \$BASEDIR/$dir && ln -f -s $target \$BASEDIR/$path)";
	    if (defined($mediator))
	    {
		$alternativesName .= getAlternativesName($path);
#		$str = "[ \"\$ZONEINST\" = \"1\" ] || (update-alternatives --quiet --altdir \$BASEDIR/etc/alternatives --admindir \$BASEDIR/var/lib/dpkg/alternatives --install $path $alternativesName \$BASEDIR/$dir/$target $alternative_priority || true)";
		$str = "(update-alternatives --quiet --altdir \$BASEDIR/etc/alternatives --admindir \$BASEDIR/var/lib/dpkg/alternatives --install /$path $alternativesName /$dir/$target $alternative_priority || true)";
	    }
            push(@$postinst, $str);
        }
        else
        {
	    $str = "mkdir -p \$DEST/$dir && ln -f -s $target \$DEST/$path";
	    if (defined($mediator))
	    {
		$alternativesName .= getAlternativesName($path);
		$str = "(update-alternatives --quiet --altdir \$BASEDIR/etc/alternatives --admindir \$BASEDIR/var/lib/dpkg/alternatives --install /$path $alternativesName /$dir/$target $alternative_priority || true)";
        	push(@$postinst, $str);

#		$str = "#if [ \"\$1\" != \"upgrade\" ]; then\n";
		$str = "(update-alternatives --altdir \$BASEDIR/etc/alternatives --admindir \$BASEDIR/var/lib/dpkg/alternatives --remove $alternativesName /$dir/$target || true)\n";
#		$str .= "#	fi\n";
		push(@$prerm, $str);
	    }
	    else
	    {
		push (@$fixperms, $str);
	    }
        }

    }
#    &saveFinalFile($file, join("\n", @buff));

    $$output{'postinst'} = $postinst if (scalar(@$postinst) > 0);
    $$output{'preinst'} = $preinst if (scalar(@$preinst) > 0);
    $$output{'postrm'} = $postrm if (scalar(@$postrm) > 0);
    $$output{'prerm'} = $prerm if (scalar(@$prerm) > 0);
    $$output{'fixperms'} = $fixperms if (scalar(@$fixperms) > 0);

    $mft->{_scripts} = $output;
    return 0;
}


#------------------------------
# saveHardLinks
#------------------------------
sub saveHardLinks
{
    my ($mft) = @_;

    my $mf = $mft->getManifest();
    return 1 unless defined($$mf{'hardlink'});

    my $r = $$mf{'hardlink'};
    my $moduleName = $mft->getModuleName();
    my $ignoreDev = {};
    $ignoreDev = $mft->{_ignoreDev};

    my $str;
    my $output = {};
    $output = $mft->{_scripts} if (defined($mft->{_scripts}));

    my $postinst = [];
    my $preinst = [];
    my $postrm = [];
    my $prerm = [];
    my $fixperms = [];

    $postinst = $$output{'postinst'} if (defined($$output{'postinst'}));
    $preinst = $$output{'preinst'} if (defined($$output{'preinst'}));
    $postrm = $$output{'postrm'} if (defined($$output{'postrm'}));
    $prerm = $$output{'prerm'} if (defined($$output{'prerm'}));
    $fixperms = $$output{'fixperms'} if (defined($$output{'fixperms'}));

    foreach my $line (@$r)
    {
        my $facetdev = 1 if (defined($$line{'facet.devel'}) && !defined($$ignoreDev{$moduleName}));
        $$tmpl{'ISDEV'} = 1 if defined($facetdev);
        if ($$tmpl{'DEV'})
        {
	    next unless defined($facetdev);
        }
        else
        {
	    next if defined($facetdev);
        }

        my $path = $$line{'path'}[0];
        my $target = $$line{'target'}[0];
        my $destDir = dirname($path);
        my $destFile = basename($path);
        my $zonevariant = $$line{'variant.opensolaris.zone'}[0] if defined($$line{'variant.opensolaris.zone'});

        if (defined($zonevariant) && ($zonevariant eq 'global'))
        {

            $str = "[ \"\$ZONEINST\" = \"1\" ] || (mkdir -p \$BASEDIR/$destDir && cd \$BASEDIR/$destDir && cp -f $target $destFile)";
            push (@$postinst, $str);

            $str = "[ \"\$ZONEINST\" = \"1\" ] || rm -f \$BASEDIR/$path";
            push (@$prerm, $str);

            $str = "[ \"\$ZONEINST\" = \"1\" ] || rm -f \$BASEDIR/$path";
            push (@$preinst, $str);
        }
        else
        {
            $str = "mkdir -p \$BASEDIR/$destDir && (cd \$BASEDIR/$destDir && cp -f $target $destFile)";
            push (@$postinst, $str);

            $str = "rm -f \$BASEDIR/$path";
            push (@$prerm, $str);

            $str = "rm -f \$BASEDIR/$path";
            push (@$preinst, $str);
        }
    }

#    $str = "";
#    push (@$postinst, $str) if (scalar(@$postinst) > 0);

    $$output{'postinst'} = $postinst if (scalar(@$postinst) > 0);
    $$output{'preinst'} = $preinst if (scalar(@$preinst) > 0);
    $$output{'postrm'} = $postrm if (scalar(@$postrm) > 0);
    $$output{'prerm'} = $prerm if (scalar(@$prerm) > 0);
    $$output{'fixperms'} = $fixperms if (scalar(@$fixperms) > 0);

    $mft->{_scripts} = $output;
    return 0;
}

#------------------------------
# saveDrivers
#------------------------------
sub saveDrivers
{
    my ($mft) = @_;

    my $mf = $mft->getManifest();
    return 1 unless defined($$mf{'driver'});
    my $r = $$mf{'driver'};
    my $moduleName = $mft->getModuleName();

    my $str;
    my $output = {};
    $output = $mft->{_scripts} if (defined($mft->{_scripts}));

    my $postinst = [];
    my $preinst = [];
    my $postrm = [];
    my $prerm = [];

    $postinst = $$output{'postinst'} if (defined($$output{'postinst'}));
    $preinst = $$output{'preinst'} if (defined($$output{'preinst'}));
    $postrm = $$output{'postrm'} if (defined($$output{'postrm'}));
    $prerm = $$output{'prerm'} if (defined($$output{'prerm'}));

#    local *FILE;
#    open (FILE, ">", $mft->getModuleName().".drivers");
    foreach my $line (@$r)
    {
        my $facetdev = 1 if defined($$line{'facet.devel'});
        $$tmpl{'ISDEV'} = 1 if defined($$line{'facet.devel'});
        if ($$tmpl{'DEV'})
        {
	    next unless defined($facetdev);
        }
        else
        {
	    next if defined($facetdev);
        }

        my $name = $$line{'name'}[0] if defined($$line{'name'});
        my $privs = $$line{'privs'}[0] if defined($$line{'privs'});
        my $policy = $$line{'policy'}[0] if defined($$line{'policy'});
        my $devlink = $$line{'devlink'}[0] if defined($$line{'devlink'});
        my @devlinkFields if defined($$line{'devlink'});

        my $originalPerm;
        my $originalClonePerm;

        my $drv = 'add_drv -n ';

        $originalPerm = $$line{'perms'} if defined($$line{'perms'});
        $originalClonePerm = $$line{'clone_perms'} if defined($$line{'clone_perms'});

        my $perms = '';
        my $OPTIONS = '';

        $OPTIONS .= " -P $privs" if defined($privs);

        my $classes = $$line{'class'} if defined($$line{'class'});
        my $class = '';
        if (defined($classes))
        {
            if (scalar($classes) > 1)
            {
                $class .= join(" ", @$classes);
            }
            else
            {
                $class = @$$classes[0];
            }
            $OPTIONS .= " -c '$class'";
        }

        my $aliases = $$line{'alias'} if defined($$line{'alias'});
        my $alias = '';
        if (defined($aliases))
        {
            if (scalar($aliases) > 1)
            {
                $alias .= join("\" \"", @$aliases);
            }
            else
            {
                $alias = @$aliases[0];
            }
            $OPTIONS .= " -i '\"$alias\"'";
        }

        if (defined($originalPerm))
        {
            if ($originalPerm)
            {
                if (scalar(@$originalPerm) > 1)
                {
                    $perms = join(",", @$originalPerm);
                }
                else
                {
                    $perms = $$originalPerm[0];
                }
                $perms =~ s/"/'/g; #"
                $OPTIONS .= " -m $perms";
            }
        }

        $policy =~ s/"/'/g if defined($policy); #"
        $OPTIONS .= " -p $policy" if defined($policy);

	$str = "[ \"\$ZONEINST\" = \"1\" ] || (grep -c \"^$name \" \$BASEDIR/etc/name_to_major >/dev/null || ( $drv \$BASEDIR_OPT $OPTIONS $name ) )";
        push (@$postinst, $str);

#	unless ($moduleName eq 'sunwcs')
#	{
	    $str = "[ \"\$ZONEINST\" = \"1\" ] || ( rem_drv -n \$BASEDIR_OPT $name )";
	    push (@$prerm, $str);
#        }

        if (defined($originalClonePerm))
        {
            $drv = 'update_drv -n ';

            $perms = '';
            $OPTIONS = '';
            $OPTIONS .= " -P $privs" if defined($$line{'privs'});
            $OPTIONS .= " -i '$alias'" if (defined($$line{'alias'}) && ! defined($$line{'perms'}));
            $OPTIONS .= " -c '$class'" if (defined($$line{'class'}) && ! defined($$line{'perms'}));;

            if ($originalClonePerm)
            {
                $policy =~ s/"/'/g if defined($policy); #"
                $OPTIONS .= " -p $policy" if defined($policy);

                foreach $perms (@$originalClonePerm)
                {
                    $perms =~ s/"/'/g; #"

                    $str = "[ \"\$ZONEINST\" = \"1\" ] || (grep -c \"$perms\" \$BASEDIR/etc/minor_perm >/dev/null || $drv -a \$BASEDIR_OPT $OPTIONS -m $perms clone)";
                    push (@$postinst, $str);

                    $str = "[ \"\$ZONEINST\" = \"1\" ] || (grep -c \"$perms\" \$BASEDIR/etc/minor_perm >/dev/null && $drv -d \$BASEDIR_OPT $OPTIONS -m $perms clone)";
                    push (@$prerm, $str);
                }
            }
        }

        if(defined($devlink))
        {
            $devlink =~ s/\\t/\t/g;
	    @devlinkFields = split(' ', $devlink);
            $str = "[ \"\$ZONEINST\" = \"1\" ] || (grep -c \"^$devlinkFields[0]\" \$BASEDIR/etc/devlink.tab >/dev/null || (echo \"$devlink\" >> \$BASEDIR/etc/devlink.tab))";
            push (@$postinst, $str);

            $str = "[ \"\$ZONEINST\" = \"1\" ] || ( cat \$BASEDIR/etc/devlink.tab | sed -e '/^$devlinkFields[0]/d' > \$BASEDIR/etc/devlink.tab.new; mv \$BASEDIR/etc/devlink.tab.new \$BASEDIR/etc/devlink.tab )";
            push (@$prerm, $str);
        }
    }
#    close (FILE);

    $str = "";
    push (@$postinst, $str) if (scalar(@$postinst) > 0);
    push (@$prerm, $str) if (scalar(@$prerm) > 0);

    $$output{'postinst'} = $postinst if (scalar(@$postinst) > 0);
    $$output{'preinst'} = $preinst if (scalar(@$preinst) > 0);
    $$output{'postrm'} = $postrm if (scalar(@$postrm) > 0);
    $$output{'prerm'} = $prerm if (scalar(@$prerm) > 0);

    $mft->{_scripts} = $output;
    return 0;
}

#------------------------------
# initTemplates()
#------------------------------
sub initTemplates
{
    my $initTemplate = {};

# changelog
$$initTemplate{'changelog'} = '%%PKGNAME%% (%%PKGVER%%) unstable; urgency=low

  * Temporary file, need only for package generation process

 -- %%MAINTAINER%%  %%DATE%%
';

# compat
$$initTemplate{'compat'} = '9';

# conffiles
$$initTemplate{'conffiles'} = '%%CONFFILES%%';

# control
$$initTemplate{'control'} = 'Source: %%PKGNAME%%
Section: %%CATEGORY%%
Priority: %%PRIORITY%%
XBS-Original-Version: %%ORIGINALVERSION%%
XBS-Category: %%CATEGORY%%
Maintainer: %%MAINTAINER%%

Package: %%PKGNAME%%
Architecture: %%ARCHITECTURE%%
Depends: %%DEPENDENCES%%
%%ESSENTIAL%%
%%ORIGINALNAME%%
%%REPLACES%%
Description: %%PKGSHORTDESCRIPTION%%
 %%PKGDESCRIPTION%%
';

# control-dev
$$initTemplate{'control-dev'} = 'Source: %%PKGNAME%%
Section: %%CATEGORY%%
Priority: %%PRIORITY%%
XBS-Original-Version: %%ORIGINALVERSION%%
XBS-Category: %%CATEGORY%%
Maintainer: %%MAINTAINER%%

Package: %%PKGNAME%%
Architecture: solaris-%%ARCH%%
Depends: %%DEPENDENCES%%
Provides: %%ORIGINALNAME%%%%REPLACES%%
Description: %%PKGSHORTDESCRIPTION%%
 %%PKGDESCRIPTION%%

Package: %%PKGNAME%%-dev
Architecture: solaris-%%ARCH%%
Depends: %%PKGNAME%%
Provides: %%ORIGINALNAME%%-dev
Description: %%PKGSHORTDESCRIPTION%%
 %%PKGDESCRIPTION%% with files for development.
';

# copyright
$$initTemplate{'copyright'} = '
Copyright:

Copyright (c) 2011-2018, DilOS.  All rights reserved.
Use is subject to license terms.
';

# fixperms
$$initTemplate{'fixperms'} = '#!/bin/bash

export PATH=%%PATH%%

%%FIXPERMS%%
';

# postinst
$$initTemplate{'postinst'} = '#!/bin/bash

# postinst script for %%PKGNAME%%
#
# see: dh_installdeb(1)

#set -e

# summary of how this script can be called:
#        * <postinst> \`configure\' <most-recently-configured-version>
#        * <old-postinst> \`abort-upgrade\' <new version>
#        * <conflictor\'s-postinst> \`abort-remove\' \`in-favour\' <package>
#          <new-version>
#        * <postinst> \`abort-remove\'
#        * <deconfigured\'s-postinst> \`abort-deconfigure\' \`in-favour\'
#          <failed-install-package> <version> \`removing\'
#          <conflicting-package> <version>
# for details, see http://www.debian.org/doc/debian-policy/ or
# the debian-policy package

PATH=/usr/bin:/sbin:/usr/sbin:/usr/local/bin:/usr/local/sbin
export PATH

if [ "${BASEDIR:=/}" = "/" ]; then
    BASEDIR=""
else
    BASEDIR_OPT="-b $BASEDIR"
fi

case "$1" in
    configure)
        %%POSTINST_CONFIGURE%%
    ;;

    abort-upgrade|abort-remove|abort-deconfigure)
    ;;

    *)
        echo "postinst called with unknown argument \'$1\'" >&2
        exit 1
    ;;
esac

%%POSTINST_ADDITIONS%%

# dh_installdeb will replace this with shell code automatically
# generated by other debhelper scripts.

#DEBHELPER#

exit 0
';

# preinst
$$initTemplate{'preinst'} = '#!/bin/bash
# preinst script for %%PKGNAME%%
#
# see: dh_installdeb(1)

#set -e

# summary of how this script can be called:
#        * <new-preinst> \`install\'
#        * <new-preinst> \`install\' <old-version>
#        * <new-preinst> \`upgrade\' <old-version>
#        * <old-preinst> \`abort-upgrade\' <new-version>
# for details, see http://www.debian.org/doc/debian-policy/ or
# the debian-policy package

PATH=/usr/bin:/sbin:/usr/sbin:/usr/local/bin:/usr/local/sbin
export PATH

if [ "${BASEDIR:=/}" = "/" ]; then
    BASEDIR=""
else
    BASEDIR_OPT="-b $BASEDIR"
fi

case "$1" in
    install|upgrade)
	%%PREINST_INSTALL%%
    ;;

    abort-upgrade)
    ;;

    *)
        echo "preinst called with unknown argument \'$1\'" >&2
        exit 1
    ;;
esac

# dh_installdeb will replace this with shell code automatically
# generated by other debhelper scripts.

#DEBHELPER#

exit 0
';


# prerm
$$initTemplate{'prerm'} = '#!/bin/bash
# prerm script for %%PKGNAME%%
#
# see: dh_installdeb(1)

#set -e

# summary of how this script can be called:
#        * <prerm> \`remove\'
#        * <old-prerm> \`upgrade\' <new-version>
#        * <new-prerm> \`failed-upgrade\' <old-version>
#        * <conflictor\'s-prerm> \`remove\' \`in-favour\' <package> <new-version>
#        * <deconfigured\'s-prerm> \`deconfigure\' \`in-favour\'
#          <package-being-installed> <version> \`removing\'
#          <conflicting-package> <version>
# for details, see http://www.debian.org/doc/debian-policy/ or
# the debian-policy package

PATH=/usr/bin:/sbin:/usr/sbin:/usr/local/bin:/usr/local/sbin
export PATH

if [ "${BASEDIR:=/}" = "/" ]; then
    BASEDIR=""
else
    BASEDIR_OPT="-b $BASEDIR"
fi

case "$1" in
    remove|upgrade|deconfigure)
	%%PRERM_UPGRADE%%
    ;;

    failed-upgrade)
    ;;

    *)
        echo "prerm called with unknown argument \'$1\'" >&2
        exit 1
    ;;
esac

# dh_installdeb will replace this with shell code automatically
# generated by other debhelper scripts.

#DEBHELPER#

exit 0
';


# rules
$$initTemplate{'rules'} = '#!/usr/bin/gmake -f
# -*- makefile -*-
# Sample debian/rules that uses debhelper.
# This file was originally written by Joey Hess and Craig Small.
# As a special exception, when this file is copied by dh-make into a
# dh-make output file, you may use that output file without restriction.
# This special exception was added by Craig Small in version 0.37 of dh-make.

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

MYGATE := %%BASEGATE%%
DEST := $(CURDIR)/debian/%%PKGNAME%%

##configure: configure-stamp
##configure-stamp:
##	dh_testdir
	# Add here commands to configure the package.
##	touch configure-stamp


##build: build-stamp
build:

##build-stamp: configure-stamp 
#	dh_testdir
	# Add here commands to compile the package.
#	touch $@

clean:
	dh_testdir
	dh_testroot
	-rm -f build-stamp configure-stamp

	# Add here commands to clean up after the build process.
#	-$(MAKE) clean
#	dh_clean

##install: build
install:
##	dh_testdir
##	dh_testroot
##	dh_clean -k 
##	dh_installdirs

	# Add here commands to install the package into debian/tmp.
#	mkdir -p $(CURDIR)/debian/tmp
#	mv proto/* $(CURDIR)/debian/tmp


# Build architecture-independent files here.
##binary-indep: build install
# We have nothing to do by default.

# Build architecture-dependent files here.
##binary-arch: build install
binary-arch:
	dh_testdir
	dh_testroot
#	test -f $(CURDIR)/debian/%%PKGNAME%%.fixperms && MYSRCDIR=$(MYGATE) DEST=$(DEST) /bin/bash $(CURDIR)/debian/%%PKGNAME%%.fixperms
	test -f $(CURDIR)/debian/%%PKGNAME%%.fixperms && env DEST=$(DEST) /bin/bash $(CURDIR)/debian/%%PKGNAME%%.fixperms
#	dh_link
	dh_compress
#	dh_makeshlibs -p%%PKGNAME%%
	%%MAKESHLIBS%%
	dh_installdeb
#	dh_shlibdeps debian/tmp/lib/* debian/tmp/usr/lib/*
	-dh_shlibdeps -l $(MYGATE)/usr/lib* --dpkg-shlibdeps-params=--ignore-missing-info
	dh_gencontrol
	dh_md5sums
	# Make debootstrap life easier on non-Debian based systems by
	# compressing dpkg.deb with gzip instead of xz.
#	dh_builddeb -- -Zgzip
	dh_builddeb

##binary: binary-indep binary-arch
binary: binary-arch
##.PHONY: build clean binary-indep binary-arch binary install configure
.PHONY: clean binary-arch binary
';

# rules-dev
$$initTemplate{'rules-dev'} = '#!/usr/bin/gmake -f
# -*- makefile -*-
# Sample debian/rules that uses debhelper.
# This file was originally written by Joey Hess and Craig Small.
# As a special exception, when this file is copied by dh-make into a
# dh-make output file, you may use that output file without restriction.
# This special exception was added by Craig Small in version 0.37 of dh-make.

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

DEST := $(CURDIR)/debian/%%PKGNAME%%
DESTDEV := $(CURDIR)/debian/%%PKGNAME%%-dev

build:

clean:
	dh_testdir
	dh_testroot
#	-rm -f build-stamp configure-stamp

install:

binary-arch:
	dh_testdir
	dh_testroot
	test -f $(CURDIR)/debian/%%PKGNAME%%.fixperms && DEST=$(DEST) /bin/sh $(CURDIR)/debian/%%PKGNAME%%.fixperms
	test -f $(CURDIR)/debian/%%PKGNAME%%-dev.fixperms && DEST=$(DESTDEV) /bin/sh $(CURDIR)/debian/%%PKGNAME%%-dev.fixperms
#	dh_makeshlibs -p%%PKGNAME%%
	dh_makeshlibs
	dh_installdeb
	-dh_shlibdeps -Xdebian/sunwcs/usr/kernel/* -- --ignore-missing-info
	dh_gencontrol
	dh_md5sums
	dh_builddeb

binary: binary-arch
.PHONY: clean binary-arch binary
';

    return $initTemplate;
}

#------------------------------
# function readSpecFile()
#------------------------------
sub readSpecFile()
{
    my ( $filename ) = @_;

    my $lines = {};
    my @buff;
#    my $line = '';

    local *FILE;
    open( FILE, "<", $filename ) or die "Can't open '$filename' : $!";
    while( <FILE> ) {

        chomp;              # remove trailing newline characters

#        s/#.*//;            # ignore comments by erasing them
        next if /^(\s)*$/;  # skip blank lines
        next if /^#/;       # skip comments lines
        next if /^\s*#/;    # skip comments lines

	next unless /:/;

	@buff = split(':', $_);
	$$lines{$buff[0]} = $buff[1];
    }
    close FILE;

    return $lines;  # reference
}

#------------------------------
# function getAlternativesName()
#------------------------------
sub getAlternativesName()
{
    my ( $name ) = @_;

    $name = $name.32 if (($name =~ /\/i86\//) || ($name =~ /\/sparcv7\//));
    $name = $name.64 if (($name =~ /\/amd64\//) || ($name =~ /\/sparcv9\//));
    $name = basename ($name);
    return $name;
}
